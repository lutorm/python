from pylab import *
import scipy.optimize

perfmult={'C':1.0, 'N':1.175, 'A': 1.0}

def loadit():
    f=open('binotest.txt')
    data=[(d.split()[0],d.split()[1],d.split()[2],d.split()[3],float(d.split()[4]),float(d.split()[5]),float(d.split()[6])*perfmult[d.split()[7]],d.split()[7]) for d in f.readlines()]
    f=open('gfxcards.txt')
    f.readline()
    gfxcards={}
    [gfxcards.update([(d.split()[0],(float(d.split()[1]),float(d.split()[2]),float(d.split()[3]),d.split()[4]))]) for d in f.readlines()]
    return data,gfxcards

def model(gfxcard,par,fps_nobino):
    return (gfxcard[0]/10.)**(par[0]) * \
           (gfxcard[1]/30.)**(par[1]) * \
           (gfxcard[2]/1000.)**par[2] * \
           (fps_nobino/40.)**(0*par[3]) * \
           par[4] 

def fmin(par,*args):
    """par is the parameter vector, which is the powers of fillrate px/s, tx/s and gflops."""
    data=args[0]
    gfxcards=args[1]
    datapoint_index=args[2]
    return sum([(d[datapoint_index]-model(gfxcards[d[2]],par,d[4]))**2 for d in data])

def fitit():
    data,gfxcards=loadit()
    par=scipy.optimize.fmin_cg(fmin,(0.,0.,1.,0.,1.),
                               args=(data,gfxcards,5))
    
    clf()
    plot([d[5] for d in data if gfxcards[d[2]][3]=='A' ],[d[5]/model(gfxcards[d[2]],par,d[4]) for d in data if gfxcards[d[2]][3]=='A'],'+')
    plot([d[5] for d in data if gfxcards[d[2]][3]=='N'],[d[5]/model(gfxcards[d[2]],par,d[4]) for d in data if gfxcards[d[2]][3]=='N'],'o')
    legend(['ATI','Nvidia'])
    grid(True)
    title('bino fps=GPx^%4.2f*GTx^%4.2f*TFlops^%4.2f*(nobino)^%4.2f*%3.1g'%tuple(par))
    xlabel('bino fps')
    ylabel('bino fps relative to model')
    return par

    
    
    
