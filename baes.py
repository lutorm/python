# code to convert the outputs from baes_validation to the format that
# they want for the test
from numpy import *
import pyfits

def convert(file, tau):
    lam=logspace(-7,-3,81)
    f=pyfits.open(file)
    incl=[0,60,80,87,90]
    for c in range(5):
        im=f['CAMERA%dSTAR'%c].data
        try:
            im+=f['CAMERA%dIR'%c].data
        except:
            pass
        # convert to lambda*f_lambda
        im=im*lam[:,newaxis,newaxis]
        
        # calculate SED in flux by summing and multiplying by pixel
        # solid angle (which already is appropriate for 10Mpc because
        # that was the camera distance used)
        sed=sum(sum(im,axis=2),axis=1)*f['CAMERA%dSTAR'%c].header['PIXEL_SR']
        
        # convert image to W/m^2/arcsec^2
        im=im/206265.**2

        print 'writing output for sunrise_sed_tau%2.1f_i%d.txt'%(tau,incl[c])
        
        # write outputs
        savetxt('sunrise_sed_tau%2.1f_i%d.txt'%(tau,incl[c]),transpose(array([lam, sed])))

        phdu=pyfits.PrimaryHDU(data=im)
        phdu.writeto('sunrise_im_tau%2.1f_i%d.fits'%(tau,incl[c]))
        
        
