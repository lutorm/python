from numpy import *
import csv,pdb, pylab

def read_diversity_file(file):
    """Reads the webcaspar csv file containing degrees and years with
    either gender or race as additional classification. The file
    should be output with only one variable as row and the others as
    column for this function to be able to parse it."""

    f=open(file)
    r=csv.reader(f)
    
    # first are the selections for the variables, we just ignore those. They have one or two entries, so are easy to eat
    c=['loop init']
    numvar=-1
    #pdb.set_trace()
    while len(c)<3:
        c=r.next()
        numvar+=1

    print "File has %d independent variables:"%numvar

    # we save the name of the variable
    varnames=[]
    # variable values for the columns
    columns=[]
    # dict mappings variable values to index in data array
    varmap=[]
    # array of axes values
    axes=[]

    # read column independent variables (all but one)
    for i in range(numvar-1):
        # now follows variable as [varname, values...]
        # c=r.next() was already read in while above
        varnames.append(c[0])
        print "\t%s"%c[0]
        # generate sorted list of unique column values
        u=sort(list(set(c[1:-1])))
        varmap.append(dict(zip(u,range(len(u)))))
        axes.append(u)
        columns.append([varmap[-1][x] for x in c[1:-1]])
        # read up next line
        c=r.next()
    #pdb.set_trace()

    # next is the dependent variable name, repeated over all columns
    # except the first one that is the remaining row-wise independent variable
    # c was read at the end above
    depvar=c[1]

    # next comes the name of v4 in column 1
    c=r.next()
    varnames.append(c[0])
    print "\t%s"%c[0]
    print "Dependent variable is:\n\t%s"%depvar

    # and here comes the data. read it all into a list so we can see
    # how many rows there are
    rows=[row for row in r]
    
    # extract the values of v4
    axes.append([x[0] for x in rows])
    varmap.append(dict(zip(axes[-1],range(len(axes[-1])))))

    data=zeros([len(x) for x in axes], dtype=int32)

    # and get the data
    for i,r in enumerate(rows):
        # can you believe the retardedness of using commas in the
        # numbers to represent thousands?!?! who the fuck came up with
        # that idea...
        ind=[columns[ix] for ix in range(numvar-1)]
        ind.append(i)
        #data[columns[0],columns[1],columns[2],i]=array([int(x.replace(',','').replace('.','0')) for x in r[1:-1]])
        data[ind]=array([int(x.replace(',','').replace('.','0')) for x in r[1:-1]])
    
    return data,varnames,axes,varmap

def plot_minority_time4(data,degree,field,race, no_foreigners=True):
    """Plots the time evolution of minority fraction of data with 4
    independent variables. Temporary residents are excluded from the
    numbers."""
    year=data[2][0]
    v=data[3]

    if no_foreigners and ('Temporary Resident' in v[3]):
        # we need to subtract the temporary residents to get true
        # fractions of US people
        i=range(len(v[3]))
        i.remove(v[3]['Temporary Resident'])
        s=sum(data[0][:,v[1][degree],v[2][field],i],axis=1)
    else:
        s=sum(data[0][:,v[1][degree],v[2][field],:],axis=1)

    # cut out years with 0 -- likely missing data
    nonz=data[0][:,v[1][degree],v[2][field],v[3][race]]>0

    minority=data[0][:,v[1][degree],v[2][field],v[3][race]]*1.0/s
    return pylab.plot(year[nonz],minority[nonz])

def plot_minority_time3(data,field,race,no_foreigners=True):
    """Plots the time evolution of minority fraction of data with 3
    independent variables. Temporary residents are excluded from the
    numbers."""
    year=data[2][0]
    v=data[3]
    
    if no_foreigners and ('Temporary Resident' in v[2]):
        # we need to subtract the temporary residents to get true
        # fractions of US people
        i=range(len(v[2]))
        i.remove(v[2]['Temporary Resident'])
        s=sum(data[0][:,v[1][field],i],axis=1)
    else:
        s=sum(data[0][:,v[1][field],:],axis=1)

    # cut out years with 0 -- likely missing data
    nonz=data[0][:,v[1][field],v[2][race]]>0
    minority=data[0][:,v[1][field],v[2][race]]*1.0/s
    return pylab.plot(year[nonz],minority[nonz])

def women_plot(d,subjects, t):
    pylab.clf()
    lines=[]
    for s in subjects:
        lines.append(plot_minority_time3(d,s,'Female'))
    pylab.legend(lines,subjects,'tl')
    pylab.ylabel("Fraction women")
    pylab.xlabel("Year of Doctorate")
    pylab.title(t)


def women_plots():
    """Makes the 3 plots of female representation of doctorates over
    time in various fields."""
    d=read_diversity_file('doctorates_gender.csv')
    women_plot(d,['Education','Humanities','Law'],
               'Non-Science & Engineering')
    pylab.axis([1965,2010,0,0.7])
    pylab.grid(True)
    pylab.savefig('women_doctorates1.png')

    women_plot(d,['Psychology','Social Sciences','Life Sciences'],'Science & Engineering')
    pylab.axis([1965,2010,0,0.7])
    pylab.grid(True)
    pylab.savefig('women_doctorates2.png')

    women_plot(d,['Geosciences','Math and Computer Sciences','Physical Sciences','Engineering'],'Science & Engineering')
    pylab.axis([1965,2010,0,0.7])
    pylab.grid(True)
    pylab.savefig('women_doctorates3.png')

    
def chemistry_plot():
    """Makes the plot of black representation in chemistry over
    time. This uses a lot of data and they aren't even totally
    internally consistent, ie the doctorates and the degrees files,
    which come from different databases, show slightly different black
    representation of chemistry phds."""

    enr=read_diversity_file('enrollments_race.csv')
    deg=read_diversity_file('degrees_race.csv')
    degd=read_diversity_file('degrees_race_detailed_discipline.csv')
    doc=read_diversity_file('doctorates_race.csv')
    docd=read_diversity_file('doctorates_ethnic_detailed_discipline.csv')

    pylab.clf()
    lines=[]
    lines.append(plot_minority_time3(enr,'First-Time Freshmen',
                                     'Black, Non-Hispanic'))
    lines.append(plot_minority_time4(degd,"Bachelor's Degrees",
                                     'Chemistry','Black, Non-Hispanic'))
    lines.append(plot_minority_time4(deg,"Bachelor's Degrees",
                                     'Physical Sciences','Black, Non-Hispanic'))
    lines.append(plot_minority_time3(docd,'Chemistry','Black, Non-Hispanic'))
    lines.append(plot_minority_time3(doc,'Physical Sciences',
                                     'Black, Non-Hispanic'))
    # top 50 faculty from Nelson reports @
    # http://chem.ou.edu/~djn/diversity/top50.html
    lines.append(pylab.plot([2001,2003,2005,2007],[0.011,0.012,0.013,0.019]))
    pylab.legend(lines,['Freshmen','Chemistry BS','Phys. Sci. BS','Chemistry PhD','Phys. Sci. PhD','Chem. Faculty'],'tl')
    pylab.ylabel("Fraction Blacks")
    pylab.xlabel("Year")
    pylab.title("Black representation in physical sciences")
    pylab.grid(True)
    #pdb.set_trace()
    pylab.savefig('chemistry.png')
    
