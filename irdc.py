import pyfits, vecops, pdb
from numpy import *
from pylab import *

def findval(arr,val):
    return argwhere(arr>val)[0][0]

def disp(im, maxrank=300, f=lambda x:x):
    imshow(f(im),vmin=f(sort(im[im>0].flatten())[0]),vmax=f(sort(im.flatten())[-maxrank]),cmap=cm.gray)

def dispslice(im, l, lam, **kwargs):
    disp(im[findval(l,lam),:,:], **kwargs)
    title("%3.1f"%(lam*1e6))
    
def make_panel(fn, cam, **kwargs):
    ff=pyfits.open(fn)
    im=ff['CAMERA%d-STAR'%cam].data
    irim=ff['CAMERA%d-IR'%cam].data
    ii=im+irim
    l=ff[4].data.field('lambda')
    sed=sum(sum(ii,axis=2),axis=1)
    clf()
    subplot(331)
    dispslice(ii, l, 550e-9, **kwargs)
    subplot(332)
    dispslice(ii, l, 8e-6, **kwargs)
    subplot(333)
    dispslice(ii, l, 24e-6, **kwargs)
    subplot(334)
    dispslice(ii, l, 70e-6, **kwargs)
    subplot(335)
    dispslice(ii, l, 170e-6, **kwargs)
    subplot(336)
    dispslice(ii, l, 250e-6, **kwargs)
    subplot(337)
    dispslice(ii, l, 500e-6, **kwargs)
    subplot(338)
    dispslice(ii, l, 850e-6, **kwargs)
    subplot(339)
    loglog(l,l*sed)
    axis([1e-7,1e-3,1e-3,1e0])
    
def make_sed(fn, cam):
    """Calculates SED in annuli around the center."""

    ff=pyfits.open(fn)
    im=ff['CAMERA%d-STAR'%cam].data
    irim=ff['CAMERA%d-IR'%cam].data
    ii=im+irim
    l=ff[4].data.field('lambda')

    # make a coord array
    x=(linspace(0,ii.shape[1]-1,num=ii.shape[1])-ii.shape[1]/2)**2
    y=(linspace(0,ii.shape[2]-1,num=ii.shape[2])-ii.shape[2]/2)**2
    r2=repeat(y[newaxis,:],ii.shape[1],axis=0) + \
        repeat(x[:,newaxis],ii.shape[2],axis=1)

    rin=0
    rout=3
    totsed=sum(sum(ii,axis=2),axis=1)/product(ii.shape)
    seds=[(0,totsed,vecops.integrate_array(l,totsed,False))]
    while rout<sqrt(dot(ii.shape,ii.shape)):
        print 0.5*(rin+rout)
        mask=(r2<rout**2)&(r2>rin**2)
        masked=where(repeat(mask[newaxis,:,:],ii.shape[0],axis=0),ii,0.0)
        sed=sum(sum(masked,axis=2),axis=1)/sum(where(mask,1,0))
        #pdb.set_trace()
        seds.append((0.5*(rin+rout), sed, vecops.integrate_array(l,sed,False)))
        rin=rout
        rout=rout*2
    return l,seds

def plot_sed(fn, cam, relative=False):
    l,s=make_sed(fn,cam)
    [loglog(l,ss[1]/s[0][1] if relative else ss[1]) for ss in s[1:]]
    legend(['%3.1f'%ss[0] for ss in s[1:]],'upper left')

def howard_prop():
    f=pyfits.open('her4_200_0.1_2.5_0.5U_v2.fits')
    i=f[5].data+f[1].data
    s=l*sum(sum(i[:,90:210,90:210],axis=2),axis=1)*4*pi*(0.07*3.1e16)**2
    her4=numpy.loadtxt('her4.txt')
    l=f[4].data.field('lambda')
    loglog(l,s)
    loglog(her4[:,0],her4[:,1]*1e-26*3e8/her4[:,0]*4*pi*(5300*3.1e16)**2,'ko')
    legend(['model','her4'])
    xlabel('$\lambda$/m')
    ylabel(r'$\nu F_\nu$/W')
    axis([1e-6,2e-3,5e26,1.3e30])
    
def her4_img(l,i,lams, **kwargs):
    clf()
    lam=[where(l>ll)[0][0] for ll in lams]
    for s in range(4):
        subplot(2,2,s+1)
        imshow(log10((l[:,newaxis,newaxis]*i)[lam[s],:,:]), **kwargs)
        title("%3.1f $\mu$m"%(lams[s]*1e6))
     
