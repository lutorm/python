from sqlalchemy import *
from sqlalchemy.orm import *

from os import path
from local_mysqld import *

#db = create_engine("mysql://patrik:chk6aple@governator.ucsc.edu:3306/photos",
#                   pool_recycle = 3600)
db = create_engine(mysql_path + "photos", pool_recycle = 3600)

metadata = MetaData (db)
metadata.bind.echo=False

image_base="http://governator.ucsc.edu/photo_files/photo_html/images"
thumbnail_base="http://governator.ucsc.edu/photo_files/photo_html/thumbnails"
folder_url="http://governator.ucsc.edu/photo_files/folder.gif"
folder_x =52
folder_y =52


folder_table = Table("folders", metadata,
                     Column("id", Integer, primary_key = True),
                     Column("title", Unicode(30)),
                     Column("caption", Unicode, nullable = True),
                     Column("order", Integer, nullable = True),
                     Column("parent_id", Integer, \
                            ForeignKey("folders.id")))

# add image size to database so we can give sizes
# what about thumbnail size?
image_table = Table("images", metadata,
                    Column("id", Integer, primary_key = True),
                    Column("title", Unicode(30)),
                    Column("path", String),
                    Column("caption", Unicode, nullable = True),
                    Column("order", Integer, nullable = True),
                    Column("folder_id", Integer, \
                           ForeignKey("folders.id")),
                    Column("image_x", Integer),
                    Column("image_y", Integer),
                    Column("thumb_x", Integer),
                    Column("thumb_y", Integer),
                    Column("score", Integer),
                    Column("votes", Integer)
                    )

exiftags_table = Table("exiftags", metadata,
                       Column("id", Integer, primary_key = True),
                       Column("tag_id", Integer, ForeignKey("tagnames.id")),
                       Column("value", String(40)),
                       Column("image_id", Integer, ForeignKey("images.id")))

tagnames_table = Table("tagnames", metadata,
                       Column("id", Integer, primary_key = True),
                       Column("name", String(40)))

class Folder(object):
    def create_or_load(session, id, title, parent, caption, order):
        """Tries to load the instance from the session, otherwise
        creates it. The loaded instance has its properties updated if
        they are set, otherwise they are unchanged. This method also
        puts a newly created instance in the session."""

        try:
            f = session.query(Folder).get(id)
            # we only need to update sql if data are different
            if  f.title != title or \
               f.parent != parent or \
               f.caption != caption or \
               f.order != order:
                f.title=title
                f.parent=parent
                f.caption=caption
                f.order=order
                session.update(f)
        except:
            # not in database, instantiate it
            f = Folder(id, title, parent, caption, order)
            session.save(f)
        assert f != None
        return f
    
    create_or_load = staticmethod(create_or_load)
            
    def __init__(self, id, title, parent, caption="", order=None):
        self.id = id
        self.title=title
        self.caption=caption
        self.order=order

        if parent !=None:
            # add to the subfolder list of the parent
            parent.subfolders.append(self)
        
    def __repr__(self):
        return u"Folder(%s, \"%s\", parent=%s)" % \
               (self.id, self.title, self.parent_id)
    
    def url(self):
        "Return URL to the page for this folder."
        return u"/folder/%d" % self.id

    def parent_path(self):
        "Returns a list of the (titles, URLs) of the parent folders."
        if self.parent == None:
            return [ (self.title, self.url()) ]
        else:
            p = self.parent.parent_path()
            p.append( (self.title, self.url()) )
            return p

    def tree(self):
        return self.make_tree([(f.title, f.url()) for f in self.subfolders])
    
    def make_tree(self, subtree):
        "Returns a tree of the (titles, URLs) of the parent folders."
        #print "make_tree with subtree ",subtree
        if self.parent == None:
            # end recursion
            return [ (self.title, self.url(), subtree) ]
        else:
            this_level = []
            for f in self.parent.subfolders:
                #print f
                if f.id == self.id:
                    #print "\t appending ",f.title,subtree
                    this_level.append ((f.title, f.url(), subtree) )
                else:
                    #print "\t appending ",f.title
                    this_level.append ((f.title, f.url()))
            #print "recursing with ",this_level
            return self.parent.make_tree(this_level)

    def previous(self):
        # query first folder with order<self.order in reverse order
        try:
            p = session.query(Folder).filter("parent_id=:pid").filter("folders.order<:order").params(pid=self.parent.id,order=self.order).order_by(desc(folder_table.c.order))[0]
        except IndexError:
            # if we get indexerror, there is no such picture/folder. return to parent
            p = self.parent
        except:
            # if anything else happens we have no parent
            p = None

        if p!=None:
            return (u"Previous", p.url())
        else:
            return (u"",u"")

    def next(self):
        # query first folder with order>self.order
        try:
            n = session.query(Folder).filter("parent_id=:pid").filter("folders.order>:order").params(pid=self.parent.id,order=self.order).order_by(folder_table.c.order)[0]
        except IndexError:
            # if we get indexerror, there is no such picture/folder. return to parent
            n = self.parent
        except:
            # if anything else happens we have no parent
            n = None

        if n!=None:
            return (u"Next", n.url())
        else:
            return (u"",u"")

    def folder_list(self):
        "Returns a list of (title, url, thumbnail_url) for the subfolders."
        return [ (x.title, x.url(), folder_url,
                  folder_x, folder_y) for x in self.subfolders ]

    def image_list(self):
        "Returns a list of (title, url, thumbnail_url) for the images."
        return [ (x.title, x.url(), x.thumbnail_url(),
                  x.thumb_x, x.thumb_y) for x in self.images ]

class Image(object):
    def create_or_load(session, title, folder, path,
                       caption, order, image_size,
                       thumbnail_size):
        """Tries to load the instance from the session, otherwise
        creates it. The loaded instance has its properties updated if
        they are set, otherwise they are unchanged. This method also
        puts a newly created instance in the session."""

        id = path.__hash__()
        try:
            i = session.query(Image).get(id)
            # we only need to update sql if any data have changed
            if i.title != title or \
               i.path != path or \
               i.caption != caption or \
               i.order != order or \
               i.image_x != image_size[0] or \
               i.image_y != image_size[1] or \
               i.thumb_x != thumbnail_size[0] or \
               i.thumb_y != thumbnail_size[1]:
                i.title=title
                i.path=path
                i.caption=caption
                i.order=order            
                i.image_x = image_size[0]
                i.image_y = image_size[1]
                i.thumb_x = thumbnail_size[0]
                i.thumb_y = thumbnail_size[1]
                session.update(i)
        except:
            # not in database, instantiate it
            i = Image(title, folder, path, caption, order, image_size,
                      thumbnail_size)
            session.save(i)
        assert i != None
        return i

    create_or_load = staticmethod(create_or_load)
            
    def __init__(self, title, folder, path, caption, order,
                 image_size, thumbnail_size):
        self.id = path.__hash__()
        self.title=title
        self.path=path
        self.caption=caption
        self.order=order
        # add to folder images list
        folder.images.append(self)
        if image_size != None:
            self.image_x = image_size[0]
            self.image_y = image_size[1]
        if thumbnail_size != None:
            self.thumb_x = thumbnail_size[0]
            self.thumb_y = thumbnail_size[1]

    def __repr__(self):
        return u"Image(%s, \"%s\", folder=%s)" % \
               (self.id, self.title, self.folder_id)
        
    def url(self):
        "Return URL to the page for this image."
        return u"/image/%d" % self.id

    def image_url(self):
        "Return URL to the image file itself."
        return path.join(image_base, self.path)
    
    def thumbnail_url(self):
        "Return URL to the thumbnail image file."
        return path.join(thumbnail_base, self.path)
    
    def parent_path(self):
        "Return the parent path of the image, including itself."
        p = self.parent.parent_path()
        p.append((self.title,''))
        return p
    
    def tree(self):
        return self.parent.tree()
    
    def previous(self):
        # query first image with order<self.order in reverse order
        try:
            p = session.query(Image).filter("folder_id=:fid").filter("images.order<:order").params(fid=self.parent.id,order=self.order).order_by(desc(image_table.c.order))[0]
        except IndexError:
            # failed, return to folder view
            p = self.parent
        return (u"Previous", p.url())

    def next(self):
        # query first image with order>self.order
        try:
            n = session.query(Image).filter("folder_id=:fid").filter("images.order>:order").params(fid=self.parent.id,order=self.order).order_by(image_table.c.order)[0]
        except IndexError:
            # failed, return to folder view
            n = self.parent
        return (u"Next", n.url())

    def tag_dict(self):
        d={}
        for t in self.tags:
            d[t.name()] = t.value
            
        return d

    def rating(self):
        try:
            return float(self.score) / self.votes
        except:
            return -1

    def vote(self, vote):
        self.score += vote
        self.votes += 1

class Tag(object):
    def __init__(self, image, tag_id, value):
        image.tags.append(self)
        self.tag_id=tag_id
        self.value=value

    def name(self):
        return self.tagname.one().name
    
class Tagname(object):
    def __init__(self, name):
        self.name=name
        
folder_map = mapper(Folder, folder_table, \
                    properties = { \
    'subfolders': relation (Folder,
                            cascade="all", # this include delete cascading
                            order_by=folder_table.c.order,
                            primaryjoin=folder_table.c.parent_id==folder_table.c.id,
                            backref=backref('parent',
                                            remote_side=[folder_table.c.id])
                            ),
    'images':     relation (Image,
                            cascade="all, delete, delete-orphan",
                            order_by=image_table.c.order,
                            backref = 'parent')
    }
                    )

image_map = mapper(Image, image_table,
                   properties = { \
    'tags': relation (Tag,
                      cascade="all, delete, delete-orphan",
                      backref = 'image')
    }
                      )

tags_map = mapper(Tag, exiftags_table)

tagnames_map = mapper(Tagname, tagnames_table,
                      properties = { \
    'tags': dynamic_loader (Tag,  # sqlalchemy 0.4 only
                            backref = backref('tagname', lazy='dynamic'))
    }
                      )

session = create_session()

iq = session.query(Image)

