from numpy import *

def fragdistr(pars):
    m=logspace(0,5,60)
    m2=pars[0]
    m3=pars[1]
    Nm3=pars[2]
    p=pars[3]
    nII=pars[4]
    muII=pars[5]
    N2=pars[6]
    nIII=pars[7]
    muIII=pars[8]
    q=pars[9]

    N=where(m<m2, (m/m2)**p*nII,
            where(m<m3, nII*exp(-(m/muII)**0.5),
                  nIII*exp(-(m/muIII)**q)))
    return (m,N)
