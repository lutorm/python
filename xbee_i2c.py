import xbee_serial


pin2sample = {'d0': 'dio-0',
              'd1': 'dio-1',
              'd2': 'dio-2',
              'd3': 'dio-3',
              'd3': 'dio-3',
              'd4': 'dio-4',
              'd5': 'dio-5',
              'd6': 'dio-6',
              'd7': 'dio-7',
              }

# For writing a low, we pull the line low
lo_val = '\x04'
# For writing a high, we make the pin an input and it'll be pulled up
hi_val = '\x03'

class xbee_i2c:
    """
Class for talking i2c to devices hooked up to an XBee by
remotely bit-banging the pins.
"""

    
    def __init__(self, remote, sda_pin, scl_pin):
        """
        Create an i2c connection object to the i2c bus hooked up to the
        specified remote, with the data and clock pins hooked up to
        the specified pins, like 'd0'."""

        self.remote = remote
        self.sda_pin = sda_pin
        self.scl_pin = scl_pin
        self.verbose = False
        
        # let pins float
        self.remote.at(self.sda_pin, parameter=hi_val)
        self.remote.at(self.scl_pin, parameter=hi_val)
        self.current = {self.sda_pin: True,
                        self.scl_pin: True}
        self.stop()
        
    def set(self, pin, value):
        "Set the pin unless it's already set that way."
        if self.current[pin] is not value:
            self.remote.at(pin, parameter = hi_val if value else lo_val)
            self.current[pin] = value
            
    def start(self):
        "Send i2c start: pull data low, then clock low. "
        if self.status:
            if self.verbose:
                print "Sending repeated start"
            self.set(self.sda_pin, True)
            self.set(self.scl_pin, True)
        else:
            if self.verbose:
                print "Sending start"
            
        self.set(self.sda_pin, False)
        self.set(self.scl_pin, False)
        self.status = True

    def stop(self):
        "Send i2c stop: release clock, then data."
        if self.verbose:
            print "Sending stop"
        self.set(self.scl_pin, False)
        self.set(self.sda_pin, False)
        self.set(self.scl_pin, True)
        self.set(self.sda_pin, True)
        self.status=False

    def write_bit(self, value):
        """Write a bit by setting the data line and then toggling the
        clock. Currently does not check if the slave is stretching the
        clock."""
        if self.verbose:
            print "Writing bit %d"%(1 if value else 0)
        self.set(self.sda_pin, value)
        self.set(self.scl_pin, True)
        self.set(self.scl_pin, False)

    def write_byte(self, value):
        if self.verbose:
            print "Sending byte: %x"%value

        for i in range(7,-1,-1):
            self.write_bit(value & (1 << i))

        # read ack, should be a low
        ack = self.read_bit()
        if self.verbose:
            if ack:
                print "NAK"
            else:
                print "ACK"
        return not bool(ack)

    def read_bit(self):
        """
    Read a bit by releasing data, letting clock go high, sampling the value, and
    then pulling it low again."""
        self.set(self.sda_pin, True)
        self.set(self.scl_pin, True)
        samples=xbee_serial.parse_sample_packet(self.remote.at("IS"))
        self.set(self.scl_pin, False)
        return samples[pin2sample[self.sda_pin]]

    def read_byte(self, ack_it=True):
        "Read a byte."
        val=0
        for i in range(7,-1,-1):
            bit = self.read_bit()
            if self.verbose:
                print "Read bit %d"%(1 if bit else 0)
            if bit:
                val = val + (1<<i)

        # ack it
        if self.verbose:
            print "Read byte: 0x%x, %s"%(val,("" if ack_it else "not ")+"acking")
        self.write_bit(False if ack_it else True)
        return val

    def write(self, i2c_addr, data):
        "Write the specified data to the specified 7-bit i2c address."
        self.start()
        
        if self.verbose:
            print "Sending address: %x"%i2c_addr
        for i in range(6,-1,-1):
            self.write_bit(i2c_addr & (1 << i))
        # we are writing, so send a zero
        if self.verbose:
            print "Sending 0 for write"
        self.write_bit(False)

        # read ack, should be a low
        ack = self.read_bit()
        if ack:
            if self.verbose:
                print "NAK"
            return False
        else:
            if self.verbose:
                print "ACK"

        try:
            for d in data:
                ret = self.write_byte(d)
            return ret
        except:
            # not iterable
            return self.write_byte(data)

    def read(self, i2c_addr, n_bytes):
        "Read the specified number of bytes from the specified 7-bit i2c address."
        self.start()

        if self.verbose:
            print "Sending address: %x"%i2c_addr
        for i in range(6,-1,-1):
            self.write_bit(i2c_addr & (1 << i))
        # we are reading, so send a one
        if self.verbose:
            print "Sending 1 for read"
        self.write_bit(True)

        # read ack, should be a low
        ack = self.read_bit()
        if self.verbose:
            if ack:
                print "NAK"
            else:
                print "ACK"

        # now read the requested number of bytes
        result=[]
        for j in range(n_bytes):
            result.append(self.read_byte(j<n_bytes-1))

        return result
