# playing with metropolis algorithm to sample distributions

import random, copy, pyfits, glob, make_color, pdb
import numpy,scipy.stats
from math import *
from numpy import array, dot, cross, transpose, where
from pylab import *

p_add=0.25
p_delete=0.20
debug=False

def projection_matrix(pos, dir, up, fov, npix):
    dir=dir/sqrt(dot(dir,dir))
    up=up/sqrt(dot(up,up))
    hor=cross(dir,up)
    
    M1=numpy.matrix(
        [[ npix/(2*tan(fov/2)), 0,                   -npix/2, 0 ],
         [ 0,                   npix/(2*tan(fov/2)), -npix/2, 0 ],
         [ 0,                   0,                   1,       0 ],
         [ 0,                   0,                   -1,      0 ]])
    M2=numpy.matrix(
        [[ hor[0],  hor[1],  hor[2],  0 ],
         [ up[0],   up[1],   up[2],   0 ],
         [ -dir[0], -dir[1], -dir[2], 0 ],
         [0,        0,       0,       1 ]])
    M3=numpy.matrix(
        [[ 1, 0, 0, -pos[0] ],
         [ 0, 1, 0, -pos[1] ],
         [ 0, 0, 1, -pos[2] ],
         [ 0, 0, 0, 1       ]])
    #print M1
    #print M2
    #print M3
    return M1*M2*M3

def dprint(*args):
    if debug:
        print args
        
def HG(theta,g,cos=False):
    """The Henyey-Greenstein probability distribution for <cos
    theta>=g. If cos=True, theta is assumed to be cos(theta)."""
    if not cos:
        theta = cos(theta)
    return (1-g*g)/(4*pi*(1+g*g-2*g*theta)**1.5)

def uniform(x):
    newx = random.random*pi
    return newx,1

def gaussian(x, sigma):
    """Symmetric Gaussian transition probability, wraps to [0,pi]."""
    delta = random.gauss(0,sigma)
    return (x+delta)%pi, 1

def varsigma(p, sigma):
    """Return sigma to use for a certain probability density. sigma is
    a normalization. Takes larger steps where p is small."""
    return sqrt(sigma/p)

def vargaussian(x, sigmafunc, sigma, p):
    """Asymmetric Gaussian transition probability which takes larger
    steps when the , wraps to [0,pi]."""
    cursigma = sigmafunc(p(x), sigma)
    delta = random.gauss(0, cursigma)
    nextx = (x+delta)%pi
    # now find the corresponding sigma at destination
    nextsigma = sigmafunc(p(nextx), sigma)
    # and calculate the transition probability ratio
    tpr = scipy.stats.norm.pdf(-delta,loc=0,scale=nextsigma) / \
          scipy.stats.norm.pdf(delta,loc=0,scale=cursigma)
    #print "sigma",p(x),p(nextx),delta,cursigma,nextsigma,tpr
    return nextx, tpr
    
def make_gaussian_transition(sigma):
    return lambda x: gaussian(x, sigma)

def exponential(x, scale):
    """Symmetric exponential transition probability."""
    delta = random.expovariate(scale)*(random.randint(0,1)-0.5)*2
    return x+delta, 1

def path_intensity(x, kappa, g, a):
    """Returns the intensity of a ray traversing a uniform medium via
    the interaction points in x, which is a list of 3-arrays. kappa is
    the opacity, g is the scattering asymmetry and a the albedo."""
    intensity = 1/(4*pi) 
    for i in xrange(len(x)-1):
        # propagating from i to i+1
        delta = x[i+1] - x[i]
        r = sqrt(dot(delta, delta))
        intensity *= exp(-r*kappa)
        # and now the angular dependence of scattering into next dir
        if(i<len(x)-2):
            ndelta = x[i+2] - x[i+1]
            costheta = dot(delta/r,ndelta/sqrt(dot(ndelta,ndelta)))
            intensity *= kappa*HG(costheta, g, cos=True)*a
        
    return intensity

def perturb_point(point, scale):
    """Perturbs the points by an exponential
    distribution distance in a random direction. Returns the perturbed
    point.  """
    dr = random.expovariate(scale)
    phi = random.uniform(0,2*pi)
    costheta = random.uniform(-1,1)
    sintheta = sqrt(1-costheta*costheta)
    delta = array([sintheta*cos(phi),
                   sintheta*sin(phi),
                   costheta])*dr
    # perturb the scattering point
    point = point + delta
    return point

def perturbation_probability(x0,x,scale):
    "Returns probability of perturbing x0 -> x."
    delta=x0-x
    r=sqrt(dot(delta,delta))
    return exp(-r/scale)/(4*pi)


def perturb_scattering_point(path, scale):
    """Perturbs one of the scattering points by an exponential
    distribution distance in a random direction. Returns the perturbed
    path and the proposal ratio.  """

    if len(path)<3:
        # no scattering points
        return path,1
    scattering = random.randint(1,len(path)-2)
    path[scattering] = perturb_point(path[scattering], scale)
    dprint("Perturbing point %d"%scattering)
    # proposal ratio is 1 because we use a constant scale
    return path, 1

def add_scattering_point(path, scale):
    """Mutates the path by adding a scattering point by duplicating an
    existing point and then perturbing it. Returns the mutated path."""
    # scattering point to be duplicated
    n_vertex=len(path)
    n_edge = n_vertex-1
    scattering = random.randint(0,n_vertex-1)
    # insert new point before or after?
    if scattering >0 and scattering < n_vertex-1:
        #interior point, we can draw
        before = random.randint(0,1)
        prob_before=1.0
        prob_after=1.0
    elif scattering==0:
        # first point, must be after
        before=1
        prob_before=0.0
        prob_after=1.0
    else:
        # last point, must be before
        before=0
        prob_before=1.0
        prob_after=0.0
        
    newpoint = perturb_point(path[scattering],scale)

    
    # calculate forward proposal probability. this must reflect the
    # two ways that a point can be added on an edge
    forward_prob = p_add*(1./n_edge)* \
                   ((prob_before and
                     perturbation_probability(path[scattering],newpoint,scale)
                     or 0.0)+
                    (prob_after and
                     perturbation_probability(path[scattering+1],newpoint,scale)
                     or 0.0))

    # backward transition probability is just prob of picking the
    # vertex out of the internal vertices (AFTER adding)
    backward_prob = p_delete*(1./n_edge)
    
    path.insert(scattering+before,newpoint) 
    dprint("Adding vertex %s %d, transition prob %f"% \
          (before==1 and "after" or "before",
           scattering,backward_prob/forward_prob))

    return path, backward_prob/forward_prob

def delete_scattering_point(path, scale):
    """Mutates the path by deleting a scattering point. Returns the
    mutated path."""

    n_vertex=len(path)
    n_edge = n_vertex-1

    if n_vertex<=2:
        # nothing to delete
        return path,1

    scattering = random.randint(1,n_vertex-2)

    # forward prob is prob of picking the vertex
    forward_prob = p_delete*(1./n_vertex-2)

    # backward prob is prob of adding the deleted vertex from either of
    # the two neighbors
    backward_prob = p_add*(1./n_edge-1)* \
                   (perturbation_probability(path[scattering-1],
                                             path[scattering],scale)+
                    perturbation_probability(path[scattering+1],
                                             path[scattering],scale))

    dprint ("Deleting vertex %d, transition prob %f"%(scattering,backward_prob/forward_prob))

    del path[scattering]

    return path, backward_prob/forward_prob

def transition(path,scale):
    p=random.random()
    if p<p_add:
        return add_scattering_point(path, scale)
    elif p<p_add+p_delete:
        return delete_scattering_point(path,scale)
    else:
        return perturb_scattering_point(path,scale)

def metropolis(initial, density, transition, n,nburn=0,do_debug=True,
               add_to_image=None):
    """Run the metropolis algorithm for n samples starting with
    initial, using density as the probability density and transition
    to calculate the transition probability."""
    global debug
    debug=do_debug
    
    samples = []
    rejects = 0
    x = initial
    for i in xrange(n+nburn):
        # generate new proposal
        newx, proposalratio = transition(copy.copy(x))

        # decide whether to accept
        a = proposalratio*density(newx)/density(x)

        if callable(add_to_image):
            # add the conditional path to the image
            if(a>0):
                add_to_image(newx,min(a,1))
            if(a<1):
                add_to_image(x,1-min(a,1))
            
        if a>=1 or random.random()<a:
            x=newx
            dprint( "Accepted:",newx,a,proposalratio)
        else:
            dprint( "Rejected:",newx,a,proposalratio)
            if i>=nburn:
                # only count rejection after burn-in
                rejects += 1
            
        if i>=nburn:
            #samples[i-nburn] = x
            samples.append(x)
    print "Rejection ratio",float(rejects)/n
    return samples

def HG_for_g(g):
    return lambda x: HG(x, g)

def simple_test():
    return metropolis(0, HG_for_g(0.8), make_gaussian_transition(.5), 1000)

def test():
    npix=50
    pm=projection_matrix(numpy.array([-1,0,0]),numpy.array([1,0,0]),
                         numpy.array([0,0,1]), 1.0, npix)
    im=numpy.zeros([npix,npix])
    ip=[numpy.array([1,0,0]),numpy.array([-1,0,0])]
    return metropolis(ip, lambda x:path_intensity(x,10,0.5,0.8),
                      lambda x: transition(x,1),
                      10000,do_debug=False,add_to_image=lambda x,y:project_path(x,y,im,pm,npix)),im

def project_pos(pos,m):
    p=array([1.,1.,1.,1.])
    p[0:3]=pos
    proj=(m*numpy.mat(p).T).A.reshape([4])
    assert proj[3]!=0
    return proj[0:2]/proj[3]

def project_path(path, weight, image, projection, npix):
    """Returns the image resulting from projecting all the ray
    paths. The last vertex is assumed to be the eye. The projection is
    a projection matrix."""

    pix=project_pos(path[-2], projection)
    assert pix[0]==pix[0]
    if (pix>0).all() and (pix<npix).all():
        image[int(pix[0]),int(pix[1])]+=weight
    dprint ("Adding %f to image at (%f,%f)"%(weight,pix[0],pix[1]))

    return image

def project_paths(paths, fov,npix):

    """Returns the image resulting from projecting all the ray
    paths. The last vertex is assumed to be the eye, looking along
    +x"""

    image=numpy.zeros([npix,npix])
    map(lambda p:project_path(p,1,image,fov,npix),paths)
        
    return image

def parse_output():
    f=open("metro.txt","r")
    l=f.readlines()
    acc=[]
    rej=[]
    for i in xrange(len(l)):
        if "perturbation scale" in l[i]:
            s=float( l[i].split()[2])
            r=float( l[i].split()[4])
            res= l[i+4].split()[1]

            if "accepted" in res:
                acc.append((s,r))
            elif "rejected" in res:
                rej.append((s,r))
            else:
                1/0
    return acc,rej
    

def makestats(file):
    """Assembles acceptances and rejections of various proposal types
    from the output from Sunrise metropolis."""
    
    s=open(file)
    ss=[x.split()[1:] for x in s.readlines() if "PROP " in x]
    a=[x[1:] for x in ss if x[0]=="A"]
    r=[x[1:] for x in ss if x[0]=="R"]
    psp_a=[[float(y) for y in x[1:]] for x in a if x[0]=="PSP"]
    psp_r=[[float(y) for y in x[1:]] for x in r if x[0]=="PSP"]
    asp_a=[[float(y) for y in x[1:]] for x in a if x[0]=="ASP"]
    asp_r=[[float(y) for y in x[1:]] for x in r if x[0]=="ASP"]
    dsp_a=[[float(y) for y in x[1:]] for x in a if x[0]=="DSP"]
    dsp_r=[[float(y) for y in x[1:]] for x in r if x[0]=="DSP"]

    
    return psp_a,psp_r,asp_a,asp_r,dsp_a,dsp_r

def errornorm(mlt, nmlt, ref, make_table=False, tablehead=''):
    """Calculates the relative error norms for the images in relation
    to the reference image."""

    if type(mlt)!=type(array([])):
        mlt=transpose(pyfits.open(mlt[0])[mlt[1]].data,axes=(1,2,0))
    if type(nmlt)!=type(array([])):
        nmlt=transpose(pyfits.open(nmlt[0])[nmlt[1]].data,axes=(1,2,0))
    if type(ref)!=type(array([])):
        ref=transpose(pyfits.open(ref[0])[ref[1]].data,axes=(1,2,0))

    # restrict comparison to pixels where reference image is nonzero
    m=where((ref>0)) #&(mlt>0)&(nmlt>0))

    mlt_abs_err = (mlt-ref).astype(float64)
    nmlt_abs_err = (nmlt-ref).astype(float64)
    mlt_rel_err= mlt_abs_err/ref
    nmlt_rel_err= nmlt_abs_err/ref

    if(not make_table):
        print "Normalization error of non-MLT: ", nmlt_abs_err[m].mean()/ref[m].mean()
        print "Normalization error of MLT: ", mlt_abs_err[m].mean()/ref[m].mean()
        print
        
        print "Error norms of non-MLT/MLT (>1 means MLT wins)"
        print "L1 relative error",sum(abs(nmlt_rel_err[m]))/sum(abs(mlt_rel_err[m]))
        print "L2 relative error",sum(nmlt_rel_err[m]*nmlt_rel_err[m])/sum(mlt_rel_err[m]*mlt_rel_err[m])
        print "Loo relative error",abs(nmlt_rel_err[m]).max()/abs(mlt_rel_err[m]).max()

        print "L1 absolute error",sum(abs(nmlt_abs_err[m]))/sum(abs(mlt_abs_err[m]))
        print "L2 absolute error",sum(nmlt_abs_err[m]*nmlt_abs_err[m])/sum(mlt_abs_err[m]*mlt_abs_err[m])
        print "Loo absolute error",abs(nmlt_abs_err[m]).max()/abs(mlt_abs_err[m]).max()
        
        print "\nIgnoring normalization errors:"

    mlt*=ref.mean()/mlt.mean();
    nmlt*=ref.mean()/nmlt.mean();
    
    mlt_abs_err = mlt-ref
    nmlt_abs_err = nmlt-ref
    mlt_rel_err= mlt_abs_err/ref
    nmlt_rel_err= nmlt_abs_err/ref

    if(not make_table):
        print "L1 relative error",sum(abs(nmlt_rel_err[m]))/sum(abs(mlt_rel_err[m]))
        print "L2 relative error",sum(nmlt_rel_err[m]*nmlt_rel_err[m])/sum(mlt_rel_err[m]*mlt_rel_err[m])
        print "Loo relative error",abs(nmlt_rel_err[m]).max()/abs(mlt_rel_err[m]).max()
        
        print "L1 absolute error",sum(abs(nmlt_abs_err[m]))/sum(abs(mlt_abs_err[m]))
        print "L2 absolute error",sum(nmlt_abs_err[m]*nmlt_abs_err[m])/sum(mlt_abs_err[m]*mlt_abs_err[m])
        print "Loo absolute error",abs(nmlt_abs_err[m]).max()/abs(mlt_abs_err[m]).max()
        
    if(make_table):
        print "%s & %3.2g & %3.2g & %3.2g & %3.2g & %3.2g & %3.2g\\\\"%(tablehead, sum(abs(nmlt_rel_err[m]))/sum(abs(mlt_rel_err[m])),sum(nmlt_rel_err[m]*nmlt_rel_err[m])/sum(mlt_rel_err[m]*mlt_rel_err[m]),abs(nmlt_rel_err[m]).max()/abs(mlt_rel_err[m]).max(),sum(abs(nmlt_abs_err[m]))/sum(abs(mlt_abs_err[m])),sum(nmlt_abs_err[m]*nmlt_abs_err[m])/sum(mlt_abs_err[m]*mlt_abs_err[m]),abs(nmlt_abs_err[m]).max()/abs(mlt_abs_err[m]).max())

def make_images(mlt_dir, nmlt_dir,scale, **kwargs):
    mltfiles=glob.glob(mlt_dir+"/*.fits")
    mltfiles.sort()
    for f,s in zip(mltfiles,scale):
        make_color.write_color(f, 1, f.replace('.fits','.jpg'),
                               band=(2,1,0),scale=s,autopercentile=0.1,
                               overwrite=True, **kwargs)

    nmltfiles=glob.glob(nmlt_dir+"/*.fits")
    f=nmltfiles[0]
    for c,s in enumerate(scale):
        make_color.write_color(f, c+1, f.replace('.fits','-%d.jpg'%c),
                               band=(2,1,0),scale=s,autopercentile=0.1,
                               overwrite=True, **kwargs)
        
def make_p04():
    scale=["%f,%f,%f"%(s,s,s) for s in (.005,.01,.02,.04,.05,.05,.1,.2)]
    view=['$12.5\degrees$','$42.5\degrees$','$65.5\degrees$','$77.5\degrees$','$88.5\degrees$','$88.5\degrees$, $1/2$ fov','$88.5\degrees$, $1/4$ fov','$88.5\degrees$, $1/8$ fov']
    make_images('metro-2.5e6','nmetro-1e6', scale,Q=4)
    print "\nError table:"
    for c in range(8):
        errornorm(('metro-2.5e6/metro-2.5e6-%d.fits'%c,1),
                  ('nmetro-1e6/nmetro-1e6-all.fits',c+1),
                  ('ref-3e8-all/nm-test-all.fits',c+1), True, tablehead=view[c])

    make_images('tau1000-metro-1e9','tau1000-nmetro-9e10', [100.])

def make_wg96():
    make_images('wg96v2fix-metro-36e8','wg96v2-nmetro-5e7', [100.])
    print "\nError table:"
    errornorm(('wg96-metro-3e8/wg96-metro-3e8-0.fits',1),
              ('wg96-nmetro-69e6/wg96-nmetro-69e6-all.fits',1),
              ('wg96-nmetro-1e9/wg96-nmetro-1e9-all.fits',1), True, tablehead='')



def disaggregate(file, n_max, hduname, scale0,increase,**kwargs):
    """Loads a bidir output segregated by n and s and plots an
    overview of the images. The images are also returned as a list,
    with the coadded image added to the end."""
    if type(file)==type(""):
        f=pyfits.open(file)
        d=[transpose(f[hduname%(c)].data, axes=(1,2,0)) for c in range((n_max+2)*(n_max+1)/2+1)]
    else:
        d=file
             
    clf()
    ims=[]
    for n in range(n_max+1):
        for s in range(n+1):
            cam=n*(n+1)/2+s
            print "n %d, s %d, cam %d"%(n,s,cam)
            subplot(n_max+1,n_max+1, (n_max+1)*n+s+1)
            i=d[cam]
            if cam==0:
                sum=array(i,copy=True)
            else:
                sum += i
            ims.append(i)
            sc=scale0*increase**n
            imshow(make_color.make_image(i,return_jpeg=False,
                                         scale='%g,%g,%g'%(sc,sc,sc),**kwargs),
                   interpolation=None)
            title('s=%d'%s)
            ylabel('n=%d'%n)
        

    # final cam in second to upper left
    subplot(n_max+1,n_max+1,2*(n_max+1))
    cam=(n_max+2)*(n_max+1)/2
    i=d[cam]
    sum += i
    ims.append(i)
    sc=scale0*increase**(n_max-1)
    imshow(make_color.make_image(i,return_jpeg=False,
                                 scale='%g,%g,%g'%(sc,sc,sc),**kwargs),
                   interpolation=None)
    title('n>%d'%n_max)

    # sum in upper left
    subplot(n_max+1,n_max+1,n_max+1)
    imshow(make_color.make_image(sum,return_jpeg=False,
                                scale='%g,%g,%g'%(scale0,scale0,scale0),**kwargs),
                              interpolation=None)
    title('Total')
    ims.append(sum)
    return ims

def load_bunch(files, outfile=None, hdu='camera0-star'):
    """Calculates mean and std error of the mean of a bunch of images,
    which can be specified as a list of filenames or as a list of
    loaded images."""

    if type(files[0])==type(""):
        i=[pyfits.open(ff)[hdu].data.astype(float64)[:,:,:,newaxis] for ff in files]
    else:
        i=[x.astype(float64)[:,:,:,newaxis] for x in files]
    ai=concatenate(i,axis=3)
    mean=ai.mean(axis=3)
    # standard error of the mean
    stderr=ai.std(axis=3,ddof=1)/sqrt(ai.shape[3])
    
    # mean=reduce(lambda x,y:x+y, i)/len(i)
    # sq=[x*x for x in i]
    # sumsq=reduce(lambda x,y:x+y, sq)
    # var=sumsq/len(sq)-mean*mean
    # stdev=sqrt(var)

    if outfile:
        hdulist=pyfits.HDUList([pyfits.PrimaryHDU(),
        pyfits.ImageHDU(mean,name="CAMERA0-STAR"),pyfits.ImageHDU(stdev,name="CAMERA0-STDEV")])
        hdulist.writeto(outfile)

    ss=concatenate([sum(sum(x,axis=2),axis=1) for x in i], axis=1)
    smean=ss.mean(axis=1)
    sstderr=ss.std(axis=1, ddof=1)/sqrt(ss.shape[1])

    # smean=reduce(lambda x,y:x+y, ss)/len(ss)
    # ssq=[x*x for x in ss]
    # ssumsq=reduce(lambda x,y:x+y, ssq)
    # svar=ssumsq/len(ssq)-smean*smean
    # sstdev=sqrt(svar)

    norm=sum(ss,axis=0)
    nmean=norm.mean()
    nstderr=norm.std(ddof=1)/sqrt(norm.size)

    slopes=ss/sum(ss, axis=0)
    slmean=slopes.mean(axis=1)
    slstderr=slopes.std(axis=1, ddof=1)/sqrt(slopes.shape[1])
    #pdb.set_trace()
    mean=transpose(mean,axes=(1,2,0))
    stderr=transpose(stderr,axes=(1,2,0))

    return mean,stderr,smean,sstderr,slmean,slstderr,nmean,nstderr,ai
