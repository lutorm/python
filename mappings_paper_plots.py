import pylab, pyx,os, numpy, p04, sed, glob, mipsflux, lineflux
from pylab import *

def biasplot():
    o=numpy.loadxt(os.path.join(os.getenv('HOME'),'dust_data/kext_albedo_WD_MW_3.1_60'),skiprows=50)
    l=o[:,0]*1e-6
    t=o[:,4]/o[783,4] # tau relative to 0.9um
    pylab.clf()
    lines=[]
    legtxt=['Initial',r'$\tau_\mathrm{ref}=1$',
            r'$\tau_\mathrm{ref}=0.5$',
            r'$\tau_\mathrm{ref}=2$']
    pf=pylab.semilogx
    lines.append(pf(l,l*0.+1.))
    lines.append(pf(l,exp(1*(1-t))*t))
    lines.append(pf(l,exp(0.5*(1-t))*t))
    lines.append(pf(l,exp(2*(1-t))*t))
    pylab.xlabel(r'$\lambda$/m')
    pylab.ylabel('Relative intensity')
    pylab.figlegend(lines,legtxt,'lower right')
    pylab.axis([1e-7,1e-4,0,1.5])
    pylab.grid(True)

    return l,t

def pyxbias():
    o=numpy.loadtxt(os.path.join(os.getenv('HOME'),'dust_data/kext_albedo_WD_MW_3.1_60'),skiprows=50)
    l=o[:,0]*1e-6
    t=o[:,4]/o[783,4] # tau relative to 0.9um
    c=[0,0.25,0.50,0.75]
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                        x=pyx.graph.axis.log(min=1e-7,max=1e-4,title="$\lambda$/m"),
                        y=pyx.graph.axis.lin(min=0,max=1.5,title="Relative intensity"))

    data=[ \
        (pyx.graph.data.array([l,l*0.0+1.0],x=1,y=2,title='Initial'), pyx.color.grey.black),
        (pyx.graph.data.array([l,exp(1*(1-t))*t],x=1,y=2,title=r'$\tau_{ref}=1$'), pyx.color.rgb.red),
        (pyx.graph.data.array([l,exp(0.5*(1-t))*t],x=1,y=2,title=r'$\tau_{ref}=0.5$'), pyx.color.rgb.green),
        (pyx.graph.data.array([l,exp(2*(1-t))*t],x=1,y=2,title=r'$\tau_{ref}=2$'), pyx.color.rgb.blue)
        ]

    for d,c in data:
        g.plot(d,styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,c])])
    g.writePDFfile('bias')

def p04comparison():
    dir='/home/patrik/tests_sunrise/p04/2008/tau100/'
    g=p04.pyxp04comparison(glob.glob(dir+'tau100_s?.fits')+[dir+'tau100_final.fits'],[0,1,2],'/home/patrik/tests_sunrise/p04/pascucci_radical_sed_tau100.txt',relative=True,legendtxt=[r'$\theta=12.5^o$',r'$\theta=42.5^o$',r'$\theta=62.5^o$'])
    g.writePDFfile('p04comparison.pdf')

def mcvariance():
    sets=['set5g','set5gs1','set5gs2','set5gs3','set5gs4']
    c=['L_lambda_out%d'%i for i in range(12)]
    g=sed.pyxplot_sedsigma([[('/data/patrik/test/Sbc11i4-u4/%s/mcrx_010.fits'%s,c[0])for s in sets],[('%s/mcrx_010.fits'%s,c[6])for s in sets]],mode='relative sigma',legendtxt=['Face-on','Edge-on'],yrange=[-.01,.01],xrange=[0.89e-7,1e-3],alpha=0.5)
    g.writePDFfile("mcvariance")
    
def gridconvergence():
     c='L_lambda_scatter6'
     files=['set5as/mcrx_010.fits','set5ar/mcrx_010.fits','set5aq/mcrx_010.fits','set5ak/mcrx_010.fits','set5ao/mcrx_010.fits','set5av/mcrx_010.fits','set5aw/mcrx_010.fits']
     legendtxt=['624k cells','1.2M cells','1.5M cells','2.5M cells', \
                    '3.5M cells', '14M cells']
     g=sed.pyxplot_seds([('/data/patrik/test/Sbc11i4-u4/%s'%f,c) for f in files],relative=True,legend=True,legendtxt=legendtxt,ytitle=r'SED relative to fiducial model', range=[0.91e-7,5e-6,-0.03,0.04])
     g.writePDFfile("tautol-edgeon")

def particleconvergence():
    c=['L_lambda_out%d'%i for i in range(12)]
    files=glob.glob('/data/patrik/sims/Sbc11i4-u4/set5as/mcrx*fits')
    f10x=glob.glob('/data/patrik/sims/Sbc11i410x-u4/set5as/mcrx*fits')

    g=sed.pyxplot_sedsigma([[(f,c[0])for f in files],[(f,c[0])for f in f10x]],mode='relative mean',legendtxt=['Sbc','Sbc 10x'],xrange=[0.91e-7,1e-3],yrange=[-0.4,0.5],alpha=0.5)
    x,y=g.vpos(0.5,0.9)
    g.text(x,y,'Face-on',[pyx.text.halign.center])
    g.writePDFfile('particleconv-faceon')

    g=sed.pyxplot_sedsigma([[(f,c[6])for f in files],[(f,c[6])for f in f10x]],mode='relative mean',legendtxt=['Sbc','Sbc 10x'],xrange=[0.91e-7,1e-3],yrange=[-2,2],alpha=0.5)
    x,y=g.vpos(0.5,0.9)
    g.text(x,y,'Edge-on',[pyx.text.halign.center])
    g.writePDFfile('particleconv-edgeon')

    
def sedplots():
    sims = ['Sbcp1s-u4','Sbc11i4-u4','Sbcm1s-u4','G3il-u1','G2in-u1','G1i-u1','G0i-u1']
    yrange = [ (1e33,1e38),(1e33,1e38),(1e33,1e38),(1e33,1e38),(1e32,1e37),
               (1e32,1e37),(1e31,1e36) ]
    set = 'set5as'
    files = ['/data/patrik/sims/%s/%s/mcrx_010.fits'%(s,set) for s in sims]
    legend=['Sbc+','Sbc','Sbc-','G3','G2','G1','G0']

    # plot all galaxies for a given inclination in one plot
    g=sed.pyxplot_seds([(f,'L_lambda_out0') for f in files],
                       relative=False,legendtxt=legend)
    g.writePDFfile('faceon-seds')
    g=sed.pyxplot_seds([(f,'L_lambda_out6') for f in files],
                       relative=False,legendtxt=legend)
    g.writePDFfile('edgeon-seds')

    # plot one galaxy for varying inclinations in one plot
    for s,yr in zip(sims,yrange):
        file='/data/patrik/sims/%s/%s/mcrx_010.fits'%(s,set)
        cams=['L_lambda_out0','L_lambda_out3','L_lambda_out5','L_lambda_out6']
        legend=[r'$0^\circ$',r'$63^\circ$',r'$85^\circ$',
                '$90^\circ$']
        g=sed.pyxplot_seds([(file,c) for c in cams],
                           relative=False,legendtxt=legend,
                           range=[0.91e-7,1e-3,yr[0],yr[1]])
        g.writePDFfile('%s-seds'%s)
        
    # plot sigma for all snapshots for G0 as shaded plot
    files=glob.glob('/data/patrik/sims/G0i-u1/set5as/mcrx*fits')
    g=sed.pyxplot_sedsigma([(f,'L_lambda_out0') for f in files],
                           mode='absolute',legendtxt=None,
                           xrange=[0.91e-7,1e-3],yrange=[yrange[-1][0],yrange[-1][1]])
    g.writePDFfile('G0-seds-allsnaps')
    
    # for Sbc, split up contributions
    file='/data/patrik/test/Sbc11i4-u4/set5as/mcrx_010.fits'
    legend=[r'bare stellar population',r'stars \& MAPPINGS particles (intrinsic)',r'stars \& MAPPINGS particles (attenuated)','diffuse dust \& PAH emission','total SED']
    for c,tit in zip([0,6],['Face-on','Edge-on']):
        cams=['L_lambda_nonscatter%d'%c,'L_lambda_scatter%d'%c,'L_lambda_ir%d'%c,'L_lambda_out%d'%c]

        g=sed.pyxplot_seds([('/data/patrik/test/Sbc11i4-u4/set5ba/mcrx_010.fits','L_lambda')]+[(file,cc) for cc in cams[:-1]],
                           relative=False,legendtxt=legend[:-1],
                           range=[0.91e-7,1e-3,1e32,1e38])
        g=sed.pyxplot_seds([(file,cams[-1])],
                           relative=False,legendtxt=[legend[-1]],
                           palette = pyx.color.rgb.black,
                           range=[0.91e-7,1e-3,1e32,1e38], graph=g)
        x,y=g.vpos(0.5,0.9)
        g.text(x,y,tit,[pyx.text.halign.center])
        g.writePDFfile('Sbc-sed-contributions-cam%d'%c)

def DL07sed():
    # for Sbc, plot the DL07 template spectrum compared to the normal
    files=['%s/mcrx_010.fits'%s for s in ['set5as','set5bc']]
    legend=[r'thermal eq. + PAH template',r'DL07 templates',
            r'stars \& MAPPINGS particles (att.)']
    cam='L_lambda_ir0'
    g=sed.pyxplot_seds([(f,cam) for f in files]+
                       [(files[0],'L_lambda_scatter0')],
                       relative=False,legendtxt=legend,
                       range=[0.91e-7,1e-3,1e33,1e38])
    g.writePDFfile('Sbc-DL07-template-sed')


    
def singscomparisons():
    """Creates a whole slew of sings comparison plots, including
    parameter variation studies."""

    set_name='set5as'
    q=mipsflux.query_set(set_name,True)
    data=mipsflux.load_sql_fluxes(q,name=set_name)

    # fiducial
    makesingscomparisonplots(data, '', density=True)

def parameterstudy(smcbugfix=True):
    """Creates a whole slew of sings comparison plots, including
    parameter variation studies. Must be run from the
    data/test/Sbc11i4-u4 directory."""

    sets='abcdefghijklmnopqrstuvwxyz'
    sets=[s for s in sets]+['as','ax','ay','az','ba','bb','bc','bg','bi','bu','bv']
    data={}
    for s in sets:
        set_name = "set5%s"%s
        print "loading",set_name
        data[s]=mipsflux.load_fluxes(glob.glob('%s/broadband_010.fits'%set_name),name=set_name)

    graphs=[(['c','bv','bu'],('MW','LMC','SMC'),'dustmodel-') if smcbugfix else
            (['c','v','bv','a','bu'],('MW','LMC','LMCfix','SMC','SMCfix'),'dustmodel-'),
            (['c','f','m','n'],('60','40','20','00'),'PAHfraction-'),
            (['g','c'],('$10^5\,\mathrm{M}_{\odot}$',
                        '$10^7\,\mathrm{M}_{\odot}$'),'mappings-mcl-'),
            (['l','j','g','k'],('0.15','0.30','0.50','0.70'),
             'pah-template-fraction-'),
            (['h','g','i','bg'],('0.0','0.20','0.40','1.0'),'pdr-fraction-'),
            (['ax','as','ay'],('0.3','0.4','0.5'),'dust-to-metals-'),
            (['o','g','p'],('$0.5\cdot 10^9$\,yr',
                            '$1.1\cdot 10^9$\,yr',
                            '$1.7\cdot 10^9$\,yr'),'multiphase-t0star-'),
            (['r','g','q','t'],
             ('$0.9\cdot 10^7\,\mathrm{M}_{\odot}\mathrm{kpc}^{-3}$',
              '$1.7\cdot 10^7\,\mathrm{M}_{\odot}\mathrm{kpc}^{-3}$',
              '$3.2\cdot 10^7\,\mathrm{M}_{\odot}\mathrm{kpc}^{-3}$',
              'Single Phase'),
             'multiphase-rhoth-'),
            (['w','g','x'],
             ('$\lambda_\mathrm{ref}=0.5\mu\mathrm{m}$',
              '$\lambda_\mathrm{ref}=0.9\mu\mathrm{m}$',
              '$\lambda_\mathrm{ref}=1.5\mu\mathrm{m}$'),'lambdaref-'),
            (['as','ba'],
             ('with MAPPINGS','w/o MAPPINGS'), 'mappings-'),
            (['as','bb'],
             ('constant radius','corrected for doublecounting'), 'doublecounting-'),
            (['as','bc'],
             ('thermal eq.','DL07'), 'dl07-template-'),
            (['as','bi'],('Starburst99','Maraston05'),'stellarmodel-')
            ]
    for sets,legends,p in graphs:
        print "Now running",sets,p
        for s,l in zip(sets,legends):
            data[s].name=l
        makesingscomparisonplots([data[s] for s in sets], p, \
            density=False,plotstyle=[pyx.graph.style.symbol(symbolattrs=[pyx.color.grey.black]),pyx.graph.style.line(lineattrs=[pyx.color.grey.black])])
        


def makesingscomparisonplots(data, prefix, **kwargs):
    #data=mipsflux.fudge_fluxes(data)
    filters=[
        (['FUV_GALEX','NUV_GALEX','NUV_GALEX','V_Johnson'],(0.2,2.5),(0.01,3), 'GALEX FUV/GALEX NUV','GALEX NUV/V'),
        (['V_Johnson','Ks_2MASS','Ks_2MASS','IRAC1_SIRTF'],(0.3,10),(1.5,8), 'V/Ks',r'Ks/IRAC $3.6\,\mathrm{\mu m}$'),
        (['B_Johnson','V_Johnson','V_Johnson','R_Cousins'],(0.5,2),(0.5,3), 'B/V', 'V/R'),
        (['IRAC1_SIRTF','IRAC3_SIRTF','IRAC3_SIRTF','IRAC4_SIRTF'],(0.3,6),(0.3,4), r'IRAC $3.6\,\mathrm{\mu m}$/IRAC $5.8\,\mathrm{\mu m}$',  r'IRAC $5.8\,\mathrm{\mu m}$/IRAC $8.0\,\mathrm{\mu m}$'),
        (['IRAC1_SIRTF','IRAC3_SIRTF','IRAC1_SIRTF','IRAC4_SIRTF'],(0.3,6),(0.2,20), r'IRAC $3.6\,\mathrm{\mu m}$/IRAC $5.8\,\mathrm{\mu m}$', r'IRAC $3.6\,\mathrm{\mu m}$/IRAC $8.0\,\mathrm{\mu m}$'),
        (['IRAC1_SIRTF','IRAC4_SIRTF','IRAC4_SIRTF','MIPS24_SIRTF'],(0.2,20),(0.2,40),r'IRAC $3.6\,\mathrm{\mu m}$/IRAC $8.0\,\mathrm{\mu m}$', r'IRAC $8.0\,\mathrm{\mu m}$/MIPS $24\,\mathrm{\mu m}$'),
        (['MIPS24_SIRTF','MIPS70_SIRTF','MIPS70_SIRTF','MIPS160_SIRTF'],(0.05,1),(0.1,4),r'MIPS $24\,\mathrm{\mu m}$/MIPS $70\,\mathrm{\mu m}$', r'MIPS $70\,\mathrm{\mu m}$/MIPS $160\,\mathrm{\mu m}$'),
        (['MIPS70_SIRTF','MIPS160_SIRTF','MIPS160_SIRTF','850W_SCUBA'],(0.1,4),(30,2e3), r'MIPS $70\,\mathrm{\mu m}$/MIPS $160\,\mathrm{\mu m}$', r'MIPS $160\,\mathrm{\mu m}$/SCUBA $850\,\mathrm{\mu m}$')
        ]

    palette=pyx.color.gradient.Grey
    for i,f in enumerate(filters):
        plot_slugs = True if '850W_SCUBA' in f[0] else False
        g=mipsflux.pyxplot_fluxratios(data,filters=f[0],sings_splitby='type',xrange=f[1],yrange=f[2], plot_slugs=plot_slugs, density_palette=palette, xlabel=f[3], ylabel=f[4], height=7, **kwargs)
        g.writePDFfile("%sfluxratios-%d"%(prefix,i))

    g=mipsflux.pyx_d07_fig12(data,density_palette=palette,**kwargs)
    g.writePDFfile("%sd07_fig12"%prefix)

    g=mipsflux.pyx_d07_fig6(data,density_palette=palette,**kwargs)
    g.writePDFfile("%sd07_fig6"%prefix)

def bendoplots():
    palette=pyx.color.gradient.Grey
    sims = ['Sbcp1s-u4','Sbc11i4-u4','Sbcm1s-u4','G3il-u1','G2in-u1','G1i-u1','G0i-u1']
    s5as=mipsflux.load_image_fluxes(['/data/patrik/sims/%s/set5as/broadband_010.fits'%s for s in sims],cameras=[0],rebin_by=1)
    g=mipsflux.pyx_bendo_fig2(s5as, density=True, density_palette=palette)
    g.writePDFfile("bendo_fig2")
    g=mipsflux.pyx_bendo_fig5(s5as, density=True, density_palette=palette)
    g.writePDFfile("bendo_fig5")

def singsbestfit(require_850=True):
    
    sims = ['Sbcp1s-u4','Sbc11i4-u4','Sbcm1s-u4','G3il-u1','G2in-u1','G1i-u1','G0i-u1']
    set_name = 'set5as'

    for s in sims:
        q=mipsflux.query_sim(mipsflux.query_set(set_name,True),s)
        if s is not 'G0i-u1':
            q=mipsflux.query_snapshot(q,10)
            
        data=mipsflux.load_sql_fluxes(q,name=s)
        
        bf=mipsflux.find_best_fit(data,require_850=require_850)
        inc=data.metadata['theta'][bf[5]]*180/pi
        if inc>90:
            inc=180-inc
        g=sed.pyxplot_seds([(bf[0][0].replace('snapshot','mcrx')+'.fits',
                             "L_lambda_out%d"%bf[0][1])], \
                           relative=False,legendtxt=[r'$\theta=%3.1f^o$'%(inc)])
        g.plot(pyx.graph.data.array([bf[1],bf[2]],
                                    x=1,y=2,
                                    title='%s (%s)'%(bf[3],bf[4])))
        g.writePDFfile('sings-bestfit-%s'%s)

def lineplots():
    q=lineflux.query_set('set5as',True)
    d=lineflux.load_sql_fluxes(q)
    lineflux.halpha(d)
    lineflux.oii(d)
