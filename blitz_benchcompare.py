from pylab import *
import os,sys,pdb, re

def comp(files, dir1, dir2):
    for i in files:
        figure(1)
        f1=os.path.join(dir1,i)
        cmd=open(f1,'r').read()
        try:
            exec(cmd)
        except:
            pass
        Mf=where(Mf>100,0,Mf)
        Mf1=zeros(shape=Mf.shape)
        Mf1[:,:]=Mf[:,:]
        clf()
        f2=os.path.join(dir2,i)
        cmd=open(f2,'r').read()
        try:
            exec(cmd)
        except:
            pass
        clf()
        Mf=where(Mf>100,0,Mf)
        semilogx(parm,Mf[:,0])
        semilogx(parm,Mf1[:,0],'o-')
        #semilogx(parm,Mf1[:,1],'+-')
        axis([1,1e6,0,1.1*Mf[:,[0,2,3,4]].max()]);grid(True);draw();show()
        sys.stdin.readline()

def evalblitzfile(f):
    return
    for l in cmd:
        if "import" not in l and l[0]!="#":
            exec(l)

def make_compplots(files,dir1,dir2, to_screen=True, plotfunc=semilogx):
    """Makes plots and an html file for blitz comparisons between two
    directories."""
    maxvalid=10

    if not to_screen:
        hf=open("blitzcomp.html",'w')
        hf.write("""<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<html> <head>
<title>Blitz comparison %s vs %s</title>
</head>
<body>
<h1>Blitz comparison %s vs %s</h1>
"""%(dir2,dir1,dir2,dir1))

        hf.write("""
    These plots show a comparison of the cvs version of
    blitz with Patrik's new version which facilitates automatic
    vectorization with the Intel compiler. The TinyVector plots are
    unreliable because the TinyVector operations are too small to be
    measured compared to the loop overhead in the benchmarks. Loops
    that start with f use float operands, so have an SSE width of 4,
    while the normal loops use double and have an SSE width of
    2. The Array obviously has a lot of overhead for small arrays,
    part of this is because the Intel compiler in quite unwilling to
    completely inline the expression templates.</p>""")
             
    files.sort()
    for i in files:

        figure(1)
        f1=os.path.join(dir1,i)
        cmd=open(f1,'r').read()
        try:
            exec(cmd)
        except:
            pass

        # sohuld save title so we don't have to do shit like this
        tit=dir1+": "+re.search(r"title\(\'(.*)\'\)", cmd).group(1)
        #pdb.set_trace()
        tit=re.sub(r"((\$)(\w+))",r"\1\2",tit)
        print `tit`
        
        Mf=where((Mf>maxvalid) | (Mf <= 0),float('nan'),Mf)
        Mf1=zeros(shape=Mf.shape)
        Mf1[:,:]=Mf[:,:]

        if dir2!=None:
            clf()
            f2=os.path.join(dir2,i)
            cmd=open(f2,'r').read()
            try:
                exec(cmd)
            except:
                pass
            
        clf()
        Mf=where((Mf>maxvalid) | (Mf <= 0),float('nan'),Mf)

        maxval=max(Mf1.max(),Mf.max())
        if plotfunc==loglog:
            minval=min(Mf1.min(),Mf.min())
        else:
            minval=0
        print maxval,minval
        #subplot(121)
        lines=[]
        if dir2!=None:
            lines.append(semilogx(parm,Mf[:,0]))
        #lines=semilogx(parm,Mf[:,0])
        #semilogx(parm,Mf[:,1],'g+')
        #axis([1,1e6,0,1.1*maxval]);grid(True);
        #xlabel('Vector length')
        #ylabel('Gflops/s')
        #title(dir2+' '+tit)
        
        #subplot(122)
        lines.append(semilogx(parm,Mf1))
        #semilogx(parm,Mf1[:,1],'g+')
        axis([1,1e6,0.9*minval,1.1*maxval]);grid(True);
        xlabel('Vector length')
        ylabel('flops/cycle')
        #title(dir1+' '+tit)
        title(tit)

        figlegend(lines,legnames,'lower right')
        if to_screen:
            draw();show()
            sys.stdin.readline()
        else:
            imfil="%s.png"%i
            savefig(imfil)
            hf.write("""<img src="%s"><p>\n"""%imfil)
        

    
    hf.write("</body></html>\n")
