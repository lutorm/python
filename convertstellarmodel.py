import pylab,pyfits, vecops, os
from numpy import *
import pdb

def loadfile_sb99(file, timecol=0, lambdacol=1, sedcol=2, return_raw=False):
    """Read SB99 file and return sed array (in log units) and lambda, time
    vectors (with SI units and a reference mass of 1Msun)."""
    print "Reading SB99 file ",file
    f=open(file,'r')
    while "TIME" not in f.readline():
        pass
    f=loadtxt(f)
    time=unique(f[:,timecol])
    lam=unique(f[:,lambdacol])*1e-10
    # sed is in log units
    sed=reshape(f[:,sedcol], (time.shape[0], lam.shape[0]))
    # sed units from erg/s/A to W/m and reference mass from 1e6Msun to 1Msun
    sed+=(3.-6.)

    if return_raw:
        return time,lam,sed,f
    else:
        return time,lam,sed

def loadfile_fsps(file, Lsun=3.839e26):
    """Read a file from Charlie Conroy's FSPS model and return sed
    array (in log units) and lambda, time vectors (with SI units and a
    reference mass of 1Msun)."""
    print "Reading SB99 file ",file
    f=open(file,'r')

    # skip header lines
    l=f.readline()
    while l[0]=="#":
        l=f.readline()

    # parse number of time and wavelength points
    ntime,nlam = [int(v) for v in l.split()]

    # next read nlam wavelengths, all on one line
    lam = array([float(v) for v in f.readline().split()])
    assert lam.shape[0]==nlam
    
    # now read the spectra
    time = zeros(ntime)
    sed = zeros((ntime,nlam))
    
    for t in range(ntime):
        # first line is log(age/yr) log(mass) Log(lbol) log(SFR), where
        # for SSPs the mass is 1Msun, SFR is just set to -33, 
        l=[float(v) for v in f.readline().split()]
        time[t] = 10**l[0]

        # next line is the sed, nlam entries
        sed[t,:] = array([float(v) for v in f.readline().split()])

    # finally, convert units to what we want (ages are already in yr)
    # wavelengths A->m
    lam = lam*1e-10
    
    # sed is in L_nu (Lsun/Hz), so convert to L_lambda (W/m)
    c0 = 299792458
    sed = sed*c0*sed/(lam*lam)*Lsun
    
    return time,lam,sed

def loadfile_Maraston(file, timecol=0, lambdacol=2, sedcol=3, zcol=1):
    """Read Maraston SED file and return sed array and lambda, time
    vectors (with SI units and a reference mass of 1Msun)."""
    print "Reading Maraston file ",file
    f=open(file,'r')
    f=loadtxt(f)
    # time is in Gyr
    time=unique(f[:,timecol])
    time*=1e9
    # wavelengths in A
    lam=unique(f[:,lambdacol])
    lam*=1e-10
    sed=reshape(f[:,sedcol], (time.shape[0], lam.shape[0]))
    # sed units from erg/s/A to W/m (reference mass is already 1Msun)
    # also make log units
    sed=log10(where(sed>0,sed,1e-300)*10**(3.-0.))

    return time,lam,sed

def loadyields_sb99(file,times,timecol=0, masslosscol=13):
    """Reads SB99 yields file and extracts the yields for
    the times specified."""
    print "Reading SB99 yield file ",file
    f=open(file,'r')
    while "TIME" not in f.readline():
        pass
    f=loadtxt(f)
    time=f[:,timecol]
    ml=f[:,masslosscol]

    # convert to mass fraction remaining
    ml=1.0 - 10**(ml-6.0)
    if not all((ml>0)&(ml<=1.0)):
        print "Warning: Unphysical mass loss entries in file %s"%file

    # only return times specified
    return ml[setmember1d(time,times)]

def merge_hilo(nl,nsed,hl,hsed):
    """Merges the normal and hires spectra such that hires spectra are
    used over the wavelength range they are defined for."""
    if hl==None:
        return nl,nsed

    lolam=argwhere(nl<hl[0])[:,0]
    hilam=argwhere(nl>hl[-1])[:,0]
    nlo=lolam.shape[0]
    nhi=hilam.shape[0]
    print "Using %d lores wavelengths on blue and %d on red end of hires spectrum"%(nlo,nhi)

    # match hires sed so it is aligned with lores one by linearly interpolating the hires-lores difference over the hires interval and subtracting it.
    hsed-=outer(
        hsed[:,-1] - nsed[:,hilam[0]-1] - hsed[:,0] + nsed[:,lolam[-1]+1],
        (log10(hl)-log10(hl[0]))/
        (log10(hl[-1])-log10(hl[0]))) + \
        (hsed[:,0]-nsed[:,lolam[-1]+1])[:,newaxis]

    msed=zeros(shape=(hsed.shape[0], hsed.shape[1]+nlo+nhi))
    msed[:,lolam] = nsed[:,lolam]
    msed[:,nlo:nlo+hsed.shape[1]]=hsed
    msed[:,nlo+hsed.shape[1]:]=nsed[:,hilam]

    mlam=zeros(shape=(msed.shape[1]))
    mlam[lolam]= nl[lolam]
    mlam[nlo:nlo+hsed.shape[1]]=hl
    mlam[nlo+hsed.shape[1]:]=nl[hilam]
    print "Merged spectrum contains %d wavelengths"%mlam.shape[0]
    
    return mlam,msed

def stack_times(spectra):
    """Spectra is a list of tuples of time arrays and seds, assumed to be in
    increasing time order, and returns the total time and sed arrays
    by using times from a younger run when available."""

    # figure out stacking dimensions
    # tt keeps the times which are used for each sed, targ is the
    # positions of those times in that sed vector
    times=[x[0] for x in spectra]
    seds=[x[1] for x in spectra]
    mls=[x[2] for x in spectra]
    
    print "Spectrum 0 contains %d times between %e and %e"%(times[0].shape[0],times[0][0],times[0][-1])
    
    tt=[times[0]]
    targ=[arange(times[0].shape[0])]
    for t in times[1:]:
        older=argwhere(t>tt[-1][-1])[:,0]
        targ.append(older)
        tt.append(t[older])
        print "Next spectrum contains %d times between %e and %e, using last %d"%(t.shape[0],t[0],t[-1],older.shape[0])

    ntot = reduce(add,[t.shape[0] for t in tt])
    tottimes = zeros(shape=(ntot))
    totsed = zeros(shape=(ntot,seds[0].shape[1]))
    totml = zeros(shape=(ntot))
    
    pos=0
    for t,ta,s,m in zip(tt,targ,seds,mls):
       tottimes[pos:pos+t.shape[0]]=t
       totsed[pos:pos+t.shape[0],:]=s[ta,:]
       # mass loss is "special" because the sb99 files can be screwed up if the integration was bad from the beginning. hence we correct the starting point to the end of the previous file
       lastml = totml[pos-1] if pos>0 else m[0]
       totml[pos:pos+t.shape[0]]=m[ta]-m[ta[0]]+lastml
       #pdb.set_trace()
       pos+=t.shape[0]

    return tottimes,totsed,totml

def loadall(files, loadfunc_lores, loadfunc_hires, loadfunc_nebular=None):
    """Files is a list of tuples containing names of (normal, hires,
    yield) spectra, ordered in increasing age. If subtract_nebular is
    true, the nebular continuum is loaded from the lores spectrum and
    used to subtract the hires one."""
    spectra=[]
    for i,f in enumerate(files):
        nt,nl,ns = loadfunc_lores(f[0])
        if f[1]!=None:
            ht,hl,hs = loadfunc_hires(f[1])
            assert all(nt==ht)
            if not loadfunc_nebular ==None:
                # load nebular continuum, resample, and subtract
                print "Subtracting nebular continuum"
                neb=loadfunc_nebular(f[0])
                assert all(neb[1]==nl)
                logl=log10(neb[1])
                loghl=log10(hl)
                artifactl=[]
                artifactl.append(where((hl>3.64e-7)&(hl<3.65e-7)))
                artifactl.append(where((hl>3.41e-7)&(hl<3.43e-7)))
                # neb spectrum only matters for ages < 20Myr
                nebs=zeros(hs.shape)
                for j in range(ht.shape[0]):
                    if ht[j]<20e6:
                        hs[j,:]= \
                            log10(10.**hs[j,:]-
                                  10.**vecops.interpolate_onto(logl,neb[2][j,:],
                                                               loghl,False))
                        # there is an artefact at the Balmer break due to
                        # the interpolation. There are no features at this wavelength for these young stars, so we just replace the spectrum there
                        # with a straight interpolation
                        #pdb.set_trace()
                        for a in artifactl:
                            hs[j,a]= \
                            vecops.interpolate_onto(array([loghl[a[0][0]-1],
                                                     loghl[a[0][-1]+1]]), 
                                                    array([hs[j,a[0][0]-1],
                                                           hs[j,a[0][-1]+1]]),
                                                    loghl[a], False)
                            #print hs[j,a]                 
        else:
            ht,hl,hs = None,None,None
        if f[2]!=None:
            y=loadyields_sb99(f[2],nt)
        else:
            y=ones_like(nt)
        ml,ms=merge_hilo(nl,ns,hl,hs)
        spectra.append( (nt,ms,y) )
        #spectra.append( (nt,ns) )

    #pdb.set_trace()
    times,seds,massloss = stack_times(spectra)

    return times,ml,seds,massloss
    #return times,nl,seds

    
def save_fits(file,times,lambdas,seds, massloss, keywords, logflux=True):
    """Saves the stellar model data (for a specific metallicity) to
    the specified FITS file with the correct format. times and lambdas
    are 1D arrays. Seds is the SED array as (time, wavelength).
    Massloss gives the mass loss as the current mass as a function of
    time in relation to the ZAMS mass."""

    if logflux:
        keywords.append( ('logflux',True,'flux in log units') )
        llambdacol='log L_lambda'
    else:
        keywords.append( ('logflux',False,'flux in normal units') )
        llambdacol='L_lambda'
        
    hdulist=[pyfits.PrimaryHDU()]
    hdulist[0].header.update('FILETYPE','STELLARMODEL')

    # create parameter hdu
    if len(keywords)>0:
        smhdu=pyfits.new_table([pyfits.Column(name='junk',format='E')])
        smhdu.name='STELLARMODEL'
        for k in keywords:
            smhdu.header.update(k[0] if len(k[0])<=8 else 'HIERARCH '+k[0],
                                k[1],k[2])
        hdulist.append(smhdu)

    # create columns
    sedcol=pyfits.Column(name=llambdacol, format='%dE'%seds.shape[1],
                         unit='W/m', array=seds)
    timecol=pyfits.Column(name='time',format='D', unit='yr', array=times)
    mlcol=pyfits.Column(name='current_mass',format='D', unit='Msun',
                        array=massloss)
    cols=pyfits.ColDefs([sedcol,timecol,mlcol])
    sedhdu=pyfits.new_table(cols)
    sedhdu.name='SED'
    
    lambdacol=pyfits.Column(name='lambda',format='D', unit='m', array=lambdas)
    cols=pyfits.ColDefs([lambdacol])
    lamhdu=pyfits.new_table(cols)
    lamhdu.name='LAMBDA'
    hdulist+=[sedhdu, lamhdu]

    hdulist=pyfits.HDUList(hdulist)
    print "Writing FITS file %s"%file
    hdulist.writeto(file)

def makeallpadova_hires():
    """Runs through and creates all hires files. Since the hires files
    are only stellar continuum, only the stellar continuum is used for
    the lores parts, too."""
    infiles=[
        [('Patrik-imfKroupa-0.02Zsolar-young.spectrum1',
          'Patrik-imfKroupa-0.02Zsolar-young.hires1',
          'Patrik-imfKroupa-0.02Zsolar-young.yield1'),
         ('Patrik-imfKroupa-0.02Zsolar-old.spectrum1',
          'Patrik-imfKroupa-0.02Zsolar-old.hires1',
          'Patrik-imfKroupa-0.02Zsolar-old.yield1'),
         ('Patrik-imfKroupa-0.02Zsolar-veryold.spectrum1',
          'Patrik-imfKroupa-0.02Zsolar-veryold.hires1',
          'Patrik-imfKroupa-0.02Zsolar-veryold.yield1')],

        [('Patrik-imfKroupa-0.2Zsolar-young.spectrum1',
          'Patrik-imfKroupa-0.2Zsolar-young.hires1',
          'Patrik-imfKroupa-0.2Zsolar-young.yield1'),
         ('Patrik-imfKroupa-0.2Zsolar-old.spectrum1',
          'Patrik-imfKroupa-0.2Zsolar-old.hires1',
          'Patrik-imfKroupa-0.2Zsolar-old.yield1'),
         ('Patrik-imfKroupa-0.2Zsolar-veryold.spectrum1',
          'Patrik-imfKroupa-0.2Zsolar-veryold.hires1',
          'Patrik-imfKroupa-0.2Zsolar-veryold.yield1')],

        [('Patrik-imfKroupa-0.4Zsolar-young.spectrum1',
          'Patrik-imfKroupa-0.4Zsolar-young.hires1',
          'Patrik-imfKroupa-0.4Zsolar-young.yield1'),
         ('Patrik-imfKroupa-0.4Zsolar-old.spectrum1',
          'Patrik-imfKroupa-0.4Zsolar-old.hires1',
          'Patrik-imfKroupa-0.4Zsolar-old.yield1'),
         ('Patrik-imfKroupa-0.4Zsolar-veryold.spectrum1',
          'Patrik-imfKroupa-0.4Zsolar-veryold.hires1',
          'Patrik-imfKroupa-0.4Zsolar-veryold.yield1')],

        [('Patrik-imfKroupa-Zsolar-young.spectrum1',
          'Patrik-imfKroupa-Zsolar-young.hires1',
          'Patrik-imfKroupa-Zsolar-young.yield1'),
         ('Patrik-imfKroupa-Zsolar-old.spectrum1',
          'Patrik-imfKroupa-Zsolar-old.hires1',
          'Patrik-imfKroupa-Zsolar-old.yield1'),
         ('Patrik-imfKroupa-Zsolar-veryold.spectrum1',
          'Patrik-imfKroupa-Zsolar-veryold.hires1',
          'Patrik-imfKroupa-Zsolar-veryold.yield1')],

        [('Patrik-imfKroupa-2.5Zsolar-young.spectrum1',
          'Patrik-imfKroupa-2.5Zsolar-young.hires1',
          'Patrik-imfKroupa-2.5Zsolar-young.yield1'),
         ('Patrik-imfKroupa-2.5Zsolar-old.spectrum1',
          'Patrik-imfKroupa-2.5Zsolar-old.hires1',
          'Patrik-imfKroupa-2.5Zsolar-old.yield1'),
         ('Patrik-imfKroupa-2.5Zsolar-veryold.spectrum1',
          'Patrik-imfKroupa-2.5Zsolar-veryold.hires1',
          'Patrik-imfKroupa-2.5Zsolar-veryold.yield1')]
        ]

    outfiles=['hires-0.02.fits', 'hires-0.2.fits', 'hires-0.4.fits',
              'hires-1.0.fits','hires-2.5.fits']
    keywords=[
        [ ('track_metallicity',0.02*0.02,'0.04*Solar') ],
        [ ('track_metallicity',0.02*0.2,'0.2*Solar') ],
        [ ('track_metallicity',0.02*0.4,'0.4*Solar') ],
        [ ('track_metallicity',0.02*1.0,'1.0*Solar') ],
        [ ('track_metallicity',0.02*2.5,'2.5*Solar') ]
        ]
    common_keywords=[ ('powerunit','W',''),
                      ('lengthunit','m',''),
                      ('timeunit','yr',''),
                      ('massunit','Msun',''),
                      ('use_yields',True,'mass loss included'),
                      ('refmass',1.0,'[Msun] normalized to 1Msun'),
                      ('stellarmodel','Starburst99','Starburst99 stellar model')
                      ]
        
    for i,o,kw in zip(infiles, outfiles,keywords):
        t,l,s,ml=loadall(i, loadfunc_lores=lambda f: loadfile_sb99(f, sedcol=3),
                         loadfunc_hires=loadfile_sb99, 
                         loadfunc_nebular=lambda f: loadfile_sb99(f, sedcol=4))
        save_fits(o,t,l,s,ml,kw+common_keywords)
    

def makeallgeneva_lores():
    infiles=[
        [('Patrik-imfKroupa-geneva-0.05Zsolar-20myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.05Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.05Zsolar-200myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.05Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.05Zsolar-14Gyr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.05Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-0.2Zsolar-20myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.2Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.2Zsolar-200myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.2Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-0.4Zsolar-20myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.4Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.4Zsolar-200myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.4Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.4Zsolar-14Gyr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-0.4Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-Zsolar-20myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-Zsolar-200myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-Zsolar-14Gyr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-2Zsolar-20myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-2Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-2Zsolar-200myr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-2Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-2Zsolar-14Gyr.spectrum1',
          None,
          'Patrik-imfKroupa-geneva-2Zsolar-14Gyr.yield1')]
        ]

    outfiles=[
        'Patrik-imfKroupa-geneva-0.05Zsolar.fits',
        'Patrik-imfKroupa-geneva-0.2Zsolar.fits',
        'Patrik-imfKroupa-geneva-0.4Zsolar.fits',
        'Patrik-imfKroupa-geneva-Zsolar.fits',
        'Patrik-imfKroupa-geneva-2Zsolar.fits'
        ]

    makeallgeneva(infiles, outfiles)

def makeallgeneva_hires():
    infiles=[
        [('Patrik-imfKroupa-geneva-0.05Zsolar-20myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.05Zsolar-20myr.hires1',
          'Patrik-imfKroupa-geneva-0.05Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.05Zsolar-200myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.05Zsolar-200myr.hires1',
          'Patrik-imfKroupa-geneva-0.05Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.05Zsolar-14Gyr.spectrum1',
          'Patrik-imfKroupa-geneva-0.05Zsolar-14Gyr.hires1',
          'Patrik-imfKroupa-geneva-0.05Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-0.2Zsolar-20myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.2Zsolar-20myr.hires-interpolated',
          'Patrik-imfKroupa-geneva-0.2Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.2Zsolar-200myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.2Zsolar-200myr.hires-interpolated',
          'Patrik-imfKroupa-geneva-0.2Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.spectrum1',
          'Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.hires-interpolated',
          'Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-0.4Zsolar-20myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.4Zsolar-20myr.hires1',
          'Patrik-imfKroupa-geneva-0.4Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.4Zsolar-200myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.4Zsolar-200myr.hires1',
          'Patrik-imfKroupa-geneva-0.4Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.4Zsolar-14Gyr.spectrum1',
          'Patrik-imfKroupa-geneva-0.4Zsolar-14Gyr.hires1',
          'Patrik-imfKroupa-geneva-0.4Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-Zsolar-20myr.spectrum1',
          'Patrik-imfKroupa-geneva-Zsolar-20myr.hires1',
          'Patrik-imfKroupa-geneva-Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-Zsolar-200myr.spectrum1',
          'Patrik-imfKroupa-geneva-Zsolar-200myr.hires1',
          'Patrik-imfKroupa-geneva-Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-Zsolar-14Gyr.spectrum1',
          'Patrik-imfKroupa-geneva-Zsolar-14Gyr.hires1',
          'Patrik-imfKroupa-geneva-Zsolar-14Gyr.yield1')],

        [('Patrik-imfKroupa-geneva-2Zsolar-20myr.spectrum1',
          'Patrik-imfKroupa-geneva-2Zsolar-20myr.hires1',
          'Patrik-imfKroupa-geneva-2Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-2Zsolar-200myr.spectrum1',
          'Patrik-imfKroupa-geneva-2Zsolar-200myr.hires1',
          'Patrik-imfKroupa-geneva-2Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-2Zsolar-14Gyr.spectrum1',
          'Patrik-imfKroupa-geneva-2Zsolar-14Gyr.hires1',
          'Patrik-imfKroupa-geneva-2Zsolar-14Gyr.yield1')]
        ]

    outfiles=[
        'Patrik-imfKroupa-geneva-0.05Zsolar-hires.fits',
        'Patrik-imfKroupa-geneva-0.2Zsolar-hires.fits',
        'Patrik-imfKroupa-geneva-0.4Zsolar-hires.fits',
        'Patrik-imfKroupa-geneva-Zsolar-hires.fits',
        'Patrik-imfKroupa-geneva-2Zsolar-hires.fits'
        ]

    makeallgeneva(infiles, outfiles)

def test():
    infiles=[
        [('Patrik-imfKroupa-geneva-0.2Zsolar-20myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.2Zsolar-20myr.hires-interpolated',
          'Patrik-imfKroupa-geneva-0.2Zsolar-20myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.2Zsolar-200myr.spectrum1',
          'Patrik-imfKroupa-geneva-0.2Zsolar-200myr.hires-interpolated',
          'Patrik-imfKroupa-geneva-0.2Zsolar-200myr.yield1'),
         ('Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.spectrum1',
          'Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.hires-interpolated',
          'Patrik-imfKroupa-geneva-0.2Zsolar-14Gyr.yield1')]
        ]

    outfiles=[
        'Patrik-imfKroupa-geneva-0.2Zsolar-hires.fits'
        ]

    makeallgeneva(infiles, outfiles)

def makeallgeneva(infiles, outfiles):
    """Runs through and creates all the files using geneva
    tracks."""
    keywords=[
        [ ('track_metallicity',0.02*0.05,'0.05*Solar'),
          ('hires_metallicity',0.02*0.05,'Z of hires spectrum'),
          ('UV_line_metallicity','LMC/SMC','') ],
        [ ('track_metallicity',0.02*0.2,'0.2*Solar'),
          ('hires_metallicity',0.02*0.2,'Z of hires spectrum'),
          ('UV_line_metallicity','LMC/SMC','') ],
        [ ('track_metallicity',0.02*0.4,'0.4*Solar'),
          ('hires_metallicity',0.02*0.4,'Z of hires spectrum'),
          ('UV_line_metallicity','LMC/SMC','') ],
        [ ('track_metallicity',0.02*1.0,'1.0*Solar'),
          ('hires_metallicity',0.02*1.0,'Z of hires spectrum'),
          ('UV_line_metallicity','solar','') ],
        [ ('track_metallicity',0.02*2.0,'2.0*Solar'),
          ('hires_metallicity',0.02*2.0,'Z of hires spectrum'),
          ('UV_line_metallicity','solar','') ],
        ]
    common_keywords=[ 
                      ('powerunit','W',''),
                      ('lengthunit','m',''),
                      ('timeunit','yr',''),
                      ('massunit','Msun',''),
                      ('use_yields',True,'mass loss included'),
                      ('refmass',1.0,'[Msun] normalized to 1Msun'),
                      ('stellarmodel','Starburst99','Starburst99 stellar model'),
                      ('model_designation','Patrik-imfKroupa-geneva',''),
                      ('simulation_date','2009/12/10',''),
                      ('Starburst99_version','5.1',''),
                      ('total_stellar_mass','1.0e6','[Msun]'),
                      ('IMF_exponent','1.3,.3','[] Kroupa IMF'),
                      ('IMF_upper_limit',100,'[Msun]'),
                      ('IMF_lower_limit',.1,'[Msun]'),
                      ('IMF_mass_boundaries','0.1,0.5,100.0','[Msun] Kroupa IMF'),
                      ('supernova_cutoff',8,'[Msun]'),
                      ('black_hole_cutoff',120,'[Msun]'),
                      ('tracks','Geneva high',''),
                      ('wind_model','evolution',''),
                      ('mass_interpolation','full isochrone',''),
                      ('track_selection','all',''),
                      ('atmosphere','Pauldrach/Hillier',''),
                      ('RSG_microturb_velocity',3,'[km/s]'),
                      ('RSG_abundance_ratio','solar','')
                      ]
        
    for i,o,kw in zip(infiles, outfiles,keywords):
        t,l,s,ml=loadall(i, loadfunc_lores=lambda f: loadfile_sb99(f, sedcol=3),
                         loadfunc_hires=loadfile_sb99, 
                         loadfunc_nebular=lambda f: loadfile_sb99(f, sedcol=4))
        save_fits(o,t,l,s,ml,kw+common_keywords, logflux=True)

def makeallpadova():
    """Runs through and creates all the files using padova (our original)
    tracks."""
    infiles=[
        [('Patrik-imfKroupa-Zsolar-young.spectrum1',
          None,
          'Patrik-imfKroupa-Zsolar-young.yield1'),
         ('Patrik-imfKroupa-Zsolar-old.spectrum1',
          None,
          'Patrik-imfKroupa-Zsolar-old.yield1')]
        ]

    outfiles=['Patrik-imfKroupa-padova-Zsolar.fits']

    keywords=[
        [ ('track_metallicity',0.02*1.0,'1.0*Solar'),
          ('hires_metallicity',0.02*1.0,'Z of hires spectrum'),
          ('UV_line_metallicity','LMC/SMC','')
          ]
        ]
    common_keywords=[ 
                      ('powerunit','W',''),
                      ('lengthunit','m',''),
                      ('timeunit','yr',''),
                      ('massunit','Msun',''),
                      ('use_yields',True,'mass loss included'),
                      ('refmass',1.0,'[Msun] normalized to 1Msun'),
                      ('stellarmodel','Starburst99','Starburst99 stellar model'),
                      ('model_designation','Patrik-imfKroupa-geneva',''),
                      ('simulation_date','2009/12/10',''),
                      ('Starburst99_version','5.1',''),
                      ('total_stellar_mass','1.0e6','[Msun]'),
                      ('IMF_exponent','1.3,.3','[] Kroupa IMF'),
                      ('IMF_upper_limit',100,'[Msun]'),
                      ('IMF_lower_limit',.1,'[Msun]'),
                      ('IMF_mass_boundaries','0.1,0.5,100.0','[Msun] Kroupa IMF'),
                      ('supernova_cutoff',8,'[Msun]'),
                      ('black_hole_cutoff',120,'[Msun]'),
                      ('tracks','Padova AGB',''),
                      ('wind_model','evolution',''),
                      ('mass_interpolation','full isochrone',''),
                      ('track_selection','all',''),
                      ('atmosphere','Pauldrach/Hillier',''),
                      ('RSG_microturb_velocity',3,'[km/s]'),
                      ('RSG_abundance_ratio','solar','')
                      ]
        
    for i,o,kw in zip(infiles, outfiles,keywords):
        t,l,s,ml=loadall(i, loadfunc_lores=lambda f: loadfile_sb99(f, sedcol=3),
                         loadfunc_hires=loadfile_sb99, 
                         loadfunc_nebular=lambda f: loadfile_sb99(f, sedcol=4))
        save_fits(o,t,l,s,ml,kw+common_keywords, logflux=True)
    

def makeall_maraston():
    infiles=[
        [ ('sed.krz10m4.rhb', None, None) ],

        [ ('sed.krz0001.rhb', None, None) ],

a        [ ('sed.krz001.rhb', None, None) ],

        [ ('sed.krz002.rhb', None, None) ],

        [ ('sed.krz004.rhb', None, None) ],

        [ ('sed.krz007.rhb', None, None) ]
        ]

    outfiles=['maraston-0.005.fits', 'maraston-0.05.fits', 'maraston-0.5.fits',
              'maraston-1.0.fits','maraston-2.0.fits', 'maraston-3.5.fits']
    keywords=[
        [ ('track_metallicity',1e-4,'0.005*Solar') ],
        [ ('track_metallicity',1e-3,'0.05*Solar') ],
        [ ('track_metallicity',1e-2,'0.5*Solar') ],
        [ ('track_metallicity',2e-2,'1.0*Solar') ],
        [ ('track_metallicity',4e-2,'2.0*Solar') ],
        [ ('track_metallicity',7e-2,'3.5*Solar') ],
        ]
    common_keywords=[
                      ('powerunit','W',''),
                      ('lengthunit','m',''),
                      ('timeunit','yr',''),
                      ('massunit','Msun',''),
                      ('use_yields',True,'mass loss included'),
                      ('refmass',1.0,'[Msun] normalized to 1Msun'),
                      ('stellarmodel','Maraston rhb','Maraston red hor. branch stellar model')
                      ]
        
    for i,o,kw in zip(infiles, outfiles,keywords):
        t,l,s,ml=loadall(i,loadfunc=loadfile_Maraston)
        save_fits(o,t,l,s,ml,kw+common_keywords, logflux=True)
    

def subsample_mappings(mappings_file,factor):
    """Subsamples the SEDs in the mappings file by the specified factor. Note that it MODIFIES the mappings_file, so be sure to use it on a copy."""
    f=pyfits.open(mappings_file,mode='update')
    d=f[2].data
    l=f[1].data.field('wave')

    # the seds are in nu units and are in nu order. change to lambda
    # units, change to lambda order, and transpose array to make
    # broadcasting easier
    l=l[::-1]
    d=d[::-1]
    d=d.transpose(1,2,3,4,0)
    d=d/l**2
    
    dl=vecops.delta_quantity(l)

    # subsample wavelengths
    nl_use=floor(l.shape[0]/factor)*factor
    ll=l[:nl_use:factor].copy()
    for i in range(1,factor):
        ll+=l[i:nl_use:factor]
    ll/=factor
    
    dll=vecops.delta_quantity(ll)

    # now subsample SEDs
    dd=d[:,:,:,:,:nl_use:factor]*dl[:nl_use:factor]
    for i in range(1,factor):
        dd+=d[:,:,:,:,i:nl_use:factor]*dl[i:nl_use:factor]
    dd/=dll
    
    # now we need to undo the shuffling we did before writing it back
    # to the file
    dd=dd*ll**2
    dd=dd.transpose(4,0,1,2,3)
    dd=dd[::-1]
    ll=ll[::-1]

    f[2].data = dd
    f[1].data.field('wave')[:] = 0
    f[1].data.field('wave')[:ll.shape[0]] = ll
    f.close()
    
    
def resample_hires(file, outfile):
    """Resamples the hires SED grid so the hires part is on a strictly logarithmic wavelength sampling."""

    print "Resampling file %s"%file

    f=pyfits.open(file, mode='update')

    l=f['lambda'].data.field('lambda')
    # new wavelength grid
    ll=hstack([l[:331],logspace(log10(3e-7),log10(7e-7),13300),l[13654:]])

    logl=log10(l)
    logll=log10(ll)

    s=f['SED'].data.field('log L_lambda')
    
    ss=zeros((s.shape[0],ll.shape[0]))

    for t in range(s.shape[0]):
        print "Processing t=%d"%(t)
        ss[t,:] = vecops.interpolate_onto(logl,s[t,:],logll,False)

    print "Saving to file %s"%outfile
    
    # now we use the save_fits function to write a new output file
    save_fits(outfile, f['sed'].data.field('time'),
              ll, ss,  f['sed'].data.field('current_mass'), {})

    pyfits.append(outfile, f['stellarmodel'].data, f['stellarmodel'].header)

    
    
def convert_fsps(file, outfile, modelkeyword):
    """Driver function to convert Charlie's FSPS files to Sunrise files."""
    infiles=[
        [ (file, None, None) ] ]
    
    outfiles=[outfile]

    keywords=[ [ ('track_metallicity',2e-2,'1.0*Solar') ] ]
    
    common_keywords=[ ('powerunit','W',''),
                      ('lengthunit','m',''),
                      ('timeunit','yr',''),
                      ('massunit','Msun',''),
                      ('use_yields',False,'mass loss not included'),
                      ('refmass',1.0,'[Msun] normalized to 1Msun'),
                      ('stellarmodel',modelkeyword[0], modelkeyword[1])
                      ]

    try:
        os.remove(outfile)
    except:
        pass
    
    for i,o,kw in zip(infiles, outfiles,keywords):
        t,l,s,ml=loadall(i,loadfunc_lores=loadfile_fsps,
                         loadfunc_hires=loadfile_fsps)
        save_fits(o,t,l,s,ml,kw+common_keywords, logflux=False)
    
def makeall_fsps_CaT():
    convert_fsps('SSP_Padova_CaT_Salpeter_Zsol.out.spec',
                 'FSPS_CaT_Salpeter_Zsun.fits',
                 ('FSPS_CaT_Salpeter','FSPS Hires Model of CaT'))

def makeall_fsps_CO():
    convert_fsps('SSP_Padova_CO_Salpeter_Zsol.out.spec',
                 'FSPS_CO_Salpeter_Zsun.fits',
                 ('FSPS_CO_Salpeter','FSPS Hires Model of CO bandhead'))
