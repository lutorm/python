import pyfits
from numpy import *

"""Routines to read the grain crossections by Laor & Draine and
convert to FITS."""

def read_chunk (file):
    s=[]
    while s==[]:
        s=file.readline().split()
    size = float(s[0])
    print "Reading size",size
    s=file.readline().split()
    Qabs_col = [i for i in range(len(s)) if s[i]=='Q_abs'][0]
    Qsca_col = [i for i in range(len(s)) if s[i]=='Q_sca'][0]
    g_col = [i for i in range(len(s)) if s[i]=='g=<cos>'][0]
    print "Qabs column is ",Qabs_col,"Qsca",Qsca_col,"g",g_col
    lam=[]
    Q_abs =[]
    Q_sca = []
    g = []
    l=file.readline().split()
    while l!=[]:
        lam.append(float(l[0])*1e-6) # convert wavelength to m
        Q_abs.append(float(l[Qabs_col]))
        Q_sca.append(float(l[Qsca_col]))
        g.append(float(l[g_col]))
        l=file.readline().split()
    return size,lam, Q_abs, Q_sca, g

def read_file (input_file):
    f=open(input_file,'r')
    f.readline()
    type=f.readline().strip()
    print "Reading data for",type
    s=f.readline()
    while s.find('NRAD') == -1:
        s=f.readline()
    nsize = int(s.split()[0])
    nlambda = int(f.readline().split()[0])
    print nsize,"sizes",nlambda,"wavelengths"
    sigma_a = array(type=Float64,shape=(nlambda,nsize))
    sigma_s = array(type=Float64,shape=(nlambda,nsize))
    g = array(type=Float64,shape=(nlambda,nsize))
    lam = []
    sizes=[]
    f.readline()
    for i in range(nsize):
        size,l, Qa, Qs, gg = read_chunk(f)
        size=size*1e-6 # convert from micron to meter
        sigma_a [:,i] = array(Qa)*pi*size*size #  convert from Q to sigma
        sigma_s [:,i] = array(Qs)*pi*size*size #  convert from Q to sigma
        g [:,i] = array (gg)
        if lam==[]:
            lam=l
        else:
            assert(lam==l)
        sizes.append(size)
        
    return type,lam,sizes,sigma_a, sigma_s, g

def save_file(output_file, type, input_file, lam, sizes, sigma_a, sigma_s, g):
    phdu = pyfits.PrimaryHDU()
    phdu.header.update('species', type, 'Grain description')
    phdu.header.update('file', input_file, 'Name of original data file')

    #  make image hdus for sigma_a, sigma_s and g
    ahdu = pyfits.ImageHDU(sigma_a,name='sigma_abs')
    ahdu.header.update('unit', 'm^2','Unit of cross section')
    shdu = pyfits.ImageHDU(sigma_s,name='sigma_sca')
    shdu.header.update('unit', 'm^2','Unit of cross section')
    ghdu = pyfits.ImageHDU(g,name='g')
    ghdu.header.update('unit', 'm^2','Unit of cross section')

    # columns to table ext
    c1=pyfits.Column(name='size',unit='m',format='D',array=sizes)
    c2=pyfits.Column(name='wavelength',unit='m',format='D',array=lam)
    cols=pyfits.ColDefs([c1, c2])
    tbhdu=pyfits.new_table(cols)
    tbhdu.header.update('EXTNAME','AXES')
    hdulist=pyfits.HDUList([phdu, ahdu, shdu, ghdu, tbhdu])
    hdulist.writeto(output_file)
    
def write_baes_crosssection_file(input_file, output_file):
    # we pretend this is a 0.12um grain but it's really a model
    # average. those files are also defined to have decreasing
    # wavelengths so we have to flip the order. The cross section is
    # arbitrary so we multiply it by 1e-13 to make it more similar to
    # a real one so we don't get crazy values somewhere.
    f=loadtxt(input_file, skiprows=2)
    save_file(output_file, 'Baes et al BARE-GR-S model from Zubko et al.',
              input_file,
              f[::-1,0]*1e-6,array([0.12e-6]), (1-f[::-1,2:3])*f[::-1,1:2]*1e-14,
              f[::-1,2:3]*f[::-1,1:2]*1e-14, f[::-1,3:4])
    
def convert(input_file, output_file):
    type, lam, sizes, sigma_a, sigma_s, g = read_file (input_file)
    save_file(output_file, type, input_file, lam, sizes, sigma_a, sigma_s, g)
