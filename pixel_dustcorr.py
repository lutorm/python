import tempfile, commands, pyfits, os, pickle
from numarray import *
from pylab import *

imcopy="/usr/local/bin/imcopy"

def load_camera_cube(filename, hdu):
    "Loads the full data cube from the supplied file"

    # make temp file and copy the compressed image cube to that using
    # imcopy
    temp=tempfile.mktemp(".fits")
    cmd=imcopy+" "+filename+"["+hdu+"] "+temp
    (s,o)=commands.getstatusoutput(cmd)
    if s>0:
        raise RuntimeError,"Unable to convert compressed data cube: "+o
            
    # load the data cube
    tempf=pyfits.open(temp)
    sed=tempf[0].data.astype(Float64)
    os.remove(temp)

    return sed

def line_luminosity(lam, L_lambda, line_entry):
    """Calculates line luminosity from the supplied data cube or vector by 
    subtracting continuum.  Line_entry is the index of the line in the
    wavelength vector"""

    dlambda = 0.5*(lam [line_entry+1] - lam [line_entry- 1])
    if len(L_lambda.shape)==3:
        L_continuum = 0.5*(L_lambda [line_entry - 1, :, :] +
                           L_lambda [line_entry + 1, :, :])
        L_line = (L_lambda [line_entry, :, :] -L_continuum)*dlambda
        eq_width = L_line/L_continuum*1e10
        # deal with NaNs and Infs
        eq_width[L_continuum==0]=0
    elif len(L_lambda.shape)==1:
        # integrated spectrum
        L_continuum = 0.5*(L_lambda [line_entry - 1] +
                           L_lambda [line_entry + 1])
        L_line = (L_lambda [line_entry] -L_continuum)*dlambda
        eq_width = L_line/L_continuum*1e10
        
    return L_line, eq_width
    #return L_line

def load_lines(file , camera):
    """Loads an mcrx file and performs pixel by pixel dust correction
    to the star-formation rate"""
    
    # open file
    f = pyfits.open(file)

    # read lambda vector
    lambda_hdu=f["lambda"]
    lam=lambda_hdu.data.field('lambda')

    # read lambda entry vector in scattering_lambdas
    sl_hdu=f["scattering_lambdas"]
    entry=sl_hdu.data.field('entry')
    line=sl_hdu.data.field('line')
    # remember that the DATAx cubes are truncated
    #entry =entry-entry[1]

    # get pixel solid angle and area for conversion /sr to /area
    pixel_sr = f["CAMERA"+`camera`+"-SCATTER"].header["PIXEL_SR"]
    camdist = f["CAMERA"+`camera`+"-PARAMETERS"].header["cameradist"]
    
    # load datacubes
    cube = load_camera_cube (file, "CAMERA"+`camera`)
    # set unphysical negatives to zero?
    cube_ns = load_camera_cube (file, "CAMERA"+`camera`+"-NONSCATTER")

    # extract line luminosities and convert to physical luminosity
    lines=entry[line>0]
    sb_to_lum = pixel_sr*4*pi*(camdist*3.085678e19)**2
    halpha_lum, halpha_eqw = line_luminosity(lam[entry[1]:], cube,
                                 lines[1]-entry[1])
    halpha_lum = halpha_lum*sb_to_lum
    hbeta_lum, hbeta_eqw = line_luminosity(lam[entry[1]:], cube,
                                lines[0]-entry[1])
    hbeta_lum = hbeta_lum*sb_to_lum
    #print sum(sum(halpha_lum,axis=1))/sum(sum(hbeta_lum,axis=1))
    halpha_ns, halpha_ns_eqw = line_luminosity(lam, cube_ns, lines[1])
    halpha_ns = halpha_ns*sb_to_lum
    hbeta_ns, hbeta_ns_eqw = line_luminosity(lam, cube_ns, lines[0])
    hbeta_ns = hbeta_ns*sb_to_lum
    del cube
    del cube_ns

    return halpha_lum, halpha_eqw, hbeta_lum, hbeta_eqw, \
           halpha_ns, halpha_ns_eqw, hbeta_ns, hbeta_ns_eqw
           #0,0,0,0

def pixel_dustcorr(file, camera):

    halpha_lum, halpha_eqw, hbeta_lum, hbeta_eqw, halpha_ns, \
                halpha_ns_eqw, hbeta_ns, hbeta_ns_eqw = \
                load_lines(file, camera)

    # calculate line ratio and estimate absorption (in mags)
    r = halpha_lum/hbeta_lum

    # pixels with eqw_ha<20 are ignored because absorption infill
    # messes with intrinsic line ratios, making correction hard
    r[halpha_eqw<20]=0
    halpha_lum[halpha_eqw<20]=0
    halpha_lum[halpha_lum<0]=0
    #halpha_ns[halpha_eqw<20]=0
    #halpha_ns[halpha_lum<0]=0

    #AND if halpha lum < 1e31 we exclude the pixels otherwise we can
    # end up correcting noise to very high luminosities due to
    # extremely noisy and random line ratios
    r[halpha_lum<1e31]=0
    
    # furthermore, we do not attempt correction for pixels with line
    # ratios is smaller than intrinsic, this probably is due to
    # scattered light or noise.
    r[r<2.86]=2.86
    # finally, estimate ha absorption in magnitudes:
    # 2.5 is a fudge factor that makes it look better, should fit for
    # it
    magfudge=1.35
    aha = magfudge*1.086*1.96*log10(r/2.86)

    # open file
    f = pyfits.open(file)

    # read lambda vector
    lambda_hdu=f["lambda"]
    lam=lambda_hdu.data.field('lambda')

    # read lambda entry vector in scattering_lambdas
    sl_hdu=f["scattering_lambdas"]
    entry=sl_hdu.data.field('entry')
    line=sl_hdu.data.field('line')
    lines=entry[line>0]

    # also load actual SFR
    aux_hdu=f["CAMERA"+`camera`+"-AUX"]
    pixel_area = aux_hdu.header["CD1_1"]*aux_hdu.header["CD2_2"]
    sfr=aux_hdu.data[aux_hdu.header["SFRSLICE"]-1,:,:]*pixel_area

    # calculate integrated line ratio and correct using that
    # note that integrated spectra have full length... 
    integ = f["INTEGRATED_QUANTITIES"].data.field("L_lambda_scatter"+`camera`)
    integ_ns = f["INTEGRATED_QUANTITIES"].data.field("L_lambda_nonscatter"+`camera`)
    integ_ha = line_luminosity(lam, integ, lines[1])[0]
    integ_hb = line_luminosity(lam, integ, lines[0])[0]
    integ_ha_ns= line_luminosity(lam, integ_ns, lines[1])[0]
    print "integrated %e %e ratio %f"%( integ_ha/4e26,integ_hb/4e26,
                                  integ_ha/integ_hb)
    #integ = sum(sum(cube,axis=2),axis=1)
    #integ_ha = line_luminosity(lam[entry[1]:], integ, lines[1]-entry[1])*sb_to_lum
    #integ_hb = line_luminosity(lam[entry[1]:], integ, lines[0]-entry[1])*sb_to_lum
    #print "integrated %e %e ratio %f"%( integ_ha/4e26,integ_hb/4e26,
    #                              integ_ha/integ_hb)
    int_r = integ_ha/integ_hb
    if int_r<2.86:
        int_r=2.86
    int_aha = magfudge*1.086*1.96*log10(int_r/2.86)
    halpha_ns = integ_ha_ns
    
    # attempt to correct ha lum
    halpha_cor = halpha_lum*10**(0.4*aha)
    halpha_cor_int = halpha_lum*10**(0.4*int_aha)

    # convert to sfr using K98
    halpha_ns_sfr = halpha_ns*7.9e-35
    halpha_sfr = halpha_lum*7.9e-35
    halpha_cor_sfr = halpha_cor*7.9e-35
    
    print "line luminosities in L_sun"
    print "w/o dust: %e"%(halpha_ns/4e26)
    print "with dust: %e"%(sum(sum(halpha_lum,axis=1))/4e26)
    print "with dust, corrected: %e"%(sum(sum(halpha_cor,axis=1))/4e26)
    print "with dust, integrated corrected: %e"%(sum(sum(halpha_cor_int,axis=1))/4e26)
    #print "sfr"
    #print "w/o dust: ",sum(sum(halpha_ns_sfr,axis=1))
    #print "with dust: ",sum(sum(halpha_sfr,axis=1))
    #print "with dust, corrected: ",sum(sum(halpha_cor_sfr,axis=1))
    print "intrinsic: ",sum(sum(sfr,axis=1))

    return (halpha_ns/4e26,
            sum(sum(halpha_lum,axis=1))/4e26,
            sum(sum(halpha_cor,axis=1))/4e26,
            sum(sum(halpha_cor_int,axis=1))/4e26,
            sum(sum(sfr,axis=1)))
#return halpha_ns,halpha_lum,halpha_cor,sfr


def compare_dustcorr():
    f=pyfits.open("index.fits")
    snaps=f[1].data.field("snapshot_file")
    cams=f[1].data.field("camera")
    snaps=['snapshot_038' for x in range(11)]#,'snapshot_038']
    cams=[0,1,2,3,4,5,6,7,8,9,10]
    #n=cams.shape
    #print n
    int_cor=[]
    cor=[]
    uncor=[]
    sfr=[]
    for s,c in zip(snaps,cams):
        file=s.replace("snapshot","mcrx")+".fits"
        print file,c
        a=pixel_dustcorr(file,c)
        print a[1]/a[0],a[2]/a[0],a[3]/a[0],a[4]
        uncor.append(a[1]/a[0])
        int_cor.append(a[3]/a[0])
        cor.append(a[2]/a[0])
        sfr.append(a[4])

    #clf()
    #figure(1)
    #plot(sfr,uncor,'.')
    #plot(sfr,int_cor,'r.')
    #plot(sfr,cor,'g.')
    
    return (sfr,uncor,cor,int_cor)

def runit():
    a=compare_dustcorr()
    f=open("pixelcorr2.dat","w")
    pickle.dump(a,f)
    f.close()
    
