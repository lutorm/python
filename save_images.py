#!/usr/bin/env python25

"""Functions for saving all wavelength slices of an image cube as
separate png files."""

import pyfits,time,os,sys
from numpy import *
import matplotlib
from pylab import *
matplotlib.use('Agg')

def animate(file):
    hot()
    print "loading ",file
    
    f=pyfits.open(file)
    lam=f['INTEGRATED_QUANTITIES'].data.field('lambda')
    ii= f['CAMERA0-STAR'].data + f['CAMERA0-IR'].data
    m=ii.max()
    #make lambda*Flambda
    i=where(ii<1e-5,1e-5,ii)*lam[:,newaxis,newaxis] 
    print "rendering"
    n=0
    for c in range(0,i.shape[0],10):
        clf()
        imshow(log10(i[c,:,:]),vmin=log10(1e-7),vmax=log10(m))
        text(10,10,'%3.4f um' % (lam[c]*1e6),color='white')
        print c,lam[c]
        savefig('cube/test'+`n`)
        try:
            os.remove('cube/test'+`n+1`+'.png')
        except:
            pass
        #os.symlink('test'+`n`+'.png','cube/test'+`n+1`+'.png')
        n+=1
    
    return ii

def animate2(file):
    hot()
    print "loading ",file
    
    f=pyfits.open(file)
    lam=f['INTEGRATED_QUANTITIES'].data.field('lambda')
    ii= f['CAMERA0-STAR'].data + f['CAMERA0-IR'].data

    print "rendering"
    for c in range(0,ii.shape[0],1):
        clf()
        m=ii[c,:,:].max()
        imshow(log10(ii[c,:,:]),vmin=log10(m)-4,vmax=log10(m),cmap=gray())
        text(10,10,'%3.4f um' % (lam[c]*1e6),color='white')
        outfilenm='cube/test'+ ('%3.3f' % (lam[c]*1e6)) +'.png'
        print c,lam[c],outfilenm
        savefig(outfilenm)
    
    return ii

#animate(sys.argv[1])
animate2(sys.argv[1])
