from numpy import array,zeros
import pdb

def extract(l,key, maxstages, maxthreads):
    """Extracts the data for the specified key as a
    maxstages*maxthreads array."""
    stage=0
    val=zeros(shape=(maxstages, maxthreads))

    for stage in range(maxstages+1):
        for thread in range(maxthreads+1):

            try:
                temp=[float(x.split()[3]) for x in l if key in x and "T%d S%d"%(thread,stage) in x]
            except:
                temp=[float(x.split()[-1]) for x in l if key in x and " S%d"%stage in x]
            if temp!=[]:
                val[stage, thread] = temp[0]
        
    return val

def get_data(file, maxstages, maxthreads):

    l=open(file).readlines()

    #qtys=["INS","CYC","Instr./c","stalled","LLC ref","LLC miss/","LLC miss rate"]
    qtys=["INS","CYC","Instr/cyc"]

    val=dict([ (q,extract(l,q, maxstages, maxthreads))  for q in qtys])
    # cyc=[ [extract(ll,"CYC",s) for ll in l] for s in range(stages)]
    # ic=[ [extract(ll,"Instr./c",s) for ll in l] for s in range(stages)]
    # stall=[ [extract(ll,"stalled",s) for ll in l] for s in range(stages)]
    # llcref=[ [extract(ll,"LLC ref",s) for ll in l] for s in range(stages)]
    # llcmpi=[ [extract(ll,"LLC miss/",s) for ll in l] for s in range(stages)]
    # llcmr=[ [extract(ll,"LLC miss rate",s) for ll in l] for s in range(stages)]

    #return ins,cyc,ic,stall,llcref,llcmpi,llcmr
    return val
    [plot(ones(x.size)*x.size,x,'.') for x in llcr0]
    

def total_cycles_for_thread(data,thread):
    """Returns the total number of cycles for the specified thread."""
    return data['CYC'][:,thread].sum()

def total_cycles_for_stage(data,stage):
    """Returns the total number of cycles for the specified thread."""
    return data['CYC'][stage,:].sum()

def get_cycles(data,thread,stage):
    """Returns the number of cycles for the specified thread/stage."""
    return data['CYC'][stage,thread]
