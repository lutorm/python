import h5py, pdb, sys, os
from numpy import *

def get_dataset(group,dataset, size=(0,), dtype=float32):
    try:
        return group[dataset]
    except:
        group.create_dataset(dataset, size, dtype=dtype)
        return group[dataset]
        


def add_backgroundgrid(file, outfile, n, boxsize, backgrounddensity=0):
    """Adds a background grid of specified dimensions to an hdf5
    snapshot file. Unlike the Arepo version of add_backgroundgrid,
    this simply generates a grid of N^3 and if there is not already a
    mesh point in a cell, adds one there. It also translates the
    origin in the snapshot to boxsize/2.

    The background density is in H/cm^3, for simplicity. 1H/cm^3
    corresponds to 2.5e7Msun/kpc^3 = 0.0025 in default code units."""

    print "Adding a %d^3 background grid in box with size %f to file %s"%(n,boxsize,file)

    dr=array((.5*boxsize,.5*boxsize,.5*boxsize))
    cellsize=array((boxsize,boxsize,boxsize),dtype=float32)/n
    grid=zeros(shape=(n,n,n), dtype=bool)
    
    f = h5py.File(file, 'r')
    of = h5py.File(outfile,'w')

    try:
        # copy the input file, except for Parttype0 which needs to be resized.
        of.copy(f['Header'], "Header")

        of.create_group('PartType0')
        for i in range(1,6):
            try:
                of.copy(f['PartType%d'%i], "PartType%d"%i)
                # translate positions
                of['PartType%d/Coordinates'%i][:,:] += dr
                print "Copying group PartType%d to output file"%i
            except:
                print "No group PartType%d - skipping"%i

        # loop over gas particles to find grid bins without particles
        print "Finding occupied bins"
        try:
            pos=f['PartType0']['Coordinates']
            nold = pos.shape[0]

            # this expression should produce the cell indices of all the particles
            ind = floor((pos[:,:]+dr)/cellsize).astype(int)
            
            if not (ind>=0).all() or not (ind<n).all():
                print "Error: particles outside of specified box size"
                sys.exit(1)

            for i in range(ind.shape[0]):    
                grid[tuple(ind[i,:])] = True;

        except:
            # if there are no gas particles in input file,
            nold=0
            pass
    
        nadd = grid[ grid==False ].size
        ngas = nold+nadd
        ntot_old = f['Header'].attrs['NumPart_Total'].sum()
        nptf = of['Header'].attrs['NumPart_ThisFile']
        nptf[0] = ngas
        of['Header'].attrs['NumPart_ThisFile'] = nptf
        of['Header'].attrs['NumPart_Total'] = nptf
        assert (f['Header'].attrs['NumPart_Total_HighWord'] == 0).all()

        print "Adding %d mesh points, new number is %d"%(nadd,ngas)


        # Add the new datasets to PartType0
        print "Copying old data to new file"
        opt0=of['PartType0']
        opt0.create_dataset('Coordinates', (ngas, 3), dtype=float32)
        opt0.create_dataset('InternalEnergy', (ngas,), dtype=float32)
        opt0.create_dataset('Metallicity', (ngas,), dtype=float32)
        opt0.create_dataset('ParticleIDs', (ngas,), dtype=int32)
        opt0.create_dataset('Velocities', (ngas, 3), dtype=float32)
        opt0.create_dataset('Masses', (ngas,), dtype=float32)

        # try to copy old file (will fail if it has no gas)
        try:
            pt0=f['PartType0']
            opt0['Coordinates'][:nold,:] = pt0['Coordinates'][:,:]+dr
            opt0['InternalEnergy'][:nold] = pt0['InternalEnergy'][:]
            opt0['Metallicity'][:nold] = pt0['Metallicity'][:]
            opt0['ParticleIDs'][:nold] = pt0['ParticleIDs'][:]
            opt0['Velocities'][:nold,:] = pt0['Velocities'][:,:]

            # we also need to add a dataset for mass even if it doesn't exist,
            # because now they will have nonequal mass
            try:
                opt0['Masses'][:nold] = pt0['Masses'][:]
            except:
                opt0['Masses'][:nold] = f['Header'].attrs['MassTable'][0]
            of['Header'].attrs['MassTable'][0]=0
        except KeyError:
            # this means the input file had no gas
            pass

        opt0['InternalEnergy'][nold:] = 0
        # Because the extrapolation scheme can lead to negative
        # metallicities if they are zero, we init the background particles
        # with very small, but nonzero metallicity
        opt0['Metallicity'][nold:] = 1e-10
        # add new IDs at the end
        opt0['ParticleIDs'][nold:] = arange(ntot_old+1, ntot_old+1+nadd)
        opt0['Velocities'][nold:,:] = 0

        # what do we do about masses in the empty region? I assume the
        # code will barf if it's zero density?
        backgroundmass = 0.0025*backgrounddensity*(1.0*boxsize/n)**3
        print "Mass of background particles: ",backgroundmass
        opt0['Masses'][nold:] = backgroundmass


        # finally add the positions of the grid points
        print "Setting positions of grid points"
        xp=tile((arange(0,n)[:,newaxis,newaxis]+0.5)*cellsize[0],(1,n,n))
        yp=tile((arange(0,n)[newaxis,:,newaxis]+0.5)*cellsize[1],(n,1,n))
        zp=tile((arange(0,n)[newaxis,newaxis,:]+0.5)*cellsize[2],(n,n,1))

        opt0['Coordinates'][nold:,0] = xp[ grid==False ].ravel()
        opt0['Coordinates'][nold:,1] = yp[ grid==False ].ravel()
        opt0['Coordinates'][nold:,2] = zp[ grid==False ].ravel()

    finally:
        f.close()
        of.close()


def make_uniform_gasbox(header_file, outfile, n, boxsize, mstar=1e-4,
                        density=1.0, pos=(0,0,0)):
    """Generates an Arepo snapshot with a uniform grid of gas and a
    single type 4 particle in the center. takes the header from
    header_file. This is for RT tests.
"""

    print "Making uniform gas box %d^3 snapshot with size %f to file %s"%(n,boxsize,file)

    # we do this by first copying a snapshot, replacing the particles
    # with a single star particle
    # and then calling add_backgroundgrid
    
    f = h5py.File(header_file, 'r')
    of = h5py.File("temp"+outfile,'w')

    try:
        # copy the input file header
        of.copy(f['Header'], "Header")

        of['Header'].attrs.modify('MassTable', zeros((6,)))
        np=zeros((6,))
        np[4]=1
        of['Header'].attrs.modify('NumPart_ThisFile',np)
        of['Header'].attrs.modify('NumPart_Total',np)
        of['Header'].attrs.modify('NumPart_Total_HighWord', zeros((6,)))

        # create PartType4 group
        p4=of.create_group("PartType4")

        p4.create_dataset('ParticleIDs', (1,), dtype=int32)
        p4['ParticleIDs'][:] = 0

        p4.create_dataset('Coordinates', (1, 3), dtype=float32)
        p4['Coordinates'][0,:] = pos
        
        p4.create_dataset('Velocities', (1, 3), dtype=float32)
        p4['Velocities'][:,:] = 0
        
        p4.create_dataset('Masses', (1,), dtype=float32)
        p4['Masses'][:] = mstar

        p4.create_dataset('Metallicity', (1, 3), dtype=float32)
        p4['Metallicity'][:] = 0.02
        
        p4.create_dataset('StellarFormationTime', (1,), dtype=float32)
        p4['ParticleIDs'][:] = 0

    finally:
        f.close()
        of.close()

    add_backgroundgrid("temp"+outfile, outfile, n, boxsize, \
                       backgrounddensity=density)

    os.unlink("temp"+outfile)
    
