import snapshot
from numpy import *

def create(input, output, bhid):
    """Adds an extra particle to a snapshot to mimic a BH
    particle. It's added as a bulge particle because snapshot.py
    doesn't know about BH particles, but it works for loading initial
    conditions."""
    
    s=snapshot.GadgetSnapshot()
    f=open(input,'rb')
    s.read(f)
    f.close()

    nb = s.header.nParticles[snapshot.BULGE]+1
    ns = nb+s.header.nParticles[snapshot.DISK]
    s.header.nParticles[snapshot.BULGE]=nb
    s.header.nTotal[snapshot.BULGE]=nb
    
    # resize fields
    s.ms.resize([ns])
    s.rs.resize([ns,3])
    s.vs.resize([ns,3])
    s.sIdentity.resize([ns])
    s.sPotential.resize([ns])
    s.sType.resize([ns])

    # insert data
    s.ms[-1]=s.ms[-2]
    s.sType[-1]=snapshot.BULGE
    s.sIdentity[-1]=bhid

    # write
    of=open(output, 'wb')
    s.write(of)
    of.close()
