import crcmod,re,pdb,os,serial
import heating
import mqtt
from time import sleep

# This is the MQTT topic under which the data will be published
mqtt_topic = "homeassistant/sensor/hanport/"

# These are the channels we're interested in
channels = {
    "1-0:1.8.0": ("Energy used", "kWh","ENERGY","total_increasing"),
    "1-0:1.7.0": ("Power", "kW","POWER","measurement"),
    "1-0:21.7.0": ("Power_L1", "kW","POWER","measurement"),
    "1-0:41.7.0": ("Power_L2", "kW","POWER","measurement"),
    "1-0:61.7.0": ("Power_L3", "kW","POWER","measurement"),
    "1-0:32.7.0": ("Voltage_L1", "V","VOLTAGE","measurement"),
    "1-0:52.7.0": ("Voltage_L2", "V","VOLTAGE","measurement"),
    "1-0:72.7.0": ("Voltage_L3", "V","VOLTAGE","measurement"),
    "1-0:31.7.0": ("Current_L1", "A","CURRENT","measurement"),
    "1-0:51.7.0": ("Current_L2", "A","CURRENT","measurement"),
    "1-0:71.7.0": ("Current_L3", "A","CURRENT","measurement"),
}

def read_han(ser):
    "Read a Hanport message, check CRC, and return list of decoded key-value pairs."

    packet = []
    
    # Make sure we are at the start of the message
    ch = ser.read(1)
    if not ch:
        print ("No data from sensor.")
        return None
    while ch is not '/':
        ch = ser.read(1)

    # we have now read the /. Read until ! eom marker
    packet.append(ch)
    while ch is not '!':
        ch = ser.read(1)
        if not ch:
            # we timed out in the middle of the packet.
            # rather than looping forever looking for a
            # packet, bail.
            raise "Packet truncated"
        packet.append(ch)

    # Now read the crc
    crcstr = ser.readline()
    try:
        crc = int(crcstr, base=16)
    except:
        crc = 0
        
    return parse_han(''.join(packet), crc)

def parse_han(packet, crc):
    crc16 = crcmod.predefined.Crc('crc-16')
    for i in packet:
        crc16.update(i)
    if crc != crc16.crcValue:
        # CRC mismatch, return empty value
        print ("CRC mismatch in HAN packet")
        return None

    lines=packet.split('\r\n')[2:-1]
    data={}
    print ("\nReceived: ")
    print (lines)
    for l in lines:
        
        ll = re.split('[\(\)*]',l)
        #print ll[0]
        if ll[0] in channels:
            if ll[2] == channels[ll[0]][1]:
                data[ll[0]] = float(ll[1])
            else:
                print ((ll[0],float(ll[1]),"unit mismatch"))
    return data

def hanport_log():
    os.chdir('/home/patrik/heating')
    mqtt_channels={}
    device_map = {"identifiers": ["hanport"],
                  "name": "Hanport",
                  "manufacturer": "Patrik"}
    for k,v in channels.items():
        key = "ch"+k[4:].replace(".","_")
        mqtt_channels[key] = v
    mqtt.publish_config(mqtt_topic, mqtt_channels,
                        unique_id_prefix="hanport_",
                        device_map=device_map,
                        expire_after = 25)
    port="/home/patrik/xbee_0x13a200409eda5bL"
    han=serial.Serial(port, timeout=15, write_timeout=1)
    han.flushInput()
    
    for k in channels.keys():
        heating.create_database(k)
    
    while True:
        try:
            data = read_han(han)
        except Exception as e:
            print "Error reading sensor: %s"%(`e`)
            han.close()
            try:
                han = serial.Serial(port, timeout=15, write_timeout=1)
            except:
                print "Could not open port"
                sleep(15)
                pass
            continue
            
        if data:
            #print data
            heating.update_database(data)
            mqtt_data = {}
            for k,v in data.items():
                key = "ch"+k[4:].replace(".","_")
                mqtt_data[key] = v
            
            mqtt.publish_data(mqtt_topic, mqtt_data)

