# contains interface routines for SGE

import commands, re
import local_mcrxrun

# the ncpu keyword is "nodes" on a distr.mem. machine (eg jacquard)
# and "ncpus" on shared mem (eg. columbia) so this has to be a
# configurable

job_skeleton = """#!/bin/sh
#$ -V
#$ -cwd
#$ -N <jobname>
#$ -j y
#$ -o <outfile>
#$ -pe 1way <ncpus>
#$ -q <queue>
#$ -P <jobtype>
#$ -l h_rt=<limit_hours>
"""

release_command="qrls "
hold_command="qhold "
kill_command="qdel "
use_seconds=True

# regexp for finding job number from qsub
submission_regexp=re.compile(r"Your job +(\S+) +\(\".*\"\) has been submitted")

def submit_job(job):
    "Submits a job and returns the job id."
    output = commands.getstatusoutput ("qsub " + job)
    jobid=submission_regexp.search(output[1])
    if jobid==None:
        # This means that the job did not submit properly
        print "Submission Failed: " +job+ ":"
        print output[1]

    #print output[1]
    return jobid.group(1)
    
def read_job_status(jobidfile):
    "Reads a job ID from the jobidfile and queries for status. Will throw an exception if the job doesn't exist"
    f = open (jobidfile, 'r')
    jobid = f.read ().splitlines () [0]
    cmd = "qstat | grep " + jobid
    output= commands.getstatusoutput (cmd)
    #print output
    
    if output[0]!=0:
        raise ValueError,"Job ID not found"

    status = output[1].splitlines ()[local_mcrxrun.job_status_line].split()[local_mcrxrun.job_status_column]
    #print "status is",status
    return "%-2s"%status

# status letters returned from pbs
status_queued="qw "
status_running="r "
status_notqueued=""
status_held="hr"

