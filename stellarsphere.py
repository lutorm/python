from numpy import *
from math import *
from snapshot import *

def draw_radius(rmin,rmax):
    return (3*random.uniform(0,1)*(rmax**3-rmin**3)+rmin**3)**(1./3)

def stellarsphere(filename, n,radius=(1e2,1e3),m=(9,10),age=(7,10),Z=0.02,size=1):
    """Makes a snapshot that is a sphere of stars. Used for the
    background in the Sbc202 movie."""

    s=emptyFullSnapshot()
    
    s.rs=zeros((n,3))
    s.vs=zeros((n,3))
    s.ms=zeros(n)
    s.nFormationTime=zeros(n)
    s.nMetallicity=ones(n)*Z
    s.nOriginalMass=zeros(n)
    s.nOriginalSmoothingLength=ones(n)*size
    s.nParentId=ones(n)*-1
    s.sIdentity=arange(n)
    s.sPotential=zeros(n)
    s.sType= zeros(n)
    
    for i in range(n):
         phi=random.uniform(0,2*pi)
         theta=acos(random.uniform(-1,1))

         s.rs[i,:] = array([cos(phi)*sin(theta),
                           sin(phi)*sin(theta),
                           cos(theta)])*draw_radius(radius[0],radius[1])
         s.nFormationTime[i] =-10**random.uniform(age[0],age[1])
         s.ms[i]=10**random.uniform(m[0],m[1])
         s.nOriginalMass[i]=s.ms[i]
         s.sType[i]=NEW
        
    s.header.nParticles[NEW]=n
    s.header.nTotal[NEW]=n

    s.write(filename)

    return s.rs

    
