import simulations_sql as sql
import patrik_sqlengine, mipsflux, numpy

sql.setup(patrik_sqlengine.metadata, transactional=False)

q=sql.ioq.filter_by(has_dust=True).filter('imageobjects.number>0').join(['camera','snapshot','simulation']).filter_by(runname='set3')

def dump_to_file(query,file):
    # iterate over query returns
    qtys=['camera.snapshot.simulation.gadgetrunname','camera.snapshot.simulation.runname','camera.snapshot.snapshot_file','camera.number','number','has_dust','sfr','l_bol','m_g','gas_metallicity()','m_stars','gini']
    for q in qtys:
        print >> file,q,
    for m in sorted(query.first().magnitudes,cmp=lambda l,r:l.filter.id<r.filter.id):
        print >> file, m.filter.name,
    print >> file
    
    for o in query:
        for q in qtys:
            print >> file, eval('o.%s'%q),
        for m in sorted(o.magnitudes,cmp=lambda l,r:l.filter.id<r.filter.id):
            print >> file, m.ab_mag,
        print >> file

def dump_magnitudes(query, file):
    """Dumps the magnitudes for a simulation to a file as an ascii table."""

    d=mipsflux.load_sql_fluxes(query)
    filters=d.magnitudes.keys()
    mags=numpy.zeros(shape=(len(filters),d.magnitudes[filters[0]].shape[0]))

    for i in range(len(filters)):
        mags[i,:]=d.magnitudes[filters[i]]

    title=['snapshot','camera']+filters

    f=open(file,'w')
    print >> f, "#",
    for t in title:
        print >> f,t,'\t',
    print >> f

    for i in range(mags.shape[1]):
        print >> f, d.metadata['snapshot'][i],'\t',d.metadata['camera'][i],'\t',
        for j in range(mags.shape[0]):
            print >> f,mags[j,i],'\t',
        print >> f
    f.close()
    
