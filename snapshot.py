# TODO -
# - handling of coordinate systems.  See note below.  Snapshots should
#   know how to go to body coordinates?  How to choose between
#   coordinate systems?
# - multiple files
# FIXME -- write should take a string

import os, stat, types, tempfile, shutil

from scipy import io
from numpy import *

import local

GAS=0; DARK=1; DISK=2; BULGE=3; NEW=4

class BracketAccessMixin:
    """Allow access to attributes via dictionary syntax."""
    def __getitem__(self, name):
        return getattr(self, name)

    def __setitem__(self, name, value):
        return setattr(self, name, value)

def uncan(file):
    """More convenient interactive syntax for pickle"""
    # If filename, should this read until all exhausted?
    if type(file) is str: f=open(file, 'rb')
    else: f=file    

    obj = cPickle.load(f)

    if type(file) is str: f.close()

    return obj

class GenericGadgetHeader (BracketAccessMixin):
    def bytesRead(self):
        """Return number of bytes you're supposed to have read,
        according to the Fortran pad words"""
        return self._bytesRead

    def bytesWritten(self):
        """Return number of bytes you're supposed to have written,
        according to the Fortran pad words"""
        return self._bytesWritten

    def bytesSwapped(self):
        return self._bytesSwapped
    
    def fields(self):
        """Return a list of names of fields that refer to header
        fields, rather than internal methods, etc."""
        return [field for field in dir(self)
                if not field.startswith('_') \
                   and not callable(self[field])]
    
class RawGadgetHeader (GenericGadgetHeader):    
    _sizeType = 'i'
    _byteType = 'B'
    _totalSize = 256
    _fields = [('nParticles',      6, 'i'),
               ('mass',            6, 'd'),
               ('time',            1, 'd'),
               ('redshift',        1, 'd'),
               ('sfr',             1, 'i'),
               ('feedbackTp',      1, 'i'),
               ('nTotal',          6, 'i'),
               ('cooling',         1, 'i'),
               ('nFiles',          1, 'i'),
               ('boxSize',         1, 'd'),
               ('omega0',          1, 'd'),
               ('omegaLambda',     1, 'd'),
               ('hubbleParam',     1, 'd'),
               ('multiphase',      1, 'i'),
               ('stellarAge',      1, 'i'),
               ('sfrHistogram',    1, 'i'),
               ('starGenerations', 1, 'i'),
               ('potential',       1, 'i'),
               ('metals',          1, 'i'),
               ('energyDetail',    1, 'i'),
               ('parentId',        1, 'i'),
               ('starOriginal',    1, 'i')]
    
    def _calcSize(cls):
        """Calculate number of bytes of the header that's actually
        used."""
        total=0
        for (name, size, type) in RawGadgetHeader._fields:
            total += array(0,type).itemsize*size
        return total

    def read(self, f):
        """Read a gadget header from file object f.  Fill the current
        object with the info from hte header.  Return the number of
        bytes read from the header.  The part of the disk file
        corresponding to the header is considered to be the header
        itself and both Fortran padding words (one before, one after)."""
        sizeType = RawGadgetHeader._sizeType
        byteType = RawGadgetHeader._byteType

        usedSize = self._calcSize()
        totalSize = io.fread(f,1,sizeType)[0]

        # Try to decide if we need to byteswap
        if   totalSize ==   256:
            self._bytesSwapped = False
        elif totalSize == 65536:
            totalSize = 256
            self._bytesSwapped = True
        else:
            raise RuntimeError, "Couldn't determine byte swapping" 

        # Set the number of bytes read
        self._bytesRead = totalSize + 2*array(0, sizeType).itemsize

        # Read each field of the header
        for (name, size, type) in RawGadgetHeader._fields:
            self[name] = io.fread(f, size, type, type, self._bytesSwapped)

        # Read bytes to get to end of header
        io.fread(f,totalSize-usedSize, byteType, byteType, self._bytesSwapped)

        # Read trailing pad word
        io.fread(f, 1, sizeType, sizeType, self._bytesSwapped)

    def write(self, f, size=None, endian=None):
        """Read a gadget header from file object f"""
        sizeType = RawGadgetHeader._sizeType
        byteType = RawGadgetHeader._byteType
        usedSize = self._calcSize()

        if size is None: size = RawGadgetHeader._totalSize

        # Set the number of bytes written
        self._bytesWritten = size + 2*array(0, sizeType).itemsize

        assert endian in [None, 'native', 'big', 'little', 'swap']
        if endian == 'big' and local.littleEndian: byteswap = True
        elif endian == 'little' and local.bigEndian: byteswap = True
        elif endian == 'swap': byteswap = not self.bytesSwapped()
        else: byteswap = False

        io.fwrite(f, 1, array([size], sizeType), sizeType, byteswap)
        for (name, fieldSize, type) in RawGadgetHeader._fields:
            io.fwrite(f, fieldSize, array(self[name], type), type, byteswap)

        io.fwrite(f,size-usedSize, zeros(size, byteType), byteType, byteswap)

        # Write trailing pad word
        io.fwrite(f, 1, array([size], sizeType), sizeType, byteswap)

class GadgetHeader (GenericGadgetHeader):
    _flags = ['sfr', 'feedbackTp', 'cooling', 'multiphase', 'stellarAge',
              'sfrHistogram', 'starGenerations', 'starOriginal', 'potential', 'metals',
              'energyDetail', 'parentId']
    _intBinaryType = int32
    _floatBinaryType = float64
    _boolBinaryType = bool
    _boolBinaryTrue = array([1], int32)
    _boolBinaryFalse = array([0], int32)

    def __init__(self, file=None):
        """If file is a string, open the file with that name, read it,
        and close it.  If file is a file object, read that file and
        leave it open.  If file is None, don't do anything to
        initialize"""
        if file and type(file) is types.StringType:
            f = open(file)
            self.read(f)
            f.close()
        elif file:
            self.read(file)

    def read(self, f):
        raw = RawGadgetHeader()
        raw.read(f)
        self._bytesRead = raw.bytesRead()
        self._bytesSwapped = raw.bytesSwapped()
        
        for name in raw.fields():
            # Arrays put into current object directly
            if not len(raw[name]) == 1:
                self[name] = raw[name]
            # Flags are put into current object as booleans
            elif name in GadgetHeader._flags:
                if raw[name][0] == 0: self[name] = False
                else: self[name] = True
            # Finally, scalars put into current object
            else:
                self[name] = raw[name][0]

    def write(self, f, *a, **kw):
        """Extra args passed to RawGadgetHeader.write()"""
        raw = RawGadgetHeader()
        raw._bytesSwapped = self._bytesSwapped
        
        for name in self.fields():
            value = self[name]

            floatType = GadgetHeader._floatBinaryType
            intType = GadgetHeader._intBinaryType
            boolType = GadgetHeader._boolBinaryType
            binaryTrue = GadgetHeader._boolBinaryTrue
            binaryFalse = GadgetHeader._boolBinaryFalse
            
            # Arrays put into current object directly
            if type(value) is ndarray:
                raw[name] = self[name]
            # Booleans to 4 byte integers set to zero or one
            elif type(value) in (bool, types.BooleanType):
                if value: raw[name] = binaryTrue
                else:     raw[name] = binaryFalse
            # Floats are 8 byte floats
            elif type(value) in (floatType, types.FloatType):
                raw[name] = array([value], float64)                
            # Integers are 4 byte ints

            # type(...) is for rather obscure bug where array(...,
            # 'i') and array(..., int32) make arrays where the
            # elements claim to be <type 'numpy.int32'> but they have
            # different object id's... so the type is getting
            # instantiated twice?
            # TODO -- low priority, functional workaround, report bug
            elif type(value) in (intType, types.IntType,
                                 type(array([0], 'i')[0])):
                raw[name] = array([value], intType)                
            else:
                raise RuntimeError, "Unknown type in header!"
            
        raw.write(f, *a, **kw)
        # Set the number of bytes written
        self._bytesWritten = raw.bytesWritten() 

class GenericGadgetSnapshot (BracketAccessMixin):
    def fields(self):
        """Return a list of names of fields that refer to header
        fields, rather than internal methods, etc."""
        return [field for field in dir(self)
                if not field.startswith('_') \
                   and not callable(self[field])]

class RawGadgetSnapshot (GenericGadgetSnapshot):
    _floatType = 'f'
    _intType = 'i'
    _padType = 'i'
    
    def __init__(self):
        self.header = GadgetHeader()

    def _printSnapshotInfo(self, name, base, size, padSize):
        print name, "pad start:", base
        print name, "start:", base + padSize
        print name, "data size:", size, "total size: ", size + 2*padSize
        print name, "end:", base + padSize + size - 1
        print name, "pad end:", base + 2*padSize + size - 1
            
    def read(self, f, verbose=False, snapshot=True, initialCondition=False,
             hackHeader=False):
        """ Read snapshot data straight from the file, doing nothing other than
        dividing it up into different types of quantities

        snapshot: Not sure what this is (TODO)
        verbose: If true, print out diagnostic messages"""

        # Short aliases for relevant types
        floatType = RawGadgetSnapshot._floatType
        intType = RawGadgetSnapshot._intType
        padType = RawGadgetSnapshot._padType

        def skip(f):
            """Skip Fortran pad word.  Free variable -- byteswap"""
            return io.fread(f,1,padType, padType, byteswap)[0]
            
        def skipInfo(f, name=''):
            """Skip Fortran pad word, recording info about it.  Add to
            bytesRead[0] the contents of word (presumably the number
            of bytes in the field) plus 2*sizeof(padWordType),
            yielding the number of bytes read from the file.  You can
            then check the number of bytes you think you read against
            the size of the file.

            Free variables
            verbose: print a diagnostic message if verbose is True
            bytesRead: record number of bytes you're supposed to have read.
            byteswap: Whether or not to swap bytes when reading"""
            bytes = io.fread(f,1,padType, padType, byteswap)[0]
            if verbose: 
                self._printSnapshotInfo(name, bytesRead[0], bytes, array(0, padType).itemsize)
            bytesRead[0] += bytes + 2*array(0, padType).itemsize
            return bytes

        def readArray(name, n, type=floatType, shape=None):
            """Read an array with Fortran pad words.  name is the
            destination of the read array as an attribute of self (the
            current object).  n is the number of elements to read.
            type is the type of the elements (default floatType).
            shape is the desired shape of the result.  If shape is
            none, the result will be a 1d array.

            Free variable: byteswap"""
            size1 = skipInfo(f, name)
            arr = io.fread(f, n, type, type, byteswap)
            if shape: arr = reshape(arr, shape)
            self[name] = arr
            size2 = skip(f)

            if not size1 == size2 == arr.nbytes:
                raise RuntimeError, "Problem with Fortran Pad Words!"

        def postlude():
            if verbose:
                print 'Total Bytes', bytesRead[0]

            # On Patrik's advice, try to read one more byte and flag an
            # error if it's not EOF
            before_seek = f.tell()
            f.seek(-1,1)   # move back one byte
            after_seek = f.tell()
            # make sure that the seek worked, that the last byte read is
            # valid, and that the next byte to read is invalid.
            if not (before_seek - after_seek == 1 and f.read(1) != '' and f.read(1) == ''):
                raise RuntimeError, "Snapshot read didn't end up at end of file!"

            if os.stat(f.name)[stat.ST_SIZE] != bytesRead[0]:
                raise RuntimeError, "File size doesn't match bytes written count."

        bytesRead=[0]      # Number of bytes you're supposed to have read.
                           # It's a list so that local functions can
                           # modify the first element.        
        h = self.header
        h.read(f)
        if verbose:
            self._printSnapshotInfo('header', bytesRead[0],
                                h.bytesRead() - 2*array(0, padType).itemsize,
                                array(0, padType).itemsize)
        bytesRead[0] += h.bytesRead()
        byteswap = h.bytesSwapped()
        if hackHeader:
            h.energyDetail = True
            h.potential = True

        if not (h.nFiles == 1 or h.nFiles == 0 and initialCondition):
            raise NotImplementedError

        if h.nParticles[5:].sum() != 0:
            raise RuntimeError, "Behavior differs from loadsnap.c, probably bug in loadsnap.c"

        nTotal = h.nParticles.sum()
        nGas = h.nParticles[GAS]
        nNew = h.nParticles[NEW]
        nWithMass = h.nParticles[ h.mass==0.0 ].sum()

        readArray('position', 3*nTotal, shape=(nTotal, 3))
        readArray('velocity', 3*nTotal, shape=(nTotal, 3))
        readArray('identity', nTotal, type=intType)

        ##############################
        # Mass arrays are a horrid special case
        # Here we read the arrays separately and then concatenate them
        # b/c of the conditional statement inside the loop
        if nWithMass > 0: massSize1 = skipInfo(f, 'mass')
        masses=[]
        massesBytesRead = 0
        for i in range(len(h.nParticles)):
            if h.mass[i] == 0:
                if h.nParticles[i] > 0:
                    masses.append(io.fread(f, h.nParticles[i], floatType, floatType, byteswap))
                    massesBytesRead += masses[-1].nbytes
            else:
                masses.append(h.mass[i]*ones(h.nParticles[i], floatType))
        self.mass = concatenate(masses)
        if nWithMass > 0: massSize2 = skip(f)
        if nWithMass > 0 and not massSize1 == massSize2 == massesBytesRead:
            raise RuntimeError, "Problem with Fortran Pad Words!"
        ##############################

        types = [zeros(h.nParticles[i]) + i
                       for i in range(len(h.nParticles))]
        self.type = concatenate(types)

        if (nGas > 0):
            readArray('gInternalEnergy', nGas)
        if initialCondition:
            postlude()
            return
        
        if (nGas > 0):
            if (snapshot):
                readArray('gDensity', nGas)
            if (h.cooling):
                readArray('gElectronDensity', nGas)
                readArray('gNeutralHydrogenDensity', nGas)
            if (h.sfr and not h.starGenerations):
                readArray('gMassFormedStars', nGas)
                raise NotImplementedError        
            if (h.multiphase):
                readArray('gMassColdClouds', nGas)
                raise NotImplementedError        
            if (snapshot):
                readArray('gSmoothingLength', nGas)
            if (h.sfr):
                readArray('gStarFormationRate', nGas)
        if (h.stellarAge):
            if (h.starGenerations and nNew>0):
                readArray('nFormationTime', nNew)
            if (not h.starGenerations and nGas>0):            
                readArray('gFormationTime', nGas)
                raise NotImplementedError        
        if (h.feedbackTp and nGas>0):
            readArray('gTurbulentEnergy', nGas)
        if h.potential:
            readArray('potential', nTotal)
        if h.metals and nGas>0:
            readArray('gMetallicity', nGas)
        if h.metals and nNew>0:
            readArray('nMetallicity', nNew)
        if h.energyDetail and nGas>0:
            readArray('gRadiatedEnergy', nGas)
            readArray('gShockedEnergy', nGas)
            if not hackHeader:
                readArray('gFeedbackEnergy', nGas)
        if not hackHeader:
            if h.parentId and nNew>0:
                readArray('nParentId', nNew, type=intType)
                if h.starOriginal and nNew>0:
                    readArray('nOriginalMass', nNew)
                    readArray('nOriginalSmoothingLength', nNew)

        postlude()
        
    def write(self, f, verbose=False, snapshot=True, endian=None,
              filename=None, initialCondition=False):
        """ Read snapshot data straight from the file, doing nothing other than
        dividing it up into different types of quantities

        snapshot: Not sure what this is (TODO)
        verbose: If true, print out diagnostic messages"""

        # Short aliases for relevant types
        floatType = RawGadgetSnapshot._floatType
        intType = RawGadgetSnapshot._intType
        padType = RawGadgetSnapshot._padType

        def writePad(f, bytes):
            """Write Fortran pad word.  Free variable -- byteswap"""
            io.fwrite(f,1, array([bytes], padType), padType, byteswap)
            
        def writePadInfo(f, bytes, name=''):
            """Skip Fortran pad word, recording info about it.  Add to
            bytesRead[0] the contents of word (presumably the number
            of bytes in the field) plus 2*sizeof(padWordType),
            yielding the number of bytes read from the file.  You can
            then check the number of bytes you think you read against
            the size of the file.

            Free variables
            verbose: print a diagnostic message if verbose is True
            bytesRead: record number of bytes you're supposed to have read.
            byteswap: Whether or not to swap bytes when reading"""
            if verbose:
                self._printSnapshotInfo(name, bytesWritten[0], bytes,
                                        array(0, padType).itemsize)
            io.fwrite(f,1,array([bytes], padType), padType, byteswap)
            bytesWritten[0] += bytes + 2*array(0, padType).itemsize

        def writeArray(name, n, type=floatType):
            """Read an array with Fortran pad words.  name is the
            destination of the read array as an attribute of self (the
            current object).  n is the number of elements to read.
            type is the type of the elements (default floatType).
            shape is the desired shape of the result.  If shape is
            none, the result will be a 1d array.

            Free variable: byteswap"""            
            arr = array(ravel(self[name]), type)
            if len(arr) != n:
                raise RuntimeError, "Size of array is incorrect!"
            
            writePadInfo(f, arr.nbytes, name)
            io.fwrite(f, n, arr, type, byteswap)
            writePad(f, arr.nbytes)
            
        def postlude():
            """Do some checks at the end of the file writing process"""
            if verbose:
                print 'Total Bytes', bytesWritten[0]

            f.flush()
            if os.stat(filename or f.name)[stat.ST_SIZE] != bytesWritten[0]:
                raise RuntimeError, "File size doesn't match bytes written count."

        assert endian in [None, 'native', 'big', 'little', 'swap']
        if endian == 'big' and local.littleEndian: byteswap = True
        elif endian == 'little' and local.bigEndian: byteswap = True
        # data read to native byte order, so you should do the
        # opposite of what you did to read it in.  If swapped, don't
        # swap again.  If not swapped, swap when writing.
        elif endian == 'swap': byteswap = not self.header.bytesSwapped()
        else: byteswap = False

        bytesWritten=[0]      # Number of bytes you're supposed to have read.
                              # It's a list so that local functions can
                              # modify the first element.
        h = self.header
        h.write(f, endian=endian)
        if verbose:
            self._printSnapshotInfo('header', bytesWritten[0],
                                    h.bytesRead() - 2*array(0, padType).itemsize,
                                    array(0, padType).itemsize)
        bytesWritten[0] += h.bytesWritten()
        
        if not (h.nFiles == 1 or h.nFiles == 0 and initialCondition):
            raise NotImplementedError

        if h.nParticles[5:].sum() != 0:
            raise RuntimeError, "Behavior differs from loadsnap.c, probably bug in loadsnap.c"

        nTotal = h.nParticles.sum()
        nGas = h.nParticles[GAS]
        nNew = h.nParticles[NEW]
        nWithMass = h.nParticles[ h.mass==0.0 ].sum()

        writeArray('position', 3*nTotal)
        writeArray('velocity', 3*nTotal)
        writeArray('identity', nTotal, type=intType)

        ##############################
        # Mass arrays are a horrid special case
        masses = [ array(self.mass[self.type == i], floatType)
                   for i in range(len(h.nParticles)) ]
        # Get number of bytes for mass arrays if masses are not in the
        # snapshot header
        massBytes = sum( [ mass.nbytes
                           for mass, headerMass in zip(masses, h.mass)
                           if headerMass == 0.0] )

        if nWithMass > 0:    
            writePadInfo(f, massBytes, 'mass')
        for mass, headerMass, nParticles in zip(masses, h.mass, h.nParticles):            
            if headerMass == 0 and nParticles > 0:
                io.fwrite(f, nParticles, mass, floatType, byteswap)
            elif len(mass) != 0 and (mass != mass[0]).any():
                raise RuntimeError
            # This should always be true
            elif len(mass) != 0 and abs(2*(mass[0]-headerMass)/(mass[0] + headerMass)) > 0.001:
                raise RuntimeError
            # This should be true if we're being pedantic
            # FIXME
            # elif mass[0] != headerMass: raise RuntimeError                    
        if nWithMass > 0:
            writePad(f, massBytes)
        ##############################

        if (nGas > 0):
            writeArray('gInternalEnergy', nGas)

        if initialCondition:
            postlude()
            return
        
        if (nGas > 0):
            if (snapshot):
                writeArray('gDensity', nGas)
            if (h.cooling):
                writeArray('gElectronDensity', nGas)
                writeArray('gNeutralHydrogenDensity', nGas)
            if (h.sfr and not h.starGenerations):
                writeArray('gMassFormedStars', nGas)
                raise NotImplementedError        
            if (h.multiphase):
                writeArray('gMassColdClouds', nGas)
                raise NotImplementedError        
            if (snapshot):
                writeArray('gSmoothingLength', nGas)
            if (h.sfr):
                writeArray('gStarFormationRate', nGas)
        if (h.stellarAge):
            if (h.starGenerations and nNew>0):
                writeArray('nFormationTime', nNew)
            if (not h.starGenerations and nGas>0):            
                writeArray('gFormationTime', nGas)
                raise NotImplementedError        
        if (h.feedbackTp and nGas>0):
            writeArray('gTurbulentEnergy', nGas)
        if h.potential:
            writeArray('potential', nTotal)
        if h.metals and nGas>0:
            writeArray('gMetallicity', nGas)
        if h.metals and nNew>0:
            writeArray('nMetallicity', nNew)
        if h.energyDetail and nGas>0:
            writeArray('gRadiatedEnergy', nGas)
            writeArray('gShockedEnergy', nGas)
            writeArray('gFeedbackEnergy', nGas)
        if h.parentId and nNew>0:
            writeArray('nParentId', nNew, type=intType)
        if h.starOriginal and nNew>0:
            writeArray('nOriginalMass', nNew)
            writeArray('nOriginalSmoothingLength', nNew)

        postlude()
        
class GadgetSnapshot (GenericGadgetSnapshot):
    # TODO -- low priority.  Would it be better to have regular long
    # names and define aliases for things I want shortened?

    # This is the code to define an alias
    #def _getLongName(self): return self.longName
    #def _setLongName(self, value): self.longName = value
    #shortName = property(_getLongName, _setLongName)
    
    _splitFields = dict(position =dict(gas='rg', dark='rd', stars='rs'), 
                        velocity =dict(gas='vg', dark='vd', stars='vs'),
                        mass     =dict(gas='mg', dark='md', stars='ms'),
                        identity =dict(gas='gIdentity', dark='dIdentity', stars='sIdentity'),
                        potential=dict(gas='gPotential', dark='dPotential', stars='sPotential'),
                        type     =dict(gas='gType', dark='dType', stars='sType'))

    def __init__(self, file=None, **kw):
        """If file is a string, open the file with that name, read it,
        and close it.  If file is a file object, read that file and
        leave it open.  If file is None, don't do anything to
        initialize"""
        self.header = GadgetHeader()
        self._bodyCoordinatesFlag = False
        
        if file and type(file) is types.StringType:
            f = open(file)
            self.read(f, **kw)
            f.close()
        elif file:
            self.read(file, **kw)

    def read(self, f, **kw):
        raw = RawGadgetSnapshot()
        raw.read(f, **kw)
        stars = logical_or(raw.type==DISK,
                           logical_or(raw.type==NEW, raw.type==BULGE))
        gas = raw.type==GAS
        dark = raw.type==DARK
        
        for field in raw.fields():
            if field in GadgetSnapshot._splitFields.keys():
                names = GadgetSnapshot._splitFields[field]
                self[names['gas']]   = raw[field][gas]
                self[names['dark']]  = raw[field][dark]
                self[names['stars']] = raw[field][stars]
            else:
                self[field] = raw[field]
        
    def write(self, f, **kw):
        # ensure that the star particles are ordered by type.
        tmpType = array(self.sType, copy=1)
        if (tmpType != self.sType).any():
            raise RuntimeError 
        
        raw = RawGadgetSnapshot()
        closeFile = False
        if type(f) in types.StringTypes:
            closeFile = True
            f = open(f, 'w')

        ##############################
        # Mass arrays are a horrid special case
        # Allow initial conditions objects not to specify masses by
        # pulling them out of the header here
        if not hasattr(self, 'mg'):
            self.mg = self.header.mass[GAS]*ones(header.nParticles[GAS], floatType)

        if not hasattr(self, 'md'):
            self.md = self.header.mass[DARK]*ones(header.nParticles[DARK], floatType)        

        if not hasattr(self, 'ms'):
            self.ms = zeros(len(self.sType), floatType)
            for typ in (DISK, BULGE, NEW):
                self.ms[ self.sType==typ ] = header.mass[typ]
        
        actFields = []    # These trigger unification
        unifiedNames = [] # These are the names of the unified arrays
        ignoreFields = [] # Thee are included by virtue of unification
        for k,v in GadgetSnapshot._splitFields.iteritems():
            actFields.append(v['gas'])
            names = v.values()
            names.remove(v['gas'])
            ignoreFields += names 
            unifiedNames.append(k)
                                    
        for field in self.fields():
            if field in actFields:
                uName = unifiedNames[actFields.index(field)]
                # Order of sNames in significant
                sNames = [ GadgetSnapshot._splitFields[uName][k]
                           for k in ('gas', 'dark', 'stars') ]
                uData = concatenate( [ self[name] for name in sNames] )
                raw[uName] = uData
            elif field in ignoreFields:
                pass
            else:
                raw[field] = self[field]
        
        raw.write(f, **kw)
        if closeFile:
            f.close()

    def bodyCoordinates(self):
        return self._bodyCoordinatesFlag
    
    # NOTE -- Could fix this by having snapshot keep track of which
    # coordinates it's in.  Could have it remember transformations
    # that have been applied and erase them when a new one is
    # requested.  Could have a dict of transformation matricies and
    # have toBodyCoordinates select them by name.

#     def toBodyCoordinates(self, props):
#         if not self._bodyCoordinatesFlag:        
#             xForm = particles.affineXFormer(props['matrix'])

#             self.rs = xForm(self.rs)
#             self.rg = xForm(self.rg)
#             self.rd = xForm(self.rd)
#             self.vs = xForm(self.vs, velocity=True)
#             self.vg = xForm(self.vg, velocity=True)
#             self.vd = xForm(self.vd, velocity=True)

#             # Finally, set a flag noting that this has been done
#             self._bodyCoordinatesFlag = True

    def mass(self):
        return self.md.sum() + self.ms.sum() + self.mg.sum()
    
    def centerOfMass(self):
        def wsum(ms, rs):
            return (ms[:, newaxis]*rs).sum(axis=0)
        
        mtot = self.mass()
        rc = (wsum(self.md, self.rd) + wsum(self.ms, self.rs) + wsum(self.mg, self.rg)) / mtot
        vc = (wsum(self.md, self.vd) + wsum(self.ms, self.vs) + wsum(self.mg, self.vg)) / mtot
        return rc, vc

    def toCenterOfMass(self):
        rc, vc = self.centerOfMass()
        self.rs -= rc;  self.rd -= rc; self.rg -= rc
        self.vs -= vc;  self.vd -= vc; self.vg -= vc

    def toRv(self, dr, dv):
        self.rs += dr;  self.rd += dr; self.rg += dr
        self.vs += dv;  self.vd += dv; self.vg += dv

#     def eulerXform(self, phi, th, psi):
#         self.rd = particles.eulerActive(self.rd, phi, th, psi)
#         self.rs = particles.eulerActive(self.rs, phi, th, psi)
#         self.rg = particles.eulerActive(self.rg, phi, th, psi)
#         self.vd = particles.eulerActive(self.vd, phi, th, psi)
#         self.vs = particles.eulerActive(self.vs, phi, th, psi)
#         self.vg = particles.eulerActive(self.vg, phi, th, psi)
        
def snapshots(raw=True,prefix=None):
    """Return a list of all snapshot files under directory name
    prefix.  If prefix is not given, try to pick a reasonable default,
    consulting raw to see if pickled or raw snapshots are desired."""
    if prefix is None: prefix = './data'

    snaps = []
    for root, dirs, files in os.walk(prefix):
        for f in files:
            if f.startswith('snapshot'):
                snaps.append(root + os.sep + f)
    return snaps

def byteswap(fn, output=None, endian='swap'):
    """Byteswap snapshot file fn.  If output is given, write to that
    filename.  If not, byte swap to a temporary file and then
    overwrite the original file.

    swap can be 'big', 'little', 'native', or 'swap'."""
    assert endian in ['native', 'swap', 'big', 'little']

    header = GadgetHeader(fn)

    # Sometimes there's nothing to do.  If output is specified,
    # there's always something to do.
    swapped = header.bytesSwapped()
    if not output and \
           (endian == 'native' and not swapped or 
            endian == 'big' and local.bigEndian and not swapped or 
            endian == 'big' and local.littleEndian and swapped or 
            endian == 'little' and local.bigEndian and swapped or 
            endian == 'little' and local.littleEndian and not swapped):
        # return false if you didn't do anything
        return False 

    snap = GadgetSnapshot(fn)

    if output:        
        outfn = output
        outf = open(outfn, 'w')
    else:
        temp = tempfile.NamedTemporaryFile()
        outfn = temp.name
        outf = temp.file
        
    snap.write(outf, endian=endian, filename=outfn)

    # If no separate output file specified, overwrite input file
    if not output:
        shutil.copyfile(outfn, fn)
    # return true if you did something
    return True

##################################################
## Rickety Code
##################################################

# def ke(self, p):
#     return (0.5*self['m'+p]*particles.mag(self['v'+p])**2).sum()
    
def pe(self, p):
    return (0.5*self['m'+p]*self[p+'Potential']).sum()

def oe(self):
    return [(self.mg*self.gInternalEnergy).sum(),
            (self.mg*self.gFeedbackEnergy).sum(),
            (self.mg*self.gRadiatedEnergy).sum(),
            (self.mg*self.gTurbulentEnergy).sum()]

def totOe(self):
    return sum(oe(self))

def totKe(self):
    return ke(self, 'g') + ke(self, 'd') + ke(self, 's')

def totPe(self):
    return pe(self, 'g') + pe(self, 'd') + pe(self, 's')

def totEn(self):
    return totKe(self) + totPe(self) + totOe(self)
    
def allEn(self):
    return [ke(self, k) for k in ('d', 's', 'g')] + [pe(self, k) for k in ('d', 's', 'g')] + oe(self)

def allEnFrac(self):
    tot = totEn(self)
    return [v/abs(tot) for v in allEn(self)]


def fetchSnap(id, qty=None,prefix=None,raw=True):
    """Fetch data and basic info about a snapshot.  The snapshot is
    identified by a string via 'G3/snapshot_067' or simply 'G3/067'.
    For the basic data, use quantity dictionary qty if given,
    otherwise load it from ./mergers.dat.  If not raw, load a pickled
    snapshot from ./data, otherwise, load a raw snapshot file from
    [big,little]-endian-data"""
    if qty is None: qty = uncan('mergers.dat')
    if prefix is None: prefix = './data'

    sim,snap = id.split('/')
    if not snap.startswith('snapshot_'): snap = 'snapshot_' + snap

    if raw: data = GadgetSnapshot(prefix + os.sep + sim + os.sep +snap)
    else: data = uncan(prefix+'/'+sim+'/'+snap)

    return data, qty[sim][snap]

def checkAllIntegrity(prefix=None):
    problems = []
    for fn in snapshots(raw=True, prefix=prefix):
        try:
            zed = GadgetSnapshot(fn)
        except:
            problems.append(fn)
    return problems

def checkAllSwapped(prefix=None):
    return [fn for fn in snapshots(raw=True,prefix=prefix)
            if GadgetHeader(fn).bytesSwapped()]

def swapAll(prefix=None):
    pass

def emptySnapshot():
    s = GadgetSnapshot()
    
    s.header.boxSize = 0.0
    s.header.cooling = True
    s.header.energyDetail = True
    s.header.feedbackTp = True
    s.header.hubbleParam = 0.7
    s.header.mass = zeros(6, local.Float32)
    s.header.metals = True    
    s.header.multiphase = False
    s.header.nFiles = 1    
    s.header.nParticles = zeros(6, local.Int32)
    s.header.nTotal = zeros(6, local.Int32)
    s.header.omega0 = 0.0
    s.header.omegaLambda = 0.0
    s.header.parentId = True
    s.header.potential = True
    s.header.redshift = 0.0
    s.header.sfr = True
    s.header.sfrHistogram = False
    s.header.starGenerations = True
    s.header.starOriginal = True
    s.header.stellarAge = True
    s.header.time = 0.0
    s.header._bytesSwapped = False

    s.rg = zeros((0,3), dtype=local.Float32)
    s.rd = zeros((0,3), dtype=local.Float32)
    s.rs = zeros((0,3), dtype=local.Float32)
    s.vg = zeros((0,3), dtype=local.Float32)
    s.vd = zeros((0,3), dtype=local.Float32)
    s.vs = zeros((0,3), dtype=local.Float32)
    s.mg = zeros(0, dtype=local.Float32)
    s.md = zeros(0, dtype=local.Float32)
    s.ms = zeros(0, dtype=local.Float32)

    s.gType = zeros(0, dtype=local.Int32)
    s.dType =    zeros(0, dtype=local.Int32)
    s.sType = zeros(0, dtype=local.Int32)
    s.gIdentity = zeros(0, dtype=local.Int32)
    s.dIdentity = zeros(0, dtype=local.Int32)
    s.sIdentity = zeros(0, dtype=local.Int32)

    s.gInternalEnergy = zeros(0, dtype=local.Float32)

    return s

def emptyFullSnapshot():
    s = emptySnapshot()
    
    s.dPotential = zeros(0, dtype=local.Float32)
    s.sPotential = zeros(0, dtype=local.Float32)
    s.gPotential = zeros(0, dtype=local.Float32)

    s.gDensity = zeros(0, dtype=local.Float32)
    s.gElectronDensity = zeros(0, dtype=local.Float32)
    s.gFeedbackEnergy = zeros(0, dtype=local.Float32)
    s.gMetallicity = zeros(0, dtype=local.Float32)
    s.gNeutralHydrogenDensity = zeros(0, dtype=local.Float32)
    s.gRadiatedEnergy = zeros(0, dtype=local.Float32)
    s.gShockedEnergy = zeros(0, dtype=local.Float32)
    s.gSmoothingLength = zeros(0, dtype=local.Float32)
    s.gStarFormationRate = zeros(0, dtype=local.Float32)
    s.gTurbulentEnergy = zeros(0, dtype=local.Float32)
    s.nFormationTime = zeros(0, dtype=local.Float32)
    s.nMetallicity = zeros(0, dtype=local.Float32)
    s.nOriginalMass = zeros(0, dtype=local.Float32)
    s.nOriginalSmoothingLength = zeros(0, dtype=local.Float32)
    s.nParentId = zeros(0, dtype=local.Float32)

    return s


