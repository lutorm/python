# TODO - make rtype, gist, and apropos magic commands
# TODO - make 'is StringType' into 'in StringTypes'

#from __future__ import with_statement

import os, sys, types, cPickle, stat, re, itertools

# sys.version >= 2.5
try: import hashlib
except ImportError: hashlib=None

import numpy

##################################################
## Syntax extensions
class BracketAccessMixin:
    """Allow access to attributes via dictionary syntax."""
    def __getitem__(self, name):
        return getattr(self, name)

    def __setitem__(self, name, value):
        return setattr(self, name, value)

class Container (object):
    """Simple object that has a nice syntax for filling in a few
    attributes:
    c = Container(a=1, b=2)"""
    def __init__(self, **kw):
        for k in kw: self.__setattr__(k, kw[k])

class sstr(object):
    """Simple String.  Used for pretty output in IPython (no quotes)."""
    def __init__(self, name): self._name = name        
    def __repr__(self): return str(self._name)
    def __str__(self): return str(self._name)


##################################################
## Functions
    
def wrap(x, min, max):
    return ((x - min) % (max - min)) + min
    range = float(max-min)
    return x - range*floor(x/range) 

def flatten(L):
    """Return a flat list with the same elements as arbitrarily nested
    list L"""
    if type(L) is not list: return [L]
    if L == []: return L
    return flatten(L[0]) + flatten(L[1:])

def group(L, n):
    idx = range(0, len(L)+n, n)
    return [L[l:h] for l, h in zip(idx[:-1], idx[1:])]

# Less concise version should recursion become a problem.
# def flatten(L):
#    """Return a flat list with the same elements as arbitrarily nested
#    list L"""
#     result = []
#     stack = [L]
#     while stack:
#         print stack
#         current = stack.pop()
#         if type(current) is list:
#             if current[1:]:
#                 stack.append(current[1:])
#             stack.append(current[0])
#         else:
#             result.append(current)
#     return result

def iterable(obj):
    """Test if you can iterate over an object"""
    try: len(obj)
    except TypeError: return False
    return True

def dictKeys(d):
    """Given a dict, return a list of keys that are also dicts"""
    return [k for k,v in d.iteritems() if type(v) is dict]

# Will be obsolete in Python 2.5
if 'sorted' in __builtins__:
    sorted = __builtins__['sorted']
else:
    def sorted(lst, **kw):
        """Return a sorted copy of lst"""
        newlst = list(lst)
        newlst.sort(**kw)
        return newlst
    
def mergeComp(l1, l2):
    """Make a comparison function for mergeSorted"""
    def comp(x,y):
        if   x in l1 and y in l1:
            return cmp(l1.index(x), l1.index(y))
        elif x in l2 and y in l2:
            return cmp(l2.index(x), l2.index(y))
        elif x not in l1 and x not in l2 or \
             y not in l1 and y not in l2:
            # This should never happen
            raise RuntimeError, "This should never happen"
        else:
            # Find a third element with the same relation to x and y,
            # meaning it's between the two
            for pivot in common:
                if comp(x, pivot) == comp(pivot, y) and comp(x, pivot) != 0:
                    return comp(x, pivot)
            # This may happen
            raise RuntimeError, "Cannot find relation for %s and %s" % (x, y)
                    
    common = [el for el in l1 if el in l2]
    return comp

def mergeSorted(l1, l2, check=True):
    """Merge two sorted lists that contain some, but not all, of the
    same elements.  The two lists imply an ordering which (hopefully)
    leaves no ambiguity.  Ambiguity is still possible, in which case
    an exception is raised.  Check=True means checks that the lists
    define a self-consistent ordering (which makes the sorting N^2
    instead of N log N."""
    all = list(set(l1).union(l2))
    if check:
        cmp1 = mergeComp(l1, l2)
        cmp2 = mergeComp(l2, l1)
        for e1 in all:
            for e2 in all:
                assert cmp1(e1, e2) ==  cmp2(e1, e2)
                assert cmp1(e1, e2) == -cmp1(e2, e1)
                assert cmp2(e1, e2) == -cmp2(e2, e1)
    return sorted(all, cmp=mergeComp(l1, l2))

# This definition allows these uses
# def every(*args): return reduce(and2, args)
# every(True, True, False) => False
# every(*[True, True, False]) => False
# every([True, True, False]) => [True, True, False]
#
# The last is very bad and an easy mistake.

# This definition allows these:
# def every(args): return reduce(and2, args)
# every([True, True, False]) => False
# every(*[True, True, False]) => Error
# every(True, True, False) => Error
#
# This is mostly for programmatic use anyway, so the first use case is
# the most common one.  It's not a lot of skin off of your nose to put
# in the brackets when you have a few values and are writing them
# explicitly in the source code.  Finally, you'd be using explicit
# ands in that case anyway.

def allSame(args): return every([arg is args[0] for arg in args])
def every(args): return reduce(lambda x,y: x and y, args, True)
def some(args): return reduce(lambda x,y: x or y, args, False)
def none(args): return not some(args)

# def fAnd(*fs) allows:
# g = fAnd(f1, f2)   and  g = fAnd(*foo(bar))
# g = fAnd( fOr(f1, f2), fOr(f3, f4))
#
# def fAnd(fs), allows:
# g = fAnd((f1, f2))  and  g = fAnd(fOr(bar))
# g = fAnd( (fOr((f1, f2)), fOr((f3, f4))) )

# Since the intent is to make composable function predicates, I think
# that the third use case is the decisive one.

def fAnd(*fs):
    """Return a function that ands together the result of every
    function given as an argument"""
    return lambda *a, **kw: every([f(*a, **kw) for f in fs])

def fOr(*fs):
    """Return a function that ors together the result of every
    function given as an argument"""
    return lambda *a, **kw: some([f(*a, **kw) for f in fs])

def ifExp(cond, true, false):
    """An if statment expression.  Doens't conditionally execute code,
    but does conditionally return a value"""
    if cond: return true
    return false

def grabList(lst, pred):
    """Return a list of elements of lst that satisfy predicate.  The
    elemnts are removed from lst."""
    result = [el for el in lst if pred(el)]
    for el in result:
        lst.remove(el)
    return result

def dictUnion(*ds, **kw):
    iters = [d.iteritems() for d in ds] + [dict(**kw).iteritems()]
    return dict(itertools.chain(*iters))

def crossSet(*sets):
    """Given lists, generate all possible combinations
    ie, crossSet([a], [b,c]) returns [[a,b], [a,c]]"""
    if len(sets) == 1:
        return [ [el] for el in sets[0] ]
    result = []
    for el in sets[0]:
        result += [ [el] + subProd for subProd in crossSet(*sets[1:])]
    return result

###################################################
## Function Argument conveniences
def popKeys(d, *names):
    return dict([(k, d.pop(k)) for k in names if k in d])

def given(*args):
    """Return True if all of the arguments are not None.  Intended for
    use in argument lists where you can reasonably specify different
    combinations of parameters.  You can then write conditionals as if
    given(a,b): instead of if a is not None and b is not None:"""
    return all( [arg is not None for arg in args] )

##################################################
## Files
def can(obj, file, protocol=2):
    """More convenient interactive syntax for pickle"""
    if type(file) is str: f=open(file,'wb')
    else: f=file

    cPickle.dump(obj, f, protocol=protocol)

    if type(file) is str: f.close()

def uncan(file):
    """More convenient interactive syntax for pickle"""
    # If filename, should this read until all exhausted?
    if type(file) is str: f=open(file, 'rb')
    else: f=file    

    obj = cPickle.load(f)

    if type(file) is str: f.close()

    return obj

def fileSize(path, human=False):
    """Return the size of a file in bytes"""
    bytes = os.lstat(path)[stat.ST_SIZE] 
    if not human:
        return bytes    
    if bytes < 1024: result = int(bytes), 'B'
    elif bytes < 1024**2: result = int(bytes/1024), 'K'
    elif bytes < 1024**3: result = int(bytes/1024**2), 'M'
    elif bytes < 1024**4: result = int(bytes/1024**3), 'G'
    else: result = int(bytes/1024**4), 'T'
    return sstr('%d %c' % result)

def find(pred, dir='.', withDirectories=True, withFiles=True):
    """Similar to the shell command find"""
    result = []
    for root, dirs, files in os.walk(dir):
        if withDirectories:
            for d in dirs:
                if pred(root, d): result.append(os.path.join(root, d))
        if withFiles:
            for f in files:
                if pred(root, f): result.append(os.path.join(root, f))
    return result

def memoryUsage(pid=None, unit='MB'):
    """On linux systems, get info about memory use from the /proc filesystem"""
    if pid is None: pid = os.getpid()    
    result = {}
    #with open('/proc/%d/status' % pid) as f:
    f = open('/proc/%d/status' % pid)
    for line in f:
        for name, regexp in (("size", "VmData:\s*([0-9]+)\s([A-z]B)"),
                             ("resident", "VmRSS:\s*([0-9]+)\s([A-z]B)")):
            match = re.match(regexp, line)
            if match:
                num, givenUnit = match.groups()
                if   givenUnit == 'kB': num = 1000*int(num)
                else: raise RuntimeError, "Unknown Unit!"
                result[name] = num
    f.close()
    
    for k,v in result.iteritems():
        if unit == 'B': pass
        elif unit == 'kB': result[k] = result[k] / 10**3
        elif unit == 'MB': result[k] = result[k] / 10**6
        elif unit == 'GB': result[k] = result[k] / 10**9
        elif unit == 'kiB': result[k] = result[k] / 2**10
        elif unit == 'MiB': result[k] = result[k] / 2**20
        elif unit == 'GiB': result[k] = result[k] / 2**30
        # These are for completeness so I can remind myself of this one day...
        elif unit == 'b': result[k] = 8*result[k]
        elif unit == 'kb': result[k] = 8*result[k] / 10**3
        elif unit == 'Mb': result[k] = 8*result[k] / 10**6
        elif unit == 'Gb': result[k] = 8*result[k] / 10**9
        elif unit == 'kib': result[k] = 8*result[k] / 2**10
        elif unit == 'Mib': result[k] = 8*result[k] / 2**20
        elif unit == 'Gib': result[k] = 8*result[k] / 2**30        
        else: raise RuntimeError, "Unknown Unit!"
    return result
    
##################################################
## Functions that enhance functions
def stringKey(*a, **kw):
    return repr(a) + repr(kw)

def pickleKey(*a, **kw):
    return cPickle.dumps((a, kw), protocol=-1)

def hashKey(*a, **kw):
    return hashlib.sha256(cPickle.dumps((a, kw), protocol=-1)).digest()
        
def memoize(f, withFile=True, keyf=hashKey):
    """Return a 'fast' version of long-running function f If called
    with the same arguments, it just returns the previous return-value
    instead of recomputing.  It does this by converting all positional
    and keyword arguments to strings and indexing into a dictionary.
    A different method of generating keys can be used if you specify
    the keyf funciton.  If withFile is true, pickle the resulting
    dictionary to a file with name f.func_name.dat.  Be careful of
    using this feature with several instances of Python
    running... they will each overwrite the file.  It should not lead
    to corruption, though since the dict is only loaded from the file
    when memoize itself is called."""
    def g(*args, **kw):
        key = keyf(*args, **kw)
        if not key in results:
            results[key] = f(*args, **kw)            
            if withFile: can(results, fname)
        return results[key]
    
    fname = f.func_name + '.dat'
    if withFile and os.path.isfile(fname): results = uncan(fname)
    else: results = {}
    
    g.func_doc, g.func_name = f.func_doc, f.func_name
    return g

def logize(f,withFile=True, withArgs=False):
    """Sometimes I want to build up statistics of calls to a
    long-running function.  I don't want to try to substitute
    previously computed values (ie, not memoization), and accordingly
    I'm not necssarily as worried about making sure that every
    possible set of arguments generates a unique key.  Therefore I
    return a new function that captures calls to the original function
    and logs them.  A second function is necessary to get a hold of
    the resulting dict.  The function keyf generates keys from the
    arguments of the call.

    A more conventional object-based implementation of the same thing
    is below, but frankly that doesn't read any better than this to me.

    usage:
    def slow_fn():
        blah blah

    fn, fn_results = logize(slow_fn)
    """
    
    def g(*ar, **kw):
        value = f(*ar, **kw)        
        if withArgs: results.append((ar, kw, value))
        else: results.append(value)
        
        if withFile: can(results, fname)
        return value

    def getResults():        
        return results

    fname = f.func_name + '.dat'    
    if withFile and os.path.isfile(fname): results = uncan(fname)
    else: results = []
    
    g.func_doc, g.func_name = f.func_doc, f.func_name
    return g, getResults

class FunctionLogger:
    """Usage:
    def slow_fn():
        blah blah

    flogger = function_logger(slow_fn)
    fn = flogger.run
    fn_results = flogger.results
    """

    def __init__(self, f, withFile=True, withArgs=False):
        fname = f.func_name + '.dat'

        if withFile and os.path.isfile(fname): self.results = uncan(fname)
        else: self.results = []

        self.f = f
        self.withArgs = withArgs
        
    def run(self, *a, **kw):
        value = self.f(*a, **kw)
        if self.withArgs: self.results.append((a, kw, value))
        else: self.results.append(value)
        return value
    

# lifted from http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/511474
def forkingCall(f, *args, **kwds):
    """Fork a separate process in which to run f.  Exceptions raised
    in the child process are caught and re-raised in the parent
    process."""
    SUCCESS, EXCEPTION, UNPICKLABLE = (0,1,2)

    pread, pwrite = os.pipe()
    pid = os.fork()
    if pid > 0:
        os.close(pwrite)
        # GSN -- moved this up from below the with statement
        os.waitpid(pid, 0)
        f = os.fdopen(pread, 'rb')
        status, result = cPickle.load(f)
        f.close()
        if status == SUCCESS: return result
        else: raise result
    else: 
        try: 
            try: 
                try:
                    result = f(*args, **kwds)
                    status = SUCCESS
                except Exception, exc:
                    result = exc
                    status = EXCEPTION
                try:
                    os.close(pread)
                    f = os.fdopen(pwrite, 'wb')
                    cPickle.dump((status,result), f, protocol=-1)
                except Exception, exc:
                    cPickle.dump((UNPICKLABLE,exc), f, protocol=-1)
            finally: f.close()
        # Make sure that this thread exits to avoid the parent waiting
        # indefinitely or getting unexpectedly popped into the child
        # process.  On the other hand, maybe that'd be nice for
        # exception handling...
        finally: os._exit(0)

# FIXME -- uncomment when I move to python 2.5
# def forkingCall(f, *args, **kwds):
#     """Fork a separate process in which to run f.  Exceptions raised
#     in the child process are caught and re-raised in the parent
#     process."""
#     SUCCESS, EXCEPTION, UNPICKLABLE = (0,1,2)

#     pread, pwrite = os.pipe()
#     pid = os.fork()
#     if pid > 0:
#         os.close(pwrite)
#         # GSN -- moved this up from below the with statement
#         os.waitpid(pid, 0)
#         with os.fdopen(pread, 'rb') as f: status, result = cPickle.load(f)
#         if status == SUCCESS: return result
#         else: raise result
#     else: 
#         try: 
#             try:
#                 result = f(*args, **kwds)
#                 status = SUCCESS
#             except Exception, exc:
#                 result = exc
#                 status = EXCEPTION
#             try:
#                 os.close(pread)
#                 with os.fdopen(pwrite, 'wb') as f:            
#                     cPickle.dump((status,result), f, protocol=-1)
#             except Exception, exc:
#                 cPickle.dump((UNPICKLABLE,exc), f, protocol=-1)
#         # Make sure that this thread exits to avoid the parent waiting
#         # indefinitely or getting unexpectedly popped into the child
#         # process.  On the other hand, maybe that'd be nice for
#         # exception handling...
#         finally: os._exit(0)

def forkify(f):
    """Return a function that forks and calls f in separate process."""
    return lambda *a, **kw: forkingCall(f, *a, **kw)

def constantly(val):
    """Return a function that returns val regardless of its arguments"""
    def constant(*a, **kw):
        return val
    return constant

##################################################
## Introspection
def gist(obj, verbose=False, pretty=True):
    """See what an object is all about.  Make a dict where the keys
    are the names of each type of attribute in the object.  The values
    are a list of the names of the attribute of that type."""
    if pretty: string = sstr
    else: string = str
        
    info = [(name, type(getattr(obj, name))) 
            for name in dir(obj) 
            if verbose or (not verbose and not name.startswith('_'))]

    types = sorted(set([el[1] for el in info]))
    result = {}
    for t in types:
        names = [string(name) for name, theType in info if theType is t]        
        result[string(t.__name__)] = names
        #result.append((t.__name__, names))
    return result

simpleTypes = [bool, complex, float, int, long, str, unicode,
               types.NoneType,
               numpy.bool8,
               numpy.complex64, numpy.complex128,
               numpy.float32, numpy.float64,
               numpy.int0, numpy.int8, numpy.int16, numpy.int32, numpy.int64,
               numpy.uint0, numpy.uint8, numpy.uint16, numpy.uint32, numpy.uint64]

if hasattr(numpy, 'float128') and hasattr(numpy, 'complex256'):
    simpleTypes += [numpy.float128, numpy.complex256]
        
compositeTypes = [list, tuple, dict, set, frozenset, numpy.ndarray,]

def rtype(obj, max=50):
    """Recursive type() function.  Try to give a concise description
    of the type of an object and all objects it contains."""
    def rtypesEqual(els):
        firstType = rtype(els[0])
        return every([ rtype(el) == firstType for el in els])
    def typesEqual(els):
        firstType = type(els[0])
        return every([ type(el) is firstType for el in els])
    def typesSimple(els):
        return every([ type(el) in simpleTypes for el in els])
    def name(obj):
        return type(obj).__name__
    def shape(obj):
        if type(obj) is numpy.ndarray: return str(obj.shape)
        return str(len(obj))
    def contents(obj):
        if   type(obj) in (list, tuple): return obj
        elif type(obj) in (set, frozenset): return list(obj)
        elif type(obj) is dict: return [obj[k] for k in sorted(obj.keys())]
        elif type(obj) is numpy.ndarray: return obj.flat
        return None
    
    if type(obj) in compositeTypes:
        if typesEqual(contents(obj)) and typesSimple(contents(obj)):
            return '%s of %s %s' % (name(obj), shape(obj), name(contents(obj)[0]))
        elif rtypesEqual(contents(obj)):
            return ['%s of %s' % (name(obj), shape(obj)), rtype(contents(obj)[0])]
        elif len(contents(obj)) > max:
            return ['%s of' % name(obj)] \
                   + [rtype(el) for el in contents(obj) [:max] ] \
                   + ['........']
        else: 
            return ['%s of' % name(obj)] \
                   + [rtype(el) for el in contents(obj)]         
    return name(obj)

def importGraph(withSystem=True, outFile=sys.stdout,
                excludes=None, excludeRegexps=None):    
    """Construct a graph of which python modules import which others,
    suitable for consumption by graphviz."""
    def include(module):
        return not module in excludes \
               and none([re.match(regexp, module) for regexp in excludeRegexps])
        
    excludes = excludes or []
    excludeRegexps = excludeRegexps or []    
    
    pyfiles = [fn for fn in os.listdir('.') if fn.endswith('.py')]
    localSources = [fn.replace('.py', '') for fn in pyfiles]
    localSources = [mod for mod in localSources if include(mod)]
    output = []
    output.append('digraph gr {\n')
    if withSystem:
        for node in localSources:
            output.append('%s color=red;' % node)
            
    for fn, src in zip(pyfiles, localSources):
        #with open(fn) as f:
        f = open(fn)
        for line in f:
            fromMatch = re.match(r"from ([A-z]+) import", line)
            importMatch = re.match(r"import " + 10*"(?:([A-z]+),? *)?", line)

            if fromMatch: dests = fromMatch.groups()
            elif importMatch:
                dests = [gp for gp in importMatch.groups() if gp]
                if len(dests) >= 3 and dests[-2] == 'as':
                    dests = dests[:-2]
            else: dests = []

            for dest in dests:
                if include(dest) and (dest in localSources or withSystem):
                    output.append("%s -> %s;\n" % (src, dest))
        f.close()
        
    output.append('}\n')
    if type(outFile) is str:
        #with open(outFile, "w") as outf:
        outf = open(outFile, "w")
        outf.writelines(output)
        outf.close()
    else:
        outFile.writelines(output)

##################################################
## Rickety code
##################################################

def reloadAll():
    import extensions
    reload(extensions)
    for fn in os.listdir('.'):
        if fn.endswith('.py') and not fn.startswith('.'):
            moduleName = fn[:-3]
            if moduleName in sys.modules.keys():
                reload(__import__(moduleName))

def resetPyx():
    mods = [mod for mod in sys.modules.keys() if mod.startswith('pyx')]
    for mod in mods:
        del sys.modules[mod]    
    reloadAll()
    
