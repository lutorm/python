import urllib, httplib

def post(data, public_key, private_key):

    params = urllib.urlencode(data)

    print "Posting data to phant"
    server="krypton:9090"
    
    headers={}
    headers["Content-Type"] = "application/x-www-form-urlencoded"
    headers["Connection"] = "close"
    headers["Content-Length"] = len(params)
    headers["Phant-Private-Key"] = private_key

    c = httplib.HTTPConnection(server)

    try:
        c.request("POST", "/input/" + public_key, params, headers)
    except:
        print "http request failed"
        return

    r = c.getresponse().read()

    if r[0]!="1":
        print "post failed: %s"%r
        

