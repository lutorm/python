#!/usr/bin/python

import matplotlib
import pyfits, pylab,numarray
from pylab import *

matplotlib.use('PS')

def factorial(k):
    if(k==0):
        return 1
    else:
        return k*factorial(k-1)
    
def poisson(k,rate):
    return exp(-rate)*rate**k/factorial(k)

def plot_slice(file,slice,nfrag=1.,probarea=None,outfile=None):
    clf()
    phdu=pyfits.open(file)[0]
    xmin=phdu.header['xmin']
    xmax=phdu.header['xmax']
    ymin=phdu.header['ymin']
    ymax=phdu.header['ymax']
    zmin=phdu.header['zmin']
    dz=phdu.header['dz']
    i=phdu.data*nfrag
    if probarea != None:
        # instead of flux, we calculate prob(h) for the spec area
        # prob(hit) is 1-poisson(0,expectation value=flux*area)
        i=1-poisson(0,i[slice,:,:]*probarea)
        maxval=1
        minval=0
        barformat='%2.2f'
        barticks=numarray.array(range(11),Float)*.1
        titlestring='Hit probability for area=$%3.2f\mathrm{m}^2$, '%probarea
    else:
        # plot log10 of flux
        #m=i.max()
        maxval=log10(1e4)
        minval=log10(1e-2)
        i=log10(i[slice,:,:])
        #minval=log10(minval)
        barformat='$10^{%1.0f}/\mathrm{m}^2$'
        barticks=[-2,-1,0,1,2,3,4]
        titlestring='Fragment flux for '
    imshow(i,vmin=minval,vmax=maxval,
           extent=[xmin,xmax,ymin,ymax])
    #plot([xmin,xmax],[(ymax+ymin)/2,(ymax+ymin)/2],'k-')
    #plot([(xmax+xmin)/2,(xmax+xmin)/2],[ymin,ymax],'k-')
    grid()
    xlabel('x/m')
    ylabel('y/m')
    title(titlestring+'theta=%3.1f, f=%3.2f, nfrag=%d, z=%2.1fm'%(phdu.header['theta'],phdu.header['f'],nfrag,slice*dz+zmin))
    colorbar(format=barformat,ticks=barticks)

    if outfile:
        print "saving ",outfile
        savefig(outfile,papertype='letter',orientation='portrait')
    
def save_all(probarea=None):
    files=['frags0.fits','frags20.fits','frags40.fits',
           'frags60.fits','frags80.fits','frags89.fits']
    slices=[0,2,5]

    for f in files:
        for s in slices:
            probtxt=''
            if probarea:
                probtxt='ph'
            outfile=f.replace(".fits","")+probtxt+'_s'+`s`+'.ps'
            print "file ",f," slice ",s
            plot_slice(f,s,nfrag=1359,probarea=probarea,outfile=outfile)

save_all()
save_all(probarea=.3)
