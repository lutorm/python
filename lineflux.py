import mipsflux, pyx
from numpy import *
import pdb

def set_query(set,base):
    return  base.join(['camera','snapshot','simulation']).filter_by(runname=set)

def sim_query(sim,base):
    return  base.join(['camera','snapshot','simulation']).filter_by(gadgetrunname=sim)

def dust_query(dust,base):
    return base.join([]).filter_by(has_dust=dust)

def query_set(set, dust, base=None):
    """Returns a query that will pick out all objects for a given
    simulation set. The query is joined with the supplied base query,
    if supplied."""
    if not base:
        base=mipsflux.sql.ioq
    return base.filter_by(has_dust=dust).join(['camera','snapshot','simulation']).filter_by(runname=set)

def load_sql_fluxes(query):

    # get the lines
    lines={}
    lines.update([(q.id,q.name) for q in mipsflux.sql.liq])
    #pdb.set_trace()
    # put line strengths into a dict of arrays indexed by line name
    ls={'sfr':[],'object':[],'camera':[],'theta':[],
        'simulation':[],'snapshot':[]}
    tls={}
    for lid,l in lines.items():
        tls[lid]=[]
    
    for o in query:
        for l in o.line_strengths:
            tls[l.line_id].append(l.luminosity)
        ls['sfr'].append(o.sfr)
        ls['camera'].append(o.camera.number)
        ls['theta'].append(o.camera.theta)
        ls['object'].append(o.id)
        ls['snapshot'].append(o.camera.snapshot.snapshot_file)
        ls['simulation'].append(o.camera.snapshot.simulation.gadgetrunname)
        
    # now make final arrays indexed by line
    for lid,luminosity in tls.items():
        ls[lines[lid]]=array(luminosity)
    ls['sfr']=array(ls['sfr'])
    ls['theta']=array(ls['theta'])
    
    return ls

    
def halpha(data):
    """Makes a plot of the H Alpha luminosities vs SFR."""
    g=lineplot(data['sfr'], data['H Alpha']/3.5e34,
               (5e-4,10), (5e-4,20),
               r"$\mathit{SFR}/\mathrm{M}_\odot\mathrm{yr}^{-1}$",
               r"$\mathit{SFR}(L_{\mathrm{H}\alpha})/\mathrm{M}_\odot\mathrm{yr}^{-1}$")
    g.plot(pyx.graph.data.function("y(x)=x", min=1e-4, max=1e2,title=None))
    g.writePDFfile('halpha.pdf')

    # balmer decrement
    bd=data['H Alpha']/data['H Beta']
    g=lineplot(data['sfr'],
               data['H Alpha']*10**(0.4*1.086*1.96*log(bd/2.86))/3.5e34,
               (5e-4,10), (5e-4,20),
               r"$\mathit{SFR}/\mathrm{M}_\odot\mathrm{yr}^{-1}$",
               r"$\mathit{SFR}(L_{\mathrm{H}\alpha},\mbox{dust corrected})/\mathrm{M}_\odot\mathrm{yr}^{-1}$")
    g.plot(pyx.graph.data.function("y(x)=x", min=1e-4, max=1e2,title=None))
    g.writePDFfile('halphacorr.pdf')

def oii(data):
    """Makes a plot of the [OII]3727 luminosities vs SFR."""
    g=lineplot(data['sfr'], data['OII 3727']*1.4e-34,
               (5e-4,10), (5e-4,20),
               r"$\mathit{SFR}/\mathrm{M}_\odot\mathrm{yr}^{-1}$",
               r"$\mathit{SFR}(L_{\mbox{[O{\sc ii}]3727}})/\mathrm{M}_\odot\mathrm{yr}^{-1}$")
    g.plot(pyx.graph.data.function("y(x)=x", min=1e-4, max=1e2,title=None))
    g.writePDFfile('oii.pdf')

    # balmer decrement
    bd=data['H Alpha']/data['H Beta']
    g=lineplot(data['sfr'],
               data['OII 3727']*10**(0.4*1.086*1.96*log(bd/2.86))*1.4e-34,
               (5e-4,10), (5e-4,20),
               r"$\mathit{SFR}/\mathrm{M}_\odot\mathrm{yr}^{-1}$",
               r"$\mathit{SFR}(L_{\mbox{[O{\sc ii}]3727}}, \mbox{dust corrected})/\mathrm{M}_\odot\mathrm{yr}^{-1}$")
    g.plot(pyx.graph.data.function("y(x)=x", min=1e-4, max=1e2,title=None))
    g.writePDFfile('oiicorr.pdf')

def lineplot(xdata, ydata, xrange, yrange, xlabel, ylabel):
    valid=(xdata>0)&(ydata>0)
    xdata=xdata[valid]
    ydata=ydata[valid]
    
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(pos='br'),
                        x=pyx.graph.axis.log(min=xrange[0], max=xrange[1],
                                             title=xlabel),
                        y=pyx.graph.axis.log(min=yrange[0], max=yrange[1],
                                             title=ylabel))
    mipsflux.pyx_densityplot(g, xdata,
                             ydata)
    return g

    
