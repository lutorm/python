from numpy import *

def mag(vs):
    """Compute the norms of a set of vectors or a single vector."""
    vs = asarray(vs)
    if len(vs.shape) == 1:
        return sqrt( (vs**2).sum() )
    return sqrt( (vs**2).sum(axis=1) )

def interpolator(x1, x2, y1, y2):
    def f(x):
        assert x >= x1 and x <= x2
        zed = (x - x1) / float(x2 - x1)
        return (y2 - y1) * zed + y1
    return f

def normed(v):
    return v/mag(v)
        
def anim1():
    # galaxy is 0 or 1, indicating which galaxy should be followed at the beginning.

    # initial COM pos + vel of stars in each galaxy, 0 = retrograde, 1 = prograde
    r0 = array([ -88.9,  -45.5,   0.0])
    v0 = array([ 129.1,   30.9,    0.386])
    r1 = array([  88.8,   45.6,   0.0])
    v1 = array([ -128.7,  -31.2,  0.0229])
    # final COM pos and vel of remnant
    rf = array([  2.63,  17.2,  -1.72])
    vf = array([ 2.12,  9.16, -2.36])
    # z position of camera, used to calculate fov
    z0 = 200.0   

    f = [0, 100, 300, 1000, 1501]  # transition frames

    # Python needs these names to be defined
    pos_interp1 = pos_interp2 = fov_interp1 = fov_interp2 = lambda x: 1/0
    
    def time(frame):
        return 0.002*frame

    def position(frame):
        t = time(frame)

        if f[0] <= frame and frame < f[1]:
            # move along with one galaxy
            if galaxy == 0:
                r = r0 + v0*t
            elif galaxy == 1:
                r = r1 + v1*t

            r[2] = z0
            return r

        elif f[1] <= frame and frame < f[2]:
            # zoom out, interpolate between moving camera and stationary one
            return pos_interp1(frame)
        
        elif f[2] <= frame and frame < f[3]:
            # steady camera
            return array([0, 0, z0])

        elif f[3] <= frame and frame < f[4]:
            # zoom in on remnant, moving with com
            return pos_interp2(frame)
            
        elif frame == f[4]:
            # final position, value used in making interpolator
            r = array(rf)
            r[2] = z0
            return r

        else:
            # Conditions above are more explicit than necessary,
            # written that way b/c it's easier on my brain to write
            # the interpolation expressions.  Reaching here means I
            # screwed them up.
            raise AssertionError
        
    def direction(frame):
        return array([0,0,-1.0])

    def up(frame):
        return array([0,1.0,0])
    
    def fov(frame):
        if f[0] <= frame and frame < f[1]:
            # move along with one galaxy, filling frame
            return 45.0/z0

        elif f[1] <= frame and frame < f[2]:
            # zoom out, interpolate between moving camera and stationary one
            return fov_interp1(frame)
        
        elif f[2] <= frame and frame < f[3]:
            # steady camera
            return 150.0/z0

        elif f[3] <= frame and frame < f[4]:
            # zoom in on remnant, moving with com
            return fov_interp2(frame)
        
        elif frame == f[4]:
            # final fov, value used in making interpolator
            return 22.5/z0
        
        else:
            raise AssertionError
    
    pos_interp1 = interpolator(f[1], f[2], position(f[1]-1), position(f[2]))
    pos_interp2 = interpolator(f[3], f[4], position(f[3]-1), position(f[4]))
    fov_interp1 = interpolator(f[1], f[2], fov(f[1]-1), fov(f[2]))
    fov_interp2 = interpolator(f[3], f[4], fov(f[3]-1), fov(f[4]))

    return position, direction, up, fov


##################################################
##################################################
##################################################

def anim2(galaxy):
    # galaxy is 0 or 1, indicating which galaxy should be followed at the beginning.
    
    # initial COM pos + vel of stars in each galaxy, 0 = retrograde, 1 = prograde
    r0 = array([ -88.9,  -45.5,   0.0])
    v0 = array([ 129.1,   30.9,    0.386])
    r1 = array([  88.8,   45.6,   0.0])
    v1 = array([ -128.7,  -31.2,  0.0229])
    # final COM pos and vel of remnant
    rf = array([  2.63,  17.2,  -1.72])
    vf = array([ 2.12,  9.16, -2.36])
    # z position of camera, used to calculate fov
    z0 = 100.0   

    f = [0, 100, 300, 1000, 1501]  # transition frames

    # Python needs these names to be defined before they're referenced
    pos_interp = fov_interp1 = fov_interp2 = lambda x: 1/0
    up_interp = dir_interp1 = dir_interp2 = lambda x: 1/0
    
    def time(frame):
        return 0.002*frame

    def position(frame):
        t = time(frame)

        if f[0] <= frame and frame < f[1]:
            # move along with one galaxy
            if galaxy == 0:
                r = r0 + v0*t
            elif galaxy == 1:
                r = r1 + v1*t
            else:
                raise AssertionError
            
            r[2] = z0
            return r

        elif f[1] <= frame and frame < f[2]:
            # slide down into plane, 
            return pos_interp(frame)
        
        elif f[2] <= frame and frame < f[3]:
            # steady camera, in plane
            r = position(f[1]-1)
            r[2] = 40.0
            return r
        
        elif f[3] <= frame and frame < f[4]:
            # zoom in on remnant, moving with com
            return position(f[3]-1)

        elif f[4] == frame:
            # zoom in on remnant, moving with com
            return position(f[3]-1)
            
        else:
            raise AssertionError

    pos_interp = interpolator(f[1], f[2], position(f[1]-1), position(f[2]))
        
    def direction(frame):
        if f[0] <= frame and frame < f[1]:
            # Look down on the galaxy
            return array([0,0,-1])

        elif f[1] <= frame and frame < f[2]:
            # slide down into plane, 
            return dir_interp1(frame)
        
        elif f[2] <= frame and frame < f[3]:
            # steady view of origin
            return - normed(position(frame))
        
        elif f[3] <= frame and frame < f[4]:
            # zoom in on remnant, moving with com
            return dir_interp2(frame)
            
        elif f[4] == frame:
            # final view is of com position
            return normed( rf - position(frame))

        else:
            raise AssertionError

    dir_interp1 = interpolator(f[1], f[2], direction(f[1]-1), direction(f[2]))
    dir_interp2 = interpolator(f[3], f[4], direction(f[3]-1), direction(f[4]))

    def up(frame):
        if f[0] <= frame and frame < f[1]:
            # a little trickiness here: Arrange to have up vector
            # pointed toward the origin when the "zoomed in on a
            # progenitor" shot _ends_)
            r = position(f[2])
            r[2] = 0.0
            return -normed(r)

        elif f[1] <= frame and frame < f[2]:
            # slide down into plane, 
            return up_interp(frame)
        
        elif f[2] <= frame and frame < f[3]:
            # steady view of origin
            return array([0,0,1.0])
        
        elif f[3] <= frame and frame < f[4]:
            # zoom in on remnant, moving with com
            return array([0,0,1.0])
            
        else:
            raise AssertionError

    up_interp = interpolator(f[1], f[2], up(f[1]-1), up(f[2]))
    
    def fov(frame):
        if f[0] <= frame and frame < f[1]:
            # move along with one galaxy, filling frame
            return 45.0/z0

        elif f[1] <= frame and frame < f[2]:
            # zoom out, interpolate between moving camera and stationary one
            return fov_interp1(frame)
        
        elif f[2] <= frame and frame < f[3]:
            # guess that I need 90 deg of fov to see both gals.
            return 1.8

        elif f[3] <= frame and frame < f[4]:
            # zoom in on remnant, moving with com
            return fov_interp2(frame)
        
        elif frame == f[4]:
            # final fov, value used in making interpolator
            return 22.5/mag(position(frame)-rf)
        
        else:
            raise AssertionError

    fov_interp1 = interpolator(f[1], f[2], fov(f[1]-1), fov(f[2]))
    fov_interp2 = interpolator(f[3], f[4], fov(f[3]-1), fov(f[4]))

    return position, direction, up, fov

import cPickle
def tabulatedAnim(fn):
    f=open(fn, 'rb')
    theData = cPickle.load(f)
    f.close()

    return (lambda x: theData[0][x], lambda x: theData[1][x],
            lambda x: theData[2][x], lambda x: theData[3][x])


#def snapshot(i):
#    f = [0, 100, 130, 205, 235, 310, 340, 540, 840, 1140, 1741]
#    # map frame number to snapshot number
#    if f[0] <= i < f[1]:   return i
#    elif f[1] <= i < f[6]: return f[1]
#    else:                  return i - (f[6]-f[1])

# def snapshot(i):
#     f = [0, 100, 130, 205, 235, 435, 735, 1035,
#          1635 - 17, 1665 - 17, 2025 - 17, 2055 - 17]

#     # map frame number to snapshot number
#     if   f[0]   <= i < f[1]-3:     return i
#     elif f[1]-3 <= i < f[1]+7:
#         s0 = snapshot(f[1] + 7)
#         if   f[1]-3 <= i < f[1]-2: return s0 - 4
#         elif f[1]-2 <= i < f[1]:   return s0 - 3
#         elif f[1]   <= i < f[1]+3: return s0 - 2
#         elif f[1]+3 <= i < f[1]+7: return s0 - 1
#         else: raise AssertionError
#     elif f[1]+7 <= i < f[4]-6:     return f[1]
#     elif f[4]-6 <= i < f[4]+4:
#         s0 = snapshot(f[4]-7)
#         if   f[4]-6 <= i < f[4]-2: return s0 + 1
#         elif f[4]-2 <= i < f[4]+1: return s0 + 2
#         elif f[4]+1 <= i < f[4]+3: return s0 + 3
#         elif f[4]+3 <= i < f[4]+4: return s0 + 4
#         else: raise AssertionError
#     elif f[4]+4 <= i < f[8]-3:
#         print "blah"
#         return i - (f[4]-f[1])

#     elif f[8]-3 <= i < f[8]+7:
#         s0 = snapshot(f[8] + 7)
#         print "blow", s0
#         if   f[8]-3 <= i < f[8]-2: return s0 - 4
#         elif f[8]-2 <= i < f[8]:   return s0 - 3
#         elif f[8]   <= i < f[8]+3: return s0 - 2
#         elif f[8]+3 <= i < f[8]+7: return s0 - 1
#         else: raise AssertionError
#     elif f[8]+7 <= i < f[11] or i == f[11]: return 1483


# def snapshot(i):
#     f = [0, 100, 130, 205, 235, 435, 735, 1035,
#          1635 - 17, 1665 - 17, 2025 - 17, 2055 - 17]

#     # map frame number to snapshot number
#     if   f[0]   <= i < f[1]-2:     return i
#     elif f[1]-2 <= i < f[1]+4:
#         s0 = snapshot(f[1] + 4)
#         if   f[1]-2 <= i < f[1]-1: return s0 - 3
#         elif f[1]-1 <= i < f[1]+1: return s0 - 2
#         elif f[1]+1 <= i < f[1]+4: return s0 - 1
#         else: raise AssertionError
#     elif f[1]+4 <= i < f[4]-3:     return f[1]
#     elif f[4]-3 <= i < f[4]+3:
#         s0 = snapshot(f[4]-4)
#         if   f[4]-3 <= i < f[4]  : return s0 + 1
#         elif f[4]   <= i < f[4]+2: return s0 + 2
#         elif f[4]+2 <= i < f[4]+3: return s0 + 3
#         else: raise AssertionError
#     elif f[4]+3 <= i < f[8]-2:     return i - (f[4]-f[1])

#     elif f[8]-2 <= i < f[8]+4:
#         s0 = snapshot(f[8] + 4)
#         if   f[8]-2 <= i < f[8]-1: return s0 - 3
#         elif f[8]-1 <= i < f[8]+1: return s0 - 2
#         elif f[8]+1 <= i < f[8]+4: return s0 - 1
#         else: raise AssertionError
#     elif f[8]+4 <= i < f[11] or i == f[11]: return 1483

#     raise AssertionError


def snapshot(i):
    f = [0, 100, 130, 205, 235, 435, 735-230, 1035,
         1635 - 17, 1665 - 17, 2025 - 17, 2055 - 17]

    # map frame number to snapshot number
    if   f[0]   <= i < f[1]-2:     return i
    elif f[1]-2 <= i < f[1]+4:
        s0 = snapshot(f[1] + 4)
        if   f[1]-2 <= i < f[1]-1: return s0 - 3
        elif f[1]-1 <= i < f[1]+1: return s0 - 2
        elif f[1]+1 <= i < f[1]+4: return s0 - 1
        else: raise AssertionError
    elif f[1]+4 <= i < f[4]-3:     return f[1]
    elif f[4]-3 <= i < f[4]+3:
        s0 = snapshot(f[4]-4)
        if   f[4]-3 <= i < f[4]  : return s0 + 1
        elif f[4]   <= i < f[4]+2: return s0 + 2
        elif f[4]+2 <= i < f[4]+3: return s0 + 3
        else: raise AssertionError
    elif f[4]+3 <= i < f[8]-2:     return i - (f[4]-f[1])

    elif f[8]-2 <= i < f[8]+4:
        s0 = snapshot(f[8] + 4)
        if   f[8]-2 <= i < f[8]-1: return s0 - 3
        elif f[8]-1 <= i < f[8]+1: return s0 - 2
        elif f[8]+1 <= i < f[8]+4: return s0 - 1
        else: raise AssertionError
    elif f[8]+4 <= i < f[11] or i == f[11]: return 1483

    raise AssertionError
