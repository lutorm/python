# contains interface routines for pbs

import commands
import local_mcrxrun

# the ncpu keyword is "nodes" on a distr.mem. machine (eg jacquard)
# and "ncpus" on shared mem (eg. columbia) so this has to be a
# configurable

job_skeleton = """#!/bin/sh
#PBS -j oe
#PBS -o <outfile> 
#PBS -q <queue>
#PBS -l <ncpu_keyword>=<ncpus>
#PBS -l walltime=<limit_hours>
#PBS -V

if [ "x${PBS_O_WORKDIR}x" != "xx" ]
then
    cd $PBS_O_WORKDIR
else
    PBS_O_WORKDIR=.
fi
"""

release_command="qrls "
hold_command="qhold "
kill_command="qdel "
use_seconds=True

def submit_job(job):
    "Submits a job and returns the job id."
    output = commands.getstatusoutput ("qsub " + job)
    if output [0] != 0:
        # This means that the job did not submit properly
        print "Submission Failed: " +job+ ":"
        print output [1]

    # by default it just prints the jobid on the output, so this is easy
    # with pbs
    return output[1]
    
def read_job_status(jobidfile):
    "Reads a job ID from the jobidfile and queries for status. Will throw an exception if the job doesn't exist"
    f = open (jobidfile, 'r')
    jobid = f.read ().splitlines () [0]
    cmd = "qstat " + jobid
    output= commands.getoutput (cmd)
    #print output
    if output.find("Unknown Job Id")!=-1:
        raise ValueError,"Job ID not found"

    status = output.splitlines ()[local_mcrxrun.job_status_line].split()[local_mcrxrun.job_status_column]
    #print "status is",status
    return "%-2s"%status

# status letters returned from pbs
status_queued="Q "
status_running="R "
status_notqueued=""
status_held="H "

