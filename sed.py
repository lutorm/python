import pyfits, glob, pylab, numpy, copy as cpy, stats, vecops, types
from pylab import *
import pyxstart as pyx,pyxutil, pdb

def sed_generator(objects, logarithmic, sed_only=False):
    """Returns a generator that yields the (lambda,SED) arrays of the
    (file,column) tuples listed in objects. If logarithmic is true,
    then it returns the log of the SED. Specifying an integer for
    column results in the INTENSITY vector for that cell number being
    returned instead. If sed_only is true, only the sed array itself
    is returned"""

    for f,c in objects:
        ff=pyfits.open(f)
        iq=ff['INTEGRATED_QUANTITIES'].data
        lam=iq.field('lambda')
        if type(c)==type(1):
            # we want a grid cell instead
            int=ff['INTENSITY'].data
            sed=10.**int[:,c]
        else:
            sed=iq.field(c)
        if logarithmic:
            sed=log10(sed + 1e-20)
        if sed_only:
            yield sed 
        else:
            yield lam,sed
        

def extract_seddata(to_plot,relative=False):
    """A general SED extraction function. to_plot is a list of
    (file,column) tuples specifying the file/column of the SEDs to
    return. If relative is true, the SEDs are divided by the first
    (file,column). If you want to use several different reference SEDs
    in the same plot, to_plot can also be a list of such lists of
    (file,column). The return value is a list of
    (lambda,data,legendtxt) tuples."""

    seds=[]
    
    if type(to_plot[0]) == types.TupleType:
        # if it's only one list, encase it in another so we can treat
        # it uniformly
        to_plot = [to_plot]

    for group in to_plot:
        # we use the generator to read the SEDs
        sedgen = sed_generator(group, False)
        file_iter = iter(group)

        if relative:
            # load up reference SED
            refsed = sedgen.next()[1]
            file_iter.next()
            legtxt_suffix=""# rel. to %s, %s"%e.next()
            
        for s,f in zip(sedgen, file_iter):
            l,sed = s # s[0] is lambda, s[1] is sed

            legtxt= " %s, %s"%(f[0],f[1]) + \
                (legtxt_suffix if relative else "")

            if relative:
                seds.append((l, sed/refsed-1.0, legtxt))
            else:
                seds.append((l, sed*l, legtxt))
    return seds


def extract_sedsigma(to_plot, relative=False):
    """Like extract_seddata, except it will extract the mean and sigma for
    each set of (file,column) specifications. If relative is True, the
    results are relative to the first mean. If relative is true, the
    sigma calculation is done in logspace, ie the +- 1sigma will be
    10**(mean+sigma) and 10**(mean-sigma)."""

    seds=[]
    
    if type(to_plot[0]) == types.TupleType:
        # if it's only one list, encase it in another so we can treat
        # it uniformly
        to_plot = [to_plot]

    # we need a lambda vector, take it from first sed
    l = sed_generator(to_plot[0], False).next()[0]

    groups = iter(to_plot)
    if relative:
        g = groups.next()
        refmean,sigma,n = stats.sigma(sed_generator(g, not relative, True))
        seds.append((l, refmean/refmean, sigma/refmean))

    for g in groups:
        mean,sigma,n = stats.sigma(sed_generator(g, not relative, True))

        if relative:
            seds.append((l, mean/refmean, sigma/refmean))
        else:
            seds.append((l, mean, sigma))
    return seds


def plot_sedsigma(to_plot,mode='absolute',legendtxt=None):
    """Like plot_seds, but plots the +- 1 sigma spread as a shaded
    region. to_plot is a list of (file,column) tuples specifying the
    file/column of the SEDs to plot. If mode='relative mean', the SEDs
    are plotted compared to the first [(file,column)...] group. If
    mode='relative sigma', the SEDs groups are all normalized by their
    means, so you only see the relative error on the different
    groups. To get a legend, you must specify legendtxt manually,
    since it's difficult to construct a legend automatically from this
    spec."""

    seds=iter(extract_sedsigma(to_plot,mode[0]=='r'))
    
    cla()
    lines=[]        
    for l,mean,sigma in seds:

        if mode=='relative mean':
            lines.append(semilogx(l,mean))
            fill(vecops.make_loop(l,l),vecops.make_loop(mean+sigma, 
                                                        mean-sigma),
                 alpha=0.3,facecolor=lines[-1][0].get_color())
        elif mode=='relative sigma':
            lines.append(semilogx(l,mean/mean))
            fill(vecops.make_loop(l,l),vecops.make_loop(sigma/mean, 
                                                        -sigma/mean),
                 alpha=0.3,facecolor=lines[-1][0].get_color())
            
        else:
            lines.append(loglog(l,l*10**mean))
            fill(vecops.make_loop(l,l), \
                     vecops.make_loop(l*10**(mean+sigma), l*10**(mean-sigma)), \
                     alpha=0.3,facecolor=lines[-1][0].get_color())

    if legendtxt:
        pylab.legend(lines, legendtxt, 'lower right')
    grid(True)

def pyxplot_sedsigma(to_plot,mode='absolute',legendtxt=None, alpha=0.8,ytitle='Relative SED',xrange=[None,None],yrange=[None,None]):
    """Like plot_seds, but plots the +- 1 sigma spread as a shaded
    region. to_plot is a list of (file,column) tuples specifying the
    file/column of the SEDs to plot. If relative is true, the SEDs are
    plotted compared to the first (file,column). If you want to use
    several different reference SEDs in the same plot, to_plot can
    also be a list of such lists of (file,column). To get a legend,
    you must specify legendtxt manually, since it's difficult to
    construct a legend automatically from this spec. Note that if the
    shaded region clips on the top and bottom, things will get fucked
    up because the paths are then split."""

    seds=extract_sedsigma(to_plot,mode[0]=='r')
    xmin=xrange[0] if xrange[0] else min(seds[0][0])*0.999
    xmax=xrange[1] if xrange[1] else max(seds[0][0])*1.001
    print xmin,xmax,yrange
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(pos="br"),
                        x=pyx.graph.axis.log(min=xmin,max=xmax,title=r'$\lambda/\rm{m}$'),
                        y=pyx.graph.axis.lin(min=yrange[0],max=yrange[1],title=ytitle) if mode[0]=='r' \
                        else pyx.graph.axis.log(min=yrange[0],max=yrange[1],title=r'$\lambda L_\lambda/\rm{W}$') )

    # First pass - plot means
    ltxt =iter(legendtxt) if legendtxt else None
    plotdata=[]
    palette=pyx.color.linearpalette.Rainbow
    for l,mean,sigma in seds:

        if mode=='relative mean':
            plotdata.append(pyx.graph.data.array([l,mean-1],
                                                 x=1,y=2,
                                                 title=ltxt.next() if ltxt else None))
        elif mode=='relative sigma':
            plotdata.append(pyx.graph.data.array([l,mean/mean],
                                                 x=1,y=2,
                                                 title=ltxt.next() if ltxt else None))
        else:
            plotdata.append(pyx.graph.data.array([l,l*10**mean],
                                                 x=1,y=2,
                                                 title=ltxt.next() if ltxt else None))

    g.plot(plotdata,styles=[pyx.graph.style.line([pyx.style.linestyle.solid,
                                                    palette])])
    #pdb.set_trace()
    # pass 2 - now make the shaded areas using the color of the mean
    loops=[]
    ncol=max(len(seds)-1,1)
    for i,lms in enumerate(seds):
        l,mean,sigma=lms
        loop=[]
        if mode=='relative mean':
            loop.append(pyx.graph.data.array([l,mean+sigma-1],
                                                 x=1,y=2,
                                                 title= None))
            loop.append(pyx.graph.data.array([l,mean-sigma-1],
                                                 x=1,y=2,
                                                 title= None))
        elif mode=='relative sigma':
            loop.append(pyx.graph.data.array([l,sigma/mean],
                                                 x=1,y=2,
                                                 title= None))
            loop.append(pyx.graph.data.array([l,-sigma/mean],
                                                 x=1,y=2,
                                                 title= None))
        else:
            loop.append(pyx.graph.data.array([l,l*10**(mean-sigma)],
                                                 x=1,y=2,
                                                 title= None))
            loop.append(pyx.graph.data.array([l,l*10**(mean+sigma)],
                                                 x=1,y=2,
                                                 title= None))
        plotitems=g.plot(loop,
                     styles= \
                         [pyx.graph.style.line([pyx.style.linestyle.solid,
                                                pyx.style.linewidth.THIN,
                                                palette.getcolor(i*1.0/ncol)
                                                ,pyx.color.transparency(1.0)
                                                ])])
        loops.append(plotitems)
    # d now contains the plotitems, get paths
    g.finish()
    for i,l in enumerate(loops):
        area= (l[0].path << l[1].path.reversed()).normpath()
        area[-1].close()
        g.stroke(area, [pyx.deco.filled([palette.getcolor(i*1.0/ncol)
                                         ,pyx.color.transparency(alpha)
                                         ]),
                        pyx.style.linewidth.THIN,pyx.color.transparency(1.0),
                        palette.getcolor(i*1.0/ncol)])

    #pdb.set_trace()
    return g

def plot_seds(to_plot,relative=False,legend=True,legendtxt=None, clear=True):
    """A general SED plotting function. to_plot is a list of
    (file,column) tuples specifying the file/column of the SEDs to
    plot. If relative is true, the SEDs are plotted compared to the
    first (file,column). If you want to use several different
    reference SEDs in the same plot, to_plot can also be a list of
    such lists of (file,column)."""

    seds=extract_seddata(to_plot,relative)
    
    if clear:
        cla()
    lines=[]

    for l,s,t in seds:
        
        if relative:
            lines.append(semilogx(l,s))
        else:
            lines.append(loglog(l,s))

    if legend:
        pylab.legend(lines, legendtxt if legendtxt != None else \
                     [x[2] for x in seds],
                     'lower right')
    grid(True)

def pyxplot_seds(to_plot,relative=False,range=[.91e-7,1e-3,3e35,1e38],legend=True,legendtxt=None,ytitle=None, palette=pyx.color.gradient.Rainbow, graph=None):
    """A general SED plotting function. to_plot is a list of
    (file,column) tuples specifying the file/column of the SEDs to
    plot. If relative is true, the SEDs are plotted compared to the
    first (file,column). If you want to use several different
    reference SEDs in the same plot, to_plot can also be a list of
    such lists of (file,column). If a graph is supplied with the
    keyword graph, the plot will be done to that, otherwise a new
    graph is generated."""

    seds=extract_seddata(to_plot,relative)
    #pdb.set_trace()
    if graph!=None:
        print "Adding to graph"
        g=graph
    else:
        g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(pos="br"),
                            x=pyx.graph.axis.log(min=range[0],max=range[1],title=r'$\lambda/\rm{m}$'),
                            y=pyx.graph.axis.lin(title=ytitle,min=range[2],max=range[3]) if relative \
                            else pyx.graph.axis.log(title=r'$\lambda L_\lambda/\rm{W}$',min=range[2],max=range[3]) )

    plotdata=[]
    lt=iter(legendtxt) if legendtxt != None else []
    #pdb.set_trace()
    for l,s,t in seds:
        ltxt=pyxutil.texclean(t) if legendtxt==None else lt.next()
        plotdata.append(pyx.graph.data.array([l,s],
                                             x=1,y=2,
                                             title=ltxt if legend else None))
        
        print plotdata[-1].title
        
    g.plot(plotdata,styles=[pyx.graph.style.line([pyx.style.linestyle.solid,palette])])
    if relative:
        g.stroke(g.ygridpath(0))
    
    return g
    
def plot(files=glob.glob("mcrx*fits"), columns=['L_lambda_out0'],
         relative=False,range=[.91e-7,1e-3,3e35,1e38],legend=True,legendtxt=None):
    #print files
    cla()
    lines=[]
    legtxt=[]
    start=0
    startcol=0
    if relative:
        ff=pyfits.open(files[0])
        iq=ff['INTEGRATED_QUANTITIES'].data
        refsed=iq.field(columns[0])
        start=1
        if len(columns) > 1:
            startcol=1
        
    for f in files[start:]:
        ff=pyfits.open(f)
        iq=ff['INTEGRATED_QUANTITIES'].data
        l=iq.field('lambda')
        for c in columns[startcol:]:
            sed=iq.field(c)
            if relative:
                lines.append(semilogx(l,sed/refsed))
            else:
                lines.append(loglog(l,sed*l))
                #lines.append(pylab.plot(l,sed*l))
            
            if len(columns)>1:
                legtxt.append(f+' '+c)
            else:
                legtxt.append(f)
    print legtxt
    if legendtxt!=None:
        legtxt=legendtxt

    if not relative:
        axis (range)
    if legend:
        pylab.legend(lines,legtxt,'lower right')
    if len(columns)>1:
        titcol=''
    else:
        titcol=columns[0]
        
    if relative:
        title(titcol+' SEDs relative to '+files[0])
    else:
        title(titcol+' SEDs')
    xlabel(r'$\lambda/\rm{m}$')
    if not relative:
        ylabel(r'$\lambda L_\lambda/\rm{W}$')
    grid()



def sigma(files=[glob.glob("mcrx*fits")], column='L_lambda_out0',
          range=None,legend=None,relative=False,error=False,title=None,overplot=False):
    #print files
    if not overplot:
        clf()
    lines=[]
    n=0
    legendtxt=[]
    lines=[]
    refmean=None
    for s in files:
        mean,sigma,n = stats.sigma(sed_generator(s, column, not relative))
        l = pyfits.open(s[0])['INTEGRATED_QUANTITIES'].data.field('lambda')
          
        if error:
            lines.append(loglog(l,sigma/mean))
        else:
            if relative:
                if refmean==None:
                    refmean=mean
                    
                lines.append(pylab.semilogx(l,mean/refmean-1))
                fill(vecops.make_loop(l,l),vecops.make_loop((mean+sigma)/refmean-1, \
                                              (mean-sigma)/refmean-1), \
                     alpha=0.3,facecolor=lines[-1][0].get_color())
            else:
                lines.append(loglog(l,l*10**mean))
                if n>1:
                    fill(vecops.make_loop(l,l), \
                         vecops.make_loop(l*10**(mean+sigma), l*10**(mean-sigma)), \
                         alpha=0.3,facecolor=lines[-1][0].get_color())
                
                #legendtxt.append('\\verb@'+s[0]+'@')
        legendtxt.append(s[0])

    if title==None:
        if error:
            pylab.title(r'Fractional error ($\sigma$/mean)')
        elif relative:
            pylab.title('Relative mean SEDs with error')
        else:
            pylab.title('Mean SED')
            ylabel(r'$\lambda L_\lambda/\rm{W}$')
    else:
        pylab.title(title)
    if range==None:
        axis([min(l),max(l),
              gca().get_ylim()[0],
              gca().get_ylim()[1]])
    else:
        axis(range)
    xlabel(r'$\lambda/\rm{m}$')
    if legend!=None:
        pylab.figlegend(lines,legend,'lower right')
    else:
        pylab.figlegend(lines,legendtxt,'lower right')
        
    grid()
            

def imsigma(files, hdu='INTENSITY'):
    # sigma calc due to Knuth
    print 'files:',files
    S=None
    mean=None
    n=0
    for f in files:
        print 'Reading ',f
        sed=10.**pyfits.open(f)[hdu].data

        if S==None:
            # init arrays
            S=sed*0.
            mean=sed*0.
            
        n+=1
        delta=sed-mean
        mean = mean + delta/n
        S+= delta*(sed-mean)
        del sed

    sigma=sqrt(S/(n-1))

    return mean,sigma


def plot_lines(files=glob.glob("mcrx*fits"),column='L_lambda_out',
               legend=True,hold=False,legendtxt=None):
    if not hold:
        clf()
    lines=[]

    for f in files:
        ff=pyfits.open(f)
        iq=ff['INTEGRATED_QUANTITIES'].data
        l=iq.field('lambda')
        colnames=ff['INTEGRATED_QUANTITIES'].columns.names
        columns = [c for c in range(len(colnames)) \
                   if colnames[c].find(column)>=0]
        oii=[]
        ha=[]
        hb=[]
        for c in columns:
            s=iq.field(c)
            oii.append(line_luminosity(l,s,258))
            ha.append(line_luminosity(l,s,386))
            hb.append(line_luminosity(l,s,316))
            
        print ha
        print hb
        print oii
        lines.append(pylab.plot(array(ha)/array(hb),array(oii)/array(ha),'.'))
            
    grid()
    if legend:
        if legendtxt!=None:
            pylab.figlegend(lines,legendtxt,'lower right')
        else:
            pylab.figlegend(lines,files,'lower right')
    xlabel('Ha/Hb')
    ylabel('OII/Ha')

def line_luminosity(lam,sed,i):
#    return ((sed[i]-sed[i-1])*(lam[i]-lam[i-1]) +
 #           (sed[i+1]-sed[i])*(lam[i+1]-lam[i]))/2
    return ((sed[i]-sed[i-1])*(lam[i+1]-lam[i]))


def attenuation(files=glob.glob("mcrx*fits"), cameras=[0],
                astau=False,normalize=False, legend=True,
                range=[.89e-7,2e-6,1e-3,1]):
    """Plots attenuation curves (ie scatter/nonscatter) of the specified
    cameras and files."""
    #clf()
    lines=[]
    legendtxt=[]
    for f in files:
        ff=pyfits.open(f)['INTEGRATED_QUANTITIES'].data
        lam=ff.field('lambda')
        lv=argwhere(lam>0.55e-6)[0,0]
        print lv
        for c in cameras:
            attn=ff.field('L_lambda_scatter%d'%c)/ff.field('L_lambda_nonscatter%d'%c)
            if astau:
                tau=-log(attn)
                if normalize:
                    tau/=tau[lv]
                lines.append(semilogx(lam,tau))
            else:
                lines.append(loglog(lam,attn))

            legendtxt.append("%s camera %d"%(f,c))
    xlabel(r'$\lambda/\rm{m}$')
    if astau:
        title('Attenuation optical depths')
        if normalize:
            ylabel(r'$\tau/\tau_V$')
        else:
            ylabel(r'$\tau$')
        print "range is",range
        range=cpy.copy(range)
        junk=-log(range[2])
        range[2]=-log(range[3])
        range[3]=junk
        print "range is",range
    else:
        title('Attenuation')
        ylabel('Attenuation')
    if legend:
        figlegend(lines,legendtxt,'lower right')
    grid(True)
    print range
    axis(range)
