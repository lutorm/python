import csv,pdb, numpy

from pylab import *

def read_csv(file):
    r=csv.reader(open(file, 'r'))
    # header has keys, keys2, units
    keys=r.next()
    keys2=r.next()
    units=r.next()

    keys = [ k+' '+k2 for k,k2 in zip(keys,keys2)]
    d=dict()
    for k in keys:
        d[k]=[]

    for row in r:
        for k,v in zip(keys,row):
            try:
                d[k].append(float(v))
            except:
                d[k].append(v)

    # attempt to make numpy arrays
    for k in keys:
        d[k]=numpy.array(d[k])

    # get rid of RDAC2 fields
    for k in keys:
        if 'RDAC2' in k:
            del d[k]
        if 'RDAC1' in k:
            d[k.replace(' RDAC1','')] = d[k]
            del d[k]

    # synthesize power field
    maxrpm = 3400
    d['power'] = d['ERPM']/maxrpm*d['MAP']/1013
    return d

def leaningplot_data(data,x,decoration):
    plot(x,data['TC1'],'b'+decoration)
    plot(x,data['TC4'],'g'+decoration)
    plot(x,data['FuelF']*20,'r'+decoration)
    #plot(x,data['ERPM']/5,'k'+decoration)

def leaningplot():
    st=read_csv('Enigma_20170618_132439.CSV')
    p1=read_csv('Plenum run 1.CSV')
    p2=read_csv('Plenum run 2.CSV')
    p3=read_csv('Enigma_20180509_172922.CSV')

    st_x=arange(st['TC1'].shape[0])-142
    p1_x=arange(p1['TC1'].shape[0])-481
    p2_x=arange(p2['TC1'].shape[0])-421
    p3_x=arange(p3['TC1'].shape[0])-460

    clf()
    plot(p3_x,p3['TC1'],'b')
    plot(p3_x,p3['TC4'],'g')
    plot(p3_x,p3['FuelF']*20,'r')
    plot(p3_x,p3['ERPM']/5,'k')
    plot(st_x,st['TC1'],'b--')
    plot(st_x,st['TC4'],'g--')
    plot(st_x,st['FuelF']*20,'r--')
    plot(st_x,st['ERPM']/5,'k--')
    #plot(p2_x,p2['TC1'],'b--')
    #plot(p2_x,p2['TC4'],'g--')
    #plot(p2_x,p2['FuelF']*20,'r--')
    #plot(p2_x,p2['ERPM']/5,'k--')

    xlim(-30,40)
    grid()
    ylim(300,750)
    legend(['#1 EGT','#4 EGT','FF','RPM'])
    xlabel('time/s')
    title('Plenum test run vs stock and printed intake elbow')

def gami_spread():
    "Plot EGT vs fuel flow"
    
    st=numpy.loadtxt('Stock leaning.txt')
    pl=numpy.loadtxt('Plenum leaning 5.txt')
    #pl=numpy.loadtxt('Needleswap leaning.txt')
    #st=numpy.loadtxt('Plenum leaning.txt')
    #st=None

    clf()
    plot(pl[:,0],pl[:,1],'b+-')
    plot(pl[:,0],pl[:,2],'g+-')
    plot(pl[:,0],pl[:,3],'r+-')
    plot(pl[:,0],pl[:,4],'c+-')
    if st!=None:
        plot(st[:,0],st[:,1],'bx')
        plot(st[:,0],st[:,2],'gx')
        plot(st[:,0],st[:,3],'rx')
        plot(st[:,0],st[:,4],'cx')

    grid()
    legend(['#1','#2','#3','#4'])
    xlabel('Fuel flow (l/h)')
    ylabel('EGT (C)')
    title('EGT vs fuel flow')
