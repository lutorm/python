import pyfits, glob, pylab, pdb, os, pyx, Image, vecops, sed, pyxutil
import __builtin__ as pystd
from pylab import *
from numpy import *
import parse_html_table
import simulations_sql as sql
import patrik_sqlengine

sql.setup(patrik_sqlengine.metadata, transactional=False)
sql.m.bind.echo=True


# make comparisons between simulations and the SINGS sample

class flux_collection(object):
    """This object carries the data necessary to plot, so it's passed
    to the plotting routines."""
    def __init__(self, lambdaeff, fluxes, magnitudes, name, metadata={}):
        self.lambdaeff=lambdaeff
        self.fluxes=fluxes
        self.magnitudes=magnitudes
        self.name=name
        self.metadata=metadata

    def q_lambda(self, filter):
        return self.fluxes[filter]
    def q_nu(self, filter):
        return self.fluxes[filter]*self.lambdaeff[filter]**2/3e8
    def q_energy(self, filter):
        return self.fluxes[filter]*self.lambdaeff[filter]

def query_set(set, dust, base=None):
    """Returns a query that will pick out all magnitudes for a given
    simulation set. The query is joined with the supplied base query,
    if supplied."""
    if not base:
        base=sql.maq
    return base.join('object').filter_by(has_dust=dust).join(['object','camera','snapshot','simulation']).filter_by(runname=set)

def query_sim(base, sim):
    return base.join(['object','camera','snapshot','simulation','gadget_simulation']).filter_by(runname=sim)
    
def query_snapshot(base, snapnum):
    """Constructs a join to query a certain snapshot number, which
    means the sequential number, not the number in the filename."""
    return base.join(['object','camera','snapshot']).filter_by(number=snapnum)
    
def query_camera(base, cameranum):
    """Constructs a join to query a certain camera number."""
    return base.join(['object','camera']).filter_by(number=cameranum)
    
def load_sql_fluxes(query, name=None):
    """Loads fluxes for a set of sql Image_objects and puts it into a
    dict. (Like load_fluxes but uses sql to get data.)"""

    # get all magnitude objects in one big sql join
    mags=query.all()

    # get all the filters
    filters=sql.fiq.join('magnitudes').filter_by(object=mags[0].object).all()
    
    # lambdaeff is a dict mapping name to effective wavelength
    lambdaeff = dict(zip([f.name.replace('AB_','') for f in filters],
                         [f.eff_lambda for f in filters]))

    # put fluxes for objects in a dict of arrays indexed by filter name
    fluxes={}
    magnitudes={}
    for f in filters:
        fluxes[f.name.replace('AB_','')] = array([m.l_lambda_eff for m in mags if m.filter==f])
        magnitudes[f.name.replace('AB_','')] = array([m.ab_mag for m in mags if m.filter==f])
        #fluxes[f.name] = array([sql.maq.filter_by(object=o,filter_id=filters[0].id).one().l_lambda_eff for o in objects])

    # add solid angle metadata -- will be INCORRECT for anything but
    # the newmodels papers camera distribution.
    metadata={}
    omegamap=solid_angle_weight()
    metadata['omega']=array(map(lambda c: omegamap[c],
                                [m.object.camera.number for m in mags if m.filter==filters[0]]))

    metadata['camera'] = array([m.object.camera.number for m in mags if m.filter==filters[0]])
    metadata['theta'] = array([m.object.camera.theta for m in mags if m.filter==filters[0]])
    metadata['snapshot'] = array(["%s/%s/%s"%
                                  (m.object.camera.snapshot.simulation.gadgetrunname,
                                   m.object.camera.snapshot.simulation.runname,
                                   m.object.camera.snapshot.snapshot_file)
                                   for m in mags if m.filter==filters[0]])
    metadata['camera'] = array([m.object.camera.number for m in mags if m.filter==filters[0]])
    
    return flux_collection(lambdaeff, fluxes, magnitudes, name if name!=None else `query`, metadata=metadata)


def select_simulation_set(set, dust):
    """Selects all objects in simulations that were run with sunrise
    runname set and has has_dust=dust."""
    objects=sql.ioq.filter_by(has_dust=dust).join(['camera','snapshot','simulation']).filter_by(runname=set).all()
    return objects

def L_TIR(d):
    """Calculate L_TIR from Dale & Helou 02 based on Spitzer
    fluxes in L_nu_eff."""
    return 1.559*d.q_energy('MIPS24_SIRTF') + \
           0.7686*d.q_energy('MIPS70_SIRTF') + \
           1.347*d.q_energy('MIPS160_SIRTF')

def L_UV(data):
    """Calculate L_UV as
    Leff(fuv)*nueff(fuv)+Leff(nuv)*nueff(nuv) as in Dale et al
    07, but since Lnu*nu=Llambda*lambda we do that """
    return data.q_energy('FUV_GALEX') + data.q_energy('NUV_GALEX')

def beta(data):
    """Calculate UV slope as the ratio of galex NUV and FUV lambda fluxes"""
    return data.q_lambda('FUV_GALEX')/data.q_lambda('NUV_GALEX')

def Bendo_PAH8(data):
    """Calculates the 8um PAH surface brightness, stellar subtracted,
    using eq.7 in Bendo(08)."""
    PAH8 = data.q_nu('IRAC4_SIRTF') - 0.232*data.q_nu('IRAC1_SIRTF')
    # take care of oversubtraction
    PAH8[PAH8<1e-30]=1e-30
    return PAH8

def Bendo_24(data):
    """Calculates the stellar-subtracted 24um surface brightness using
    eq.8 in Bendo(08). (yeah it's not PAH but for conformity of naming
    with 8um one, we call it this.)"""
    S24= data.q_nu('MIPS24_SIRTF') - 0.032*data.q_nu('IRAC1_SIRTF')
    # take care of oversubtraction
    S24[S24<1e-30]=1e-30
    return S24

def Bendo_TIR(data):
    """Calculates TIR surface brightness using eq. 9 in Bendo (08)."""
    return 0.95*3e8*Bendo_PAH8(data)/data.lambdaeff['IRAC4_SIRTF'] + \
        1.115*3e8*Bendo_24(data)/data.lambdaeff['IRAC4_SIRTF'] + \
        data.q_energy('MIPS70_SIRTF') + data.q_energy('MIPS160_SIRTF')

def itemcomp(x,y):
    if x[1]==y[1]:
        return 0
    if x[1]<y[1]:
        return -1
    else:
        return 1
#fx = lambdaeff.items()
#fx.sort(itemcomp)
#lam=array([x[1] for x in fx])
#filters = [x[0] for x in fx]

def solid_angle_weight():
    """Hard-coded solid angle for the camera positions in the
    newmodels paper. Returns a dict that maps camera # to omega."""
    #return {0:0.1, 1: 0., 2:0., 3:0., 4:0., 5: 0.82, 6:0.55,
    #        7:0.82, 8:0., 9:0., 10:0., 11:0., 12:0.10}
    return {0:0.60, 1: 1.14, 2:1.14, 3:1.14, 4:1.17, 5: 0.82, 6:0.55,
            7:0.82, 8:1.17, 9:1.14, 10:1.14, 11:1.14, 12:0.60}

def load_fluxes(files,name=None, dust=True):
    """Loads fluxes from broadband files and put them into
    flux_collection."""
    mags=[]
    for f in files:
        fhdu=pyfits.open(f)['FILTERS']
        mags.append(fhdu.data)

    filtermap,cols,nscols,lambdaeff = find_filters_columns(fhdu)
    cols = cols if dust else nscols

    fluxes = {}
    metadata={}
    for f in filtermap.keys():
        fluxes[f] = array( reduce(lambda x,y:x+y,
                                  [[m[filtermap[f]].field(i) \
                                    for i in cols] \
                                   for m in mags], [] ))

    metadata['camera'] = array( reduce(lambda x,y:x+y,
                                       [[ i
                                          for i in range(len(cols))] \
                                        for m in mags], [] ))
    metadata['snapshot'] = array( reduce(lambda x,y:x+y,
                                         [[ f
                                            for i in range(len(cols))] \
                                          for f in files], [] ))

    # get surface angle weight based on camera -- this is hardcoded is
    # INCORRECT for simulations that don't use the camera positions
    # for the mappings paper simulations.
    omegamap=solid_angle_weight()

    metadata['omega']=array(map(lambda c: omegamap[c],
                                range(len(cols))*len(mags)
                                ))
    
    return flux_collection(lambdaeff,fluxes,None,name if name is not None else file, metadata=metadata)


def load_image_fluxes(files,name=None,cameras=None, rebin_by=1):
    """Loads the image pixel surface brightnesses from broadband files and
    puts them into flux_collection."""

    # get the mapping between filters and slices in the data cubes
    filtermap,cols,nscols,lambdaeff = \
        find_filters_columns(pyfits.open(files[0])['FILTERS'])

    # the number of cameras is the number of entries in cols
    if not cameras:
        cameras=range(len(cols))
    size=array(pyfits.open(files[0])['CAMERA0-BROADBAND'].data.shape[1:])/rebin_by

    fluxes = {}
    for f in filtermap.keys():
        fluxes[f] = \
            concatenate ( \
            [ concatenate (
                    [ vecops.rebin_factor(pyfits.open(ff)['CAMERA%d-BROADBAND'%c].data[filtermap[f],:,:],size).ravel() \
                             for c in cameras])
                  for ff in files])
            
    # the plotting routines barf if we have infs in the arrays
    # (because infs throw exceptions on math.log10 instead of
    # returning inf, which numpy.log10 does. Hence, we clean the arrays to only the set for which all fluxes are >0.
    v = iter(fluxes.values())
    ind=where(v.next()>0)[0]
    for vv in v:
        ind = intersect1d(ind, where(vv>0)[0])
    for f in fluxes.keys():
        fluxes[f] = fluxes[f][ind].astype(float64)

    return flux_collection(lambdaeff,fluxes,None,name if name is not None else file)

def singsplot(data, xfunc, yfunc, lines,legtxt, split_by=None):
    """Plots the sings data, optionally splitting the galaxies up in
    different types either by type or nuc."""
    if split_by==None:
        lines.append(loglog(xfunc(data), yfunc(data),'.', markersize=5))
        legtxt.append('SINGS galaxies')
    else:
        if split_by=="type":
            types=['S','I','E']
        elif split_by=="nuc":
            types=['SB','L','Sy']
        lines.append(loglog(xfunc(data), yfunc(data),
                            'k.', markersize=5))
        legtxt.append('SINGS other')

        for t in types:

            i=argin(data.metadata[split_by],t)
            lines.append(loglog(xfunc(data)[i], yfunc(data)[i],
                                '.', markersize=5))
            legtxt.append('SINGS '+t)

def extract_valid_data(xdata,ydata):
    """Returns the set of x,y points that have valid data points."""
    valid=intersect1d_nu(where(xdata==xdata)[0],
                         where(ydata==ydata)[0])
    xdata=xdata[valid]
    ydata=ydata[valid]
    return xdata,ydata,valid

def pyxsingsplot(g, data, xfunc, yfunc):
    """Plots the sings data."""
    # pick out the valid data values (some are NaN which means missing data)
    xdata,ydata,valid= extract_valid_data(xfunc(data),yfunc(data))

    n=xdata.shape[0]
    # make arrays of numerical type and nuc data
    types=[('S0-Sab',['S0','Sa','Sab']),
           ('Sb-Sc', ['Sb','Sbc','Sc']),
           ('Scd-Sm',['Scd','Sd','Sdm','Sm']),
           ('I',['I']),
           ('E',['E'])]
    nucs=['SB','L','Sy']

    typearr=zeros(n)
#     for t in range(len(types)):
#         i=argin(data.metadata['type'],types[t])
#         typearr[i]=(t*1.0+1)/(len(types)) 
    nucarr=zeros(n)
    for t in range(len(nucs)):
        i=argin(data.metadata['nuc'][valid],nucs[t])
        nucarr[i]=(t*1.0+1)/(len(nucs))

    # now map typearr/nucarr to the range in Hue that is visible
    # something like [0.10 - 0.8]. this gives orange, green, blue, purple
        #typearr=typearr*0.7+0.1
    nucarr=nucarr*0.7+0.1

    #splitby=(nucs,data.metadata['nuc'])
    #color_by=3
    splitby=(types,data.metadata['type'][valid])
    color_by=4
    size_by=5
    #pdb.set_trace()
    noind=arange(n)
    plotdata=[]
    for ttitle,ttypes in splitby[0]:
        # then loop over subtype
        ii=empty(0,dtype=int)
        for ttt in ttypes:
            ii=union1d(ii,where(splitby[1]==ttt)[0])
        noind=setxor1d(noind,intersect1d(noind,ii))

        if ii.shape[0]>0:
            plotdata.append(pyx.graph.data.array([xdata[ii],ydata[ii],
                                                  typearr[ii],nucarr[ii],
                                                  ones_like(ii)],
                                                 x=1,y=2,color=color_by,
                                                 size=size_by,
                                                 title=ttitle))
    if noind.shape[0]>0:
        plotdata.append(pyx.graph.data.array([xdata[noind],
                                              ydata[noind],
                                              typearr[noind],nucarr[noind],
                                              ones_like(noind)],
                                             x=1,y=2,color=color_by,size=size_by,
                                             title='other'))
    print "Color means",types if color_by==3 else nucs," and n/a as orange,green, blue, purple"
    #pdb.set_trace()
    g.plot(plotdata,
           [pyxutil.coloredsymbol(size=0.2, palette=pyx.color.gradient.Hue)])

def plot_one_many(data, xfunc, yfunc, sings_splitby, plot_sings=True):
    """Plots the supplied data using the x and y functions."""
    lines=[]
    legtxt=[]
    
    if not iterable(data):
        data=[data]

    for d in data:
        lines.append(loglog(xfunc(d),yfunc(d),'o'))
        legtxt.append(d.name)

    if plot_sings:
        s = read_sings(data[0].lambdaeff)
        singsplot(s,xfunc,yfunc,lines,legtxt,sings_splitby)

    return lines,legtxt

def densityplot_one_many(data, xfunc, yfunc, sings_splitby, plot_sings=True):
    """Plots the supplied data using the x and y functions as a 2D density
plot."""
    lines=[]
    legtxt=[]
    

    hist=None
    for d in data:
        h,bins=histogramdd((log10(xfunc(d)),log10(yfunc(d))),
                           bins=20)
        if hist:
            hist+=h
        else:
            hist=h

    imshow(hist,interpolation='nearest')

    if plot_sings:
        s = read_sings(data[0].lambdaeff)
        singsplot(s,xfunc,yfunc,lines,legtxt,sings_splitby)

    return lines,legtxt

def make_image(values, palette):
    img = Image.new("RGB", values.shape)
    pix = img.load()
    maxval=values.max()
    for x in range(values.shape[0]):
        for y in range(values.shape[1]):
            c = palette.getcolor(values[x,y]/maxval).rgb().color
            # must make it into a tuple
            pix[x,y]=(int(c['r']*255),int(c['g']*255),int(c['b']*255))
    return img
    
def pyx_densityplot(g, xdata, ydata, weights=None,
                    density_palette=pyx.color.gradient.Gray):
    """Plots the supplied data points as a density plot. The plot g
    must be ready to dolayout(), because we need the extents to map
    the image. If weights are supplied, they are used to determine the
    weighting of the data points."""
    
    g.dolayout()

    # because the bins are equal size on the graph, we need to
    # convert the data values to graph values, ie apply the axis
    # transformation. After this, the (valid) data values are in [0,1]
    hist,xbins,ybins=histogram2d([g.axes['x'].convert(v) for v in xdata ],
                                 [g.axes['y'].convert(v) for v in ydata ],
                                 bins=20,range=((0,1),(0,1)),
                                 weights= weights)

    # because the graph has y values increasing upward, we have to
    # flip the image in the y direction
    img = make_image(hist[:,::-1], density_palette)
    # we may want to upsample the image here because some
    # viewers (ie preview) will display a linearly interpolated image.

    # determine the geometry of the graph so we can insert the image
    # and put the bitmap into the graph
    ll=g.vpos(0,0)
    ur=g.vpos(1,1)
    g.insert(pyx.bitmap.bitmap(ll[0], ll[1], img, 
                               width=ur[0]-ll[0],height=ur[1]-ll[1]))

    return g

def pyx_densityplot_one_many(g, data, xfunc, yfunc, sings_splitby, 
                             plot_sings=True, plot_slugs=False,
                             density_palette=pyx.color.gradient.WhiteRed):
    """Plots the supplied data using the x and y functions as a density
    plot."""
    
    if iterable(data):
        print "Sorry: density plots can only be done with one dataset."
        raise 0

    # plot sings data
    if plot_sings:
        s = read_sings(data.lambdaeff)
        pyxsingsplot(g,s,xfunc,yfunc)

    if plot_slugs:
        sl= read_slugs(data)
        xsl,ysl,valid = extract_valid_data(xfunc(sl),yfunc(sl))
        g.plot(pyx.graph.data.array([xsl,ysl],
                                    x=1,y=2,title=sl.name),
               [pyx.graph.style.symbol(symbol=pyx.graph.style.symbol.circle,symbolattrs=[pyx.deco.stroked])])

    pyx_densityplot(g,xfunc(data),yfunc(data),weights= data.metadata['omega'] if data.metadata.has_key('omega') else None, density_palette=density_palette)
    
    return g

def pyx_plot_one_many(g, data, xfunc, yfunc, sings_splitby, plot_sings=True,plotstyle=[pyx.graph.style.symbol(symbolattrs=[pyx.color.gradient.Rainbow])],plot_slugs=False, **kwargs):
    """Plots the supplied data using the x and y functions."""
    
    if not iterable(data):
        data=[data]

    plotdata=[]
    for d in data:
        plotdata.append(pyx.graph.data.array([xfunc(d),yfunc(d)],
                                             x=1,y=2,title=d.name))

    g.plot(plotdata,styles=plotstyle)
    if plot_sings:
        s = read_sings(data[0].lambdaeff)
        pyxsingsplot(g,s,xfunc,yfunc)

    if plot_slugs:
        sl= read_slugs(data[0])
        xsl,ysl,valid = extract_valid_data(xfunc(sl),yfunc(sl))
        g.plot(pyx.graph.data.array([xsl,ysl],
                                    x=1,y=2,title=sl.name),
               [pyx.graph.style.symbol(symbol=pyx.graph.style.symbol.circle,symbolattrs=[pyx.deco.stroked])])

    return g

def pyx_d07_fig6(data, sings_splitby=None,density=False, **kwargs):
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                        x=pyx.graph.axis.log(min=0.09,max=2.0,title=r'f$_\nu$(70)/f$_\nu$(160)'),
                        y=pyx.graph.axis.log(min=0.1,max=200,title=r'$L_{TIR}/L_{UV}$'))

    f = pyx_densityplot_one_many if density else pyx_plot_one_many
    f(g, data,
      lambda d: d.q_nu('MIPS70_SIRTF')/d.q_nu('MIPS160_SIRTF'),
      lambda d: L_TIR(d)/L_UV(d),
      sings_splitby, **kwargs)
    return g

def d07_fig6(data, legend=True, sings_splitby=None):
    clf()

    lines,legtxt=plot_one_many(data,
                               lambda d: d.q_nu('MIPS70_SIRTF')/d.q_nu('MIPS160_SIRTF'),
                               lambda d: L_TIR(d)/L_UV(d),
                               sings_splitby)

    axis([0.09,2,0.1,200])
    xlabel(r'f$_\nu$(70)/f$_\nu$(160)')
    ylabel('L_TIR/L_UV')
    title('Figure 6 from Dale et al 07')
    if legend:
        figlegend(lines,legtxt,'lower right')
    grid(True)

def pyx_d07_fig12(data, sings_splitby=None,density=False,**kwargs):
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(pos='br'),
                        x=pyx.graph.axis.log(min=3,max=0.3,title=r'$L_\lambda(1500)/L_\lambda(2300)$'),
                        y=pyx.graph.axis.log(min=0.1,max=1e3,title=r'$L_{TIR}/\lambda L_\lambda(1500)$'))

    f = pyx_densityplot_one_many if density else pyx_plot_one_many
    f(g, data,
      lambda d: beta(d),
      lambda d: L_TIR(d)/d.q_energy('FUV_GALEX'),
      sings_splitby,**kwargs)
    return g


def d07_fig12(data, legend=True, sings_splitby=None):
    clf()

    lines,legtxt=plot_one_many(data,
                               lambda d: beta(d),
                               lambda d: L_TIR(d)/d.q_energy('FUV_GALEX'),
                               sings_splitby)

    axis([3,0.3,0.1,1e3])
    xlabel(r'L$_\lambda$(1500)/L$_\lambda$(2300)')
    ylabel(r'L_TIR/$\lambda$L$_\lambda$(1500)')
    title('Figure 12 from Dale et al 07')
    grid(True)
    if legend:
        figlegend(lines,legtxt,'lower right')
    
def pyxplot_fluxratios(data,legend=True,quantity=flux_collection.q_energy,
                    filters=['MIPS70_SIRTF','MIPS160_SIRTF','MIPS24_SIRTF','MIPS70_SIRTF'],
                    sings_splitby=None,xrange=[None,None],
                       yrange=[None,None], density=False, xlabel=None,
                       ylabel=None, **kwargs):
    """Make a PyX color-color plot, optionally as a density plot."""
    crap={}
    for k in ['height','ratio']:
        try:
            crap[k]=kwargs.pop(k)
        except:
            pass

    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(pos='br'),
                        x=pyx.graph.axis.log(min=xrange[0],max=xrange[1],title=xlabel if xlabel else pyxutil.texclean(filters[0]+'/'+filters[1])),
                        y=pyx.graph.axis.log(min=yrange[0],max=yrange[1],title=ylabel if ylabel else pyxutil.texclean(filters[2]+'/'+filters[3])), **crap)

    f = pyx_densityplot_one_many if density else pyx_plot_one_many
    f(g, data,
      lambda d: quantity(d,filters[0])/
      quantity(d,filters[1]),
      lambda d: quantity(d,filters[2])/
      quantity(d,filters[3]),
      sings_splitby, **kwargs)
    
    return g

def plot_fluxratios(data,legend=True,quantity=flux_collection.q_energy,
                    filters=['MIPS70_SIRTF','MIPS160_SIRTF','MIPS24_SIRTF','MIPS70_SIRTF'],
                    sings_splitby=None,plot_sings=True,clear=True):
    if clear:
        clf()

    lines=[]
    legtxt=[]

    lines,legtxt=plot_one_many(data,
                               lambda d: quantity(d,filters[0])/
                               quantity(d,filters[1]),
                               lambda d: quantity(d,filters[2])/
                               quantity(d,filters[3]),
                               sings_splitby,plot_sings)

    map(lambda x: x.set_fontsize(16), gca().get_xticklabels())
    map(lambda x: x.set_fontsize(16), gca().get_yticklabels())

    if legend:
        figlegend(lines,legtxt,'lower right')
    title(#data.qty_name[quantity]+ \
        'flux ratios')
        
    xlabel(filters[0]+'/'+filters[1])
    ylabel(filters[2]+'/'+filters[3])
    grid()
    return lines

def bendo_fig2(data, legend=True):
    clf()

    lines,legtxt=plot_one_many(data,
                               lambda d: Bendo_24(d)/1e-20,
                               lambda d: Bendo_PAH8(d)/Bendo_24(d),
                               None,plot_sings=False)

    axis([0.03,50,0.2,3])
    xlabel(r'I$_\nu$(24$\mu$m)/MJy sr$^{-1}$')
    ylabel(r'I$_\nu$(PAH 8$\mu$m)/I$_\nu$(24$\mu$m)')
    title('Figure 2 from Bendo et al 08')
    grid(True)
    if legend:
        figlegend(lines,legtxt,'lower right')

def pyx_bendo_fig2(data, density=False, **kwargs):
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                        x=pyx.graph.axis.log(min=0.03,max=50,title=r'I$_\nu$(24$\mu$m)/MJy sr$^{-1}$'),
                        y=pyx.graph.axis.log(min=0.2,max=3,title=r'I$_\nu$(PAH 8$\mu$m)/I$_\nu$(24$\mu$m)'))

    f = pyx_densityplot_one_many if density else pyx_plot_one_many
    f(g, data, \
          lambda d: Bendo_24(d)/1e-20, \
          lambda d: Bendo_PAH8(d)/Bendo_24(d), \
          None,plot_sings=False, **kwargs)
    return g

def bendo_fig5(data, legend=True):
    clf()

    lines,legtxt=plot_one_many(data,
                               lambda d: d.q_nu('MIPS160_SIRTF')/1e-20,
                               lambda d: Bendo_PAH8(d)/d.q_nu('MIPS160_SIRTF'),
                               None,plot_sings=False)

    axis([1,800,.003,.05])
    xlabel(r'I$_\nu$(160$\mu$m)/MJy sr$^{-1}$')
    ylabel(r'I$_\nu$(PAH 8$\mu$m)/I$_\nu$(160$\mu$m)')
    title('Figure 5 from Bendo et al 08')
    grid(True)
    if legend:
        figlegend(lines,legtxt,'lower right')
    
def pyx_bendo_fig5(data, density=False, **kwargs):
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                        x=pyx.graph.axis.log(min=1,max=800,title=r'I$_\nu$(160$\mu$m)/MJy sr$^{-1}$'),
                        y=pyx.graph.axis.log(min=0.003,max=0.05,title=r'I$_\nu$(PAH 8$\mu$m)/I$_\nu$(160$\mu$m)'))

    f = pyx_densityplot_one_many if density else pyx_plot_one_many
    f(g, data, \
          lambda d: d.q_nu('MIPS160_SIRTF')/1e-20, \
          lambda d: Bendo_PAH8(d)/d.q_nu('MIPS160_SIRTF'), \
          None,plot_sings=False, **kwargs)
    return g


def plot_seds(data,legend=True):

    clf()

    lines=[]
    legtxt=[]
    
    if not iterable(data):
        data=[data]

    for d in data:
        legtxt.append(d.name)

        # create wavelength-sorted list of filters
        sortw=argsort(d.lambdaeff.values())
        filters=array(d.lambdaeff.keys())[sortw]
        lam=array([d.lambdaeff[f] for f in filters])
        
        # loop over entries in data set
        for l in range(len(d.fluxes.values()[0])):
            print "object",l
            fluxes=array([d.fluxes[ff][l]*d.lambdaeff[ff]
                           for ff in filters])
            lines.append(loglog(lam,
                                fluxes,#/d.fluxes['IRAC4_SIRTF'][l],
                                '-',linewidth=2))
            legtxt.append(f)
            
    if legend:
        figlegend(lines,legtxt,'lower left')
    title('SEDs')
    xlabel(r'$\lambda$/m')
    ylabel(r'$\lambda L_\lambda$')
    grid()

def find_best_fit(data,require_850=True):
    """Goes through the SINGS galaxies and finds the galaxy that best
    fits any of the data (thinking that data are the same galaxy for
    different inclinations and times)."""
    
    s = read_sings(data.lambdaeff)

    # turn the sings data into one big array. we normalize the fluxes
    # by the K-band flux
    ngals = len(s.fluxes.values()[0])
    nbands = len(s.fluxes.keys())
    sfluxes = zeros((ngals, nbands))
    # error array is only used to mask out missing data points
    serr = ones_like(sfluxes)

    # create wavelength-sorted list of filters
    sortw=argsort(s.lambdaeff.values())
    filters=array([f for f in array(s.lambdaeff.keys())[sortw] if f in s.fluxes.keys()])
    lam=array([data.lambdaeff[f] for f in filters])
    #pdb.set_trace()
    for g in range(ngals):
        sfluxes[g,:] =array([s.fluxes[ff][g]*s.lambdaeff[ff]
                      for ff in filters])
    kband=where(filters=='Ks_2MASS')[0]
    
    # normalize by K-band
    sfluxes/=sfluxes[:,kband]

    # set missing data points to 1 with huge error
    missing=where(sfluxes!=sfluxes)
    sfluxes[missing]=1
    serr[missing]=1e100
    npoints=sum(where(serr<1e99,1.,0.),axis=1)

    if require_850:
        # must have 850 and > 13 data points
        icanhas850=where((serr[:,-1]==1)&(npoints>=13))[0]
        # also mask out the bad galaxies from draine et al 07
        badgals=array(['NGC4254','NGC4579','NGC5194','NGC6946','NGC1097',
                       'NGC4321','NGC4736','NGC5033','NGC4594'])
        badgali=where(any(tile(s.metadata['name'],
                               (badgals.shape[0],1))==
                          tile(expand_dims(badgals,1),
                               (1,s.metadata['name'].shape[0])),
                          axis=0))[0]
        icanhas850=setxor1d(icanhas850, badgali)
        sfluxes=sfluxes[icanhas850,:]
        serr=serr[icanhas850,:]
        npoints=npoints[icanhas850]
        ngals=sfluxes.shape[0]
        s.metadata['name']=s.metadata['name'][icanhas850]
        s.metadata['type']=s.metadata['type'][icanhas850]
    
    # ok, we have sings seds. now we can evaluate chisq for the simulations
    ndata=len(data.fluxes.values()[0])
    minchi=1e100

    #pdb.set_trace()
    for g in range(ndata):
        ssed =array([data.fluxes[ff][g]*s.lambdaeff[ff]
                    for ff in filters])
        kflux=ssed[kband]
        ssed /=kflux

        # evaluate "reduced" chisq. (in log) for all the galaxies and find min
        chisq= sum((log10(sfluxes/ssed)/serr)**2,axis=1)/(npoints-1)
        i=chisq.argmin()
        print "%s camera %d"%(data.metadata['snapshot'][g],data.metadata['camera'][g])
        print "\tmin chisq %f,%f for galaxy %d,%s"%(chisq[i],npoints[i],i,s.metadata['name'][i])
        if chisq[i]< minchi:
            minchi=chisq[i]
            mingal=i
            minsim=g
            
    bestsnap=(data.metadata['snapshot'][minsim],
              data.metadata['camera'][minsim])
    print "Min chisq was %f for %d, %s camera %d"%(minchi,minsim,bestsnap[0], bestsnap[1])
    print "\tmin chisq %f for galaxy %d,%s"%(minchi,mingal,s.metadata['name'][mingal])

    # regenerate the data for the best fit
    ssed =array([data.fluxes[ff][minsim]*s.lambdaeff[ff]
                for ff in filters])
    
    mask=where(serr[mingal,:]<1e99)
    #pdb.set_trace()
    #loglog(lam[mask],sfluxes[i,mask][0,:],'+')
    #loglog(lam,sed)

    #sed.plot_seds([("%s.fits"%(bestsnap[0].replace('snapshot','mcrx')),
    #                "L_lambda_out%d"%bestsnap[1])],legend=False)

    mask=where(serr[mingal,:]<1e99)
    #loglog(lam[mask],sfluxes[mingal,mask][0,:]*ssed[kband])

    return bestsnap,lam[mask],sfluxes[mingal,mask][0,:]*ssed[kband],s.metadata['name'][mingal],s.metadata['type'][mingal],minsim

def read_sings_html(file, filters):
    d=parse_html_table.read(file)

    names=array([n.replace(' ','').upper() for n in d[0]])
    shuffle=argsort(names)
    
    # make dict for return, sorted in name order
    res={}
    for i in filters.keys():
        res[i]=d[filters[i]][shuffle]
    return res,names[shuffle]

def read_sings(lambdaeff):
    """Reads the sings fluxes in Janskys, ie L_nu_eff."""
    
    filters_uvvis = {'FUV_GALEX':2, 'NUV_GALEX':3, 'B_Johnson':4, \
                     'V_Johnson':5, 'R_Cousins':6, 'I_Cousins':7, \
                     'J_2MASS':8, 'H_2MASS':9, 'Ks_2MASS':10}
    res,names = read_sings_html(os.path.join(os.getenv("HOME"),
                                             'observational_data/sings_uvvis.html'),
                                filters_uvvis)

    filters_ir = {'IRAC1_SIRTF': 1, 'IRAC2_SIRTF': 2, 'IRAC3_SIRTF': 3,
                  'IRAC4_SIRTF': 4, 'MIPS24_SIRTF': 5, 'MIPS70_SIRTF': 6,
                  'MIPS160_SIRTF': 7, '850W_SCUBA': 9}
    res2,names2=read_sings_html(os.path.join(os.getenv("HOME"),
                                            'observational_data/sings_ir.html'),
                               filters_ir)
    # verify names are the same, ie tables are sorted identically
    assert (names==names2).all()
    #for n1,n2 in zip(names,names2):
    #assert n1==n2
    res.update(res2)
    

    # GALEX fluxes are in *milli*jansky, fix:
    res['FUV_GALEX']*=1e-3
    res['NUV_GALEX']*=1e-3

    # convert to lambda units to be consistent with our data
    for f in res.keys():
        if lambdaeff.has_key(f):
            res[f]/=lambdaeff[f]**2

    # add sample data, which is sorted differently...
    sample=read_sings_sampledata()
    i=argmatch(names, sample['name'])
    #for n1,n2 in zip(names,sample['name']):
    #assert n1==n2
    #pdb.set_trace()
    for k in sample.keys():
        sample[k]=sample[k][i]
    assert (names==sample['name']).all()
    
    return flux_collection(lambdaeff, res, None, 'SINGS galaxies', sample)

def read_sings_sampledata():
    """Reads the SINGS sample data so we can plot that too, and select
    different populations to compare with."""
    filename=os.path.join(os.getenv("HOME"),
                          'observational_data/sings_sample.txt')
    l=open(filename).readlines()
    s=[[lll.strip() for lll in ll.split(',')] for ll in l]

    # make sure they are in name-sorted order
    names=array([ss[0].replace(' ','').upper() for ss in s])
    shuffle=names.argsort()
    
    sample={}
    sample['name']=names[shuffle]
    sample['type']=array([ss[1] for ss in s])[shuffle]
    sample['nuc']=array([ss[2] for ss in s])[shuffle]
    
    return sample

    
def read_sings_old(file):
    f=open(file)
    lines=f.readlines()
    flux=[]
    i=0
    for l in lines:
        if len(l)>2:
            # remove title and then split
            tok=l.split('\t')
            #print tok
            try:
                flux.append([float(x.split()[0]) for x in tok[1:8] ])
            except:
                pass
                #print "failed at ",l
            #print flux[i,:]
            i+=1

    flux=array(flux)
    
    filters={'IRAC1_SIRTF': 0, 'IRAC2_SIRTF': 1, 'IRAC3_SIRTF': 2,
             'IRAC4_SIRTF': 3, 'MIPS24_SIRTF': 4, 'MIPS70_SIRTF': 5,
             'MIPS160_SIRTF': 6}

    # make dict for return
    res={}
    for i in filters.keys():
        res[i]=flux[:,filters[i]]
        
    return res

def find_filters_columns(fhdu):
    """Finds the names of the filters in fhdu, and returns the columns
    in the table corresponding to scatter and nonscatter values, as
    well as a dict mapping filter name to row number."""
    
    # find filters
    filternames=fhdu.data.field('filter')
    temp_lambdaeff = fhdu.data.field('lambda_eff')
    
    filtermap={}
    lambdaeff={} #use global
    
    for fff in range(len(filternames)):
        fnam = filternames[fff].replace('.res','')
        filtermap[fnam]=fff
        lambdaeff[fnam] = temp_lambdaeff[fff]
        
    #print filtermap
    
    # find columns
    colnames=fhdu.columns.names
    nscols = [c for c in range(len(colnames)) \
              if colnames[c].find('L_lambda_eff_nonscatter')>=0]
    cols = [c for c in range(len(colnames)) \
           if colnames[c].find('L_lambda_eff')>=0]
    cols = filter(lambda c: not c in nscols, cols)
    #print "Using scatter columns ",cols
    #print "Using nonscatter columns ",nscols

    return filtermap,cols,nscols,lambdaeff

def argmatch(a1, a2):
    """Returns an index array such that a2[index]==a1. Useful if you
    want to select and order an array to match another. If there are
    entries in a1 that do not exist in a2, or that are duplicated, an
    error is generated."""
    i=[]
    for a in a1:
        m=argwhere(a2==a)
        assert m.shape[0]==1
        i.append(m[0][0])
    return array(i)

def argin(a, substr):
    """Works like argwhere() but uses the string 'in' function to
    select the entries that have i as a substring. Only works with 1D
    arrays."""
    i=[]
    for ai in range(len(a)):
        if substr in a[ai]:
            i.append(ai)
    return array(i)

def fudge_fluxes(data, fudgefactor=[0.0, 0.25, 0.05, 0.05]):
    """Moves some energy around, for playing with what can fix the
    model problems."""

    # move some 70um to 24um
    denergy7024 = data.fluxes['MIPS70_SIRTF']*70*fudgefactor[0]

    # move some 70 to 160
    denergy70160 = data.fluxes['MIPS70_SIRTF']*70*fudgefactor[1]

    # move some 8 to 24
    denergy824 = data.fluxes['IRAC4_SIRTF']*8*fudgefactor[2]

    # move some 8 to 5.6
    denergy85 = data.fluxes['IRAC4_SIRTF']*8*fudgefactor[3]
    
    data.fluxes['MIPS70_SIRTF']-=denergy7024/70
    data.fluxes['MIPS24_SIRTF']+=denergy7024/24

    data.fluxes['MIPS70_SIRTF']-=denergy70160/70
    data.fluxes['MIPS160_SIRTF']+=denergy70160/160

    data.fluxes['IRAC4_SIRTF']-=denergy824/8
    data.fluxes['MIPS24_SIRTF']+=denergy824/24

    data.fluxes['IRAC4_SIRTF']-=denergy85/8
    data.fluxes['IRAC3_SIRTF']+=denergy85/5.6

    return data

def read_slugs(data):
    """Reads the SLUGS galaxy data from Chris's files."""
    path='/home/patrik/observational_data/slugs'
    
    filters = ['J_2MASS', 'H_2MASS', 'Ks_2MASS',
               'IRAC1_SIRTF', 'IRAC2_SIRTF', 'IRAC3_SIRTF',
               'IRAC4_SIRTF', '12_IRAS', 'IRS16_SIRTF', 'MIPS24_SIRTF',
               '25_IRAS', '60_IRAS', 'MIPS70_SIRTF',
               '100_IRAS', 'MIPS160_SIRTF', '450W_SCUBA', '850W_SCUBA',
               'something']
    wavelengths = array([ 1.2418, 1.648, 2.1622, 3.55, 4.493, 5.731, 7.872,
                          11.215, 15.777, 23.675, 23.3456, 59.3524, 71.42,
                          100.207, 155.9, 449.475, 850.0, 199766.64 ])
    
    files=os.listdir(path)
    print "Reading %d SLUGS data files"%len(files)

    fluxes={}
    metadata={}
    for f in filters:
        fluxes[f]=[]
    metadata.update([('name', []), ('z',[])])

    for fn in files:
        print "Opening file %s"%fn
        f=open(os.path.join(path, fn), 'r')
        # first metadata
        md=f.readline().split()
        metadata['name'].append(md[1])
        metadata['z'].append(float(md[3]))
        print "\t%s"%md[1]

        # add invalid data which are then overwritten with valid data
        # if they exist
        for fil in filters:
            fluxes[fil].append(nan)
            
        for l in f.readlines():
            d=l.split()
            lobs= float(d[3])
            # find filter
            filter=abs(wavelengths-lobs).argmin()
            #pdb.set_trace()
            print "\t",lobs,filter,filters[filter],d[1]
            fluxes[filters[filter]][-1] = float(d[1])


    # convert to lambda units
    for fil in filters:
        fluxes[fil]=array(fluxes[fil])
        if data.lambdaeff.has_key(fil):
            fluxes[fil]/=data.lambdaeff[fil]**2
        
    return flux_collection(data.lambdaeff, fluxes, {}, 'SLUGS', metadata=metadata)
