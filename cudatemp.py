from scipy.io.numpyio import fread
import numpy, pylab, pdb, pyx, pyxutil

def loaddata(filename):
    """Load a dump file from the cuda temp calculation. Returns a
    tuple containing lambda, size, heating, cpu heating, temperature,
    cpu temperature, sed and cpu sed arrays. The arrays are ordered
    (s,c) for the heating/temp arrays and (c,l) for the SEDs."""

    # if loading old files from the revisions used for the paper, some
    # fields are saved as single precision and you need to change 'd'
    # to 'f' for lamb, heating, temp, stemp and sed for the file to
    # load correctly.
    f=open(filename,'rb')
    nc,ns,nl=[int(x) for x in f.readline().split()]

    lamb=fread(f,nl,'d')
    sizes=fread(f,ns,'d')

    heating=fread(f,nc*ns,'d')
    heating=heating.reshape((ns,nc))

    sheating=fread(f,nc*ns,'d')
    sheating=sheating.reshape((ns,nc))

    temp=fread(f,nc*ns,'d')
    temp=temp.reshape((ns,nc))

    stemp=fread(f,nc*ns,'d')
    stemp=stemp.reshape((ns,nc))

    sed=fread(f,nc*nl,'d')
    sed=sed.reshape((nc,nl))

    ssed=fread(f,nc*nl,'d')
    ssed=ssed.reshape((nc,nl))

    return lamb,sizes,heating,sheating,temp,stemp,sed,ssed

def plot_sedsigma(sed,ssed,lamb):
    #a=sed/ssed
    # if ssed is all zero for some wavelengths then the weights are
    # singular and we get an error. in that case we know that the
    # numbers are crap anyway so it doesn't matter.

    #bad=numpy.where(numpy.sum(ssed,axis=0)==0)
    
    # this is eq to the ratio of the sums of the seds
    #mean=numpy.average(a,axis=0, weights=w)
    mean=numpy.sum(sed,axis=0)/numpy.sum(ssed,axis=0)
    #sigma=numpy.sqrt(numpy.var(a,axis=0))
    sumsq=numpy.sum(sed*sed/ssed, axis=0)/numpy.sum(ssed, axis=0)
    sigma=numpy.sqrt(sumsq-mean**2)
    pylab.semilogx(lamb,mean-1)
    pylab.semilogx(lamb,mean-1+sigma)
    pylab.semilogx(lamb,mean-1-sigma)
    
def plot_tempsigma(temp,stemp,sizes):
    a=temp/stemp
    mean=numpy.mean(a,axis=1)
    sigma=numpy.sqrt(numpy.var(a,axis=1))
    pylab.semilogx(sizes,mean-1)
    pylab.semilogx(sizes,mean-1+sigma)
    pylab.semilogx(sizes,mean-1-sigma)
    
def plot_guess(f,sz,a):
    if callable(a):
        n=a(sz)
    else:
        n=sz[s]**a
        #for s in numpy.linspace(0,sz.shape[0]-1,5):
        #s=int(s)
        #pylab.semilogx(f[s*200:(s+1)*200,5]/sz[s]**a,f[s*200:(s+1)*200,2],'.')
        #pylab.semilogx(f[s*200:(s+1)*200,1],f[s*200:(s+1)*200,2],'.')
    #pylab.loglog(f[s*200:(s+1)*200,5]/n[s*200:(s+1)*200],f[s*200:(s+1)*200,1],'.')    
    pylab.loglog(f[:,5]/n,f[:,1],'.')
    pdb.set_trace()
        #pylab.semilogx(f[s*200:(s+1)*200,5]/n,f[s*200:(s+1)*200,1]/f[s*200:(s+1)*200,3],'.')

def pyxplot_sedsigma(sed,ssed,l, alpha=0.5,     
                     palette=pyx.color.grey.black):
    mean=numpy.sum(sed,axis=0)/numpy.sum(ssed,axis=0)
    #sigma=numpy.sqrt(numpy.var(a,axis=0))
    sumsq=numpy.sum(sed*sed/ssed, axis=0)/numpy.sum(ssed, axis=0)
    sigma=numpy.sqrt(sumsq-mean**2)

    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(pos="br"),
                        x=pyx.graph.axis.log(min=3e-6,
                                             title=r'$\lambda/\rm{m}$'),
                        y=pyx.graph.axis.lin(min=-.0025,
                                             max=.02,
                                             title='GPU SED/CPU SED -1'))

    # First pass - plot means
    plotdata=[]

    plotdata.append(pyx.graph.data.array([l,mean-1],
                                         x=1,y=2,
                                         title=None))
    
    g.plot(plotdata,styles=[pyx.graph.style.line([pyx.style.linestyle.solid,
                                                  palette])])

    #pdb.set_trace()
    # pass 2 - now make the shaded areas using the color of the mean
    loops=[]
    loop=[]
    i=0
    ncol=1
    loop.append(pyx.graph.data.array([l,mean+sigma-1],
                                     x=1,y=2,
                                     title= None))
    loop.append(pyx.graph.data.array([l,mean-sigma-1],
                                     x=1,y=2,
                                     title= None))
    plotitems=g.plot(loop,
                     styles= \
                         [pyx.graph.style.line([pyx.style.linestyle.solid,
                                                pyx.style.linewidth.THIN,
                                                #palette.getcolor(i*1.0/ncol),
                                                pyx.color.transparency(1.0)
                                                ])])
    loops.append(plotitems)
    # d now contains the plotitems, get paths
    g.finish()
    for i,l in enumerate(loops):
        area= (l[0].path << l[1].path.reversed()).normpath()
        area[-1].close()
        g.stroke(area, [pyx.deco.filled([#palette.getcolor(i*1.0/ncol),
                                         pyx.color.transparency(alpha)
                                         ]),
                        #palette.getcolor(i*1.0/ncol),
                        pyx.style.linewidth.THIN,pyx.color.transparency(1.0)
                        ])

    #pdb.set_trace()
    g.writePDFfile('sedsigma.pdf')
    return g

def pyxplot_speed():
    t=numpy.loadtxt('size_test_091208_times.txt')
    g=numpy.loadtxt('size_test_091208_gputot_times.txt')
    cputimes=t[4::5,3]
    gputimes=g[:,3]
    # last entry in times is for cpu interpolation
    ncells=numpy.array([2**x for x in range(5,20)])
    print ncells
    
    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(pos="tr"),
                        x=pyx.graph.axis.log(
                                             title=r'Number of cells'),
                        y=pyx.graph.axis.log(
                                             title='Time per cell/ms'))
    plotdata=[]
    plotdata.append(pyx.graph.data.array([ncells,cputimes[:-1]/ncells],
                                         x=1,y=2,
                                         title='CPU'))
    plotdata.append(pyx.graph.data.array([ncells,gputimes[:-1]/ncells],
                                         x=1,y=2,
                                         title='GPU'))
    
    g.plot(plotdata, styles=[pyx.graph.style.line()])
    g.plot(pyx.graph.data.array([ncells[-1:],cputimes[-1:]/ncells[-1:]],
                                         x=1,y=2,
                                         title='CPU (interpolation)'),
           styles=[pyx.graph.style.symbol()])
    #pdb.set_trace()
    g.writePDFfile('speedpercell.pdf')
    return g


