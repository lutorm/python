from pylab import *
from numpy import *
import scipy.linalg
import pdb

def scalar_triple(a,b,c):
    """Computes the scalar triple product of vectors a,b & c."""
    return dot(a, cross(b, c))

def plane_ray_intersection(plane, line):
    """Returns the intersection point between the plane, specified
    through the implicit equation n.(x-x0)=0 when plane[0] is the
    normal vector and plane[1] is a point in the plane. the line is
    specified as x(l)=line[0]*l + line[1]. The value returned is the l
    parameter of the intersection point."""
    d=dot(line[0], plane[0])
    if d==0:
        # plane parallel with line, no intersection
        return None
    else:
        return dot(plane[1] - line[1], plane[0])/d

class delaunay_grid(object):
    """Represents an unstructured grid made up of a delaunay
    tesselation. pts is the array of points and tri is the array of
    the indices of the nodes in the tetrahedrons making up the
    delaunay tesselation. """

    def __init__(self, pointsfile, trifile):
        self.pts, self.tri = self.read(pointsfile, trifile)
        # generate tetrahedra
        self.cells = [ tetrahedron(self, t) \
                       for t in range(self.tri.shape[0])]
        self.reconstruct_densities()
        
    def read(self, pointsfile, trifile):
        """Reads points and triangulation from files and returns as arrays."""
        p=open(pointsfile).readlines()
        app=array([[float(y) for y in x.split()] for x in p[2:]])
        t=[x.split() for x in open(trifile).readlines()[1:]]
        atri=array([(int(x[1]),int(x[2]),int(x[3]),int(x[4])) for x in t])
        return app,atri

    def density_estimate(self, p):
        """Estimates density at point p as the inverse volume of the
        contiguous Voronoi cell per DTFE paper."""

        # find tetrahedra which have p as vertex
        tt=argwhere(self.tri==p)[:,0]
        V=reduce(add,map(lambda i: self.cells[i].volume(),
                         argwhere(self.tri==p)[:,0]))
        return 4./V

    def reconstruct_densities(self):
        """Returns the density and gradient arrays that makes it possible
        to interpolate density in the cells."""
        self.densities=array([self.density_estimate(p) for p in range(self.pts.shape[0])])
        self.gradients=zeros(shape=(self.tri.shape[0],3))
        for t in range(self.tri.shape[0]):
            self.gradients[t,:] = self.cells[t].calculate_gradient()[:]


class triangle(object):
    """A triangle with 3 vertices represented as their indices."""

    def __init__(self, vids, grid):
        """vids is the point indices of the vertices into the grid.pts array."""
        self.grid = grid
        self.vids = vids

    def vertices(self):
        """Returns vertices of tetrahedron."""
        return self.grid.pts[self.vids,:]
        
    def intersect_line(self, line):
        """Calculates the whether the line intersects the triangle,
        and if so returns the line parameter of the intersection
        point. line is defined by x(l)=line[0]*l+line[1]."""

        # translate so that the ray starts at origin
        xv=self.vertices()-line[1]
        c0=cross(xv[0,:], xv[1,:])
        c1=cross(xv[1,:], xv[2,:])
        c2=cross(xv[2,:], xv[0,:])
        
        Q0=dot(line[0], c0)
        Q1=dot(line[0], c1)
        Q2=dot(line[0], c2)
        # if all have same sign, intersect
        if Q0*Q1>0 and Q1*Q2>0:
            # calculate intersection point.
            n=c0+c1+c2
            return plane_ray_intersection( (n, xv[0,:]),
                                           (line[0], zeros(shape=(1,3))))
        else:
            return None
            
        
        
    def draw(self):
        """Returns the line segments of a triangle consisting of the 3
        points indexed in index."""
        loop=list(self.vids)+[self.vids[0]]
        print loop
        return ([self.grid.pts[i,0] for i in loop ],
                [self.grid.pts[i,1] for i in loop ],
                [self.grid.pts[i,2] for i in loop ])

class tetrahedron(object):
    """A tetrahedron in the delaunay grid."""

    triangle_ordering=array([(0,1,2),(0,3,1),(0,2,3),(1,3,2)])

    def __init__(self, grid, tid):
        """Creates a tetrahedron object respresenting the tid
        tetrahedron in grid."""
        self.grid = grid
        self.tid = tid
        self.tri = self.triangulize()
        
    def vertices(self):
        """Returns vertices of tetrahedron."""
        return self.grid.pts[self.grid.tri[self.tid,:],:]

    def gradient(self):
        return self.grid.gradients[self.tid]
    
    def volume(self):
        """Returns the volume of the tetrahedron."""
        return tetrahedron_volume(self.vertices())

    def triangulize(self):
        """Returns a list of triangles that make up the faces of the
        tetrahedron, given the 4 indices of the nodes."""

        return [ \
            triangle(array([ self.grid.tri[self.tid,i] for i in j]),
                     self.grid) for j in self.triangle_ordering ]

    def calculate_gradient(self):
        """Calculates the density gradient inside the tetrahedron t based
        on DTFE."""
        
        drho = self.grid.densities[self.grid.tri[self.tid, 1:]] - \
               self.grid.densities[self.grid.tri[self.tid, 0 ]]
        xv=self.vertices()
        dx=xv[1:,:]-xv[0,:]
        try:
            grad = scipy.linalg.solve(dx,drho)
        except LinAlgError:
            # singular matrix -- cell has no volume. we can set an
            # arbitrary gradient because it will not affect output
            assert volume(t,tri,pts)<1e-10
            grad=0.0*drho
        return grad

    def density(self, x):
        """Returns the interpolated density at point x."""
        # rho(x) = rho(x0) + (x-xo).g
        x0=self.vertices()[0,:]
        return self.grid.densities[self.grid.tri[self.tid,0]] + \
               dot(x-x0, self.gradient())

    def contains(self, x):
        """Tests if the point x is contained within the
        tetrahedron by testing the signs of 4 determinants."""
        xv=self.vertices()
        M0=ones((4,4))
        M0[:,:-1]=xv
        D0 = det(M0)
        if D0==0:
            # degenerate
            return False

        M1=ones((4,4))
        M1[[1,2,3],:-1] = xv[1:,:]
        M1[0,:-1]=x
        D1=det(M1)

        M2=ones((4,4))
        M2[[0,2,3],:-1] = xv[[0,2,3],:]
        M2[1,:-1]=x
        D2=det(M2)

        M3=ones((4,4))
        M3[[0,1,3],:-1] = xv[[0,1,3],:]
        M3[2,:-1]=x
        D3=det(M3)

        M4=ones((4,4))
        M4[:-1,:-1] = xv[:-1,:]
        M4[3,:-1]=x
        D4=det(M4)

        if D1*D2*D3*D4==0:
            # point is on boundary
            return None
        if D0*D1>0 and D0*D2>0 and D0*D3>0 and D0*D4>0:
            # all have same sign -- inside
            return True
        else:
            return False

    def intersect(self, line):
        """Tests for an intersection between the line and the
        tetrahedron faces. Returns None if no intersection, otherwise
        the parameters of the line at the intersection point(s)."""
        

    def density_on_line(self, line):
        """Returns the coefficients for how rho(l) varies on the line
        given by x(l)=line[0]*l+line[1]. Within the tetrahedron, we
        have rho(x)=rho(x0)+g.(x-x0), so the result is that
        rho(l)=rho(x0)+g.(line[1]-x0)+(g.line[0])*l."""
        return ( dot(self.gradient(),line[0]), \
                 self.grid.densities[self.grid.tri[self.tid,0]] + \
                 dot(self.gradient(), line[1]-self.vertices()[0,:]) )

def tetrahedron_volume(x):
    """Calculates volume of tetrahedron whose vertices are given by
    the 4x3 array x."""
    V=abs( dot( x[0,:] - x[3,:],
                cross( x[1,:] - x[3,:],
                       x[2,:] - x[3,:]))
           )/6.0
    return V





