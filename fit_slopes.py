from scipy.optimize import *
import numpy,pdb, vecops

def line(par,x):
    return par[0]*x+par[1]

def line_res(par,x,y,sigma):
    """Returns residuals from a line for use with leastsq."""
    err = (y-line(par,x))/sigma
    return err

def fit_slope(lam, L_lambda):
    """Fits the spectrum to a power law and returns the slope"""
    p0=[1.,1.]

    # power law fit is lin in log-space
    p=leastsq(line_res, p0, args = (numpy.log10(lam),numpy.log10(L_lambda),1.0))
    #print "fit returns",p[0][0]
    return p[0][0]

def delta(x):
    """Calculate the delta-x values associated with each x. x must be
    in ascending order."""
    i=numpy.where(x<x[-2])[0]
    d=numpy.empty(x.shape, dtype=numpy.double)
    d[list(i+1.)]=(x[list(i+2.)]-x[list(i)])/2
    d[0] = x[1]-x[0]
    d[-1] = x[-1] - x[-2]
    return d

def calculate_luminosity(lam, L_lambda):
    """Integrate the luminosity over the supplied interval"""
    lum = vecops.integrate_array(lam, L_lambda, False)
    return lum

def line_luminosity(lam, L_lambda, line_lambda):
    """Calculates line luminosity and equivalent width. Assumes line
    is contained in one bin. Assumes the bins are like Brent described
    in his email, ie that bin i has the central wavelength lam[i] and
    width lam[i+1]-lam[i]."""
    
    temp = numpy.where(lam>line_lambda)[0]
    i=temp[0]
    # now test whether i or i-1 is closest to line center and pick
    # that
    if abs(line_lambda - lam[i-1]) < abs(line_lambda - lam[i]):
        i=i-1

    # now we need a continuum. we use the minimum of the three points on
    # either side to fit a line. this is so we handle NII which is in
    # the bin next to Ha, and Hbeta which is in the middle of the
    # Hbeta abs line.
    left = L_lambda[i-3:i].argmin() + i-3
    right = L_lambda[i+1:i+4].argmin() + i+1
    #left = L_lambda[i-2]<L_lambda[i-1] and i-2 or i-1
    #right = L_lambda[i+2]<L_lambda[i+1] and i+2 or i+1
    #pdb.set_trace()
    
    # evaluate continuum lum at line center based on straight line fit
    cont = (L_lambda[right]-L_lambda[left])/(lam[right]-lam[left])* \
           (lam[i]-lam[left]) + L_lambda[left]
    delta = (lam[i+1] - lam[i])

    line_lum = (L_lambda[i]-cont)*delta
    
    return line_lum, line_lum/cont

def calculate_slopes(file):

    bM95_lambda_lower = [1.268e-7,1.309e-7,1.342e-7,1.407e-7,1.562e-7,1.677e-7,1.760e-7,1.866e-7,1.930e-7,2.400e-7]
    bM95_lambda_upper = [1.284e-7,1.316e-7,1.371e-7,1.515e-7,1.583e-7,1.740e-7,1.833e-7,1.890e-7,1.950e-7,2.580e-7]

    bH98_lambda_lower= [1.250e-7]
    bH98_lambda_upper= [1.850e-7]

    d = file['INTEGRATED_QUANTITIES'].data
    lam = d.field('lambda')

    # assemble ranges over which to fit
    bM95_index=[]
    for l in zip(bM95_lambda_lower, bM95_lambda_upper):
        bM95_index+=list(numpy.where((lam > l[0]) & (lam < l[1]))[0])

    bH98_index=[]
    for l in zip(bH98_lambda_lower, bH98_lambda_upper):
        bH98_index+=list(numpy.where((lam > l[0]) & (lam < l[1]))[0])

    ret={}
    ret['betaM95']=[]
    ret['betaM95_ns']=[]
    ret['betaH98']=[]
    ret['betaH98_ns']=[]
    ret['l_tir']=[]
    ret['l_tir_ns']=[]
    ret['l_1600']=[]
    ret['l_1600_ns']=[]
    ret['l_1900']=[]
    ret['l_1900_ns']=[]
    cam=0
    while True:
        # loop until we hit a nonexistent camera
        try:
            L_lambda = d.field('L_lambda_scatter'+`cam`)
            # try to add the ir sed if it exists
            try:
                L_lambda += d.field('L_lambda_ir'+`cam`)
            except:
                pass
        except:
            break
        ret['betaM95'].append(fit_slope(lam[bM95_index],L_lambda[bM95_index]))
        ret['betaH98'].append(fit_slope(lam[bH98_index],L_lambda[bH98_index]))
        ret['l_tir'].append(tir_luminosity(lam, L_lambda))
        ret['l_1600'].append(l1600(lam, L_lambda))
        ret['l_1900'].append(l1900(lam, L_lambda))
        
        L_lambda = d.field('L_lambda_nonscatter'+`cam`)
        ret['betaM95_ns'].append(fit_slope(lam[bM95_index],L_lambda[bM95_index]))
        ret['betaH98_ns'].append(fit_slope(lam[bH98_index],L_lambda[bH98_index]))
        ret['l_tir_ns'].append(tir_luminosity(lam, L_lambda))
        ret['l_1600_ns'].append(l1600(lam, L_lambda))
        ret['l_1900_ns'].append(l1900(lam, L_lambda))

        cam+=1

    return ret


def line_luminosities(lam,lum):
    """Returns a dict of a bunch of lines we calculate."""
    line_names = [
        # first hydrogen lines
        ('H Alpha',   'H', 6562.81e-10),
        ('H Beta',     'H', 4861.32e-10),
        ('H Gamma',    'H', 4340.46e-10),
        ('H Delta',    'H', 4104.73e-10),
        ('H Epsilon',  'H', 3970.07e-10),
        ('Pa Beta',    'H', 1.28181e-6),
        ('Br Gamma',   'H', 2.16550e-6),
        # then others
        ('OIII 4959',  'O', 4958.83e-10),
        ('OIII 5007',  'O', 5006.77e-10),
        ('OII 3727',   'O', 3727.00e-10), # doublet is unresolved in mappings
        ('NII 6548',   'Ne',6547.96e-10), 
        ('NII 6583',   'Ne',6583.34e-10),
        ('SIII 9069',  'S', 9069.29e-10),
        ('SIII 9532',  'S', 9532.03e-10)
        ]

    lines=[]
    for l in line_names:
        lines.append(line_luminosity(lam,lum,l[2]))

    return line_names,lines

def calculate_lines(file):
    d = file['INTEGRATED_QUANTITIES'].data
    lam = d.field('lambda')

    lines=[]
    lines_ns=[]
    cam=0
    while True:
        # loop until we hit a nonexistent camera
        try:
            L_lambda = d.field('L_lambda_scatter'+`cam`)
            # try to add the ir sed if it exists
            try:
                L_lambda += d.field('L_lambda_ir'+`cam`)
            except:
                pass
        except:
            break
        lines.append(line_luminosities(lam,L_lambda))

        L_lambda = d.field('L_lambda_nonscatter'+`cam`)
        lines_ns.append(line_luminosities(lam,L_lambda))
        cam+=1

    ret={}
    ret['lines'] = lines
    ret['lines_ns'] = lines_ns
    return ret

def binplot(x,y):
    """Plots the x,y values assuming y(i) is a bin value valid for
    x(i-1) to x(i)."""
    x2=empty(2*x.shape[0]-1,dtype=double)
    y2=empty(2*x.shape[0]-1,dtype=double)
    ix=arange(x.shape[0])
    # map x0,1,2,3,...,n-1,n -> x0,0,1,1,2,2,3,3,...,n-1,n-1,n
    x2[list(2*ix)]=x[list(ix)]
    x2[list(2*ix+1)[:-1]]=x[list(ix)]
    # map y0,1,2,3,...,n-1,n -> y0,1,1,2,2,3,3,...,n-1,n-1,n,n
    y2[list(2*ix)]=y[list(ix)]
    y2[list(2*ix[:-1]+1)]=y[list(ix[1:])]
    return (x2,y2)

def tir_luminosity(lam, L_lambda):
    """Integrates total infrared luminosity at wavelengths between 5-1000um."""
    i = list(numpy.where((lam>=5e-6) & (lam<=1e-3))[0])
    return calculate_luminosity(lam[i], L_lambda[i])

def l1600(lam, L_lambda):
    """Integrates luminosity at wavelengths around 1600A."""
    i = list(numpy.where((lam>=1.425e-7) & (lam<=1.775e-7))[0])
    if len(i)==1:
        return L_lambda[i[0]]*1.6e-7;
    else:
        return calculate_luminosity(lam[i], L_lambda[i])*1.6e-7/(1.775e-7-1.425e-7)

def l1900(lam, L_lambda):
    """Integrates luminosity at wavelengths around 1900A."""
    i = list(numpy.where((lam>=1.863e-7) & (lam<=1.963e-7))[0])
    if len(i)==1:
        return L_lambda[i[0]]*1.9e-7;
    else:
        return calculate_luminosity(lam[i], L_lambda[i])*1.9e-7/(1.963e-7-1.863e-7)
