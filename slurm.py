# contains interface routines for slurm

import re,commands

job_skeleton = """#!/bin/sh
#SBATCH --partition <queue>
#SBATCH -c <ncpus> --share
#SBATCH --output <outfile> 
#SBATCH --time <limit_hours>
"""


mcrx_job_skeleton="""
RESUBMIT_COMMAND=\"submit_and_print_job <jobfile> > <jobidfile>\"; export RESUBMIT_COMMAND
MCRX_PP_CMD=""; export MCRX_PP_CMD
WALL_CLOCK_LIMIT=`getwallclocklimit`; export WALL_CLOCK_LIMIT

echo RESTART

mcrx <configfile>
MCRXSTATUS=$?
if [ $MCRXSTATUS -eq 42 ]
then
    echo COMPLETE
    touch <completefile>
    submit_and_print_job <nextjob> > <nextjobidfile>
else
    echo FAILED!
fi
"""


release_command=""
hold_command=""
kill_command="scancel "
use_seconds=True

submission_regexp=re.compile(r"batch job (\d+)")

def submit_job(job):
    "Submits a job and returns the job id."
    output = commands.getstatusoutput ("sbatch " + job)
    if output [0] != 0:
        # This means that the job did not submit properly
        print "Submission Failed: " +job+ ":"
        print output [1]

    # extract job ID number from output
    try:
        jobid=submission_regexp.search(output[1]).group(1)
    except:
        print "Submission Failed: "+job+ ":"
        print output[1]
        return None
    return jobid

def read_job_status(jobidfile):
    "Reads a job ID from the jobidfile and queries for status. Will throw an exception if the job doesn't exist"
    f = open (jobidfile, 'r')
    jobid = f.read ().splitlines () [0]
    cmd = "squeue -j " + jobid
    output= commands.getoutput (cmd)
    status = output.splitlines ()[1].split()[4]
    return status

# status letters returned from pbs
status_queued="PD"
status_running="R "
status_notqueued=""
status_held="H "

