import xml
import xml.etree.ElementTree as ET
import numpy as np
import pdb

def read_table(file):
    "Reads an exported table from Megalogviewer. Returns a tuple of (xaxis, yaxis, table)."

    e = xml.etree.ElementTree.parse(file).getroot()

    ns={'efi':'http://www.EFIAnalytics.com/:table'}
    xaxis = get_axis(e.find("efi:table",ns).find("efi:xAxis",ns), False)
    yaxis = get_axis(e.find("efi:table",ns).find("efi:yAxis",ns), True)
    zvalues = get_table(e.find("efi:table",ns).find("efi:zValues",ns))

    return xaxis,yaxis,zvalues[::-1,:]

def get_axis(axis, isrow):
    size = int(axis.attrib['rows' if isrow else 'cols'])
    contents = axis.text.split()
    assert len(contents) == size

    data = np.array([float(x) for x in contents])
    return data

def get_table(table):
    cols = int(table.attrib['cols'])
    rows = int(table.attrib['rows'])
    # Cells with no data have nothing in them, so we need to infer
    # where they are from the spaces. Super annoying.
    contents=[]
    
    lines = table.text.split('\n')
    assert len(lines) == rows+2
    # First and last lines are empty
    for l in lines[1:-1]:
        # First 9 spaces are just padding, remove them
        l = l[9:]
        # now split on spaces, we should then have cols columns plus
        # one empty at the end
        elems = l.split(' ')
        assert len(elems) == cols+1

        row=[]
        for r in elems[:-1]:
            if r:
                row.append(float(r))
            else:
                row.append(float('nan'))
                
        contents.append(row)

    data = np.array(contents)
    return data

    
    

def write_table(file, data, outfile=file):
    ET.register_namespace('', 'http://www.EFIAnalytics.com/:table')
    tree = ET.parse(file)
    r = tree.getroot()
    ns={'efi':'http://www.EFIAnalytics.com/:table'}

    zvals = r.find("efi:table",ns).find("efi:zValues",ns)
    zvals.text = '\n'.join([ ' '.join([`c` for c in r]) for r in data[::-1]])

    tree.write(outfile, method="xml", xml_declaration=True, encoding="UTF-8")
    
    
