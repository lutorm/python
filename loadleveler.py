# contains interface routines for load leveler

import re, commands

job_skeleton = """#!/usr/bin/sh
#@ job_name	= <jobname> 
#@ account_no	= mp363
#@ output	= <outfile> 
#@ error	= <outfile> 
#@ job_type	= <jobtype>
<node>
#@ environment	= COPY_ALL
#@ notification	= error
#@ class	= <class>
#@ wall_clock_limit	=<limit_hours>

#@ queue
"""

release_command="llhold -r "
hold_command="llhold "
kill_command="llcancel "

submission_regexp=re.compile(r"The job +\"(\S+)\" +has been submitted")

def submit_job(job):
    "Submits a job and returns the job id."
    output = commands.getstatusoutput ("llsubmit " + job)
    if output [0] != 0:
        # This means that the job did not submit properly
        print "Submission Failed: " +job+ ":"
        print output [1]

    # extract job ID number from output
    jobid=submission_regexp.search(output[1]).group(1)
    return jobid

def read_job_status(jobidfile):
    "Reads a job ID from the jobidfile and queries for status. Will throw an exception if the job doesn't exist"
    f = open (jobidfile, 'r')
    jobid = f.read ().splitlines () [0]
    cmd = "llq -f %st " + jobid
    status = commands.getoutput (cmd).splitlines () [2]
    return status

# status letters returned by loadleveler
status_queued="I "
status_running="R "
status_notqueued="NQ"
status_held="HU"
