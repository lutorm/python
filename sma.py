import yasdi, heating, os, time, phant

sn = 1913044676
channels = [
        "Pac",
        "A.Ms.Amp",
        "B.Ms.Amp",
        "A.Ms.Vol",
        "B.Ms.Vol",
        "A.Ms.Watt",
        "B.Ms.Watt",
        "GridMs.PhV.phsA",
        "GridMs.PhV.phsB",
        "GridMs.Hz",
#        "GridMs.TotPF",
#        "Mode",
#        "Op.Health",
#        "Op.GriSwStt",
        "E-Total"
]

phant_fields = {
    'Inv_Pac': 'p_ac',
    'Inv_Pdc': 'p_dc',
    'Inv_strA_Vpanel': 'stra_u',
    'Inv_strA_Ipanel': 'stra_i',
    'Inv_strA_Ppanel': 'stra_p',
    'Inv_strB_Vpanel': 'strb_u',
    'Inv_strB_Ipanel': 'strb_i',
    'Inv_strB_Ppanel': 'strb_p',
    'Inv_phA_V': 'pha_u',
    'Inv_phB_V': 'phb_u',
    'Inv_freq': 'freq',
#    'Inv_pf': 'pf',
#    'Inv_mode': 'mode',
#    'Inv_health': 'health',
#    'Inv_grid_switch': 'switch',
    'Inv_tot_energy': 'etot'
    }
             

string_a_np = 6
string_b_np = 5
string_a_sets = 2
string_b_sets = 1

class sma:
    "Class for wrapping communication with SMA inverters."
    yasdi_drivers = []
    
    def __init__(self, inifile):
        if not sma.yasdi_drivers:
            sma.yasdi_drivers = yasdi.initialize(inifile)
        self.device_handle = None

    def __del__(self):
        yasdi.shutdown()
        
    def detect(self, sn):
        "Ask YASDI to detect the device with specified SN."
        
        device_handles = yasdi.detect(1)
        for d in device_handles:
            this_sn = yasdi.get_device_sn(d)
            if this_sn == sn:
                self.device_handle = d

        if not self.device_handle:
            print "Could not find requested device"

    def set_channels(self, channels):
        "Set up the list of channels to retrieve."
        self.channels = {}
        for c in channels:
            channel_handle = yasdi.get_channel_handle(self.device_handle, c)
            if channel_handle == 0:
                print "Channel %s not found"%c
            else:
                channel_name = yasdi.get_channel_name(channel_handle)
                channel_unit = yasdi.get_channel_unit(channel_handle)

                self.channels[channel_name] = (channel_handle, channel_unit)

    def get_values(self):
        "Get the spot values for the channels."
        values = {}
        for chname,ch in self.channels.items():
            try:
                val,txt = yasdi.get_channel_value(ch[0], self.device_handle)
                values[chname] = (val, txt)
            except IOError as e:
                print "Error getting inverter channel",chname
        return values
    
    def print_values(self, values):
        for chname,c in self.channels.items():
            h = c[0]
            print chname,values[h][0],values[h][1],c[1]

def inverter_log():
    os.chdir('/home/patrik/heating')
    cmd_port="/home/patrik/xbee_0x13a20040b18b7fL_cmd"

    initialized = False
    while not initialized:
        try:
            print "Opening YASDI"
            sb=sma('/home/patrik/.yasdi.ini')
            print "Detecting ",sn
            sb.detect(sn)
            print "Set channels"
            sb.set_channels(channels)
            initialized = True
        except KeyboardInterrupt:
            break
        except:
            del sb
        
    called_create = False

    datafuncs = [
    ('Inv_Pac', lambda val: val['Pac'][0]),
    ('Inv_Pdc', lambda val: val['A.Ms.Watt'][0]+val['B.Ms.Watt'][0]),
    ('Inv_strA_Vpanel', lambda val: val['A.Ms.Vol'][0]/string_a_np),
    ('Inv_strB_Vpanel', lambda val: val['B.Ms.Vol'][0]/string_b_np),
    ('Inv_strA_Ipanel', lambda val: val['A.Ms.Amp'][0]/string_a_sets),
    ('Inv_strB_Ipanel', lambda val: val['B.Ms.Amp'][0]/string_b_sets),
    ('Inv_strA_Ppanel', lambda val: val['A.Ms.Watt'][0]/(string_a_np*string_a_sets)),
    ('Inv_strB_Ppanel', lambda val: val['B.Ms.Watt'][0]/(string_b_np*string_b_sets)),
    ('Inv_phA_V', lambda val: val['GridMs.PhV.phsA'][0]),
    ('Inv_phB_V', lambda val: val['GridMs.PhV.phsB'][0]),
    ('Inv_freq', lambda val: val['GridMs.Hz'][0]),
#    ('Inv_pf', lambda val: val['GridMs.TotPF'][0]),
#    ('Inv_mode', lambda val: val['Mode'][0]),
#    ('Inv_health', lambda val: val['Op.Health'][0]),
#    ('Inv_grid_switch', lambda val: val['Op.GriSwStt'][0]),
    ('Inv_tot_energy', lambda val: val['E-Total'][0])
    ]

    try:
        while True:
            val = sb.get_values()

            # Set Pac/Pdc to zero if we can't read them. This is necessary so the average comes out right
            data = {        }
            for d in datafuncs:
                try:
                    data[d[0]] = d[1](val)
                except:
                    pass

            if not called_create and len(data)>0:
                for d in data.keys():
                    heating.create_database(d)
                    called_create = True

            vsupply = heating.read_vsupply(cmd_port)
            if vsupply:
                data[vsupply[0]] = vsupply[1]

            heating.update_database(data)

            #phant.post(dict([(phant_fields[d[0]],d[1]) for d in data.items()]),
            #           'Kwoj93Zgz1SzKlDrWJ9qFma8mY3',
            #           'A079PaeYBASn7vNqwg40I6a869n')

            time.sleep(15)
        
    finally:
        del sb
            
