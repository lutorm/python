import pylab,pyfits,glob,pdb,os,vecops
from numpy import *

def load(dust_model):
    """Loads the Draine spectra."""

    Us=["0.10","0.15","0.20","0.30","0.40","0.50","0.70","0.80","1.00",
        "1.20","1.50","2.00","2.50","3.00","4.00","5.00","7.00","8.00",
        "12.0","15.0","20.0","25.0","1e2","3e2"]

    # get directories and their U values
    dirs=glob.glob("/home/patrik/dust_data/crosssections/DL07_emission/U*")
    base=os.path.split(dirs[0])[0]
    dirs=[os.path.split(d)[1] for d in dirs]
    Us=array([float(u[1:]) for u in dirs])
    i=argsort(Us)
    dirs=array(dirs)[i]
    Us=Us[i]
    
    print dirs,Us
    
    # now load the files for all the U values
    seds=None
    for i,d in enumerate(dirs):
        file="%s/%s_%s_%s.txt"%(d,d,d[1:],dust_model)
        print "Loading %s"%file,i

        data=pylab.load(os.path.join(base,file),skiprows=61)
        if seds==None:
            seds=zeros((data.shape[0],dirs.shape[0]))
        
        # files are in units of j_nu = Jy*cm^2/sr, ie 1e-30 W/(Hz*sr)
        # we convert this to j_lambda = j_nu*c/lambda^2 W/(m*sr)
        lam=data[:,0]*1e-6
        seds[:,i]=data[:,2]*1e-30*3e8/lam**2

    return lam[::-1],Us,seds[::-1,:]

def write_fits(dust_model, outfile):
    """Writes the DL07 emission spectra to a fits file. The file has a
    bintable with columns intensity (in U), sed vectors, and
    bolometric luminosity."""

    l,u,s=load(dust_model)
    s *= 1.0973e59*4*pi

    lbol=zeros_like(u)
    for k in range(lbol.shape[0]):
        lbol[k] = vecops.integrate_array(l,s[:,k], True)
        print k,lbol[k]
        
    int_col=pyfits.Column(name='intensity',format='D',array=u,unit='U')
    lbol_col=pyfits.Column(name='L_bol',format='D',array=lbol,unit='W')
    sed_col=pyfits.Column(name='L_lambda',format='%dD'%s.shape[0],
                          array=swapaxes(s,0,1),unit='W/m')

    cols=pyfits.ColDefs([int_col, lbol_col, sed_col])
    h=pyfits.Header()
    h.update('EXTNAME','SED')
    hdu=pyfits.new_table(cols,header=h)
    file=pyfits.HDUList(pyfits.PrimaryHDU())
    file.append(hdu)

    # also make the wavelength table
    lambda_col=pyfits.Column(name='lambda',format='D',array=l,unit='m')
    cols=pyfits.ColDefs([lambda_col])
    h=pyfits.Header()
    h.update('EXTNAME','LAMBDA')
    hdu=pyfits.new_table(cols,header=h)
    file.append(hdu)
    print "Saving model to fits file",outfile
    file.writeto(outfile)
    
def write_all_models():
    m=['MW3.1_%d0'%i for i in range(7)]
    # these files are screwed up for u1e2 and higher
    #m+=['LMC2_00','LMC2_05','LMC2_10','smc']
    for i in m:
        try:
            write_fits(i,'DL07_%s.fits'%i)
        except:
            print "Error writing fits file for",i
        
def make_emission(gridfile, mcrxfile, dust_model='MW3.1_60',minu=0.1):
    """Makes up the emission from the grid cells using the Draine
    template."""

    d07l,u,s=load(dust_model)
    #  calculate the SED in luminosity per unit mass of dust
    s *= 1.0973e59*4*pi

    md=pyfits.open(gridfile)['GRIDDATA'].data.field('mass_metals')*0.4
    mf=pyfits.open(mcrxfile)
    l=mf['LAMBDA'].data.field('lambda')
    intensities=10.**mf['INTENSITY'].data
    # i is subsampled so we average the wavelengths
    subsamp=l.shape[0]/intensities.shape[0]
    l2=zeros((intensities.shape[0]))
    for i in range(subsamp):
        l2+=l[i::subsamp]
    l=l2/subsamp

    # now integrate the intensities
    ib=zeros_like(md)
    for k in range(ib.shape[0]):
        ib[k]=vecops.integrate_array(l,intensities[:,k],False)
    # bolometric intensity in units of U
    ib=ib*4*pi/3e8/8.65e-14
    
    # make the histogram. set bin edges to be midpoints
    um=[0]+list(0.5*(u[:-1]+u[1:]))+[1e30]
    h=histogram(ib,weights=md,new=True,bins=um)

    # we also make a slightly more accurate sed by correcting for the
    # individual values of the intensity so the energy in conserved
    sed=zeros_like(d07l)
    for k in range(ib.shape[0]):
        # find sed index for the intensity in this cell
        i=searchsorted(um,ib[k])-1
        sed += s[:,i]*ib[k]/u[i]*md[k]
    
    return d07l,s,h,sed
