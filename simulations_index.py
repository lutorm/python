import glob,pyfits,sys, tempfile, commands, os, types, re

from math import sqrt,log10
import fit_slopes
import numpy as na
import numpy, pdb

import simulations_sql as sql
import patrik_sqlengine


sql.setup(patrik_sqlengine.metadata, transactional=True)

sql.m.bind.echo=False # no echo for this 

# configuration options
mcrx_base = 'mcrx'
broadband_base = 'broadband'
segmap_directory = "/data/jlotz/sims"

# these keywords are just extracted from the specified HDU
simple_gadget_keywords = [("SFRHIST", ["nobject"]),
                          ("MERGER", ["ecc", "phi1","phi2", "theta1",
                                      "theta2", "rstart", "velf"]),
                          ("GADGET", ['indextpn', 'factortpfeedback',
                                      'factorsfr', 'sfrlawn',
                                      'stargenerations'])
                          ]

simple_sunrise_keywords = [("SFRHIST", ["stellar_dispersion",
                                        "max_radius","stellarmodelfile"]),
                           ("STELLARMODEL", ["track_metallicity"]),
                           ("MCRX", ["dust_to_gas_ratio","dust_to_metal_ratio",
                                     "mcrx_version"])
                            ]
simple_snapshot_keywords = [("SFRHIST", ["snaptime", "snapshot_file"]),
                            ("INTEGRATED_QUANTITIES", ["l_bol_grid",
                                                       "l_bol_absorbed"]),
                            ("MAKEGRID", ["m_g_tot", "sfr_tot",
                                          "m_metals_tot"])
                            ]

# these object keywords are either in a numbered hdu or are numbered
# themselves

gadget_object_keywords =[("OBJECT%-PARAMETERS",
                          ["bdist","gasdist","gasexpa","hi_gds","hi_gmf",
                           "plcuto", "plgamma","q","rd","rvir","vvir","a",
                           "c","f","h","jd",
                           "lambda","mb","md","mvir",
                           "n_bulge","n_disk","n_gas",
                           "n_halo","z","z0"])]

gadget_numbered_object_keywords = []

sunrise_object_keywords = []

sunrise_numbered_object_keywords=[("SFRHIST",
                                   ["ic_file","diskpopage","diskpopmode",
                                    "diskpoptau", "bulgepopage",
                                    "bulgepopmode","bulgepoptau", 
                                    "central_metallicity",
                                    "metallicity_gradient"])]

snapshot_object_keywords = []

snapshot_numbered_object_keywords = []

# these camera keywords are either in a numbered HDU or are numbered
# themselves

camera_keywords = [("CAMERA%-PARAMETERS",
                    ["theta", "phi", "linear_fov", "xsize", "ysize"])]

camera_numbered_keywords = []

# these now are in the image object
image_object_numbered_keywords = [("INTEGRATED_QUANTITIES", ["l_scat"])]

# keywords to read from Jennifer's files
lotz_object_keywords =  \
                     ['morph_image', 'signal_to_noise',
                      'r_half_cir', 'r_half_ell', 'r_pet_cir', 'r_pet_ell',
                      'concentration', 'r_20', 'r_80', 'asymmetry',
                      'clumpiness', 'gini', 'm_20', 'flag_morph']
lotz_camera_keywords = ['r_projected']

filter_dict = {}
line_dict = {}

def make_float(x):
    # this is a hack, but we convert any integer value keywords
    # here to floats, since TJ's ICs are interpreted as ints
    # other types are passed through
    if type(x.value) == types.IntType:
        x.value=float(x.value)
    return x

def assemble_vector(kw, header):
    "Returns a 3-array of the keyword appended by 'XYZ', respectively."
    v=na.array([header[kw+"x"].value,
                      header[kw+"y"].value,
                      header[kw+"z"].value])
    return Keyword(v,header[kw+"x"].unit,header[kw+"x"].comment)

class Keyword:
    "A value, unit, comment tuple used for writing FITS keywords.  "
    def __init__(self, val, un="", cmt=""):
        if un=="''":
            un=""
        if cmt=="''":
            cmt=""
        self.value=val
        self.unit=un
        self.comment=cmt

class sql_keyword_dict(dict):
    """An extension of a dictionary that also keeps an SQL Alchemy object
    updated when items are added. If the value of a Keyword already in
    the dict is updated, it MUST be done with the update method,
    otherwise the update won't be propagated to the SQL Alchemy."""
    
    def __init__(self, object):
        dict.__init__(self)
        self.sqlobject=object

    def __setitem__(self, key, keyword):
        dict.__setitem__(self, key, keyword)
        # update sqlobject
        if type(keyword.value) != type(na.array(1)):
            if type(keyword.value) == types.FloatType:
                # mysql can't handle ieee special cases
                if keyword.value == na.inf:
                    keyword.value = 1e300
                elif keyword.value == -na.inf:
                    keyword.value = -1e300
                elif na.isnan(keyword.value):
                    keyword.value = None
            self.sqlobject.__setattr__(key, keyword.value)
        else:
            self.sqlobject.__setattr__(key+'_x', keyword.value[0])
            self.sqlobject.__setattr__(key+'_y', keyword.value[1])
            self.sqlobject.__setattr__(key+'_z', keyword.value[2])
            

    def update(self, key, value):
        dict[key].value = value
        self.sqlobject.__setattr__(key, value)

class Gadget_simulation:
    """Keeps track of keywords for a Gadget simulation."""
    def __init__(self, file):
        # get runname from file
        name=file["SFRHIST"].header["runname"]
        sep=name.find("/")
        if sep==-1:
            print "WARNING: Could not decode run name ", name
            name = self.check_simpar()
            print "Using deduced run name ",name
            sep=name.find("/")
            assert sep != -1
        self.fullname = (name[:sep],name[sep],name[sep+1:])

        # Load SQL object if already present, otherwise create
        o = sql.gsq.filter_by(runname=self.fullname[0]).all()
        assert len(o)<2
        if len(o) == 0:
            self.keywords = sql_keyword_dict(sql.Gadget_simulation())
            sql.session.save(self.keywords.sqlobject)
        else:
            self.keywords = sql_keyword_dict(o[0])

        self.keywords["runname"] = Keyword(self.fullname[0],'','')

        self.objects = [] #List of Gadget_object objects
        self.sunrise_simulations = [] # List of Sunrise_simulation objects

        read_simple_keywords(self, simple_gadget_keywords, file)
        for i in range (self.keywords ["nobject"].value):
            o = Gadget_object (self, i+1, file)

        # Create and process sunrise simulation
        #s = Sunrise_simulation(self, name[2], file)

        # flush database before we return
        sql.session.flush()
        
    def check_simpar(self):
        # tries to figure out a reasonable value for simpar keyword if
        # it's screwed up
        wd=os.getcwd()
        (wd,mcrxsim)=os.path.split(wd)
        (wd,tjsim)=os.path.split(wd)
        runname=os.path.join(tjsim,mcrxsim)
        return runname
    
class Gadget_object:
    """Keeps track of keywords for a Gadget object."""
    def __init__(self, parent, number, file):
        # Load SQL object if already present, otherwise create
        o = sql.goq.filter_by(simulation=parent.keywords.sqlobject,
                              number=number).all()
        assert len(o)<2
        if len(o) == 0:
            self.keywords = sql_keyword_dict(sql.Gadget_object())
            parent.keywords.sqlobject.objects.append(self.keywords.sqlobject)
            sql.session.save(self.keywords.sqlobject)
        else:
            self.keywords = sql_keyword_dict(o[0])

        self.keywords ["number"] = Keyword(number,'',"Object number")
        parent.objects.append(self)
        
        # read keywords
        read_object_keywords (self, gadget_object_keywords, file)
        read_numbered_object_keywords (self,
                                       gadget_numbered_object_keywords,
                                       file)

        
class Sunrise_simulation:
    """Keeps track of keywords for a Sunrise simulation."""
    def __init__(self, file):
        """This constructs a gadget simulation as its own parent."""
        parent = Gadget_simulation(file)
        gadgetname = parent.fullname[0]
        name = parent.fullname[-1]
        
        # Because we are going to overwrite an existing simulation, we
        # can delete it and all its associated data so we don't have
        # to deal with checking from here on.
        
        # Load SQL object if already present
        o = sql.ssq.filter_by(gadget_simulation=parent.keywords.sqlobject,
                              runname=name).all()
        assert len(o)<2
        if len(o)>0:
            # delete it and flush
            print "DELETING OLD SUNRISE SIMULATION!"
            # we now delete using SQL syntax directly, so mysqld does
            # the cascading deletes instead of the sqlalchemy orm. It
            # is orders of magnitude faster
            sql.m.bind.engine.execute(sql.sunrise_simulations_table.delete(sql.and_(sql.sunrise_simulations_table.c.runname == name,
                                                                                           sql.sunrise_simulations_table.c.gadgetrunname== gadgetname)))
            #sql.session.delete(o[0])
            #sql.session.flush()
            sql.session.clear()
            # get new Gadget_simulation object since the session was reset
            parent = Gadget_simulation(file)
            
        # now generate new objects
        self.keywords = sql_keyword_dict(sql.Sunrise_simulation())
        parent.keywords.sqlobject.sunrise_simulations.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)

        self.keywords ["runname"] = Keyword(name,'',"Sunrise run designation")
        parent.sunrise_simulations.append(self)
        self.parent = parent

        self.objects=[]
        self.snapshots=[]
        
        read_simple_keywords(self, simple_sunrise_keywords, file)
        for i in range (self.parent.keywords ["nobject"].value):
            o = Sunrise_object (self, i+1, file)

        # Snapshots are NOT created/processed here, they are done
        # externally
        
class Sunrise_object:
    """Keeps track of keywords for a Sunrise object."""
    def __init__(self, parent, number, file):
        # SQL object has already been deleted
        self.keywords = sql_keyword_dict(sql.Sunrise_object())
        parent.keywords.sqlobject.objects.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)

        print "adding sunrise object number ",number
        self.keywords ["number"] = Keyword(number,'','Object number')
        parent.objects.append(self)
        
        # read keywords
        read_object_keywords (self, sunrise_object_keywords, file)
        read_numbered_object_keywords (self,
                                       sunrise_numbered_object_keywords,
                                       file)

    
class Snapshot:
    """Keeps track of keywords for one snapshot. Also keeps an sql
    object of the snapshot too"""
    
    def __init__(self, parent, number, files):
        # SQL object has already been deleted
        self.keywords = sql_keyword_dict(sql.Snapshot ())
        parent.keywords.sqlobject.snapshots.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)

        parent.snapshots.append (self)
        self.parent=parent
        
        self.cameras = []
        self.objects=[]

        # now process snapshot
        mcrxfile = pyfits.open(files[0])
        broadbandfile = pyfits.open(files[1])
        
        self.keywords ["number"] = Keyword(number, '',
                                           'Snapshot sequential number')
        self.keywords['mcrx_file'] = Keyword(files[0],"","Mcrx file")
        self.keywords['broadband_file'] = Keyword(files[1],"","Broadband file")

        read_simple_keywords (self, simple_snapshot_keywords,
                              mcrxfile)

        # add objects
        for i in range (self.parent.parent.keywords ["nobject"].value):
            o = Snapshot_object (self, i+1, mcrxfile)

        self.complicated_stuff(mcrxfile)

        # remaining quantities are in the cameras, so create and
        # process them
        for i in range (mcrxfile ["mcrx"].header ["n_camera"]):
            c = Camera (self, i, (mcrxfile, broadbandfile))

        # fit spectral slopes, and other quantities, and add to
        # whole-image objects
        slopes=fit_slopes.calculate_slopes(mcrxfile)
        slope_keys=[x for x in slopes.keys() if '_ns' not in x]
        for c in self.cameras:
            cam = c.keywords['number'].value
            print '\tSlopes for camera ',cam
            for o in c.objects:
                if not o.keywords.has_key('number'):
                    if o.keywords['has_dust'].value:
                        suffix=''
                    else:
                        suffix='_ns'

                    for k in slope_keys:
                        o.keywords[k]= Keyword(slopes[k+suffix] \
                                               [c.keywords['number'].value])
                        print '\t\t',k+suffix,' = ',o.keywords[k].value

        # calculate line luminosities and equivalent widths
        lines = fit_slopes.calculate_lines(mcrxfile)
        for c in self.cameras:
            cam = c.keywords['number'].value
            for o in c.objects:
                if not o.keywords.has_key('number'):
                    if o.keywords['has_dust'].value:                        
                        key='lines'
                    else:
                        key='lines_ns'
                    print '\tLines for camera ',cam,o.keywords['has_dust'].value

                    curlines = lines[key][cam]
                    for l in zip(curlines[0],curlines[1]):
                        #pdb.set_trace()
                        ls = Line_strength(o, l[0][0], l[0][2], l[0][1])
                        ls.keywords['luminosity'] = Keyword(l[1][0])
                        ls.keywords['eqw'] = \
                                           Keyword(l[1][1] if \
                                                   l[1][1]==l[1][1] else 0.0)
                        print '\t\t',l[0][0],' = ', \
                              ls.keywords['luminosity'].value, \
                              ls.keywords['eqw'].value*1e10,' A'
                        
        sql.session.flush()
        
    def complicated_stuff(self, file):
        # Calculate some complicated quantities
        try:
            # gas metallicity
            self.keywords ["Z_gas_new"] = \
                          Keyword(s.keywords ["m_metals_tot"].value/ \
                                  s.keywords ["m_g_tot"].value,"", \
                                  "Gas metallicity in simulation")
        except:
            pass

        try:
            self.keywords ["Z_gas"] = \
                          Keyword(self.parent.keywords ["dust_to_gas_ratio"].value*0.02/0.008 \
                                  +self.keywords ["m_metals_tot"].value/ \
                                  self.keywords ["m_g_tot"].value,"", \
                                  "Metallicity inferred from dust/gas ratio")
        except:
            pass

        # now treat vector keywords
        h= get_header (file ["SFRHIST"])
        # Read "translate_origin" keyword
        keywords=["translate_origin"]
        for kw in keywords:
            try:
                self.keywords[kw] = assemble_vector(kw, h)
                print "\t\t", kw, "=", self.keywords [kw].value
            except KeyError:
                print "\t\t", kw, ": Not Found"

        # calculate separation if there are 2 objects
        if len(self.objects)==2:
            try:
                delta= self.objects[0].keywords["galcenter"].value- \
                       self.objects[1].keywords["galcenter"].value
                sep=\
                      Keyword(sqrt(sum(delta*delta)),
                              self.objects[0].keywords["galcenter"].unit,"")
                print "\t\tseparation = ",sep.value
                self.keywords["separation"] = sep

            except KeyError:
                print "\t\tseparation not found"

                    
class Snapshot_object:
    """Keeps track of keywords for a Snapshot object."""
    def __init__(self, parent, number, file):
        # SQL object has already been deleted
        self.keywords = sql_keyword_dict(sql.Snapshot_object())
        parent.keywords.sqlobject.objects.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)

        self.keywords ["number"] = Keyword(number,'','Object number')
        parent.objects.append(self)
        
        # read keywords
        read_object_keywords (self, snapshot_object_keywords, file)
        read_numbered_object_keywords (self,
                                       snapshot_numbered_object_keywords,
                                       file)

        # read vector keywords
        h= get_header (file ["SFRHIST"])
        keywords=["galcenter"]
        for kw in keywords:
            try:
                self.keywords[kw] = assemble_vector(kw+`number`, h)
                print "\t\t", kw, "=", self.keywords [kw].value
            except KeyError:
                print "\t\t", kw, ": Not Found"

    
                                             

class Camera:
    "Keeps track of keywords for one camera"
    def __init__(self, parent, number, files):
        # SQL object has already been deleted
        self.keywords = sql_keyword_dict(sql.Camera ())
        parent.keywords.sqlobject.cameras.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)

        self.keywords ["number"] = Keyword(number,'','Camera number')
        parent.cameras.append (self)
        self.parent=parent
        
        read_object_keywords(self, camera_keywords, files [0])
        read_numbered_object_keywords (self,
                                       camera_numbered_keywords,
                                       files [0])

        self.objects = []
        # add the entire-image objects
        Image_object(self, None, True, files)
        Image_object(self, None, False, files)

        # check for segmap
        segmap = self.read_segmap(files)
        if segmap != None:
            #  we have a segmentation map, create those objects
            nobject = segmap.max()
            print '\t',nobject,' objects in segmap'
            # bug in segmap code can give 65536 objects
            if nobject>2:
                nobject=2
            for i in range(nobject):
                Image_object(self, i+1, True, files)
                Image_object(self, i+1, False, files)
        sql.session.flush()
        
        # and calculate object quantities
        for o in self.objects:
            o.calculate_aux (segmap, files)
            o.calculate_magnitudes (segmap, files)
            
    def read_segmap(self, files):
        """Calculates the object quantities for this camera by loading
        the segmentation map, if available, and then integrating the
        images over the segmentation map"""
        cam=self.keywords["number"].value

	# check for segmap presence
	pattern = \
	    os.path.join(segmap_directory, \
                         self.parent.parent.parent.keywords["runname"].value, \
			 self.parent.parent.keywords["runname"].value, \
			 "segmaps",
			 os.path.split(self.parent.keywords['broadband_file'].value)[-1].replace(".fits",
                                                                              "_cam"+`cam`+ \
                                                                              "_*seg_sm.fits"))
	#print "Looking for ", pattern
	segmap_filename = glob.glob(pattern)
	#print "Found segmap file ",segmap_filename
	if len(segmap_filename)==0:
	    print "No segmap found -- no object quantities will be included"
            print pattern
	    return None
	elif len(segmap_filename)>1:
	    print "Ambiguous segmap files -- no AUX quantities will be included"
	    print segmap_filename
	    1/0
	
	# Load segmap
	print "\tLoading segmap file: ",segmap_filename[0]
	segmap_file = pyfits.open(segmap_filename[0])
	segmap = segmap_file[0].data
	self.keywords["segmap_file"]=Keyword(segmap_filename[0],"",
					     "Segmentation map file")
        return segmap
    

class Magnitude:
    """Keeps track of a magnitude measurement."""

    def __init__(self, parent, filter, eff_lambda=None, eff_width=None):
        # NEED TO DO FIRST SO WE CAN GET A FILTER ID
        # find filter sql object, create if necessary. Note that
        # unlike the other classes we only have sql objects.
        # BUT next object won't see this unless we commit. lets make a
        # global dict instead.
        if filter_dict.has_key(filter):
            f=filter_dict[filter]
        else:
            # not in dict. query it
            f=sql.fiq.filter_by(name=filter).all()
            assert len(f)<2
            if len(f)==0:
                print "Adding filter: ",filter
                f = sql.Filter()
                f.name = filter
                sql.session.save(f)
                filter_dict[filter]=f
                # we need to flush the filter to database here so we
                # get an ID...
                sql.session.flush()
            else:
                print "Loading filter", filter
                filter_dict[filter]=f[0]
                f=f[0]
            # check that eff_lambda and eff_width are set
            if ((f.eff_lambda==None) or (f.eff_width==None)) and \
               ((eff_lambda!=None) and (eff_width!=None)):
                print "\tUpdating effective wavelength and width: %.2e %.2e"%(eff_lambda,eff_width)
                f.eff_lambda=eff_lambda
                f.eff_width=eff_width
                sql.session.save_or_update(f)
                
        # SQL object has already been deleted
        self.keywords = sql_keyword_dict(sql.Magnitude ())
        parent.keywords.sqlobject.magnitudes.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)
        parent.magnitudes.append (self)

        # and link filter with this magnitude object
        #f.magnitudes.append(self.keywords.sqlobject)
        # actually we do NOT do that because it's an expensive mapping
        # instead we update the field directly
        self.keywords.sqlobject.filter_id = f.id
        
class Line_strength:
    """Keeps track of a line strength measurement."""

    def __init__(self, parent, line, wavelength, element): 
        # NEED TO DO FIRST SO WE CAN GET A LINE ID
        # find line sql object, create if necessary. Note that
        # unlike the other classes we only have sql objects.
        # BUT next object won't see this unless we commit. lets make a
        # global dict instead.
        if line_dict.has_key(line):
            l=line_dict[line]
        else:
            # not in dict. query it
            l=sql.liq.filter_by(name=line).all()
            assert len(l)<2
            if len(l)==0:
                print "Adding line: ",line
                l = sql.Line()
                l.name = line
                l.wavelength = wavelength
                l.element = element
                sql.session.save(l)
                line_dict[line]=l
            else:
                line_dict[line]=l[0]
                l=l[0]

        # SQL object has already been deleted
        self.keywords = sql_keyword_dict(sql.Line_strength ())
        parent.keywords.sqlobject.line_strengths.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)
        parent.line_strengths.append (self)
              
        # and link filter with this magnitude object
        #l.line_strengths.append(self.keywords.sqlobject)
        # actually we do NOT do that because it's an expensive mapping
        # instead we update the field directly
        self.keywords.sqlobject.line_id = l.id

        
class Image_object:
    """Keeps track of an object in a camera. The number is the object
    number, or None if the quantities are for the entire camera.
    Has_dust is true if the quantities are derived from the camera
    with dust, false if they are from the nonscatter camera."""

    def __init__(self, parent, number, has_dust, files):
        # SQL object has already been deleted
        self.keywords = sql_keyword_dict(sql.Image_object ())
        parent.keywords.sqlobject.objects.append(self.keywords.sqlobject)
        sql.session.save(self.keywords.sqlobject)

        if number != None:
            self.keywords ["number"] = Keyword(number,'','Object number')
        self.keywords["has_dust"] = Keyword(has_dust, '', 'Image with dust')
        
        parent.objects.append (self)
        self.parent=parent
        
        self.magnitudes = []
        self.line_strengths = []

        self.read_magnitudes(files[1])

        
    def read_magnitudes (self, file):
        """Reads magnitudes  numbered by camera in file into
        Image_object. This is only for objects that are whole-image
        objects, otherwise we need to get the magnitudes some other way."""

        if "number" in self.keywords.keys():
            # this is a segmap object, we need to do something else then
            return
            
        cam=self.parent.keywords["number"].value
        if not self.keywords["has_dust"].value:
            suffix = "_nonscatter"
            desc = "dust-free "
        else:
            suffix = ""
            desc = ""

        filters = map(lambda x: "AB_"+x.replace (".res", ""),
                      file["FILTERS"].data.field ("filter"))

        # try to load effective wavelength and width, but they are not
        # in old files
        try:
            eff_lambda = \
                       file["FILTERS"].data.field ("lambda_eff")
            ewidth = \
                   file["FILTERS"].data.field ("ewidth_lambda")
        except NameError:
            eff_lambda=[None]*len(filters)
            ewidth=[None]*len(filters)
            
        magnitudes = \
           file["FILTERS"].data.field ("AB_mag" + suffix + `cam`)
        luminosities = \
           file["FILTERS"].data.field ("L_lambda_eff" + suffix + `cam`)
        # mysql can't handle Inf or NaN in floating columns, so we
        # need to substitute these for something reasonable. The
        # magnitudes and luminosities are NULL ie NaN if the bands
        # are outside of the wavelength range calculated. In this
        # case, we set both to -1.
        nans=(magnitudes!=magnitudes)
        luminosities[nans]=-1
        magnitudes[nans]=-1
        # another situation is that luminosity is simply zero, in
        # which case magnitude is inf. In that case, we just set the
        # magnitude to something really large.
        magnitudes[magnitudes>1e300]=1e300

        print "\tMagnitudes for "+desc+" camera ",cam," :"
        for j in range (len (filters)):
            filtername = filters [j]
            assert all(magnitudes==magnitudes)

            m = Magnitude (self, filtername, eff_lambda[j], ewidth[j])

            # sometimes if a run was not done the sed is zero,
            # luminosities  and the
            # mag is inf, which chokes mysql.
            m.keywords['ab_mag'] = \
                                 Keyword(magnitudes [j], "AB", \
                                         desc + "magnitude in "+filtername)
            m.keywords['l_lambda_eff'] = \
                                       Keyword(luminosities [j], "", \
                                               desc + "luminosity in "+filtername)
            print "\t\t", filtername, "=", \
                  m.keywords['ab_mag'].value, \
                  m.keywords ['l_lambda_eff'].value

    def calculate_aux(self, segmap, files):
	# read CAMERAi-AUX data cube
        cam=self.parent.keywords["number"].value

        if not self.keywords["has_dust"].value:
            desc = "dust-free "
        else:
            desc = ""

        auxextname = "CAMERA" + `cam`+ "-AUX"

        # try to open aux hdu
        try:
            aux_hdr = get_header(files[0][auxextname])
        except KeyError:
            # not found -- just skip the aux keywords
            return

	#print aux_hdr.keys()

	# multiply by pixel scale area unit to convert from surf.dens. to qty
	pix_area=aux_hdr["cd1_1"].value*aux_hdr["cd2_2"].value
	aux_data = files[0][auxextname].data*pix_area

	# extract individual object and collapse
        if "number" not in self.keywords.keys ():
            # This is a whole-image object
            # do sum as float64 even if the data aren't, because the
            # luminosity can overflow (images are now float64 in aux_emergence)
	    aux = na.sum(na.sum(aux_data.astype(na.float64),axis=2),axis=1)
            obj=None
        else:
            # this is a segmapped object
            obj = self.keywords ["number"].value
	    print "\tObject ",obj,": ",na.sum(na.sum(na.where(segmap==obj,1,0)))," pixels"
	    aux = na.sum(na.sum(na.where(segmap==obj,1.,0.)*aux_data.astype(na.float64),
                                axis=2),
                         axis=1)
            
        print "\tAUX quantities for "+desc+" camera ",cam,", object ",obj,":"
        
	# These are the fields and what the keywords should be called
	aux_fields=[("mg_slice","m_g","mg_unit"),
		    ("mm_slice","m_metals","mm_unit"),
		    ("sfrslice","sfr","sfr_unit"),
		    ("ms_slice","m_stars","ms_unit"),
		    ("mmsslice","m_metals_stars","mms_unit"),
		    ("lb_slice","l_bol","lb_unit"),
		    ("fir_slice","l_bol_absorbed","fir_unit")]

        assert aux_hdr["cd1_1"].unit == aux_hdr["cd2_2"].unit
        lengthunit = aux_hdr["cd2_2"].unit

	for f in aux_fields:
	    # Figure out unit (this is a MAJOR PITA all the time...)
            try:
                unit = aux_hdr[f[2]].value
                if unit.find("/"+lengthunit+"^2")>-1:
                    # remove area unit in denominator
                    unit = unit.replace("/"+lengthunit+"^2","")
                else:
                    # if it's not there, just multiply by area
                    unit = unit + "*"+lengthunit+"^2"

                slice=aux_hdr[f[0]].value-1
                val=aux[slice]
                assert val==val
                assert val<1e300
                kw=f[1]
                self.keywords[kw] = Keyword(val,"",aux_hdr[f[0]].comment)
                print "\t\t",kw,"=",val,unit
            except:
                print "\t\t",kw, " Not found"
            
    def calculate_magnitudes(self, segmap, files):
        """Calculate magnitudes of individual objects by integrating
        the broadband images using the segmentation maps.  """
        
        if "number" not in self.keywords.keys ():
            # This is a whole-image object
            # we have these data already!            
            return
        
	# read CAMERAi-BROADBAND data cube
        cam=self.parent.keywords["number"].value

        if not self.keywords["has_dust"].value:
            desc = "dust-free "
            HDU_suffix = "-NONSCATTER"
        else:
            desc = ""
            HDU_suffix = ""
            
        broadband_extname = "CAMERA" + `cam`+ "-BROADBAND" + HDU_suffix
        campar_extname = "CAMERA" + `cam`+ "-PARAMETERS"

        filters = map(lambda x: "AB_"+x.replace (".res", ""),
                      files[1]["FILTERS"].data.field ("filter"))
        #filters_hdr = get_header (files [1] ["FILTERS"])
        #filters = files [1] ["FILTERS"].data.field ("filter")

        # this is the conversion from xxx_lambda to xxx_nu units,
        # since the images are in lambda units and we need nu units
        # for the magnitudes
        try:
            lambda_nu = files [1] ["FILTERS"].data.field("ewidth_lambda") / \
                        files [1] ["FILTERS"].data.field("ewidth_nu")
        except:
            # must be old-style file
            lambda_nu = files [1] ["FILTERS"].data.field("L_lambda_to_L_nu")

        # we need the surface area subtended by the pixels from a
        # distance of 10pc for the absolute magnitude. we can do that
        # by using the pixel_sr value and multiplying by
        # (cameradist/10pc)^2. We assume cameradist is in kpc.
        try:
            pixel_sr = files[1][broadband_extname].header["pixel_sr"] * \
                       (files[1][campar_extname].header["cameradist"]/1e-3)**2
        except:
            #print "OLD FILE, LOOKING FOR PIXEL_SR IN ALL THE WRONG PLACES!"
            pixel_sr = files[0]["CAMERA%d-SCATTER" % cam].header["pixel_sr"] * \
                       (files[1][campar_extname].header["cameradist"]/1e-2)**2
	sb = files[1][broadband_extname].data

        
	# extract individual object and collapse
        if "number" not in self.keywords.keys ():
            # This is a whole-image object
            # we have these data already!
            return
	    flux = na.sum(na.sum(sb, axis=2), axis=1) * pixel_sr 
            obj=None
        else:
            # this is a segmapped object
            obj = self.keywords ["number"].value
	    print "\tObject ",obj,": ",na.sum(na.sum(na.where(segmap==obj,1,0)))," pixels"
	    flux = na.sum(na.sum(na.where(na.tile(na.expand_dims(segmap==obj,0),(sb.shape[0],1,1)),sb,0.),
                                 axis=2),
                          axis=1) * pixel_sr
        
        print "\tMagnitudes for "+desc+" camera ",cam,", object ",obj,":"

        for j in range (len (filters)):
            filtername = filters [j]

            m = Magnitude (self, filtername)

            if flux[j]>0:
                m.keywords['ab_mag'] = \
                                     Keyword(-2.5*log10(flux[j]*lambda_nu[j]/3.63e-23),"AB", \
                                             desc + "magnitude in "+filtername)
            else:
                m.keywords['ab_mag'] = Keyword(1e300,"AB",desc + "magnitude in "+filtername)
            # to convert flux at 10pc to luminosity, we multiply by
            # 4pi*10pc^2 = 1.19649565e36 m^2
            m.keywords['l_lambda_eff'] = \
                                       Keyword(flux[j]*1.19649565e36,"", \
                                         desc + "luminosity in "+filtername)
            
            print "\t\t", filtername, "=", \
                  m.keywords['ab_mag'].value, \
                  m.keywords ['l_lambda_eff'].value

    def calculate_centers(self, galcenter):
        "Calculate the camera pixel positions of the galaxy centers"
        
def read_simple_keywords(object, simple_keywords, file):
        "Reads the simple keywords from the file and adds them to object"
        for hdu in simple_keywords:
            try:
                #try:
                h= get_header (file [hdu [0]])
                #print h
                print "\t", hdu [0]
                for kw in hdu [1]:
                    try:
                        if hdu[0]=="MERGER": # hack
                            object.keywords[kw] = make_float(h [kw])
                        else:
                            object.keywords[kw] = h [kw]
                        print "\t\t", kw, "=", object.keywords [kw].value
                    except KeyError:
                        print "\t\t", kw, ": Not Found"
            except KeyError:
                print "\t\t HDU ", hdu [0], " Not Found"

def read_object_keywords(object, object_keywords, file):
        """Reads the supplied  keywords and puts them in the
        object. The object_keywords is a list of (hdu, list of
        keywords) tuples. For the HDU, the % character is replaced
        with the object number."""
        # open and read HDUs
        for hdu in object_keywords:
            num = object.keywords['number'].value
            hdu_name=hdu[0].replace('%',`num`)
            print "\t", hdu_name

            h = get_header(file [hdu_name])

            # read keywords
            for kw in hdu[1]:
                try:
                    object.keywords[kw] = make_float(h[kw])
                    print "\t\t", kw, "=", object.keywords [kw].value
                except KeyError:
                    print "\t\t", kw, ": Not Found"


def read_numbered_object_keywords(object, numbered_object_keywords, file):
    "Reads the keywords, numbered by object number, from the specified HDUs."

    for hdu in numbered_object_keywords:
        # open and read HDU
        h = get_header(file [hdu[0]])
        print "\t", hdu[0]

        for kw in hdu[1]:
            new_kw=kw+`object.keywords["number"].value`
            try:
                object.keywords[kw] = make_float(h[new_kw])
            except KeyError, e:
                # if it fails, we try to read the keyword without
                # number and consider that valid for all objects
                try:
                    object.keywords[kw] = make_float(h[kw])
                except KeyError, e:
                    pass
            try:
                print "\t\t", new_kw, "=", object.keywords [kw].value
            except KeyError, e:
                print "\t\t", new_kw, ": Not Found"


def calculate_deltat(snapshots):
    "Calculates the time between snapshots and saves this as keyword dt."
    snapshots.sort (time_compare)
    timeunit = snapshots[0].keywords["snaptime"].unit
    cmt = "Time between adjacent snapshots."
    if len(snapshots)==1:
        snapshots[0].keywords["dt"]=Keyword(1.,timeunit,cmt)
    else:
        for i in range (len (snapshots)):
            t = snapshots [i].keywords ["snaptime"].value
            if i == 0:
                dt = snapshots [1].keywords ["snaptime"].value -t
            elif i == len (snapshots) - 1:
                dt = t- snapshots [i- 1].keywords ["snaptime"].value
            else:
                dt = (snapshots [i+ 1].keywords ["snaptime"].value - \
                      snapshots [i- 1].keywords ["snaptime"].value)/2
            assert dt>0
            snapshots [i] .keywords ["dt"] = Keyword(dt,timeunit,cmt)



def time_compare (a, b):
    "Predicate comparing the snaptime values of a and b and returns true if a is earlier than b"
    ta = a.keywords ["snaptime"].value
    tb = b.keywords ["snaptime"].value
    if ta < tb:
        return -1
    elif ta == tb:
        return 0
    else:
        return 1

def get_header (hdu):
    "Reads the header keywords of the HDU and creates a dictionary of keyword objects."
    regexp = re.compile(r"(\[(.*)\])?\s*(.*)")
    keywords={}
    # loop over cards
    for i in hdu.header.ascardlist():
        kw=i.key.lower()
        value=i.value
        #split comment into the unit and comment, match 1 is unit and 2 is cmt
        if i.comment!=None:
            m=regexp.search(i.comment).groups()
        else:
            m="","",""
            
        k=Keyword(value,m[1],m[2])
        keywords[kw]=k
        #print kw,k.value,k.unit,k.comment
            
    return keywords


def load_lotz_file (simulation, camera, has_dust):
    """Reads one of Jennifer's files."""

    # check for lotz table presence
    pattern = \
        os.path.join(segmap_directory, \
                     simulation.parent.keywords["runname"].value, \
                     simulation.keywords["runname"].value, \
                     "tables",
                     "morph_cam%d%s_*.fits" % \
                     (camera, {True:"", False:"nd"}[has_dust]))
    
    #print "Looking for ", pattern
    filename = glob.glob(pattern)
    if len(filename)==0:
        print "No Lotz tables found -- no morphology quantities will be included"
        print pattern
        return None
    elif len(filename)>1:
        print "Ambiguous Lotz file -- no morphology quantities will be included"
        print segmap_filename
        1/0

    # Load segmap
    print "\tLoading Lotz table: ",filename[0]
    file = pyfits.open(filename[0])
    table = file[1].data

    return table


def process_lotz_files (simulation):
    """Processes Jennifer's files and adds those keywords."""

    print "Processing Lotz files"
    
    # loop over cameras and load all 
    ncam = len(simulation.snapshots[0].cameras)

    ltables = []
    ltables_ns = []
    for c in range(ncam):
        ltables.append(load_lotz_file(simulation, c, True))
        ltables_ns.append(load_lotz_file(simulation, c, False))

    
    # loop over snapshots and cameras
    for s in simulation.snapshots:
        snapfile = s.keywords['snapshot_file'].value
        snapnum = snapfile[snapfile.rfind('_')+1:]
        print snapfile

        # because Jen's files may not always contain dust and no-dust,
        # we need to process them independently without assuming they
        # always exist.

        # loop over dust and no-dust
        for d,table in zip([True, False],[ltables, ltables_ns]):            
            for c in s.cameras:
                camnum = c.keywords['number'].value
                curtable=table[camnum]
                print "\tCamera ",camnum

                if curtable == None:
                    # we don't have a segmap for this camera, try next
                    continue

                # find entries containing this snapshot 
                row = numpy.where(curtable.field('snapshot')==snapnum)[0]
                if len(row) == 1:
                    row = row[0]
                else:
                    # this snapshot not here
                    print "Snapshot",snapnum,"not found in Lotz table"
                    continue

                # camera keywords are duplicated in both dust and
                # nodust tables, but that doesn't matter. we
                # potentially duplicate the assignment here but that's
                # ok, because we want to get it even if it's only in
                # one of the tables
                for kw in lotz_camera_keywords:
                    c.keywords[kw] = Keyword(curtable.field(kw)[row])
                    print "\t\t",kw," = ",c.keywords[kw].value

                # loop over objects in this camera
                for o in c.objects:
                    # excluding the whole-object cameras, picking the
                    # correct dust or no-dust one
                    if o.keywords.has_key('number') and \
                           (o.keywords['has_dust'].value==d):
                        objnum = o.keywords['number'].value

                        print '\t\t\tObject ',objnum, \
                              (' with' if d else ' w/o'), 'dust'
                        # extract keywords 
                        for kw in lotz_object_keywords:
                            kwname = 'OBJECT%d_%s' % (objnum, kw)
                            o.keywords[kw] = Keyword(curtable.field(kwname)[row])
                            print "\t\t",kw," = ",o.keywords[kw].value

                        
            
# def process_idl(idl_output, snapshots):
#     "Decodes the idl output into keywords."
    
#     try:
#         idl_output.index ("Error")
#         print "Warning: IDL output contains errors:"
#         print idl_output
#     except:
#         pass
#     results = idl_output.splitlines ()
#     for s in results:
#         ss = s.split ()
#         try:
#             if ss [0] == "columns":
#                 # it's the column descriptor line, add to the column name list
#                 column_names=ss[2:]
#                 units=[""]*len(column_names)
#                 comments=[""]*len(column_names)
#             if ss [0] == "units":
#                 # it's the column descriptor line, add to the column name list
#                 units=ss[2:]
#             if ss [0] == "comments":
#                 # it's the column descriptor line, add to the column name list
#                 comments=ss[2:]
#         except:
#             # no [0] index -- assume it's an empty line
#             pass
#         try:
#             # decode snapshot name and camera number
#             snapname=ss[0]
#             cam = int (ss [1])
#         except:
#             cam = -1
#         if (cam >= 0) and snapshots.has_key (snapname) and \
#            (cam < len(snapshots[snapname].cameras)):
#             # camera and snapshot exists, so we have valid output.
#             # Remaining columns are keyword values, loop over them and
#             # put in keywords for cameras
#             for j in range(2, len (ss)):
#                 key=column_names[j-2]
#                 kw=Keyword(float (ss [j]),units[j-2],comments[j-2])
#                 print snapname," ",cam,"\t",key," = ",kw.value," ",kw.unit
#                 snapshots[snapname].cameras[cam].keywords[key] = kw
                
#                 #print column_names [j- 2], ss [j]

def make_fits_column(title, keywords):
    "Returns a FITS column with given title containing keywords. Unit is taken from first keyword."
    if isinstance(keywords[0].value, na.NumArray):
        f=`keywords[0].value.shape[0]`+"D"
    else:
        f = format [type (keywords [0].value)]
    values=[k.value for k in keywords]
    if f == "A":
        # Find the maximum string length
        f = `max ([len (s.value) for s in keywords]) `+ f
        v = na.strings.array (values)
    else:
        v = na.array (values)
    #print kw, values
    #print kw, v
    return pyfits.Column (kw, f, keywords[0].unit, \
                          array = v)


def make_index():
    # find files
    l=os.listdir('.')
    mcrx_files = [x for x in l \
                  if re.search('(^|.*/)'+mcrx_base+'_\d+.fits$',x)!=None ]
    broadband_files = [x for x in l \
                      if re.search('(^|.*/)'+broadband_base+'_\d+.fits$',x)!=None ]
    #mcrx_files = glob.glob (mcrx_base + "_*.fits")
    #broadband_files = glob.glob (broadband_base + "_*.fits")
    #mcrx_files=["mcrx_017.fits"]#,"mcrx_018.fits","mcrx_019.fits"]
    #broadband_files=["broadband_017.fits"]#,"broadband_018.fits","broadband_019.fits"]

    if len (mcrx_files) != len (broadband_files):
        print "Error: Different number of mcrx and broadband files"
        sys.exit(1)
    if len (mcrx_files) == 0:
        print "No fits files to index"
        sys.exit(1)

    mcrx_files.sort ()
    broadband_files.sort ()
    # assemble the two lists into a list of tuples
    files = zip(mcrx_files, broadband_files)
    # should probably check that they don't get off-sequence here...

    # temporary command file for calling idl
    temporary = tempfile.mktemp ()
    temporary_file = open (temporary, "w")
    print >> temporary_file, ".r betaslope"

    # generate sunrise simulation object from first file
    ss = Sunrise_simulation(pyfits.open(files[0][0]))
    sql.session.flush()

    # loop over files and generate snapshots
    for i in range(len(files)):
        print "Reading files ", files[i]

        s = Snapshot(ss, i, files[i])

        # idl command to fit slopes
        #print >>temporary_file, "calculate_slopes, '" +files[i][0] + \
         #     "', [0," + `len(s.cameras) - 1 `+ "]"


    # Now we have to call idl and parse the idl output
#     print "Calling IDL to perform calculations..."
#     print "IDL command file: " + temporary
#     print >>temporary_file, "exit"
#     temporary_file.close () 
#     idl = commands.getoutput ("module load idl; idl " + temporary)
#     os.unlink (temporary)
#     print "Done."

#     process_idl(idl, ss.snapshots)

    # Calculate Delta_t
    calculate_deltat(ss.snapshots)
    sql.session.flush()

    process_lotz_files(ss)
    sql.session.flush()
    

    print "Done."

    return ss
    # Build FITS columns
#     columns = []
#     format = {types.FloatType: "D", types.IntType: "J", types.StringType: "A"}
#     snaps = ss.snapshots
#     snaps.sort (time_compare)
#     keys =snaps [0].keywords.keys ()
#     keys.sort () 
#     for kw in keys:
#         values = []
#         for s in snaps:
#             # duplicate value for all cameras
#             values += [s.keywords[kw]]*len(s.cameras)
#         columns.append(make_fits_column(kw,values))

#     keys =snaps [0].cameras [0].keywords.keys ()
#     keys.sort () 
#     for kw in keys:
#         values = []
#         for s in snaps:
#             for i in range (len (s.cameras)):
#                 try:
#                     values.append (s.cameras [i].keywords [kw])
#                 except KeyError:
#                     # not found -- just substitute zero 
#                     values.append(Keyword(0,"",""))
#         columns.append(make_fits_column(kw,values))

#     #keys =snaps [0].cameras [0].magnitudes.keys ()
#     #keys.sort ()
#     # do we need to have these in order? 
#     #keys = magnitude_order
#     for kw in filters+map(lambda x:x+"_NODUST", filters):
#         values = []
#         for s in snaps:
#             for i in range (len (s.cameras)):
#                     values.append (s.cameras [i].magnitudes [kw])
#         columns.append(make_fits_column(kw,values))


#     # Write fits file
#     try:
#         os.unlink ("index.fits")
#     except:
#         pass
#     index_HDU = pyfits.new_table (columns)
#     HDU_list = pyfits.HDUList ([pyfits.PrimaryHDU (),index_HDU])
#     HDU_list.writeto ("index.fits") 

#     # print list of snapshots in time order
#     order_file = open ("snapshot_order", "w")
#     for s in snaps:
#         print >> order_file,s.keywords ["snapshot_file"].value[1:-1]
#     order_file.close ()
