import httplib, datetime, time, sys

class EST(datetime.tzinfo):
    def utcoffset(self, dt):
        return datetime.timedelta(hours=0)
    def dst(self, dt):
        return datetime.timedelta(0)
    def tzname(self,dt):
        return "EST"

def readwatts():
    c=httplib.HTTPConnection('www.wattvision.com')
    oldt=datetime.datetime.now(tz=EST())
    while(True):
        
        c.request('GET','/api/download/latest?h=263927&k=59c3dcadf5720ae7&v=0.1')
        r=c.getresponse()
        if r.status != 200:
            print "HTTP error"
            break

        d=r.read()[1:-1].split(',')
        d=[int(dd) for dd in d]
        # d is a list of (unix time stamp, watt, cost)
        
        t=datetime.datetime.fromtimestamp(d[0], tz=EST())
        #t=datetime.datetime.fromtimestamp(d[0])
        if t!=oldt:
            dt = (t-oldt).seconds
            # naive estimate of power based on one revolution (7.2Wh =
            # 25920 Ws) over the delta-t
            Pest = 25920./dt
            print t,"%dW (delta-t %d s, estimated %fW)"%(d[1],dt, Pest)
            sys.stdout.flush()
            
        time.sleep(14)
        oldt=t
        
