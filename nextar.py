import datetime

verbose=False
def flush(serial):
    serial.read(serial.inWaiting())
    
def send_cmd(serial, cmd):
    if verbose:
        print "Sending \"%s\""%`cmd`
    serial.write(cmd+"\n")
    return read_response(serial)

def read_response(serial):
    "Read response from nexstar, until a #"
    resp=[]
    while True:
        c=serial.read(1)
        if c=='':
            # Error
            return None
        if c=='#':
            break
        resp.append(c)

    if verbose:
        print "Got ",`resp`
    return ''.join(resp)

def get_pos(serial):
    if not serial:
        return 0.,0.
    resp = send_cmd(serial, "Z");
    if resp:
        az,alt = resp.split(',')
        alt= 360.*int(alt,16)/65536
        if alt>180:
            alt = alt-360
        return 360.*int(az,16)/65536,alt
    else:
        flush(serial)
        return get_pos(serial)

def set_slewrate_axis(serial, rate, axis):
    """Set the slew rate of the specified axis (16 for az, 17 for alt)
    to the specified vale in deg/sec."""
    if not serial:
        return

    d = 6 if rate > 0 else 7
    
    # max commandable slew rate is about 4.5 deg/s
    absrate = min(abs(rate), 4.5)
        
    h = int(absrate*3600*4)/256
    l = int(absrate*3600*4)%256
    r = send_cmd(serial,"P\x03"+chr(axis)+chr(d)+chr(h)+chr(l)+"\0\0")
    if r==None:
        print "Error setting slew rate"
    
def set_slewrate(serial, az_rate, alt_rate):
    set_slewrate_axis(serial, az_rate, 16)
    set_slewrate_axis(serial, alt_rate, 17)
    
def goto_pos(serial, az, alt):
    "Point telescope at the given alt-az location."
    if not serial:
        return
    # it appears to refuse to slew below the horizon. it will track there though
    #if alt<0:
    #    alt = 0
    azcnt = int(az/360.*65536)
    altcnt = int(alt/360.*65536)
    r = send_cmd(serial, str.format('B{:04x},{:04x}', azcnt, altcnt))
    if r==None:
        print "Error sending goto"
    
def init(serial, pos=(19,49,42,0,155,28,5,1)):
    "Sets tracking mode, position, and location. Default location is Mauna Kea summit."
    flush(serial)
    r = send_cmd(serial, "T\0")
    assert r==''
    r = send_cmd(serial, "W%c%c%c%c%c%c%c%c"%pos)
    assert r==''
    now = datetime.datetime.utcnow()
    r = send_cmd(serial, "H%c%c%c%c%c%c%c%c"%(now.hour, now.minute, now.second, now.month, now.day, now.year-2000,0,0))
    assert r==''
    
    
