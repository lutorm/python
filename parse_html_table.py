from HTMLParser import HTMLParser
from string import *
import numpy,types
import pdb

class MyHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        #    self.table=None
        #    self.row=None
        self.active=[False]

    def handle_starttag(self, tag, attrs):
        #print "Start tag",tag
        tag = upper(tag)
        if tag=="TBODY":
            self.table = []
            self.active.append(False)
        elif tag=="TR":
            self.row = []
            self.active.append(False)
        elif tag=="TD":
            self.active.append(True)
            self.data = ""
        else:
            self.active.append(False)
            
    def handle_endtag(self, tag):
        #print "End tag",tag
        tag = upper(tag)
        a= self.active.pop()
        if tag=="TR":
            self.table.append(self.row)
        elif tag=="TD" and a:
            self.row.append(self.data)
                
    def handle_data(self, data):
        #print "Data",data
        if self.active[-1]:
            self.data += data
        

        
def read(file):
    """(Late add) I think the purpose of this function is to parse a
    table in HTML into a python list structure, and that it was used
    for parsing the SINGS data from the paper."""

    f=open(file)
    t=f.read()

    p=MyHTMLParser()
    p.feed(t)

    # now we need to translate the table from list of rows to list of
    # cols

    data=[]
    for i in range(len(p.table[0])):
        data.append([])

    #pdb.set_trace()
    for i in range(len(p.table)):
        #print p.table[i]
        if not None in p.table[i]:
            for j in range(len(p.table[i])):
                t=p.table[i][j]
                d=1
                #print "converting",t
                if t=='':
                    d=float("nan")
                elif t[:1].isdigit():
                    try:
                        k=1
                        while k<=len(t):
                            d=float(t[:k])
                            #print k,t[:k]
                            k+=1
                    except:
                        pass
                else:
                    d=t
                    #print d

                #print "\t",d
                data[j].append(d)
        else:
            pass

    for i in range(len(data)):
        if type(data[i][0])==types.FloatType or \
           type(data[i][0])==types.IntType:
            data[i] = numpy.array(data[i])
    
    return data
