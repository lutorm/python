#!/usr/bin/python

# Generates the SQL entries and image/thumbnail directories for the photo album
# (C) 2008 Patrik Jonsson

import glob, commands, os, fnmatch, Image, EXIF, codecs, copy, pdb
PIL=Image

from photo_album_sql import *
metadata.bind.echo=False
#metadata.bind.echo=True

image_ext='.jpg'
n_pixels = 10000
thumbnail_size = 100
image_size = 700
image_min_size= 300

check_image_size = False

base_path = ur"C:\Documents and Settings\patrik\My Documents\My Pictures\photo album"
image_dest = ur"C:\Documents and Settings\patrik\My Documents\My Pictures\photo_html\images"
thumbnail_dest = ur"C:\Documents and Settings\patrik\My Documents\My Pictures\photo_html\thumbnails"

tags={'EXIF DateTimeOriginal': 'Date', \
      'Image Model': 'Camera Model', \
      'EXIF ExposureTime': 'Exposure Time', \
      'EXIF FNumber': 'Aperture', \
      'EXIF FocalLength': 'Focal Length', \
      'EXIF ISOSpeedRatings': 'ISO Speed', \
      'MakerNote ExposureMode': 'Exposure Mode', \
      'MakerNote MeteringMode': 'Metering Mode', \
      'MakerNote FlashMode': 'Flash Mode', \
      'MakerNote ShortFocalLengthOfLensInFocalUnits': \
      'Min Focal Length', \
      'MakerNote LongFocalLengthOfLensInFocalUnits': \
      'Max Focal Length', \
      'MakerNote ContinuousDriveMode': 'Drive Mode', \
      'MakerNote FocusMode': 'Focus Mode', \
      'MakerNote FocusType': 'Focus Type', \
      'MakerNote ImageSize': 'Image Size', \
      'MakerNote SubjectDistance': 'Subject Distance', \
      'MakerNote Quality': 'Quality', \
      'MakerNote SelfTimer': 'Self Timer', \
      'MakerNote FocalUnitsPerMM': 'Focal Units/mm', \
      'EXIF ExposureBiasValue': 'Exposure Bias', \
      'MakerNote FirmwareVersion': 'Firmware', \
      'EXIF ImageWidth': 'Width', \
      'EXIF ImageHeight': 'Height', \
      'MakerNote WhiteBalance': 'White Balance', \
      'EXIF FocalPlaneYResolution': 'Resolution', \
      'EXIF FocalPlaneResolutionUnit': 'Resolution Unit', \
      'EXIF MaxApertureValue': 'Max Aperture'}
tagnames = {}

def exif_make_number (item):
    "Converts EXIF ratio numbers to floating-point"
    if EXIF.FIELD_TYPES [item.field_type] [1] == "R" or \
       EXIF.FIELD_TYPES [item.field_type] [1] == "SR":
        # it's a ratio, extracts components
        v=item.printable
        try:
            divide = v.index ("/")
            numerator = float(v [: divide])
            denominator = float(v [divide + 1:])
            #1/0
            if numerator/denominator < 0.1:
                # avoid returning 0, prefer ratio
                return v
            else:
                return "%.1f" % (numerator/denominator)
        except ValueError:
            return v
    else:
        return item.printable

class photo_image:
    def __init__(self, path):
        # the unixified path to the image with de-unicoded chars
        self.dest_path = (os.path.splitext(path)[0]+image_ext).encode('us-ascii','xmlcharrefreplace').replace('\\','/')
        self.orig_image = os.path.join(base_path,path)
        self.dest_image = (os.path.splitext(os.path.join(image_dest,path))[0]+image_ext).encode('us-ascii','xmlcharrefreplace')
        self.dest_thumbnail = (os.path.splitext(os.path.join(thumbnail_dest,path))[0]+image_ext).encode('us-ascii','xmlcharrefreplace')
    
        image = self.check_image_file(self.dest_image,
                                      self.generate_image,
                                      lambda x: image_size in x or \
                                      image_min_size in x)
        thumbnail = self.check_image_file(self.dest_thumbnail,
                                          lambda : self.generate_thumbnail(image),
                                          lambda x: thumbnail_size in x)
        self.size = (image.size, thumbnail.size)

    def check_image_file (self, file, generate, check_size=lambda:True):
        """Checks for the presence of file, and if check_image_size is
        true, calls check_size to see if the size is correct. If the
        file does not exist, or is the wrong size, calls generate
        which returns the image."""
        # check for image
        try:
            image = PIL.open (file)

            # check image size
            if  check_image_size and not check_size(image.size):
                # wrong size
                print "Image wrong size, regenerating..."
                image = generate ()
        except IOError:
            # We couldn't open the image, so it doesn't exist. Create it
            #pdb.set_trace() 
            image = generate ()

        return image

    def calculate_output_size(self, original_size):
        scale = 1.0*image_size/max (original_size)
        if min((original_size[0]*scale, original_size [1]*scale)) < \
           image_min_size:
            scale= 1.0*image_min_size/ min(original_size)
        return (int(original_size [0]*scale), int(original_size [1]*scale))
    
    def generate_image (self):
        "Generates a scaled-down version of the image the object is pointing to"
        print "Generating image ", self.dest_image
        # if you don't do the convert, TIF images get saved as
        # garbled JPGs (PIL 1.1.6)
        image = PIL.open (self.orig_image).convert('RGB')
        size = self.calculate_output_size(image.size)
        image= image.resize (size, PIL.ANTIALIAS)
        image.save (self.dest_image)
            
        return image

    def generate_thumbnail (self, image=None):
        "Generates a thumbnail image of the image the object is pointing to"
        print "Generating thumbnail ", self.dest_thumbnail
        if image == None:
            image = PIL.open (self.orig_image).convert('RGB')
        else:
            # if we have an image supplied here, it's the resized
            # instance for the image. we need to duplicate it, because
            # otherwise both image and thumbnail will point to the
            # thumbnail
            image = image.copy()
            
        image .thumbnail ((thumbnail_size, thumbnail_size), PIL.ANTIALIAS)
        image.save (self.dest_thumbnail)
        return image

    def generate_exif (self, image):
        """Generates the exiftags data from the EXIF headers and adds to
        the SQL data for image."""
        
        f = open (self.orig_image, "rb")
        header = EXIF.process_file (f)
        f.close()

        existing_tagids = [x.tag_id for x in image.tags]
        
        for t in tags.keys():
            # only add it if it does not already exist
            # we assume if it's there its correct, since they don't change
            if t in header and tagnames[t].id not in existing_tagids:
                tag = Tag(image, tagnames[t].id, exif_make_number(header[t]))
                session.save(tag)
                
class photo_directory:

    def __init__(self, directory, parent, order):

        """Initialize a directory instance. directory is the path to
        the directory relative to base_path, parent is the parent
        photo_directory object. Order indicates the ordering of
        folders objects in the same folder."""

        assert order !=None

        # First load data and update SQL
        self.directory = directory

        # determine id and title
        if self.directory !='':
            title = os.path.split(self.directory)[-1]
            folder_number = directory.__hash__()
        else:
            title = u"Photo Album"
            folder_number = 1
            
        print "Creating directory ",title,self.directory
        print "\tFolder id",folder_number
        
        # Read folder caption
        caption = ""
        try:
            f = open (os.path.join (base_path, directory, "caption"), "r")
            caption = unicode(f.read (), 'utf-8')
        except:
            print "Could not read folder caption",caption
        print "Caption: ",caption
        
        # initialize base object
        if parent != None:
            parent_folder = parent.folder
        else:
            parent_folder = None

        self.folder = Folder.create_or_load(session, folder_number,
                                            title, parent_folder,
                                            caption, order)
        session.flush()

        # make sure directories exist
        create_dir(os.path.join(image_dest, self.directory))
        create_dir(os.path.join(thumbnail_dest, self.directory))
        
        # the sql objects that are currently in the database for
        # this folder are already in self.folder
        existing_folders = [ f.id for f in self.folder.subfolders ]
        existing_images =  [ i.id for i in self.folder.images ]
        
        # Find images and directories, creating objects (which add
        # themselves to the sql database)
        # listdir produces unicode
        entries = os.listdir (os.path.join(base_path, self.directory))
        
        image_list = []
        i=1
        for e in entries:
            e_path = os.path.join (self.directory, e)
            if os.path.isdir (os.path.join(base_path, e_path)):
                # it's a directory.

                # create subdir object
                d = photo_directory(e_path, self, i)
                i+=1

                #remove it from the existing list so we don't kill it later
                try:
                    existing_folders.remove(d.folder.id)
                except:
                    pass
            elif fnmatch.fnmatch (e, "*.jpg") or \
                 fnmatch.fnmatch (e, "*.JPG") or \
                 fnmatch.fnmatch (e, "*.TIF") or \
                 fnmatch.fnmatch (e, "*.tif"): 
                # add to image list (just file name, no path)
                image_list.append(e)
                print "\t",e_path

        captions = self.read_captions(image_list)

        # Add images
        for i in image_list:
            caption = captions[os.path.splitext(i)[0]]
            # create photo_image instance to generate image files and
            # get size
            pi = photo_image(os.path.join(self.directory, i))
            image = Image.create_or_load(session,
                                         caption[0], self.folder,
                                         pi.dest_path,
                                         caption[1], caption[2], pi.size[0],
                                         pi.size[1])
            pi.generate_exif(image)

            # remove it from the existing images list so we don't kill it
            try:
                existing_images.remove(image.id)
            except:
                pass
            
            print caption[0], pi.dest_path, caption[1], caption[2]

        # if we have objects in the existing_xxx lists now, they are
        # in the database but have been removed from the folder
        # hierarchy. They should be dropped from the database, too.

        map(lambda id: delete_object(id, Image, "image",
                                     cleanup_imagefiles),
            existing_images)
        map(lambda id: delete_object(id, Folder, "folder",
                                     cleanup_folder_contents),
            existing_folders)
                        
        session.flush()

    def read_captions(self, images):
        "Reads the image captions from the image_captions file."
        print "Reading captions"
        image_captions = {}
        n = 0
        try:
            capfile = os.path.join (base_path, self.directory,
                                    "image_captions")
            f = codecs.open (capfile, "r", 'utf-8')
            while 1:
                # parse file
                image = f.readline ().rstrip ()
                title = f.readline ().rstrip ()
                if title == u"#":
                    # we read the # so there is no caption or title
                    title = image
                    caption = u""
                else:
                    caption = u""
                    l = f.readline ()
                    while l[0] != u"#":
                        caption = caption +l
                        l = f.readline ()
                # add to dict. Images are ordered as in the captions file
                #print image, title, caption, n
                image_captions [image] = (title, caption, n)
                n += 1
        except IndexError:
            pass
        except IOError:
            print "Failed opening captions file",capfile

        # Now update the captions file with stubs for images not there
        f = codecs.open (capfile, "a", 'utf-8')
        for i in images:
            im=os.path.splitext(i)[0]
            if not image_captions.has_key (im):
                f.write (u"%s\n#\n" % im)
                #f.write (u"#\n")
                image_captions[im] = (im, u"", n)
                n += 1

        return image_captions

def create_dir(dir):
    """Creates directory (with non-ascii chars xml encoded) if it
    doesn't already exist."""
    try:
        os.mkdir (dir.encode('us-ascii','xmlcharrefreplace'))
    except:
        pass

def delete_file(file):
    """Tries to delete file but does nothing if it's not there."""
    try:
        os.unlink(file)
    except:
        pass
    
def delete_object(id, query_class, noun, predelete_action=lambda x:None):
    """Deletes the query_class object with primary key id from the
    database, printing a message. Predelete_action is a function which
    is called with the instance before deleting, to do cleanup tasks."""
    inst=session.query(query_class).get(id)
    predelete_action(inst)
    print "Deleting removed %s %s" % (noun, inst.title)
    session.delete(inst)

def cleanup_folder_contents(folder):
    """This function goes through and calls cleanup on the folder
    contents."""
    print "Cleaning up folder", folder.title
    map(cleanup_imagefiles, folder.images)
    map(cleanup_folder_contents, folder.subfolders)
    
def cleanup_imagefiles(image):
    """Deletes the image and thumbnail files for an Image instance."""
    print "Cleaning up image", image.path
    delete_file(os.path.join(image_dest, image.path))
    delete_file(os.path.join(thumbnail_dest, image.path))
    
def load_tagnames():
    """Loads the exif tag names from the database and puts in tagnames dict."""
    global tagnames
    for k in tags.keys():
        t = session.query(Tagname).filter_by(name=tags[k]).all()
        assert len(t)<2
        if len(t)==0:
            t = Tagname(tags[k])
            session.save(t)
            tagnames[k]=t
        else:
            tagnames[k]=t[0]
    session.flush()
        
def makeit():
    load_tagnames()
    session.clear()
    d=photo_directory(u'',None,1)
    print "Flushing database..."
    session.flush()
        
