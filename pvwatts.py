from numpy import *

def load_csv(file):
    "Load pvwatts csv file."

    d=loadtxt(file,delimiter=',',skiprows=16,dtype=str)
    n=d.shape[0]

    out={}
    out['hours']=zeros(n)
    out['dco']=zeros(n)
    out['temp']=zeros(n)
    out['irr']=zeros(n)

    for i in xrange(n):
        out['hours'][i]=float(d[i,2][1:-1])
        out['irr'][i]=float(d[i,7][1:-1])
        out['temp'][i]=float(d[i,8][1:-1])
        out['dco'][i]=float(d[i,9][1:-1])

    return out
