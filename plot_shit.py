from pylab import *
import numpy

def irxbeta(sunrisesim):
    clf()
    beta=[o.betaM95 for sn in sunrisesim.snapshots for ca in sn.cameras for o in ca.objects]
    l1600=[o.l_1600 for sn in sunrisesim.snapshots for ca in sn.cameras for o in ca.objects]
    ltir=[o.l_tir for sn in sunrisesim.snapshots for ca in sn.cameras for o in ca.objects]
    lines=[]
    lines.append(plot(beta, log10(array(ltir)/array(l1600)),'.r'))

    bfit=arange(100, dtype=numpy.double)/100.*3.0-2.2
    logirxfit=log10(10.**(0.4*(4.43+1.99*bfit))-1)+.076
    lines.append(plot(bfit,logirxfit,'-k'))

    mhc=load('/home/patrik/mhc99data.txt')
    mhcbeta = mhc[:,0]
    mhcirx = mhc[:,1]
    lines.append(plot(mhcbeta,mhcirx,'.b'))

    lines.append(plot([-0.66,-0.56, -0.87, -0.09,-0.98], \
         [2.47, 2.71,2.23,3.64, 2.48], 'g+'))
    lines.append(plot([-1.35, -0.64], [1.13,  2.48], 'gx'))

    xlabel('beta')
    ylabel('IRX (L_ir/L_uv)')
    legend(lines,['Sims','MHC99 fit','MHC99','ULIRGS','LIRGS'],
           'lower right')
    axis([-2.5,1,-1,4])
    

