import nextar, numpy, datetime, time, pytz, sys, select

class trackfile:
    def __init__(self, file, t0):
        "Load a trackfile with specified t0 (a datetime object)."
        self.f = numpy.loadtxt(file, delimiter=',')
        self.t0 = t0

        # until we know where we are in the file, set current pos to 0
        self.cur = None

    def get_track(self, t):
        """Return the (az pos, alt pos, az vel, alt vel) of the
        track for the specified time, expressed as a datetime."""

        tplus = (t-self.t0).total_seconds()

        # see if we are outside of the tracking file
        if tplus < self.f[0,0]:
            self.cur = 0
            azalt = (self.f[0,1],self.f[0,2])
            vel = (0.0,0.0)
        elif tplus > self.f[-1,0]:
            self.cur = self.f.shape[0]-1
            azalt = (self.f[-1,1],self.f[-1,2])
            vel = (0.0,0.0)
        else:
            if not self.cur or tplus < self.f[self.cur,0]:
                # do an ab initio search if time went backward
                self.cur = numpy.searchsorted(self.f[:,0], tplus)-1
            else:
                # assume that the current index is close and step it
                while self.f[self.cur+1, 0] < tplus:
                    self.cur = self.cur+1

            # we should now have our time between cur and cur+1
            assert self.f[self.cur, 0] <= tplus
            assert self.f[self.cur+1, 0] >= tplus

            # interpolate position and estimate vel
            frac = (tplus - self.f[self.cur, 0])/(self.f[self.cur+1, 0] -self.f[self.cur, 0])

            azalt = self.f[self.cur,1:] + (self.f[self.cur+1, 1:]-self.f[self.cur, 1:3])*frac
            vel = (self.f[self.cur+1, 1:]-self.f[self.cur, 1:])/(self.f[self.cur+1, 0]-self.f[self.cur, 0])

        print "Track time %f, az=%f, alt=%f vel=%f, %f"%(tplus,azalt[0],azalt[1], vel[0],vel[1])
        
        return (azalt[0],azalt[1],vel[0],vel[1])
    
def track(serial, file, t0, toffset):
    """Command the mount to track the positions in the file, with t0
in the file corresponding to the specified time, offset by the
    specified offset. A larger offset makes the track time larger. The
    offset can be tweaked in 1s steps by typing + or - followed by enter."""


    t=trackfile(file, t0)
    
    # empirically determined to be stable at 0.5s control cycle
    P = 1

    utc=pytz.timezone('UTC')
    now=datetime.datetime.now(utc)

    nextar.flush(serial)
    
    # Issue goto to first position and wait for it to complete
    # Even a minor goto takes a few seconds to complete so bias the time forward a bit
    start = t.get_track(now+datetime.timedelta(0,3+toffset))
    print "Moving to start position"
    
    nextar.goto_pos(serial, start[0], start[1])
    pos = nextar.get_pos(serial)
    time.sleep(0.5)
    pos2 = nextar.get_pos(serial)
    while abs(pos[0]-pos2[0])>1 or abs(pos[1]- pos2[1])>1:
        time.sleep(0.5)
        pos = pos2
        pos2 = nextar.get_pos(serial)
        print "Waiting for move to complete: %f,%f"%(pos2)

    # Now we should be close. Start tracking.
    while True:
        if select.select([sys.stdin,],[],[],0.0)[0]:
            c=sys.stdin.read(1)
            if c=='+':
                toffset = toffset + 1
            elif c=='-':
                toffset = toffset - 1
            print "Time offset is now %f"%toffset
                
        #print "updating track"
        now=datetime.datetime.now(utc)+datetime.timedelta(0,toffset)
        track = t.get_track(now)
        #print "getting position"
        cur_pos = nextar.get_pos(serial)
        pos_error = [track[0]-cur_pos[0], track[1]-cur_pos[1]]
        if pos_error[0]>180:
            pos_error[0] = pos_error[0]-360
        if pos_error[0]<-180:
            pos_error[0] = pos_error[0]+360
        if pos_error[1]>180:
            pos_error[1] = pos_error[1]-360
        if pos_error[1]<-180:
            pos_error[1] = pos_error[1]+360
            
        # Command velocity. Incorporate a simple P term, which seems sufficient.
        azvel = track[2] + pos_error[0]*P
        altvel = track[3] + pos_error[1]*P
        #print "setting slewrate"
        nextar.set_slewrate(serial, azvel, altvel)
        print "\tcur_az=%f, cur_alt=%f, az_err=%f, alt_err=%f\n"%(cur_pos[0], cur_pos[1], pos_error[0], pos_error[1])
        time.sleep(0.5)
