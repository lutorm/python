import snapshot
from numpy import *

def newstarify(input, output, asciifile, \
               UnitLength_in_cm = 3.085678e21, \
               UnitMass_in_g = 1.989e43,\
               UnitVelocity_in_cm_per_s = 1e5 ):
    """This takes an input gadget snapshot and converts it to a
    snapshot where bulge and disk particles are new star particles,
    with ages and metallicities taken from the asciifile output from
    sfrhist. Gas metallicities are also set from that file."""

    lengthunit = UnitLength_in_cm/(100*3.085678e19)
    massunit = UnitMass_in_g/(1000*1.989e30)
    timeunit = UnitLength_in_cm/(UnitVelocity_in_cm_per_s*3600*24*365.225)
    
    s=snapshot.GadgetSnapshot()
    f=open(input,'rb')
    s.read(f)
    f.close()

    ng = s.header.nParticles[snapshot.GAS]
    nd = s.header.nParticles[snapshot.DISK]
    nb = s.header.nParticles[snapshot.BULGE]
    nn = s.header.nParticles[snapshot.NEW]

    if nn>0:
        print "Sorry, this doesn't work with preexisting new star particles."
        assert False

    # extract data out of ascii file
    f=open(asciifile,'r')
    l=[x.split() for x in f.readlines()]

    pos=0
    gdata=l[pos:pos+ng]
    pos+=ng+3
    ddata=l[pos:pos+nd]
    pos+=nd+3
    bdata=l[pos:pos+nb]
    pos+=nb+3
    sdata=ddata+bdata
    # this is not in ID order now because disk and bulge are interpersed
    # sort it
    sdata.sort(cmp=lambda x,y:int(x[0])-int(y[0]))

    # now copy the data into the empty snapshot
    os=snapshot.emptyFullSnapshot()
    for f in s.fields():
        os[f] = s[f]

    nnn = nn + nd + nb

    # the ascii data is in ID order, so we need to shuffle the snapshot
    # arrays into id order for the assignment
    gv = os.gIdentity.argsort()
    sv = os.sIdentity.argsort()

    # update gas metallicities
    os.gMetallicity[gv]= array([float(x[9]) for x in gdata],
                                           dtype=float32)

    # now we just reinterpret all s* arrays to be new stars. This means we
    # have to update sType, and then fill the n* arrays with info. If
    # nn>0, we have to make sure to put the b+d particles before the n
    # ones, because that's the way they are ordered in teh global
    # arrays.

    # we also have to back-convert the units to snapshot units...
    os.sType[:]=snapshot.NEW

    os.nMetallicity.resize((nnn))
    os.nMetallicity[sv] =array([float(x[9]) for x in sdata],
                                             dtype=float32)

    os.nFormationTime.resize((nnn))
    os.nFormationTime[sv] =array([float(x[10]) for x in sdata],
                                             dtype=float32)/timeunit

    os.nOriginalSmoothingLength.resize((nnn))
    os.nOriginalSmoothingLength[sv] =array([float(x[8]) for x in sdata],
                                                       dtype=float32)/lengthunit

    os.nParentId.resize((nnn))
    os.nParentId[sv] =array([float(x[11]) for x in sdata],
                                                       dtype=float32)

    os.nOriginalMass.resize((nnn))
    os.nOriginalMass[sv] =array([float(x[12]) for x in sdata],
                                                       dtype=float32)/massunit

    os.header.nParticles[snapshot.DISK]=0
    os.header.nParticles[snapshot.BULGE]=0
    os.header.nParticles[snapshot.NEW]=nnn

    os.header.nTotal = os.header.nParticles

    of=open(output, 'wb')
    os.write(of)
    of.close()
