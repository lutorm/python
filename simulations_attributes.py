from simulations_sql import *
import types, re, sqlalchemy

def list_quantities(obj):
    """Lists the defined columns for the specified object or class,
    excluding those defined as foreign keys. These should thus be only
    columns that define quantities for the object."""
    return [c.name for c in obj.c if c.foreign_key == None]

def list_relations(obj):
    """Lists the attributes of obj that define either relations or
    backrefs to other tables."""
    return [x for x in dir(obj) if x[0]!='_' and x not in obj.c.keys() \
            and not callable(obj.__getattribute__(x)) and not x=='c']

def parent_attr(obj):
    """Returns the name of the parent attribute of obj by looking for
    the relation that is not mapped to a list."""
    for r in list_relations(obj):
        try:
            len(obj.__getattribute__(r))
        except:
            return r

def parent_relation(obj):
    """Returns a lambda of the praent attribute."""
    a = parent_attr(obj)
    if a != None:
        return lambda x: x.__getattribute__(a)
    else:
        raise AttributeError("Class %s has no parent" % `type(obj)`)
    
def find_quantity(object, attr):
    """Returns a function that retrieves the requested attribute from
    either the specified object or any of its progenitors higher up in
    the tree, as well as the join chain necessary to get to that
    quantity and the MySQL qualified name of the quantity (by
    recursively traversing the parent).""" 
    if not type(attr) ==types.ListType:
        attr = attr.split('.')

    if len(attr)>1:
        # we are looking for a quantity in a specific class, so then
        # we only have to check if this is that class, if not continue
        table = attr[0]
        identifier = attr[1]
    else:
        table = None
        identifier = attr[0]

    try:
        if (identifier in list_quantities(object)) and \
           ((table == None) or isinstance(object, eval(table))):
            # success, attribute is in the same table. make a lambda
            # object and return it
            print "Attribute %s found in class %s" % (identifier, type(object))
            return ((lambda obj: obj.__getattribute__(attr)), \
                    [], \
                    sqlalchemy.orm.object_mapper(object).local_table.name+"."+identifier)
        else:
            # look in the parent
            print "Attribute %s not found in class %s, trying parent" % (attr, type(object))
            parfun = find_quantity(parent_relation(object)(object), attr)
            return (lambda obj: \
                    parfun[0](parent_relation(obj)(obj)),
                    [parent_attr(object)] + parfun[1], parfun[2])
    except AttributeError:
        raise AttributeError("Object %s has no attribute %s" % \
                             (`type(object)`, attr))
                             
def enumerate_quantities(object):
    """Returns a dict of possible quantities that can be retrieved
    with find_quantity(object)."""
    l =  list_quantities(object)
    try:
        l += enumerate_quantities(object.__getattribute__(object.parent_attr()))               
    except TypeError:
        # we have hit the end of the chain
        pass
    return l

def construct_query(expr):
    """Constructs a SQLAlchemy query expression based on the list of
    string expressions in expr, e.g. ["sfr_tot>10","betaH98<0"]."""
    # for each item in expr, we find the join chain to it and then add
    # the expression
    regex = re.compile(r"([\w\.]+).*")
    query = ioq
    
    for e in expr:
        print "Parsing expression %s" % e
        identifier = regex.search(e).group(1)
        
        # ioq[0] is just a random image_object which we need for the
        # traversal
        attr = find_quantity(ioq[0], identifier)
        fqe = e.replace(identifier, attr[2])
        print "Fully qualified expression: %s" % fqe

        query = reduce(lambda x,y: x.join(y), \
                       attr[1], query)
        query = query.filter(fqe)
        query.reset_joinpoint()
        
    return query

def get_selection(id):
    """Returns the Image_object query object for the objects that are included
    in the selection id."""
    return ioq.select_from(image_objects_table.join(selectionitems_table).join(selection_table)).filter(selection_table.c.id==id)

def new_selection(query):
    """Sets the selection id to be the objects defined in the
    query. The query return Image_objects."""
    # make a new entry in the selection table, and retrieve the id
    sel_id = selection_table.insert().execute().last_inserted_ids()[0]
    # insert the selection entries by iterating over the query
    ids = [o.id for o in query]
    y=[{'selection_id': sel_id, 'object_id': i} for i in ids]
    selectionitems_table.insert().execute(y)
    return sel_id


    
