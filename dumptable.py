import pyfits

def dump_table(files,outfile,hdu=1):
    """Dumps a set of fits tables to a simple text file table."""
    outf = open(outfile, 'w')

    # dump column names
    for i in pyfits.open(files[0])[hdu].columns:
        print >>outf, i.name,'\t',
    print >>outf

    for f in files:
        ext=pyfits.open(f)[hdu]
        ncol = len(ext.columns)

        # dump data
        for i in ext.data:
            for j in range(ncol):
                print >> outf, i.field(j),'\t',
            print >> outf

