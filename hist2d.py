from numpy import *
from whist import *
import pdb

def hist2d(vx,vy, wt=None, nbin=(10,10), xrange=(None,None), 
           yrange=(None,None)):
    """v1 and v2 are numpy arrays containing the values to be
histogrammed. weights can optionally contain the weights of those
points."""
    pdb.set_trace()

    # Check dimensions
    sx   = len(vx)
    sy   = len(vy)
    assert sx == sy

    # initialize weights if not specified
    if not wt:
        wt = ones_like(vx)
    else:
        assert len(wt) == sx

    # get limits
    xmin = xrange[0] if xrange[0] else min(vx)
    xmax = xrange[1] if xrange[1] else max(vx)
    ymin = yrange[0] if yrange[0] else min(vy)
    ymax = yrange[1] if yrange[1] else max(vy)

    # Remove data points outside MAX/MIN range
    # (rather keep them inside the range)
    iout = (vx >= xmin) & (vx <= xmax) & (vy >= ymin) & (vy <= ymax)
    vx=vx[iout]
    vy=vy[iout]
    wt=wt[iout]

    # create 1D scalar variable from the 2D variables
    xbinsz = (xmax-xmin)/nbin[0]
    ybinsz = (ymax-ymin)/nbin[1]
    ix = floor((vx-xmin)/xbinsz)
    iy = floor((vy-ymin)/ybinsz)
    v = ix + iy*nbin[0]

    # make histogram
    h,bins,b = whist(v, bins=linspace(0,nbin[0]*nbin[1],nbin[0]*nbin[1]+1), w=wt)

    # fold histogram back into 2D
    h = reshape(h, (nbin[0],nbin[1]))

    return h
