import datetime,pdb, commands, sys
import serial, rrdtool, struct, os.path, crcmod, serial, threading
from time import sleep, localtime, asctime, strftime, gmtime
import xbee_serial, xbee_i2c, math
from operator import itemgetter
import urllib, urllib2
import numpy
import mqtt

heatingdir = "/home/patrik/heating"
#heatingdir = "/Users/patrik/heating"
pngdir="/var/www/rrd"

probes = {
    '28C2BB7202000030': 'Hotbox enclosure',
    '288AC17202000034': 'used to be Steam Header',
    '286C2B72020000C8': 'Hotbox heater',
    '283BF071020000C5': 'Hotbox ambient',
    '28C2547202000085': 'Hotbox bottom',
    '287C9372020000E2': 'Hotbox top',
    '28E2A77102000053': 'Outside (radio mast)',
    '28C3B672020000EB': 'used to be Living room rad',
    '28EA2273020000EC': 'Outside (north side)',
    '2864A57102000088': 'used to be Living room',
    '484F54424F585554': 'Hotbox controller out',
    '484F54425852504D': 'Hotbox fan rpm',
    '4854425846414E43': 'Hotbox fan controller out',
    '484F54424F58494E': 'Hotbox controller in',
    '4C55584D45544552': 'Luxmeter',
    '4C55585341545552': 'Luxmeter saturation',
    '4C55584348304354': 'Luxmeter ch0',
    '4C55584348314354': 'Luxmeter ch1',
    '5052455353555245': 'Pressure',
    '5052455354454D50': 'Box temp',
    '4F55545354454D50': 'Outside temp',
    '4F55545348554D49': 'Outside RH',
    '5556464C55580000': 'UV flux (W/m^2)',
    '5556494E44455800': 'UV Index',
    '5241494E4D455452': 'Rain counter',
    '57494E444D455452': 'Wind speed',
    '57494E4444495252': 'Wind direction',
    '5642415454455259': 'Battery voltage',
    '4156525643430000': 'Supply voltage',
    '4241544348415247': 'Batt charging',
    '4241544641554C54': 'Charge fault',
    'rssi_0x13a20040939ce7L': 'Xbee RSSI',
    '3BF9D9410BF46D7F': 'Smoker burner',
    '534D4F4B52545031': 'Smoker probe top',
    '534D4F4B52545032': 'Smoker probe bottom',
    '534D4B5256424154': 'Smoker battery voltage',
    '534D4B4152445450': 'Smoker Arduino temp',
    '28AA5A8F4A140134': 'Drybox interior right side',
    '28DC17720200006A': 'Drybox interior left side',
    '28AA72084C14017C': 'Drybox interior top',
    '28B9F8710200007F': 'Dehumidifier cold sink',
    '28AA8EA64B14013D': 'Dehumidifier hot sink',
    '28327AF10C000008': 'Drybox ambient',
    '4845545253455450': 'Heater setpoint',
    '4845545254454D50': 'Heater temp',
    '4845415452524553': 'Heater resistance',
    '484541545250574D': 'Heater PWM',
    '424F5854454D5032': 'Dehumidifier temp 2',
    '424F5854454D5031': 'Dehumidifier temp 1',
    '424F5848554D5F31': 'Dehumidifier humidity 1',
    '424F5848554D5F32': 'Dehumidifier humidity 2',
    '424F584445575031': 'Dehumidifier dewpoint 1',
    '424F584445575032': 'Dehumidifier dewpoint 2',
    '46414E52504D5F31': 'Fan 1 RPM',
    '46414E52504D5F32': 'Fan 2 RPM',
    '46414E52504D5F30': 'Fan 3 RPM',
    '434F4F4C53455450': 'Cooler setpoint',
    '434F4F4C5250574D': 'Cooler PWM',
    '46414E50574D5F31': 'Fan 1 PWM',
    '46414E50574D5F32': 'Fan 2 PWM',
    '46414E50574D5F33': 'Fan 3 PWM',
    '445259424F585354': 'Drybox state',
    '424F585354435943': 'Drybox state cycles',
    }

# Resistance of the wind vane sensor, starting with 0 in 22.5 degree increments
windvane_resistors = [ 33e3, 6.57e3, 8.2e3, 891, 1e3, 688, 2.2e3,
                       1.41e3, 3.9e3, 3.14e3, 16e3, 14.12e3, 120e3,
                       42.12e3, 64.9e3, 21.88e3 ]

probefuncs = {
    # HTU21D report 999/998 for various problems - replace those with nans
    '4F55545354454D50': lambda x: x if x < 200 else float('nan'),
    '4F55545348554D49': lambda x: x if x < 200 else float('nan'),
    # convert rain counter to int
    '5241494E4D455452': lambda x: int(x),
    # wind meter produces a rate of one switch per second for a
    # windspeed of 1.492mph or 0.6670m/s
    '57494E444D455452': lambda x: x*0.66698368,
    # find the entry with the resistance most similar to the value we read and multiply by 22.5
    '57494E4444495252': lambda x: 22.5*min(enumerate([abs(r-x) for r in windvane_resistors]), key = itemgetter(1))[0] if x==x else x,
    # drybox state is an int
    '445259424F585354': lambda x: int(x) if x==x else x,
    # state cycles is also an int
    '424F585354435943': lambda x: int(x) if x==x else x,
    }

    
exit=False

def probename(sensor):
    if probes.has_key(sensor):
        return probes[sensor]
    else:
        return sensor

def read_sensor(ser, verbose=False):
    """Read data from the Arduino and return as a list of (ID, temp)
    tuples."""
    global exit
    if verbose:
        print "Entering read_sensor"
    tries=0
    while not exit:
        d=ser.readline()
        if d!="":
            if verbose:
                print "Received: %s"%(`d[:-1]`) # -1 becasue we have
                                        # an extra linebreak
        else:
            #print "got nada"
            pass
        
        if d[:4]=="DATA":
            try:
                dd=d.split()
                n = int(dd[1])
                tick= int("0x%s"%dd[3],0)
                #print "Found data start n=%d: %s"%(n,`d`)
                break
            except:
                pass
        tries+=1
        # serial timeout is 1 second so if we haven't gotten anything
        # for a while, fail out
        if tries>10:
            print "No data from sensor"
            return [],None

    if exit:
        return [],None

    # now we are ready to read n lines of data. we know that they are
    # supposed to be 13 bytes so we hard-read that many (otherwise
    # spurious newlines in the binary data will throw off the reading)
    chunksize=13
    input=[]
    crc=0xffff
    nread=0
    while(nread<n):
        d=ser.read(chunksize)
        if verbose:
            print "Got %d char chunk: %s"%(len(d),`d`)
        if len(d) < chunksize:
            print "Could not read entire chunk, packet corrupted"
            return [],None
        if d[-1] != '\n':
            print "Error: No endline at end of chunk, packet corrupted"
            return [],None
       
        input.append(d[:-1])
        nread+=1

    # we've read n lines. See if we get DONE
    d=ser.readline()
    if verbose:
        print "Got done chunk: %s"%`d`
    if d[:9]!="DONE CRC=":
        print "Incorrect exit from reading loop: %s"%`d`
        return [],None
    
    # check crc

    crc = 0
    try:
        crc=int("0x%s"%d[9:],0)
    except:
        print "\tCouldn't extract CRC, got",`d`
        return [],None
    crc16 = crcmod.predefined.Crc('crc-16')
    for i in input:
        crc16.update(i)
    #print "Checking CRC... %s"%hex(crc16.crcValue),
    if crc != crc16.crcValue:
        print "\tCRC check failed"
        return [],None
    else:
        pass

    # convert data to something sensible
    input=[ ('%X'%(struct.unpack(">Q",d[:8])),
             struct.unpack("f",d[8:])[0]) for d in input if d[:4]!="TICK"]

    # see if we got a tick in the binary data too
    tick2 = \
        [ struct.unpack("d",d[8:])[0] for d in input if d[:4]=="TICK" ]
    if len(tick2)>0:
        if tick2[0]!=tick:
            tick=tick2[0]
            print "Tick mismatch, real is = ",tick
    sys.stdout.flush()
    return input,tick

def create_database(sensor, interval=1):
    fnam=sensor+".rrd"

    if not os.path.exists(fnam):
        print "Creating RRD database %s"%fnam
        rrdtool.create(fnam,"--step",'1',
                       "DS:temperature:GAUGE:150:-30:125",
                       "RRA:AVERAGE:0.5:15:5760",
                       "RRA:AVERAGE:0.2:120:5040",
                       "RRA:AVERAGE:0.1:1800:2880",
                       # this last keeps 24h avg/min/max for 10y
                       "RRA:AVERAGE:0.1:86400:3650",
                       "RRA:MIN:0.1:86400:3650",
                       "RRA:MAX:0.1:86400:3650"
        )

def create_tickbase(interval=1):
    fnam='tickbase.rrd'
    if not os.path.exists(fnam):
        rrdtool.create(fnam,"--step",'1',
                       "DS:tick:GAUGE:60:U:U",
                       "RRA:MAX:.5:15:5760",
                       "RRA:MAX:.2:120:5040",
                       "RRA:MAX:.1:900:35064")

def transform_data(data):
    data_dict={}
    for d in data:
        sensor=d[0]
        value=d[1]
        if probefuncs.has_key(sensor):
            value=probefuncs[sensor](value)
        data_dict[sensor] = value
    return data_dict

def update_database(data):
    if len(data)==0:
        print "\nUpdate failed"
        return False
    else:
        print "\nUpdating RRD"
    for sensor in sorted(data.keys(), key = lambda s: probename(s)):
        value = data[sensor]
        fnam=sensor+".rrd"
        if type(value) is float:
            valstr = "%7.4f"%value
        else:
            valstr = "%d"%value
        print "%s: %s"%(probename(sensor).rjust(15), valstr)
        try:
            rrdtool.update(fnam,'N:%s'%valstr)
        except Exception as e:
            print "Error updating RRD"
    print
    sys.stdout.flush()
    return True

def update_wunderground(data):
    "Sends an update to Wunderground with supplied measurements."
    url="http://weatherstation.wunderground.com/weatherstation/updateweatherstation.php"
    wsid="KHIHILO5"
    pw="hundraprocentsluftfuktighet"
    
    # CHECK THAT WE HAVE THEM
    values = {
        'action': 'updateraw',
        'ID': wsid,
        'PASSWORD': pw,
        'dateutc': strftime('%Y-%m-%d %H:%M:%S', gmtime()),
        'winddir': data['57494E4444495252'],
        'windspeedmph': data['57494E444D455452']*2.2369363, # m/s to MPH
        'humidity': min(data['4F55545348554D49'],100),
        'tempf': data['4F55545354454D50']*1.8+32, # C to F
        'baromin': data['5052455353555245']*0.029529983, # mbar to in Hg
         # this is an estimate: 683lm/W, Sun luminous efficiency of
         # 13.6%. Assumes solar spectrum so won't be correct with
         # clouds etc...
        'solarradiation': data['4C55584D45544552']/(0.136*683),
        'UV': data['5556494E44455800'],
        'rainin': float(get_raintotal('1h')[0])/25.4 # mm to in
        }

    data = urllib.urlencode(values)
    req = urllib2.Request(url, data)
    resp = urllib2.urlopen(req, timeout=5)
    response = resp.read()
    if "success" not in response:
        print "Error updating Wunderground: ",response
        
def reset_device(cmd_port, reset_cmd_channel="D3"):
    """Resets the arduino by sending commands to toggle the xbee pin
    over the command channel."""
    print "Resetting port %s"%cmd_port
    try:
        if isinstance(cmd_port, str):
            cmd_port = serial.Serial(cmd_port, timeout=10)
        cmd_port.flushInput()
        cmd_port.write('\n{} \x04\n'.format(reset_cmd_channel))
        if "True" not in cmd_port.readline():
            print "Sending command failed"
        cmd_port.write('\n{} \x05\n'.format(reset_cmd_channel))
        if "True" not in cmd_port.readline():
            print "Sending command failed"
    except Exception as e:
        print "Error resetting port: %s"%(`e`)

def start_xbee_log():
    global exit
    exit = False
    
    ser=serial.Serial("/dev/ttyUSB0",57600, timeout=10)
    manager = xbee_serial.xbee_interface(ser)

    pm = pipe_manager()
    pm.setup_xbee_pipes(manager)

    #print "Starting hotbox thread"
    #hotboxthread = threading.Thread(target=hotbox_log, args=(manager,))
    #hotboxthread.start()

    print "Starting plotting thread"
    plotthread =  threading.Thread(target=plot_thread)
    plotthread.start()

    print "Starting weatherstation thread"
    #wsthread=threading.Thread(heating.logging_mainloop, args=("/home/patrik/xbee_0x13a20040939ce7L",rssi_port='../xbee_0x13a20040939ce7L_cmd',cmd_port='../xbee_0x13a20040939ce7L_cmd'))
    #wsthread.start()

    try:
        while True:
            sleep(10)
    except:
        pass

    print "Exiting"
    exit = True
    #hotboxthread.join()
    plotthread.join()
    wsthread.join()
    manager.close()
    del manager
    ser.close()

def hotbox_log(manager):
    hotbox = '\x00\x13\xa2\x00\x40\x9e\xda\x5b'
    hotbox_serial = manager.open_device(hotbox)
    logging_mainloop(hotbox_serial)

def weatherstation_log(manager):
    ws_xbee = '\x00\x13\xa2\x00@\x93\x9c\xe7'
    ws_serial = manager.open_device(ws_xbee)
    logging_mainloop(ws_serial)

def read_rssi(port):
    "Send AT command to query the signal strength and return a tuple (xbee s/n, dBm)."
    try:
        p = serial.Serial(port, timeout=10)
        p.flushInput()
        p.write('sh\nsl\ndb\n')
        sh=p.readline().decode('string_escape')
        sl=p.readline().decode('string_escape')
        db=p.readline().decode('string_escape')
        p.close()

        if sh and sl:
            ser = hex(struct.unpack('>I', sh.decode('string_escape')[:-1])[0] <<32 | struct.unpack('>I', sl.decode('string_escape')[:-1])[0])

            if db:
                rssi =  -struct.unpack("B",db[0])[0]

                return ("rssi_"+ser, rssi)
    except Exception as e:
        print "Error reading RSSI: %s"%(`e`)
        
    return None

def read_vsupply(port):
    "Send AT command to query the signal strength and return a tuple (xbee s/n, dBm)."
    try:
        p = serial.Serial(port, timeout=10)
        p.flushInput()
        p.write('sh\nsl\n%v\n')
        sh=p.readline().decode('string_escape')
        sl=p.readline().decode('string_escape')
        v=p.readline().decode('string_escape')
        p.close()

        if sh and sl:
            ser = hex(struct.unpack('>I', sh.decode('string_escape')[:-1])[0] <<32 |
                      struct.unpack('>I', sl.decode('string_escape')[:-1])[0])

            if v:
                v =  struct.unpack(">H", v[:-1])[0]*1.200/1024

                return ("vsupply_"+ser, v)
    except Exception as e:
        print "Error reading Vsupply: %s"%(`e`)
        
    return None

def read_sn(port):
    "Send AT commands to query the serial number of the Xbee connected to port."
    try:
        port.write('sh\nsl\n')
        sh=port.readline().decode('string_escape')
        sl=port.readline().decode('string_escape')

        if sh and sl:
            ser = hex(struct.unpack('>I', sh.decode('string_escape')[:-1])[0] <<32 |
                      struct.unpack('>I', sl.decode('string_escape')[:-1])[0])

            return ser
    except Exception as e:
        print "Error reading serial number: %s"%(`e`)
        
    return None

def read_node_identifier(port):
    "Send AT command through the specified port to query the node identifier."
    try:
        port.write('ni\n')
        ni=port.readline().decode('string_escape')
        # remove trailing newline
        return ni[:-1] 
    except Exception as e:
        print "Error reading node identifier: %s"%(`e`)
        
    return None

def logging_mainloop(port, cmd_port=None,
                     rssi_port=None, verbose=False,
                     update_wunderground=False):
    "Read the sensor data coming on the port and update database."
    global exit

    mqtt_topic = "homeassistant/sensor/xbee"
    
    os.chdir(heatingdir)

    device = serial.Serial(port, timeout=10, writeTimeout=10)

    try:
        device.flushInput()

        called_create=False
        last_good=datetime.datetime.now()
        last_dump=datetime.datetime.now()

        while not exit:
            try:
                data,tick=read_sensor(device, verbose)
                if data:
                    last_good = datetime.datetime.now()
            except Exception as e:
                print "Error reading sensor: %s"%(`e`)
                device.close()
                try:
                    device = serial.Serial(port, timeout=10, writeTimeout=10)
                except:
                    print "Could not open port"
                    sleep(15)
                    pass
                continue

            if rssi_port:
                rssi = read_rssi(rssi_port)
                if rssi:
                    data.append(rssi)

            if not called_create and len(data)>0:
                for d in data:
                    create_database(d[0])
                called_create=True

            data = transform_data(data)
            update_database(data)
            if update_wunderground:
                try:
                    update_wunderground(data)
                except Exception as e:
                    print "Exception updating wunderground: %s"%(`e`)

            # Publish data to MQTT broker. Since we in general don't
            # know what the data means here, we just send the data
            # each to their individual MQTT topics. The sensors have
            # to be configured manually in HA.
            for k,v in data.items():
                mqtt.publish_data("{}/{}/".format(mqtt_topic, k), v)
                    
            #pdb.set_trace()
            if datetime.datetime.now()-last_good > datetime.timedelta(0,300):
                print "Sensor comms seem down. Resetting"
                if cmd_port:
                    reset_device(cmd_port)
                else:
                    print "Can't reset device - no command port given"
                last_good = datetime.datetime.now()

    finally:
        device.close()
    print "logging thread exiting"


def airquality_mainloop(port, verbose=False):
    """Read the airquality sensor data from the specified port and send MQTT
    updates."""
    
    os.chdir(heatingdir)

    device = serial.Serial(port, timeout=100, writeTimeout=10)
    cmddevice = serial.Serial(port+"_cmd", timeout=10, writeTimeout=10)

    # first, query the node identifier of the xbee supplying the
    # data. This becomes the prefix for the channels.
    ni = read_node_identifier(cmddevice)
    if not ni:
        print "Could not get node identifier for port ",port
        return None
    sn = read_sn(cmddevice)
    if not sn:
        print "Could not get XBee serial number for port ",port
        return None

    mqtt_topic = "homeassistant/sensor/{}/".format(ni)
    device_map = {"identifiers": ["Air quality sensor {}".format(ni), sn],
                  "name": "Air quality sensor "+ni,
                  "manufacturer": "Patrik",
                  "model": "XBee air quality sensor",
                  "suggested_area": ni}

    # These are the channels we're interested in.
    channels = {
        "ROOMTEMP": ("Temperature", u"\u00b0C","TEMPERATURE","measurement"),
        "ROOMHUMI": ("Humidity", "%","HUMIDITY","measurement"),
        "ROOMPRES": ("Pressure", "Pa","ATMOSPHERIC_PRESSURE","measurement"),
        "ROOMTVOC": ("TVOC", "ppb","VOLATILE_ORGANIC_COMPOUNDS_PARTS","measurement"),
    }

    mqtt.publish_config(mqtt_topic, channels,
                        unique_id_prefix = ni+"_",
                        device_map = device_map,
                        expire_after = 95)
    
    try:
        device.flushInput()
        cmddevice.flushInput()
        
        last_good=datetime.datetime.now()
        last_dump=datetime.datetime.now()

        while True:
            try:
                data,tick=read_sensor(device, verbose)
                if data:
                    last_good = datetime.datetime.now()
            except Exception as e:
                print "Error reading sensor: %s"%(`e`)
                device.close()
                try:
                    device = serial.Serial(port, timeout=10, writeTimeout=10)
                except:
                    print "Could not open port"
                    sleep(15)
                    pass
                continue

            # data keys are hex values, we want to recode them into ascii
            # because we know they are ascii.
            data2={}
            for k,v in data:
                data2[str(bytearray.fromhex(k))] = None if math.isnan(v) else v

            if data2:
                mqtt.publish_data(mqtt_topic, data2)
            
            if datetime.datetime.now()-last_good > datetime.timedelta(0,300):
                print "Sensor comms seem down. Resetting"
                reset_device(cmddevice)
                last_good = datetime.datetime.now()

    finally:
        device.close()
    print "logging thread exiting"

def templogging_mainloop(port, verbose=False):
    """Read the temperature sensor data from the specified port and send MQTT
    updates."""
    
    os.chdir(heatingdir)

    device = serial.Serial(port, timeout=100, writeTimeout=10)
    cmddevice = serial.Serial(port+"_cmd", timeout=10, writeTimeout=10)

    # first, query the node identifier of the xbee supplying the
    # data. This becomes the prefix for the channels.
    ni = read_node_identifier(cmddevice)
    if not ni:
        print "Could not get node identifier for port ",port
        return None
    sn = read_sn(cmddevice)

    mqtt_topic = "homeassistant/sensor/{}/".format(ni)
    device_map = {"identifiers": [ni, sn],
                  "name": "Temperature sensor {}".format(ni),
                  "manufacturer": "Dallas",
                  "model": "DS18B20",
                  #"suggested_area": ni,
    }

    published_config = False
    
    try:
        device.flushInput()
        cmddevice.flushInput()
        
        last_good=datetime.datetime.now()
        last_dump=datetime.datetime.now()

        while True:
            try:
                data,tick=read_sensor(device, verbose)
                if data:
                    last_good = datetime.datetime.now()

                    if not published_config:
                        channels = {}
                        for k,v in data:
                            channels["onewire_"+k] = (k,
                                                      u"\u00b0C",
                                                      "TEMPERATURE",
                                                      "measurement")
                        mqtt.publish_config(mqtt_topic, channels,
                                            unique_id_prefix = ni+"_",
                                            device_map = device_map,
                                            expire_after = 95)
                        published_config = True

                    data2={}
                    for k,v in data:
                        data2["onewire_"+k] = None if math.isnan(v) else v
                    mqtt.publish_data(mqtt_topic, data2)
            
            except Exception as e:
                print "Error reading sensor: %s"%(`e`)
                device.close()
                try:
                    device = serial.Serial(port, timeout=10, writeTimeout=10)
                except:
                    print "Could not open port"
                    sleep(15)
                    pass
                continue

            
            if datetime.datetime.now()-last_good > datetime.timedelta(0,300):
                print "Sensor comms seem down. Resetting"
                reset_device(cmddevice)
                last_good = datetime.datetime.now()

    finally:
        device.close()
    print "logging thread exiting"
    

def plot_thread():
    global exit
    os.chdir(heatingdir)
    
    n=0
    while not exit:
        try:
            # update plots
            make_plots("2h")

            if n%10==0:
                # every 10 minutes
                make_plots("24h")

            if n%60==0:
                # every hour
                make_plots("7d")

            if n%60==0:
                make_plots("56d")

            if n%1440==0:
                # once per day
                make_plots("1y", True)
                
            if n%1440==0:
                make_plots("10y", True)
        except KeyboardInterrupt:
            exit = True
        except Exception as e:
            print "Error making plots: %s"%`e`
            
        n+=1
        sleep(60)

    print "plotting thread exiting"

def get_raintotal(time):
    "Returns the total amount of rain over the time period and the max rain rate in mm/h."
    rainscale = rrdtool.graph('test'
                              ,'--end','now','--start','end-%s'%time
                              ,'DEF:raincnt=weatherstation_rain.rrd:temperature:AVERAGE'
                              ,'CDEF:rainmmpers=raincnt,0.2794,*'
                              ,'VDEF:raintotal=rainmmpers,TOTAL'
                              ,'CDEF:rainmmperh=raincnt,1005.84,*'
                              ,'VDEF:rainmmperhmax=rainmmperh,MAXIMUM'
                              ,'PRINT:raintotal:%5.2lf'
                              ,'PRINT:rainmmperhmax:%5.2lf'
                              )[2]
    return rainscale

def get_powerscale(time, use_minmax):
    "Returns the total kWh produced and consumed, and the max power over the period."
    scale = rrdtool.graph('test'
                          ,'--end','now','--start','end-%s'%time
                          
                          ,'DEF:pac=Inv_Pac.rrd:temperature:AVERAGE'
                          ,'DEF:pdc=Inv_Pdc.rrd:temperature:AVERAGE'
                          ,'DEF:pdcmax=Inv_Pdc.rrd:temperature:MAX'
                          ,'DEF:totE_kWh_raw=Inv_tot_energy.rrd:temperature:MIN'

                          # Our total consumption, net, and production from curb
                          ,'DEF:curbconsumptionnan=House_consumption.rrd:temperature:AVERAGE'
                          ,'DEF:curbproductionneg=House_production.rrd:temperature:AVERAGE'
                          ,'CDEF:curbproductionnan=curbproductionneg,-1,*'
                          ,'DEF:curbnetnan=Grid_supply.rrd:temperature:AVERAGE'
                          
                          # Sub nans for 0s, except for the production we fall back to inverter production if available so we have the history
                          ,'CDEF:curbconsumption=curbconsumptionnan,UN,0.0,curbconsumptionnan,IF'
                          ,'CDEF:curbproduction=curbproductionnan,UN,pac,curbproductionnan,IF,0.0,ADDNAN'
                          ,'CDEF:curbnet=curbnetnan,UN,0.0,curbnetnan,IF'

                          # production
                          ,'CDEF:totE_kWh=totE_kWh_raw,UN,PREV,totE_kWh_raw,IF'
                          ,'VDEF:totE_first=totE_kWh,FIRST'
                          ,'CDEF:totE_kWh_trunc=totE_kWh_raw,totE_first,LE,UNKN,totE_kWh_raw,IF'
                          ,'VDEF:totE_kWh_max=totE_kWh_trunc,LAST'
                          ,'VDEF:totE_kWh_min=totE_kWh_trunc,FIRST'

                          # consumption
                          ,'VDEF:cons_Ws=curbconsumption,TOTAL'
                          ,'VDEF:prod_Ws=curbproduction,TOTAL'

                          ,'CDEF:maxwatts=%s,curbconsumption,MAXNAN'%('pdcmax' if use_minmax else 'pdc')
                          ,'VDEF:max_W=maxwatts,MAXIMUM'
                          ,'VDEF:maxcw=curbconsumption,MAXIMUM'
                          ,'CDEF:maxpwcurb=curbproduction,pdc,MAXNAN'
                          ,'VDEF:maxpw=%s,MAXIMUM'%('pdcmax' if use_minmax else 'maxpwcurb')

                          ,'PRINT:totE_kWh_min:%5.2lf'
                          ,'PRINT:totE_kWh_max:%5.2lf'
                          ,'PRINT:cons_Ws:%5.2lf'
                          ,'PRINT:prod_Ws:%5.2lf'
                          ,'PRINT:max_W:%5.2lf'
                          ,'PRINT:maxpw:%5.2lf'
                          ,'PRINT:maxcw:%5.2lf'
                          ,'PRINT:totE_first:%5.2lf'
                          )[2]
    print scale
    ss = [float(s) for s in scale]
    # convert Ws to kWh
    ss[2]/=3.6e6
    ss[3]/=3.6e6
    print "inverter kWh: %f, Consumption kWh: %f, Production kWh: %f, Max production W: %f, Max consumption W: %f"%(ss[1]-ss[0],ss[2],ss[3],ss[4],ss[5])
    
    return (numpy.nanmax([ss[1]-ss[0],ss[2],ss[3]]),numpy.nanmax([ss[3],ss[4],ss[5]]))

def make_plots(time, use_minmax = False):
    #print "Making %s plots"%time
    rrdtool.graph('%s/shed-%s.png'%(pngdir, time),'--imgformat','PNG','--width','1000',
                  '--height','300','--end','now','--start','end-%s'%time,#'--lazy',
                  '-v','deg C','-t','Geigerpad shed',
                  'DEF:arduino=28C2BB7202000030.rrd:temperature:AVERAGE',
                  #'DEF:header=288AC17202000034.rrd:temperature:AVERAGE',
                  #'DEF:hotboxheater=286C2B72020000C8.rrd:temperature:AVERAGE',
                  'DEF:shed=283BF071020000C5.rrd:temperature:AVERAGE',
                  'DEF:hotboxbot=28C2547202000085.rrd:temperature:AVERAGE',
                  'DEF:hotboxtop=287C9372020000E2.rrd:temperature:AVERAGE',
                  'LINE:arduino#000000:Arduino',
                  'GPRINT:arduino:LAST: (%2.1lf C)',
                  'LINE:hotboxbot#AAAA00:Hotbox bottom',
                  'GPRINT:hotboxbot:LAST: (%2.1lf C)',
                  'LINE:hotboxtop#00AAAA:Hotbox top',
                  'GPRINT:hotboxtop:LAST: (%2.1lf C)',
                  #'LINE:hotboxoutscaled#FF0000:Hotbox controller out (%)',
                  'LINE:shed#0000FF:Shed ambient',
                  'GPRINT:shed:LAST: (%2.1lf C)'
                  #'LINE:or#AA00AA:Rad (office)',
                  #'LINE:lr#999999:Rad (living room)',
                  #'AREA:scaledon#FF000020:Burner on',
                  #'CDEF:onmin=on,UN,0,on,IF,60,/',
                  #'VDEF:timeon=onmin,TOTAL',
                  #'GPRINT:timeon:Boiler run time %3.1lf minutes'
                  )

    rrdtool.graph('%s/hotbox-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300','--end','now','--start','end-%s'%time,
                  '-v',' ','-t','Hotbox heater','-r',#'--lazy',
                  '-l','0','-u','110',
                  'DEF:hotboxheater=286C2B72020000C8.rrd:temperature:AVERAGE',
                  'DEF:hotboxout=484F54424F585554.rrd:temperature:AVERAGE',
                  'DEF:fanout=4854425846414E43.rrd:temperature:AVERAGE',
                  'DEF:fanrpm=484F54425852504D.rrd:temperature:AVERAGE',
                  'CDEF:hotboxoutscaled=hotboxout,100,*',
                  'CDEF:fanoutscaled=fanout,100,*',
                  'CDEF:fanrpmscaled=fanrpm,100,/',
                  'LINE:hotboxheater#0000FF:Hotbox heating element',
                  'GPRINT:hotboxheater:LAST: (%2.1lf C)',
                  #'LINE:hotboxout#FF0000:Hotbox controller out (raw)',
                  'LINE:hotboxoutscaled#FF0000:Heater controller out',
                  'GPRINT:hotboxoutscaled:LAST: (%2.1lf %%)',
                  'LINE:fanoutscaled#FF00FF:Fan controller',
                  'GPRINT:fanoutscaled:LAST: (%2.1lf %%)',
                  'LINE:fanrpmscaled#00FFFF:Fan rpm',
                  'GPRINT:fanrpm:LAST: (%4.0lf rpm)',
                  )

    rrdtool.graph('%s/smoker-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300','--end','now','--start','end-%s'%time,
                  '-v',' ','-t','Smoker','-r',#'--lazy',
                  '-l','90','-u','200',
                  '--right-axis','0.01:2.20',
                  '--right-axis-label','Voltage/V',
                  '--right-axis-format','%1.2lf',
                  'DEF:burner=3BF9D9410BF46D7F.rrd:temperature:AVERAGE',
                  'DEF:smoker1=534D4F4B52545031.rrd:temperature:AVERAGE',
                  'DEF:smoker2=534D4F4B52545032.rrd:temperature:AVERAGE',
                  'DEF:vbat=534D4B5256424154.rrd:temperature:AVERAGE',
                  'DEF:ardtemp=534D4B4152445450.rrd:temperature:AVERAGE',
                  'CDEF:vbatscaled=vbat,100,*',
                  'CDEF:vbatoffset=vbatscaled,220,-',
                  'LINE:smoker1#0000FF:Smoker probe top',
                  'GPRINT:smoker1:LAST: (%2.1lf C)',
                  'LINE:smoker2#00FF00:Smoker probe bottom',
                  'GPRINT:smoker2:LAST: (%2.1lf C)',
                  'LINE:burner#FF0000:Smoker burner',
                  'GPRINT:burner:LAST: (%2.1lf C)',
                  'LINE:vbatoffset#AAAA00:Battery voltage',
                  'GPRINT:vbat:LAST: (%4.3lf V)',
                  'GPRINT:ardtemp:LAST: Enclosure\: %2.0lf C',
                  )
    
    maxval, minval = \
            rrdtool.graph('test',
                          '--end','now','--start','end-%s'%time
                          ,'DEF:weatherstation_temp=weatherstation_temp.rrd:temperature:AVERAGE'
                          ,'DEF:outside_temp=weatherstation_outside_temp.rrd:temperature:AVERAGE'
                          ,'CDEF:maxtemp=weatherstation_temp,outside_temp,MAX'
                          ,'CDEF:mintemp=weatherstation_temp,outside_temp,MIN'
                          ,'VDEF:max=maxtemp,MAXIMUM'
                          ,'VDEF:min=mintemp,MINIMUM'
                          ,'PRINT:max:%4.2lf'
                          ,'PRINT:min:%4.2lf'
                          )[2]
    
    # scale humidity so max temp is 100% and min temp is 60%
    maxval=float(maxval)
    minval=float(minval)
    humscale = (maxval-minval)/40.
    if not (humscale==humscale):
        minval = 0
        maxval = 1
        humscale = 1./40
        
    rrdtool.graph('%s/outside-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy',
                  '--right-axis','%f:%f'%(1./humscale,60-1./humscale*minval),
                  '--right-axis-label','humidity/%',
                  #'-A',#'-l','15','-u','22',
                  '--end','now','--start','end-%s'%time,
                  '-v','temp/C','-t','Geigerpad temperature and humidity','-r'
                  ,'DEF:shed=283BF071020000C5.rrd:temperature:AVERAGE'
                  ,'DEF:weatherstation_temp=weatherstation_temp.rrd:temperature:AVERAGE'
                  ,'DEF:outside_temp=weatherstation_outside_temp.rrd:temperature:AVERAGE'
                  ,'DEF:outside_max=weatherstation_outside_temp.rrd:temperature:MAX'
                  ,'DEF:outside_min=weatherstation_outside_temp.rrd:temperature:MIN'
                  ,'DEF:humidity=weatherstation_hum.rrd:temperature:AVERAGE'
                  ,'LINE:outside_temp#FF8800:Outside temp'
                  ,'GPRINT:outside_temp:LAST: (%2.1lf C)'
                  ,['LINE:outside_min#FF880069:Outside low'
                  ,'LINE:outside_max#FF880069:Outside high'] if use_minmax else []
                  ,'CDEF:humscaled=humidity,60,-,%f,*,%f,+'%(humscale, minval)
                  ,'LINE:humscaled#0000FF:Humidity'
                  ,'GPRINT:humidity:LAST: (%3.0lf %%)'
                  ,'LINE:weatherstation_temp#00800060:Weatherstation box temp'
                  ,'GPRINT:weatherstation_temp:LAST: (%2.1lf C)'
                  ,'LINE:shed#00000060:Basement temp'
                  ,'GPRINT:shed:LAST: (%2.1lf C)'
                  )

    rrdtool.graph('%s/light-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy',
                  '--end','now','--start','end-%s'%time,
                  '-v','lux','-t','Geigerpad ambient light level','-r'
                  ,'DEF:lux=weatherstation_lux.rrd:temperature:AVERAGE'
                  ,'DEF:uvi=weatherstation_uvi.rrd:temperature:AVERAGE'
                  ,'DEF:saturation=4C55585341545552.rrd:temperature:AVERAGE'
                  ,'DEF:ch0=4C55584348304354.rrd:temperature:AVERAGE'
                  ,'DEF:ch1=4C55584348314354.rrd:temperature:AVERAGE'
                  ,'CDEF:loglux=lux,0.01,LT,UNKN,lux,IF,LOG,10,LOG,/'
                  ,'LINE:loglux#FF0000:Ambient light'
                  ,'GPRINT:lux:LAST: (%5.1lf lux)'
                  ,'LINE:uvi#8800AA:UV Index'
                  ,'GPRINT:uvi:LAST: (%5.2lf)'
                  ,'VDEF:max=loglux,MAXIMUM'
                  ,'CDEF:sat=saturation,max,*'
                  ,'AREA:sat#FF000010:Luxmeter saturated'
                  ,'CDEF:ratio=ch0,ch1,/'
                  ,'CDEF:ratiolimit=ratio,6,GT,UNKN,ratio,IF'
                  ,'LINE:ratiolimit#55FF00:Count ratio'
                  )

    rrdtool.graph('%s/pressure-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy','-Y',
                  '--end','now','--start','end-%s'%time,
                  '-v','mbar','-t','Geigerpad atmospheric pressure','-r'
                  ,'DEF:p=weatherstation_pressure.rrd:temperature:AVERAGE'
                  ,'LINE:p#000000:Atmospheric pressure'
                  ,'GPRINT:p:LAST: (%5.2lf mbar)'
                  )

    # get rain total for this plot so we can scale
    rainscale = get_raintotal(time)
    try:
        raintotalscale = float(rainscale[0])/float(rainscale[1])
    except:
        raintotalscale = 1.0
    if raintotalscale == 0.0 or not (raintotalscale==raintotalscale):
        raintotalscale = 1.0
        
    rrdtool.graph('%s/rain-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy',
                  '--end','now','--start','end-%s'%time,
                  '--right-axis','%f:0'%raintotalscale,
                  '--right-axis-label','Rainfall (mm)',
                  '-v','Rainfall rate (mm/h)','-t','Geigerpad rainfall','-r'
                  ,'DEF:raincnt=weatherstation_rain.rrd:temperature:AVERAGE'
                  # each count is 0.2794mm of rain, and multiply by
                  # 3600 to convert rate to per hour.
                  ,'CDEF:rainmmperh=raincnt,1005.84,*'
                  ,'CDEF:rainmmpers=raincnt,0.2794,*'
                  ,'VDEF:raintotal=rainmmpers,TOTAL'
                  ,'CDEF:rainmmperscum=PREV,UN,0,PREV,IF,rainmmpers,UN,0,rainmmpers,IF,+'
                  ,'VDEF:lastrainmmperscum=rainmmperscum,LAST'
                  # ratio of lastrainmmperscum and raintotal is dt, so
                  # if we multiply the cumulative rate by that, we get
                  # mm. then to get cumulative backwards, we subtract
                  # it from the toal
                  ,'CDEF:rainmmcum=raintotal,raintotal,lastrainmmperscum,/,rainmmperscum,*,-'
                  # then scale it to the right axis
                  ,'CDEF:rainmmcumscaled=rainmmcum,%f,/'%raintotalscale
                  # to get the cumulative to roughly align on the same
                  # scale as the rate, we want to scale it by dt
                  ,'AREA:rainmmcumscaled#FF000020:Cumulative rainfall '             
                  ,'GPRINT:raintotal: (%5.2lg mm total)'
                  ,'LINE:rainmmperh#0000FF:Rainfall per hour'
                  ,'GPRINT:rainmmperh:LAST: (currently %5.2lf mm/h)'
                  )

    rrdtool.graph('%s/wind-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',##'--lazy',
                  '--end','now','--start','end-%s'%time,
                  '-v','Wind speed (m/s)','-t','Geigerpad wind','-r'
                  #,'--right-axis-label','Wind direction (degrees)'
                  ,'DEF:speed=weatherstation_windspeed.rrd:temperature:AVERAGE'
                  ,'DEF:maxspeed=weatherstation_windspeed.rrd:temperature:MAX'
                  ,'DEF:dir=weatherstation_winddir.rrd:temperature:AVERAGE'
                  ,'VDEF:wsmax=maxspeed,MAXIMUM'
                  ,'CDEF:wsmaxnonzero=wsmax,0.0,EQ,1.0,wsmax,IF,speed,+,speed,-'
                  ,'CDEF:diroffset=dir,360,/,wsmaxnonzero,*,wsmaxnonzero,-,0.25,*'
                  ,['LINE:maxspeed#FF0000:Max gust'] if use_minmax else []
                  ,'LINE:speed#00FF00:Wind speed'
                  ,'GPRINT:speed:LAST: (%5.2lf m/s)'
                  ,'HRULE:0#000000'
                  ,'LINE:diroffset#0000FF:Wind direction'
                  ,'GPRINT:dir:LAST: (%5.2lf deg)'
                  )

    rrdtool.graph('%s/station-status-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy',
                  '--end','now','--start','end-%s'%time,
                  '--right-axis','20:-158',
                  '--right-axis-label','RSSI (dBm)',
                  '-v','V','-t','Geigerpad weather station status','-r'
                  ,'DEF:vs=weatherstation_vs.rrd:temperature:AVERAGE'
                  ,'DEF:vbat=weatherstation_vbat.rrd:temperature:AVERAGE'
                  ,'DEF:charge=weatherstation_charge.rrd:temperature:AVERAGE'
                  ,'DEF:fault=weatherstation_fault.rrd:temperature:AVERAGE'
                  ,'DEF:rssi=rssi_0x13a20040939ce7L.rrd:temperature:AVERAGE'
                  ,'LINE:vs#000000:Regulator voltage'
                  ,'GPRINT:vs:LAST: (%4.2lf V)'
                  ,'LINE:vbat#0000ff:Battery voltage'
                  ,'GPRINT:vbat:LAST: (%4.2lf V)'
                  ,'CDEF:cs=charge,0.3,*'
                  ,'CDEF:co=cs,3.4,+'
                  ,'CDEF:rssioff=rssi,92,+,66,+'
                  ,'CDEF:rssisc=rssioff,0.05,*'
                  ,'LINE:rssisc#AAAA00:Xbee RSSI'
                  ,'GPRINT:rssi:LAST: (%2.0lf)'
                  ,'LINE:co#00FF00:Battery charging'
                  ,'GPRINT:charge:LAST: (%1.0lf)'
                  ,'CDEF:fs=fault,0.3,*'
                  ,'CDEF:fo=fs,3.41,+'
                  ,'LINE:fo#FF0000:Charge fault'
                  ,'GPRINT:fault:LAST: (%1.0lf)'
                  )

    # no longer have either curb or inverter so comment this out
    # powertotals = get_powerscale(time, use_minmax)
    # # the scale factor will be max W/total kWh
    # try:
    #     kwhscale = powertotals[0]/powertotals[1]
    # except:
    #     kwhscale = 1.0
    # if kwhscale == 0.0 or not (kwhscale==kwhscale):
    #     kwhscale = 1.0
    # print "powertotal is %s %s, kwhscale is %f"%(powertotals[0],powertotals[1],kwhscale)
    # # PV inverter plots
    # rrdtool.graph('%s/inverter-power-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
    #               '--height','300',##'--lazy',
    #               '--end','now','--start','end-%s'%time,
    #               '--right-axis','%f:0'%kwhscale,
    #               '--right-axis-label','Total energy (kWh)',
    #               '-v','W','-t','Geigerpad PV plant power','-r'
    #               ,'DEF:pac=Inv_Pac.rrd:temperature:AVERAGE'
    #               ,'DEF:pdc=Inv_Pdc.rrd:temperature:AVERAGE'
    #               ,'DEF:pacmax=Inv_Pac.rrd:temperature:MAX'
    #               ,'DEF:pdcmax=Inv_Pdc.rrd:temperature:MAX'
    #               ,'DEF:totE_kWh_raw=Inv_tot_energy.rrd:temperature:MIN'

    #               # the first value may be off because it's partial, so trash it
    #               ,'VDEF:totE_first=totE_kWh_raw,FIRST'
    #               ,'CDEF:totE_kWh_trunc=totE_kWh_raw,totE_first,LE,UNKN,totE_kWh_raw,IF'
    #               # if a value is unknown, replace with previous value
    #               # then if it is less than the previous, replace it with previous, because we know the counter never drops
    #               ,'CDEF:totE_kWh_tmp=totE_kWh_trunc,UN,PREV,totE_kWh_trunc,IF'
    #               ,'CDEF:totE_kWh=totE_kWh_tmp,PREV,LT,PREV,totE_kWh_tmp,IF'

    #               ,'VDEF:totE_kWh_max=totE_kWh,LAST'
    #               # to get cumulative with zero at present, subtract from max
    #               ,'CDEF:totE_kWh_flip=totE_kWh_max,totE_kWh,-'
    #               # then scale it to the right axis
    #               ,'CDEF:tot_kWh_scaled=totE_kWh_flip,%f,/'%kwhscale
    #               # to get delta, since we can't subtravt vdefs, we have to do this
    #               ,'VDEF:delta_kWh=totE_kWh_flip,MAXIMUM'
    #               ,'AREA:tot_kWh_scaled#20b0b020:Total production (inverter)\:'
    #               ,'GPRINT:delta_kWh:%.2lg kWh'

    #               # Our total consumption, net, and production from curb
    #               ,'DEF:curbconsumptionnan=House_consumption.rrd:temperature:AVERAGE'
    #               ,'DEF:curbproductionneg=House_production.rrd:temperature:AVERAGE'
    #               ,'CDEF:curbproductionnan=curbproductionneg,-1,*'
    #               ,'DEF:curbnetnan=Grid_supply.rrd:temperature:AVERAGE'

    #               # Sub nans for 0s, except for the production we fall back to inverter production if available so we have the history
    #               ,'CDEF:curbconsumption=curbconsumptionnan,UN,0.0,curbconsumptionnan,IF'
    #               ,'CDEF:curbproduction=curbproductionnan,UN,pac,curbproductionnan,IF,0.0,ADDNAN'
    #               ,'CDEF:curbnet=curbnetnan,UN,0.0,curbnetnan,IF'
                  
    #               # convert cons power first to Ws and then to kWh per bin
    #               ,'CDEF:curbcons_kWh=curbconsumption,STEPWIDTH,*,3.6e6,/'
    #               ,'CDEF:curbprod_kWh=curbproduction,STEPWIDTH,*,3.6e6,/'
    #               #,'CDEF:curbnet_kWh=curbnet,STEPWIDTH,*,3.6e6,/'
    #               #,'CDEF:curbcons_kWh=curbconsumption,STEPWIDTH,*,3.6e2,/'
    #               #,'CDEF:curbprod_kWh=curbproduction,STEPWIDTH,*,3.6e2,/'
    #               ,'CDEF:curbnet_kWh=curbnet,STEPWIDTH,*,3.6e6,/'
    #               # and make cumulative
    #               ,'CDEF:curbcons_kWh_cum=curbcons_kWh,PREV,0.0,MAXNAN,+'
    #               ,'VDEF:curbcons_kWh_max=curbcons_kWh_cum,LAST'
    #               ,'CDEF:curbprod_kWh_cum=curbprod_kWh,PREV,0.0,MAXNAN,+'
    #               ,'VDEF:curbprod_kWh_max=curbprod_kWh_cum,LAST'
    #               ,'CDEF:curbnet_kWh_cum=curbnet_kWh,PREV,ADDNAN'
    #               ,'VDEF:curbnet_kWh_max=curbnet_kWh_cum,LAST'
    #               # to get cumulative with zero at present, subtract from max
    #               ,'CDEF:curbcons_kWh_flip=curbcons_kWh_max,curbcons_kWh_cum,-'
    #               ,'CDEF:curbprod_kWh_flip=curbprod_kWh_max,curbprod_kWh_cum,-'
    #               ,'CDEF:curbnet_kWh_flip=curbnet_kWh_max,curbnet_kWh_cum,-'
    #               # then scale it to the right axis
    #               ,'CDEF:curbcons_kWh_scaled=curbcons_kWh_flip,%f,/'%kwhscale
    #               ,'CDEF:curbprod_kWh_scaled=curbprod_kWh_flip,%f,/'%kwhscale
    #               ,'CDEF:curbnet_kWh_scaled=curbnet_kWh_flip,%f,/'%kwhscale

    #               ,'AREA:curbprod_kWh_scaled#00ee9930:Total production (CURB)\:'
    #               #,'AREA:curbprod_kWh_cum#ff800020:Total CURB production'
    #               ,'GPRINT:curbprod_kWh_cum:LAST:%.2lg kWh'
    #               ,'AREA:curbcons_kWh_scaled#ff00ff30:Total consumption\:'
    #               #,'AREA:curbcons_kWh_cum#00FF0020:Total CURB consumption'
    #               ,'GPRINT:curbcons_kWh_cum:LAST:%.2lg kWh'
    #               ,'AREA:curbnet_kWh_scaled#40400050:Net consumption\:'
    #               #,'AREA:curbnet_kWh_cum#00000020:CURB net'
    #               ,'GPRINT:curbnet_kWh_cum:LAST:%.2lg kWh'
    #               ,'COMMENT:\n'
                  
    #               ,'LINE:pac#20cccc:Inverter AC power\:'
    #               ,'GPRINT:pac:LAST:%.2lf W'
    #               #,['LINE:pdc#0000ff:Inverter DC power'
    #               #,'GPRINT:pdc:LAST: (%4.2lf W)' ] if not use_minmax else [] # elide DC pwr over long timescale
    #               ,'LINE:curbproduction#00ee99:Production (CURB)\:'
    #               ,'GPRINT:curbproductionnan:LAST:%.2lf W'
    #               ,'LINE:curbconsumption#ff00ff:Consumption (CURB)\:'
    #               ,'GPRINT:curbconsumptionnan:LAST:%.2lf W'
    #               ,'COMMENT:\t\t\n'
    #               # a line for net is not so interesting, just the cum
    #               #,'LINE:curbnet#000000:CURB net'
    #               #,'GPRINT:curbnet:LAST: (%4.2lf W)'
    #               ,['LINE:pacmax#00ffff49:Max AC power'] if use_minmax else []
    #               #,'LINE:pgrid#999900:Power from grid'
    #               #,'GPRINT:pgrid:LAST: (%4.2lf W)'
    #               ,['LINE:pdcmax#0000ff59:Max DC power'] if use_minmax else []
                  
    #               )

    # rrdtool.graph('%s/inverter-panel-voltage-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
    #               '--height','300',##'--lazy',
    #               '--end','now','--start','end-%s'%time,
    #               '--right-axis','.1:0',
    #               '--right-axis-label','Current (A)',
    #               '-v','Voltage (V) / Power/5 (W)','-t','Geigerpad PV panel voltage','-r'
    #               ,'DEF:va=Inv_strA_Vpanel.rrd:temperature:AVERAGE'
    #               ,'DEF:vb=Inv_strB_Vpanel.rrd:temperature:AVERAGE'
    #               ,'DEF:ia=Inv_strA_Ipanel.rrd:temperature:AVERAGE'
    #               ,'DEF:ib=Inv_strB_Ipanel.rrd:temperature:AVERAGE'
    #               ,'CDEF:iasc=ia,10,*'
    #               ,'CDEF:ibsc=ib,10,*'
    #               ,'DEF:pa=Inv_strA_Ppanel.rrd:temperature:AVERAGE'
    #               ,'DEF:pb=Inv_strB_Ppanel.rrd:temperature:AVERAGE'
    #               ,'DEF:pamax=Inv_strA_Ppanel.rrd:temperature:MAX'
    #               ,'DEF:pbmax=Inv_strB_Ppanel.rrd:temperature:MAX'
    #               ,'CDEF:pasc=pa,5,/'
    #               ,'CDEF:pbsc=pb,5,/'
    #               ,'CDEF:pamaxsc=pamax,5,/'
    #               ,'CDEF:pbmaxsc=pbmax,5,/'
    #               ,'LINE:va#ff0000:String A panel voltage'
    #               ,'GPRINT:va:LAST: (%4.2lf V)'
    #               ,'LINE:iasc#ff8800:current'
    #               ,'GPRINT:ia:LAST: (%4.2lf A)'
    #               ,'LINE:pasc#ff00aa:power'
    #               ,'GPRINT:pa:LAST: (%4.2lf W)'
    #               ,['LINE:pamaxsc#ff00aa49:max power'] if use_minmax else []
    #               ,'COMMENT:\\n'
    #               ,'LINE:vb#0000ff:String B panel voltage'
    #               ,'GPRINT:vb:LAST: (%4.2lf V)'
    #               ,'LINE:ibsc#aa00ff:current'
    #               ,'GPRINT:ib:LAST: (%4.2lf A)'
    #               ,'LINE:pbsc#0088ff:power'
    #               ,'GPRINT:pb:LAST: (%4.2lf W)'
    #               ,['LINE:pbmaxsc#0088ff49:max power'] if use_minmax else []
    #               ,'COMMENT:\\n'
    #               )

    # rrdtool.graph('%s/inverter-panel-current-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
    #               '--height','300',#'--lazy',
    #               '--end','now','--start','end-%s'%time,
    #               '-v','A','-t','Geigerpad PV panel current','-r'
    #               ,'DEF:ia=Inv_strA_Ipanel.rrd:temperature:AVERAGE'
    #               ,'DEF:ib=Inv_strB_Ipanel.rrd:temperature:AVERAGE'
    #               ,'LINE:ia#ff0000:String A panel current'
    #               ,'GPRINT:ia:LAST: (%4.2lf A)'
    #               ,'LINE:ib#0000ff:String B panel current'
    #               ,'GPRINT:ib:LAST: (%4.2lf A)'
    #               )

    # rrdtool.graph('%s/inverter-panel-current-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
    #               '--height','300',#'--lazy',
    #               '--end','now','--start','end-%s'%time,
    #               '-v','A','-t','Geigerpad PV panel current','-r'
    #               ,'DEF:ia=Inv_strA_Ipanel.rrd:temperature:AVERAGE'
    #               ,'DEF:ib=Inv_strB_Ipanel.rrd:temperature:AVERAGE'
    #               ,'LINE:ia#ff0000:String A panel current'
    #               ,'GPRINT:ia:LAST: (%4.2lf A)'
    #               ,'LINE:ib#0000ff:String B panel current'
    #               ,'GPRINT:ib:LAST: (%4.2lf A)'
    #               )

    # rrdtool.graph('%s/inverter-grid-voltage-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
    #               '--height','300',#'--lazy','-Y',
    #               '--end','now','--start','end-%s'%time,
    #               '-v','V','-t','Geigerpad PV grid voltage','-r'
    #               ,'DEF:va=Inv_phA_V.rrd:temperature:AVERAGE'
    #               ,'DEF:vb=Inv_phB_V.rrd:temperature:AVERAGE'
    #               ,'LINE:va#ff0000:Grid phase A voltage'
    #               ,'GPRINT:va:LAST: (%4.2lf V)'
    #               ,'LINE:vb#0000ff:Grid Phase B voltage'
    #               ,'GPRINT:vb:LAST: (%4.2lf V)'
    #               )

    # rrdtool.graph('%s/inverter-grid-frequency-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
    #               '--height','300',#'--lazy',
    #               '-Y',
    #               '--end','now','--start','end-%s'%time,
    #               '-v','Hz','-t','Geigerpad PV grid frequency','-r'
    #               ,'DEF:f=Inv_freq.rrd:temperature:AVERAGE'
    #               ,'LINE:f#000000:Grid frequency'
    #               ,'GPRINT:f:LAST: (%4.2lf Hz)'
    #               )

    rrdtool.graph('%s/hanport-power-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy',
                  '--end','now','--start','end-%s'%time,
                  '-v','kW','-t','Kapahus power consumption','-r'
                  ,'DEF:ptot=1-0\:1.7.0.rrd:temperature:AVERAGE'
                  ,'DEF:p1=1-0\:21.7.0.rrd:temperature:AVERAGE'
                  ,'DEF:p2=1-0\:41.7.0.rrd:temperature:AVERAGE'
                  ,'DEF:p3=1-0\:61.7.0.rrd:temperature:AVERAGE'
                  ,'LINE:ptot#000000:Total incoming power'
                  ,'GPRINT:ptot:LAST: (%4.2lf kW)'
                  ,'LINE:p1#ff0000:L1 phase power'
                  ,'GPRINT:p1:LAST: (%4.2lf kW)'
                  ,'LINE:p2#0000ff:L2 phase power'
                  ,'GPRINT:p2:LAST: (%4.2lf kW)'
                  ,'LINE:p3#00ff00:L3 phase power'
                  ,'GPRINT:p3:LAST: (%4.2lf kW)'
                  )

    rrdtool.graph('%s/hanport-current-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy',
                  '--end','now','--start','end-%s'%time,
                  '-v','A','-t','Kapahus phase currents','-r'
                  ,'DEF:i1=1-0\:31.7.0.rrd:temperature:AVERAGE'
                  ,'DEF:i2=1-0\:51.7.0.rrd:temperature:AVERAGE'
                  ,'DEF:i3=1-0\:71.7.0.rrd:temperature:AVERAGE'
                  ,'LINE:i1#ff0000:L1 phase current'
                  ,'GPRINT:i1:LAST: (%4.2lf A)'
                  ,'LINE:i2#0000ff:L2 phase current'
                  ,'GPRINT:i2:LAST: (%4.2lf A)'
                  ,'LINE:i3#00ff00:L3 phase current'
                  ,'GPRINT:i3:LAST: (%4.2lf A)'
                  )

    rrdtool.graph('%s/hanport-voltage-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300',#'--lazy',
                  '--end','now','--start','end-%s'%time,
                  '-v','V','-t','Kapahus phase voltages','-r'
                  ,'DEF:u1=1-0\:32.7.0.rrd:temperature:AVERAGE'
                  ,'DEF:u2=1-0\:52.7.0.rrd:temperature:AVERAGE'
                  ,'DEF:u3=1-0\:72.7.0.rrd:temperature:AVERAGE'
                  ,'LINE:u1#ff0000:L1 phase voltage'
                  ,'GPRINT:u1:LAST: (%4.2lf V)'
                  ,'LINE:u2#0000ff:L2 phase voltage'
                  ,'GPRINT:u2:LAST: (%4.2lf V)'
                  ,'LINE:u3#00ff00:L3 phase voltage'
                  ,'GPRINT:u3:LAST: (%4.2lf V)'
                  )
    
    rrdtool.graph('%s/drybox_heater-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300','--end','now','--start','end-%s'%time,
                  '-v',' ','-t','Drybox controller','-r',#'--lazy',
                  '-l','0','-u','110',
                  'DEF:heater_setpt=drybox_heater_setpt.rrd:temperature:AVERAGE',
                  'DEF:heater_temp=drybox_heater_temp.rrd:temperature:AVERAGE',
                  'DEF:dehum_temp1=drybox_dehum_temp1.rrd:temperature:AVERAGE',
                  'DEF:dehum_hum1=drybox_dehum_hum1.rrd:temperature:AVERAGE',
                  'DEF:dehum_hum2=drybox_dehum_hum2.rrd:temperature:AVERAGE',
                  'DEF:left_temp=drybox_left.rrd:temperature:AVERAGE',
                  'DEF:right_temp=drybox_right.rrd:temperature:AVERAGE',
                  'DEF:top_temp=drybox_top.rrd:temperature:AVERAGE',
                  'DEF:ambient=drybox_ambient.rrd:temperature:AVERAGE',
                  'DEF:cooler_setpt=drybox_cooler_setpt.rrd:temperature:AVERAGE',
                  'DEF:coldsink=drybox_coldsink.rrd:temperature:AVERAGE',
                  'DEF:heater_pwm=drybox_heater_pwm.rrd:temperature:AVERAGE',
                  'DEF:cooler_pwm=drybox_cooler_pwm.rrd:temperature:AVERAGE',
                  'CDEF:heaterscaled=heater_pwm,100,*',
                  'CDEF:coolerscaled=cooler_pwm,100,*',
                  'COMMENT:Temperatures\:\\t\\t\g',
                  'LINE:dehum_temp1#AAAA00:Intake\g',
                  'GPRINT:dehum_temp1:LAST: (%2.1lf C)',
                  'LINE:top_temp#000000:Top\g',
                  'GPRINT:top_temp:LAST: (%2.1lf C)',
                  'LINE:left_temp#660000:Bottom left\g',
                  'GPRINT:left_temp:LAST: (%2.1lf C)',
                  'LINE:right_temp#000066:Bottom right\g',
                  'GPRINT:right_temp:LAST: (%2.1lf C)',
                  'LINE:ambient#00EE00:Ambient\g',
                  'GPRINT:ambient:LAST: (%2.1lf C)',
                  'COMMENT:\l',
                  'COMMENT:Humidity\:\\t\\t\g',
                  'LINE:dehum_hum1#BBEE77:Intake\g',
                  'GPRINT:dehum_hum1:LAST: (%2.1lf %%)',
                  'LINE:dehum_hum2#77EEBB:Exhaust\g',
                  'GPRINT:dehum_hum2:LAST: (%2.1lf %%)',
                  'COMMENT:\l',
                  'COMMENT:Controllers\:\\tHeater\:      \\t\g',
                  'LINE:heater_setpt#FF5555:Setpoint\g',
                  'GPRINT:heater_setpt:LAST: (%2.1lf C)',
                  'LINE:heater_temp#FF0000:Temp\g',
                  'GPRINT:heater_temp:LAST: (%2.1lf C)',
                  'LINE:heaterscaled#AA0000:PWM\g',
                  'GPRINT:heaterscaled:LAST: (%2.1lf %%)\g',
                  'COMMENT:\l',
                  'COMMENT:\\t\\tDehumidifier\:\\t\g',
                  'LINE:cooler_setpt#7777FF:Setpoint\g',
                  'GPRINT:cooler_setpt:LAST: (%2.1lf C)',
                  'LINE:coldsink#3333FF:Temp\g',
                  'GPRINT:coldsink:LAST: (%2.1lf C)',
                  'LINE:coolerscaled#4444AA:PWM\g',
                  'GPRINT:coolerscaled:LAST: (%2.1lf %%)',
                  'COMMENT:\l',
                  )



    try:
        dehum_state_num = int(rrdtool.graph('test','--end','now',
                                            '--start','end-%s'%time,
                                            'DEF:state=drybox_state.rrd:temperature:AVERAGE',
                                            'CDEF:scaledstate=state,6,*',
                                            'VDEF:currentstate=state,LAST',
                                            'PRINT:currentstate:%1.0lf')[2][0])
        dehum_states = ['Poweron','Init','Safe','Idle','Warmup', \
                        'Predry','Dry','Drydown','Cooldown']
        dehum_state = dehum_states[dehum_state_num]
    except ValueError:
        dehum_state = "Unknown"

    rrdtool.graph('%s/drybox_dehumidifier-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300','--end','now','--start','end-%s'%time,
                  '-v',' ','-t','Drybox Dehumidifier','-r',#'--lazy',
                  ['-l','10','-u','55'] if (dehum_state != 'Idle') else ['-l','-5','-u','30'],
                  'DEF:dehum_temp1=drybox_dehum_temp1.rrd:temperature:AVERAGE',
                  'DEF:dehum_temp2=drybox_dehum_temp2.rrd:temperature:AVERAGE',
                  'DEF:dehum_dewpt1=drybox_dewpt1.rrd:temperature:AVERAGE',
                  'DEF:dehum_dewpt2=drybox_dewpt2.rrd:temperature:AVERAGE',
                  'DEF:coldsink=drybox_coldsink.rrd:temperature:AVERAGE',
                  'DEF:hotsink=drybox_hotsink.rrd:temperature:AVERAGE',
                  'DEF:ambient=drybox_ambient.rrd:temperature:AVERAGE',
                  'DEF:state=drybox_state.rrd:temperature:AVERAGE',
                  'CDEF:scaledstate=state,5,*',
                  'COMMENT:Dehumidifier\:\\tIntake\: \\t\g',
                  'LINE:dehum_temp1#FF0000:Temp\g',
                  'GPRINT:dehum_temp1:LAST: (%2.2lf C)',
                  'LINE:dehum_dewpt1#FF55FF:Dewpoint\g',
                  'GPRINT:dehum_dewpt1:LAST: (%2.2lf C)',
                  'COMMENT:\l',
                  'COMMENT:\\t\\tExhaust\:\\t\g',
                  'LINE:dehum_temp2#BBEE77:Temp\g',
                  'GPRINT:dehum_temp2:LAST: (%2.2lf C)',
                  'LINE:dehum_dewpt2#AAAA77:Dewpoint\g',
                  'GPRINT:dehum_dewpt2:LAST: (%2.2lf C)',
                  'COMMENT:\l',
                  'LINE:scaledstate#000000:State\:\g',
                  'COMMENT:%s\\t\\t\\t\g'%dehum_state,
                  'LINE:ambient#00EE00:Ambient\g',
                  'GPRINT:ambient:LAST: (%2.2lf C)',
                  'LINE:coldsink#2222FF:Cold sink\g',
                  'GPRINT:coldsink:LAST: (%2.2lf C)',
                  'LINE:hotsink#FF7744:Hot sink\g',
                  'GPRINT:hotsink:LAST: (%2.2lf C)',
                  'COMMENT:\l',
                  )

    rrdtool.graph('%s/drybox_fans-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300','--end','now','--start','end-%s'%time,
                  '-v',' ','-t','Drybox Fans','-r',#'--lazy',
                  '-l','0','-u','8000',
                  'DEF:rpm1=drybox_fan1_rpm.rrd:temperature:AVERAGE',
                  'DEF:rpm2=drybox_fan2_rpm.rrd:temperature:AVERAGE',
                  'DEF:rpm3=drybox_fan3_rpm.rrd:temperature:AVERAGE',
                  'LINE:rpm1#FF0000:Fan1\g',
                  'GPRINT:rpm1:LAST: (%4.0lf RPM)',
                  'LINE:rpm2#00FF00:Fan2\g',
                  'GPRINT:rpm2:LAST: (%4.0lf RPM)',
                  'LINE:rpm3#0000FF:Fan3\g',
                  'GPRINT:rpm3:LAST: (%4.0lf RPM)',
                  'COMMENT:\l',
                  )

    rrdtool.graph('%s/drybox_state-%s.png'%(pngdir,time),'--imgformat','PNG','--width','1000',
                  '--height','300','--end','now','--start','end-%s'%time,
                  '-v',' ','-t','Drybox State Machine','-r',#'--lazy',
                  'DEF:state=drybox_state.rrd:temperature:AVERAGE',
                  'DEF:cycles=drybox_state_cycles.rrd:temperature:AVERAGE',
                  'LINE:state#000000:State\:\g',
                  'COMMENT:%s\\t\\t\\t\g'%dehum_state,
                  'LINE:cycles#0000FF:Cycles in state\:\g',
                  'GPRINT:cycles:LAST: %4.0lf',
                  'COMMENT:\l',
                  )
