"""Code for generating the PAH opacities from LI & Draine 01, etc."""

from math import *
from numpy import array
from pylab import *
import pdb, pyfits, vecops

def Drude(lam, *args):
    """Drude profile. Arguments are lambda0, gamma, sigma."""
    lam0, gam, sigint=args[0]
    return 2*gam*lam0*sigint/(( (lam/lam0 - lam0/lam)**2 + gam**2)*pi)

def cutoff(lam, lamc):
    y=lamc/lam
    return atan(1e3*(y-1)**3/y)/pi+0.5

def lamc(nc, ionized):
    m=M(nc)
    return 1e-6/(2.282/sqrt(m)+0.889) if ionized else \
           1e-6/(3.804/sqrt(m)+1.052)

def HC(Nc):
    """H/C ratio, eq 4 in LD01."""
    return 0.5 if Nc<=25 else (0.25 if Nc>=100 else 0.5/sqrt(Nc/25))

def M(nc):
    """Number of fuzed benzenoid rings."""
    return 0.4*nc if nc>40 else 0.3*nc

def Cpah_abs(lam, a, ionized, p, DL07=False):
    """Returns PAH abs cross section per carbon atom from
    Eqs 5-11 in LD01. """
    #pdb.set_trace()
    nc=Nc(a)
    x=1e-6/lam
    lc=lamc(nc,ionized)

    if x>17.25:
        # find nearest graphite
        g=argwhere(gsz>=a)[0][0]
        return vecops.interpolate_point_in_array(glam,Cgra[:,g],lam, True)
    if x>15:
        return (126.0 - 6.4943*x)*1e-22
    elif x>10:
        return (-3.0+1.35*x)*1e-22 + Cpah_abs_line(lam, nc, p[0:1,:])
    elif x>7.7:
        return (66.302 - 24.367*x + 2.950*x**2 - 0.1057*x**3)*1e-22
    elif x>5.9:
        return (1.8687 + 0.1905*x + 0.4175*(x-5.9)**2 + \
                0.04370*(x-5.9)**3)*1e-22+ Cpah_abs_line(lam, nc, p[1:2,:])
    elif x>3.3:
        return (1.8687 + 0.1905*x)*1e-22 + Cpah_abs_line(lam, nc, p[1:2,:])
    else:
        return 34.58*10**(-22-3.431/x)*cutoff(lam,lc) + \
               Cpah_abs_line(lam, nc, p[2:,:]) + \
               (3.5*10**(-23-1.45/x)*exp(-0.1*x*x) if (DL07 and ionized) \
                else 0)

def Cpah_abs_line(lam, nc, p):
    """Returns the line absorption profiles."""
    return reduce(add, \
                  [Drude(lam, (p[i,0]*1e-6, p[i,1], \
                               1e-22*p[i,2]*(1 if p[i,3]==0 else p[i,3]*HC(nc)))) \
                   for i in range(p.shape[0])]) 

# this is in um and 1e-20cm, but is converted to m in the function
LD01_neutral = array([
    [ 0.0722, 0.195, 7.97e7, 0],
    [ 0.2175, 0.217, 1.23e7, 0],
    [ 3.3,    0.012, 197,    1],
    [ 6.2,    0.032, 19.6*3, 0],
    [ 7.7,    0.091, 60.9*2, 0],
    [ 8.6,    0.047, 34.7*2, 1],
    [ 11.3,   0.018, 427,    1./3],
    [ 11.9,   0.025, 72.7,   1./3],
    [12.7,    0.024, 167.,   1./3],
    [16.4,    0.010, 5.52,   0],
    [18.3,    0.036, 6.04,   0],
    [21.2,    0.038, 10.8,   0],
    [23.1,    0.046, 2.78,   0],
    [26.0,    0.69,  15.2,   0] ])

LD01_ionized = array([
    [ 0.0722, 0.195, 7.97e7, 0],
    [ 0.2175, 0.217, 1.23e7, 0],
    [ 3.3,    0.012, 44.7,    1],
    [ 6.2,    0.032, 157*3, 0],
    [ 7.7,    0.091, 548*2, 0],
    [ 8.6,    0.047, 242*2, 1],
    [ 11.3,   0.018, 400,    1./3],
    [ 11.9,   0.025, 61.4,   1./3],
    [12.7,    0.024, 149.,   1./3],
    [16.4,    0.010, 5.52,   0],
    [18.3,    0.036, 6.04,   0],
    [21.2,    0.038, 10.8,   0],
    [23.1,    0.046, 2.78,   0],
    [26.0,    0.69,  15.2,   0] ])

DL07_neutral = array([
    [ 0.0722, 0.195, 7.97e7, 0],
    [ 0.2175, 0.217, 1.23e7, 0],
    [ 1.050,  0.055, 0,      0],
    [ 1.260,  0.11,  0,      0],
    [ 1.905,  0.09,  0,      0],
    [ 3.300,  0.012, 394,    1],
    [ 5.270,  0.034, 2.5,    0],
    [ 5.700,  0.035, 4.0,    0],
    [ 6.220,  0.030, 29.4,   0],
    [ 6.690,  0.070, 7.35,   0],
    [ 7.417,  0.126, 20.8,   0],
    [ 7.598,  0.044, 18.1,   0],
    [ 7.850,  0.053, 21.9,   0],
    [ 8.330,  0.052, 6.94,   1],
    [ 8.610,  0.039, 27.8,   1],
    [ 10.68,  0.020, 0.3,    1],
    [ 11.23,  0.012, 18.9,   1],
    [ 11.33,  0.032, 52.0,   1],
    [ 11.99,  0.045, 24.2,   1],
    [ 12.62,  0.042, 35.0,   1],
    [ 12.69,  0.013, 1.30,   1],
    [ 13.48,  0.040, 8.00,   1],
    [ 14.19,  0.025, 0.45,   0],
    [ 15.90,  0.020, 0.04,   0],
    [ 16.45,  0.014, 0.50,   0],
    [ 17.04,  0.065, 2.22,   0],
    [ 17.375, 0.012, 0.11,   0],
    [ 17.87,  0.016, 0.067,  0],
    [ 18.92,  0.100, 0.10,   0],
    [ 15,     0.8,   50,     0] ])

DL07_ionized = array([
    [ 0.0722, 0.195, 7.97e7, 0],
    [ 0.2175, 0.217, 1.23e7, 0],
    [ 1.050,  0.055, 2e4,    0],
    # the next has strength 7.8e3, not 0.078 as it is in the DL07
    # paper (Draine, private comm. Oct 21 2008)
    [ 1.260,  0.11,  7.8e3,  0],
    [ 1.905,  0.09,  -146.5, 0],
    [ 3.300,  0.012, 89.4,   1],
    [ 5.270,  0.034, 20.,    0],
    [ 5.700,  0.035, 32.,    0],
    [ 6.220,  0.030, 235.,   0],
    [ 6.690,  0.070, 59.0,   0],
    [ 7.417,  0.126, 181.,   0],
    [ 7.598,  0.044, 163.,   0],
    [ 7.850,  0.053, 197.,   0],
    [ 8.330,  0.052, 48.0,   1],
    [ 8.610,  0.039, 194.,   1],
    [ 10.68,  0.020, 0.30,   1],
    [ 11.23,  0.012, 17.7,   1],
    [ 11.33,  0.032, 49.0,   1],
    [ 11.99,  0.045, 20.5,   1],
    [ 12.62,  0.042, 31.0,   1],
    [ 12.69,  0.013, 1.30,   1],
    [ 13.48,  0.040, 8.00,   1],
    [ 14.19,  0.025, 0.45,   0],
    [ 15.90,  0.020, 0.04,   0],
    [ 16.45,  0.014, 0.50,   0],
    [ 17.04,  0.065, 2.22,   0],
    [ 17.375, 0.012, 0.11,   0],
    [ 17.87,  0.016, 0.067,  0],
    [ 18.92,  0.100, 0.17,   0],
    [ 15,     0.8,   50,     0] ])

qgra=0.01
aksi=50e-10

def Nc(a):
    """Returns number of carbon atoms for a grain size a according to
    footnote 2, p781 in LD01."""
    #return (a/1.286e-10)**3
    return 460*(a/1e-9)**3

def ksi_pah(a):
    return (1-qgra)*min(1, (aksi/a)**3)

def Ccarb_abs(lam, a, ionized, DL07):
    """Returns total carbonaceous abs cross section, eqs 2-3 in LD01."""
    nc=Nc(a)
    p= (DL07_ionized if ionized else DL07_neutral) if DL07 else \
       (LD01_ionized if ionized else LD01_neutral)
    #pdb.set_trace()
    g=argwhere(gsz>a)[0][0]
    gra=vecops.interpolate_point_in_array(glam,Cgra[:,g],lam, True)

    ccarb= ksi_pah(a)*Cpah_abs(lam, a, ionized, p, DL07)*nc + \
           (1-ksi_pah(a))*gra*nc
    assert ccarb==ccarb
    if ccarb<0:
        print "Warning, negtive opacity %e m^2/C at wavelength %e"%(ccarb/nc,lam)
        ccarb=0
    return ccarb

def load_fits(file):
    """Returns the wavelength, size, and abs cross section/Nc for the file."""
    f=pyfits.open(file)
    sigma=f['SIGMA_ABS'].data[::-1,:]
    lam=f['AXES'].data.field('wavelength')[::-1]
    sz=f['AXES'].data.field('size')[:sigma.shape[1]]
    nc=array([Nc(a) for a in sz])
    return lam,sz,sigma/nc
    
def load_gra():
    """Load graphite cross sections. Returns the cross section/Nc """
    return load_fits("/Users/patrik/dust_data/crosssections/graphite.fits")

glam, gsz, Cgra = load_gra()
    

def LD01_compare(file,ionized,ratio):
    pl,ps,psig=load_fits(file)
    for i in range(ps.shape[0])[::5]:
        mysig=array([Ccarb_abs(xx,ps[i],ionized) for xx in pl])/Nc(ps[i])
        if ratio:
            semilogx(pl,mysig/psig[:,i])
        else:
            loglog(pl,mysig)
            loglog(pl,psig[:,i],'--')

def write_file(lam,sizes,sigma_a, type, LD01_file, output_file):
    """Writes an output opacities file."""

    sigma_s = pyfits.open(LD01_file)['SIGMA_SCA'].data
    g = pyfits.open(LD01_file)['G'].data
    assert all(sigma_s.shape==sigma_a.shape) 

    phdu = pyfits.PrimaryHDU()
    phdu.header.update('species', type, 'Grain description')
    phdu.header.update('file', 'Written by pah.py',
                       '')

    #  make image hdus for sigma_a, sigma_s and g For some reason the
    # wavelength scale is flipped, so we must reverse the wavelength
    # axis. Probably has something to do with image order.
    ahdu = pyfits.ImageHDU(sigma_a[::-1,:],name='sigma_abs')
    ahdu.header.update('unit', 'm^2','Unit of cross section')
    shdu = pyfits.ImageHDU(sigma_s,name='sigma_sca')
    shdu.header.update('unit', 'm^2','Unit of cross section')
    ghdu = pyfits.ImageHDU(g,name='g')
    ghdu.header.update('unit', 'm^2','Unit of cross section')

    # columns to table ext
    c1=pyfits.Column(name='size',unit='m',format='D',array=sizes)
    c2=pyfits.Column(name='wavelength',unit='m',format='D',array=lam[::-1])
    cols=pyfits.ColDefs([c1, c2])
    tbhdu=pyfits.new_table(cols)
    tbhdu.header.update('EXTNAME','AXES')
    hdulist=pyfits.HDUList([phdu, ahdu, shdu, ghdu, tbhdu])
    hdulist.writeto(output_file)

def make_DL07():
    """Makes the DL07 opacities files."""
    print "Processing neutral PAHs"
    pl,ps,psig=load_fits('/Users/patrik/dust_data/crosssections/PAH_neutral.fits')
    sigma_abs=zeros(psig.shape)
    for i in range(ps.shape[0]):
        print ps[i]
        sigma_abs[:,i]= array([Ccarb_abs(xx,ps[i],False, True) for xx in pl])
    write_file(pl,ps,sigma_abs,'neutral PAH','/Users/patrik/dust_data/crosssections/PAH_neutral.fits','/Users/patrik/dust_data/crosssections/PAH_neutral_DL07.fits', )

    print "Processing ionized PAHs"
    pl,ps,psig=load_fits('/Users/patrik/dust_data/crosssections/PAH_ionized.fits')
    sigma_abs=zeros(psig.shape)
    for i in range(ps.shape[0]):
        print ps[i]
        sigma_abs[:,i]= array([Ccarb_abs(xx,ps[i],True, True) for xx in pl])
    write_file(pl,ps,sigma_abs,'ionized PAH','/Users/patrik/dust_data/crosssections/PAH_ionized.fits','/Users/patrik/dust_data/crosssections/PAH_ionized_DL07.fits', )
