# contains interface routines for lsf

import commands, re
import local_mcrxrun

job_skeleton = """#!/bin/sh
#BSUB -o <outfile> 
#BSUB -e <outfile> 
#BSUB -q <queue>
#BSUB -n <ncpus>
#BSUB -R "span[ptile=<ncpus>]"
#BSUB -W <limit_hours>
#BSUB -J <jobname>
#BSUB -g /sunruns

"""

release_command="bresume "
hold_command="bstop "
kill_command="bkill "
use_seconds=False

submission_regexp=re.compile(r"Job +<(\d+)> +is submitted")

def submit_job(job):
    "Submits a job and returns the job id."
    output = commands.getstatusoutput ("bsub < " + job)
    if output [0] != 0:
        # This means that the job did not submit properly
        print "Submission Failed: " +job+ ":"
        print output [1]

    jobid=submission_regexp.search(output[1]).group(1)
    return jobid
    
def read_job_status(jobidfile):
    "Reads a job ID from the jobidfile and queries for status. Will throw an exception if the job doesn't exist"
    f = open (jobidfile, 'r')
    jobid = f.read ().splitlines () [0]
    cmd = "bjobs " + jobid
    output= commands.getoutput (cmd)
    #print output
    if output.find("Unknown Job Id")!=-1:
        raise ValueError,"Job ID not found"

    status = output.splitlines ()[local_mcrxrun.job_status_line].split()[local_mcrxrun.job_status_column]
    #print "status is",status
    if status=="DONE":
        # we get these back from lsf even after the job exited, so in that case we must specifically handle it
        raise ValueError,"Job done"
    elif status=="EXIT":
        # we get these back from lsf even after the job exited, so in that case we must specifically handle it
        raise ValueError,"Job exited prematurely"
    return "%-2s"%status

# status letters returned from pbs
status_queued="PEND"
status_running="RUN"
status_notqueued=""
status_held="PSUSP"

