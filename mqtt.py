import paho.mqtt.publish as mqttpub
import json

mqtt_host = "ha"
mqtt_user = "mqttuser"
mqtt_pwd = "UKTpREW9CkwS9E"

def publish_config(mqtt_topic, channels, unique_id_prefix="",
                   device_map={}, actually_publish=True, expire_after=None):
    """Publish MQTT config messages for the supplied channels. Channels
    should be a dict, where the keys are the channel
    ids and the values are tuples of (name, unit,
    device_class, state_class). If specified, the
    device_map is supplied for the device keyword."""

    for k,v in channels.items():
        mqtt_id = k
        config = {
            "name": v[0],
            "device_class": v[2],
            "state_class": v[3],
            "state_topic": mqtt_topic+"state",
            "unit_of_measurement": v[1],
            "value_template": "{{ value_json.%s }}"%mqtt_id,
            "unique_id": unique_id_prefix + mqtt_id,
            "device": device_map
        }
        if expire_after:
            config["expire_after"] = expire_after
        
        config_map = json.dumps(config)
        print ("Publishing config for ",k,mqtt_id,config_map)

        if actually_publish:
            mqttpub.single(mqtt_topic+mqtt_id+"/config",
                           config_map,
                           hostname=mqtt_host,
                           auth={"username":mqtt_user, "password":
                                 mqtt_pwd},
                           retain=True,
                           qos=1
            )
        
    
def publish_data(mqtt_topic, data, actually_publish=True):
    "Turn data into a MQTT payload and publish it as state under the specified topic."
    payload = json.dumps(data)
    print ("Publishing mqtt message to {}: {}".format(mqtt_topic,payload))

    if actually_publish:
        try:
            mqttpub.single(mqtt_topic+"state",
                           payload,
                           hostname=mqtt_host,
                           auth={"username":mqtt_user, "password": mqtt_pwd},
                           retain=True)
        except:
            print ("Error publishing mqtt message")
