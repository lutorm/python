import xbee, collections, threading, time, pdb, struct, threading
import subprocess, tempfile, serial, os, codecs

class ReceiveError(Exception):
    def __init__(self, result):
        self.result = result
    def __str__(self):
        return repr(self.result)

class xbee_interface:
    """
A class that interfaces with an XBee coordinator through an API serial
connection. Reads/writes out the remote device's UART is done with a
serial-like interface, and callbacks can be registered for other
types of received packets."""

    def __init__(self, ser):
        "Open a connection to an Xbee connected to the specified serial port."
        
        self.ser = ser
        self.lock=threading.Condition()
        self.remotes = {}
        # maps 64-bit addresses to 16-bit ones
        self.addr16to64 = {}
        self.addr64to16 = {}
        self.frame_id = 1

        # incoming packet handler based id.
        self.handlers = {}
        
        # dict of tx_status we are waiting for, indexed by
        # frameid. the value is the 64-bit address of the transmit
        # target.  We need this so we can update the 16-bit address
        # and send it to the correct recipient
        self.transmits = {}

        self.install_handler('', self.default_handler)
        self.remove_handler('status')
        self.remove_handler('at_response')
        self.install_handler('node_id_indicator', self.id_handler)
        self.install_handler('tx_status', self.tx_status_handler)

        self.xbee = xbee.ZigBee(self.ser, callback=lambda x: self.read_data(x))

    def __del__(self):
        self.close()
        del self.xbee

    def reinit_serial(self):
        "Closes and reopens serial connection if it's stopped working"
        print "Manager reiniting serial"
        port = self.ser.port
        baud = self.ser.baudrate
        timeout = self.ser.timeout

        # Halt Zigbee thread and close serial port
        self.xbee.halt()
        self.ser.close()

        # Reopen port and restart Zigbee thread
        self.ser.open()
        self.xbee = xbee.ZigBee(self.ser, callback=lambda x: self.read_data(x))
        
    def close(self):
        [r.close() for r in self.remotes.values()]
        self.xbee.halt()

    def open_device(self, addr, timeout=1):
        "Open a connection to the remote XBee with the specified address."

        remote = xbee_serial(self, addr, timeout)
        self.remotes[addr] = remote
        return remote

    def close_device(self, remote_addr):
        del self.remotes[remote_addr]

    def increment_frameid(self):
        self.frame_id+=1
        if self.frame_id==256:
            self.frame_id=1

    def install_handler(self, id, handler):
        self.lock.acquire()
        prev = self.handlers.get(id, None)
        self.handlers[id] = handler
        self.lock.release()
        return prev

    def remove_handler(self, id):
        self.lock.acquire()
        prev = self.handlers.get(id, None)
        self.handlers[id] = None
        self.lock.release()
        return prev
    
    def read_data(self, data):
        """
Callback callnode _idd when a packet has been read. Forwards to a handler."""
        #print `data`
        if data['id'] in self.handlers and self.handlers[data['id']]:
            self.handlers[data['id']](data)
        else:
            # no handler, use default
            self.handlers[''](data)

    def tx_status_handler(self, data):
        "Handler for tx_status packets."
        
        # find it
        frame_id = data['frame_id']
        target_addr = None
        self.lock.acquire()
        try:
            target_addr = self.transmits[frame_id]
            del self.transmits[frame_id]
            # update address maps
            self.addr16to64[data['dest_addr']] = target_addr
            self.addr64to16[target_addr] = data['dest_addr']
        except KeyError:
            # It can happen that we get a frame id that doesn't exist in the transmits map
            print "No entry found for frame id %s in tx_status_handler"%frame_id
        finally:
            self.lock.release()

        if target_addr:
            self.call_remote(target_addr, data)

    def id_handler(self, data):
        "Handles received node id packets by printing a console message."
        nid_data = self.decode_node_id(data)
        print "Node ID packet received from %s (%s): %s"%(nid_data[0], nid_data[1], nid_data[2])
        
    def decode_node_id(self, data):
        "Decodes a node id packet"
        self.lock.acquire()

        d = -1
        nid = "error"
        saddr = "?"
        try:
            d = data['source_event']
            nid = data['node_id']
            saddr = hex(struct.unpack('>q', data['source_addr_long'])[0])
        finally:
            self.lock.notifyAll()
            self.lock.release()

        if d=='\x01':
            event="Pushbutton"
        elif d=='\x02':
            event="Joined"
        elif d=='\x03':
            event="Power cycle"
        else:
            event="Unknown cause %s"%`d`
        return (nid, saddr, event)

        
    def default_handler(self, data):
        "Default handler for packets. Forwards to the remote."

        # update address maps
        self.lock.acquire()
        if 'source_addr_long' in data:
            self.addr16to64[data['source_addr']] = data['source_addr_long']
            if 'source_addr' in data:
                self.addr64to16[data['source_addr_long']] = data['source_addr']
        self.lock.release()
        # and forward
        try:
            remote_addr=data['source_addr_long']

            self.call_remote(remote_addr, data)
        except:
            print "error forwarding frame ",`data`
            
    def call_remote(self, remote_addr, data):
            
        try:
            remote = self.remotes[remote_addr]
        except KeyError:
            # we don't have a connection to the remote that sent this packet

            #print "Packet from unknown source: ",`data`
            return
        remote.read_data(data)

    def wait_for(self, data, timeout=1):
        "Waits until something is written to data."
        start = time.time()

        self.lock.acquire()
        try:
            while time.time()-start < timeout:
                if data:
                    break
                self.lock.wait(timeout-(time.time()-start))
        finally:
            self.lock.release()

    def at(self, command, parameter=None, timeout=1):
        "Send a AT command to the local Xbee and wait for response."

        this_frameid=struct.pack('B', self.frame_id)
        self.increment_frameid()
        response=[]

        self.install_handler('at_response', lambda pkt: response.append(pkt))
        
        self.xbee.at(frame_id=this_frameid,
                     command=command,
                     parameter=parameter)

        self.wait_for(response, timeout=timeout)
        self.remove_handler('at_response')

        if not response:
            raise ReceiveError("timeout")
        
        assert len(response) == 1
        r = response[0]
        assert r['frame_id'] == this_frameid
        
        #print r
        if r['status'] != '\x00':
            # command failed
            return False,r

        try:
            return r['parameter']
        except:
            return True

    def node_discover(self):
        "Does a Node Discover command and returns the discovered nodes."

        response = []

        # first, get the node discover timeout so we know how long to wait
        try:
            nt = self.at('nt')
            nt_timeout = struct.unpack('>H',nt)[0]*0.1      
            sp = self.at('sp')
        
            sp_timeout = struct.unpack('>H',sp)[0]*0.01
            timeout = sp_timeout + nt_timeout
            #print "Using %f timeout for node discover to ensure all devices wake up and respond"%timeout

            self.install_handler('at_response', lambda x: response.append(x))

            this_frameid=struct.pack('B', self.frame_id)
            self.increment_frameid()
            self.xbee.at(command="nd", frame_id=this_frameid)

            # wait the requisite length for responses
            time.sleep(timeout)

        except ReceiveError:
            print "node_discover got ReceiveError"
        finally:
            self.remove_handler('at_response')

        nodes=[]
        for i in response:
            if i['status'] != '\x00':
                nodes.append("error")
                continue
            else:
                p=i['parameter']
                node = {}
                node['addr'] = p['source_addr']
                node['addr_long'] = p['source_addr_long']
                node['node_identifier'] = p['node_identifier']
                node['parent_address'] = p['parent_address']
                node['device_type'] = p['device_type']
                node['status'] = p['status']
                node['profile_id'] = p['profile_id']
                node['manufacturer_id'] = p['manufacturer']
                nodes.append(node)

                # update address maps
                self.addr16to64[node['addr']] = node['addr_long']
                self.addr64to16[node['addr_long']] = node['addr']

        return nodes


    def remote_at(self, command, dest, parameter=None):
        "Send a remote AT command to dest. The response will come as a callback."

        this_frameid=struct.pack('B', self.frame_id)
        self.increment_frameid()
        self.xbee.remote_at(dest_addr_long=dest,
                            dest_addr='\xff\xfe',
                            frame_id=this_frameid,
                            command=command,
                            parameter=parameter)
        return this_frameid

    def tx(self, dest, data):
        this_frameid=struct.pack('B', self.frame_id)
        self.increment_frameid()
        self.xbee.tx(dest_addr_long=dest,
                     dest_addr='\xff\xfe',
                     data=data,
                     frame_id=this_frameid)

        # put in tx dict
        self.lock.acquire()
        self.transmits[this_frameid] = dest
        self.lock.release()
        
        return this_frameid

    def reset_all(self):
        "Send a reset command to all remote nodes."
        for r in self.remotes.values():
            r.reset()
            
class xbee_serial:
    """
Object that handles a connection to a remote XBee. Works like a serial port for 
reading and writing, and has callbacks when other packets arrive."""
    

    def __init__(self, manager, source_addr, timeout=1):
        self.manager = manager
        self.lock=threading.Condition()
        self.source_addr = source_addr
        self.timeout = timeout
        self.read_buffer = collections.deque([], 16384)
        self.handlers = {}
        self.frame_buffer = {}
        self.install_handler('rx', self.rx_handler)
        self.install_handler('', self.default_handler)
        self.orphaned_frameids = set()
        
    def install_handler(self, id, handler):
        self.lock.acquire()
        try:
            old = self.handlers[id]
        except:
            old = None
        self.handlers[id] = handler
        self.lock.release()
        return old

    def remove_handler(self, id):
        self.lock.acquire()
        del self.handlers[id]
        self.lock.release()

    def __del__(self):
        self.close()

    def close(self):
        self.manager.close_device(self.source_addr)

    def read_data(self, data):
        """
Callback called when a packet from this device is received.
Forwards to the handlers."""

        self.lock.acquire()
        try:
            handler = self.handlers[data['id']]
        except KeyError:
            # no specific handler, use default
            handler = self.handlers['']
        self.lock.release()
        handler(data)
        
    def rx_handler(self, data):
        "Handles received remote UART data."
    
        self.lock.acquire()
        try:
            for c in data['rf_data']:
                self.read_buffer.append(c)
        finally:
            self.lock.notifyAll()
            self.lock.release()

    def default_handler(self, data):
        "Default is to put the data into the buffer."
        self.lock.acquire()
        try:
            self.frame_buffer[data['frame_id']] = data
        finally:
            self.lock.notifyAll()
            self.lock.release()
        
    def read(self, size=1):
        "Reads data from remote device as if it was a serial port."
        start = time.time()
        returnval=[]
        self.lock.acquire()
        try:
            while time.time()-start < self.timeout and len(returnval)<size:
                try:
                    while len(returnval)<size:
                        returnval.append(self.read_buffer.popleft())
                except IndexError:
                    # out of characters, wait to see if more show up
                    self.lock.wait(self.timeout-(time.time()-start))
        finally:
            self.lock.release()
            
        return ''.join(returnval)

    def readline(self, size=1000000, eol='\n'):
        returnval=''
        c=None
        now = time.time()
        while c != eol and len(returnval)<size:
            c = self.read(1)
            returnval += c
            if time.time()-now > self.timeout:
                break
        return returnval

    def flushInput(self):
        self.lock.acquire()
        try:
            self.read_buffer.clear()
        finally:
            self.lock.release()

    def write(self, data):
        n = len(data)
        this_frameid = self.manager.tx(self.source_addr,
                                       data)

        # wait for ack
        r = self.wait_for_response(this_frameid)

        if r['id'] != 'tx_status':
            print "Got unexpected frame ID from wait_for_response:\n%s"%`r`
            return 0
        
        if r['deliver_status'] != '\x00':
            # error sending, return 0 to indicate nothing was sent
            return 0
        else:
            return n

    def wait_for_response(self, frameid):
        "Waits for the response to a specific frame id and returns it."
        start = time.time()
        self.lock.acquire()
        # clean any orphans
        # make a set of the frameids in the frame buffer
        orphaned_fb=self.orphaned_frameids.intersection(set(self.frame_buffer.keys()))
        [ self.orphaned_frameids.remove(x) for x in orphaned_fb]
        for o in orphaned_fb:
            print "Found orphaned packet: ",self.frame_buffer[o]
            del self.frame_buffer[o]
                
        try:
            while time.time()-start < self.timeout:
                # BUG not all frames have a frame id
                if self.frame_buffer.has_key(frameid):
                    packet = self.frame_buffer[frameid]
                    del self.frame_buffer[frameid]
                    return packet
                self.lock.wait(self.timeout-(time.time()-start))
        finally:
            self.lock.release()

        # didn't get it - put it in orphan list so we can remove it if
        # it shows up later
        self.orphaned_frameids.add(frameid)
        raise ReceiveError("timeout")
    
    def at(self, command, parameter=None):
        "Send a AT command to the remote and return response."

        this_frameid = self.manager.remote_at(command,
                                              self.source_addr,
                                              parameter=parameter)

        r = self.wait_for_response(this_frameid)

        #assert r['id'] == 'remote_at_response'

        if r['id'] != 'remote_at_response' or r['status'] != '\x00':
            raise ReceiveError(r)
        
        try:
            return r['parameter']
        except:
            # if the command didn't return anything, return true for success
            return True

    def setDTR(self, param):
        "Doesn't do anything."
        pass

    def reset(self):
        "Send a FR reset command to the remote."
        self.at("FR")
        
class xbee_serial_pipe:
    """Class that pipes the data between an xbee serial and a real
    serial port."""

    def __init__(self, remote, port, cmdport):
        self.remote = remote
        self.port = port
        self.cmd_port = cmdport
        
        # replace existing rx handler with our own
        self.old_handler = self.remote.install_handler('rx', self.rx_handler)
        # start thread listening on input
        self.cont = True; 
        self.t =  threading.Thread(target=self.listen_thread)
        self.t.start()
        self.ct =  threading.Thread(target=self.cmd_thread)
        self.ct.start()
       
    def __del__(self):
        self.close()
        
    def close(self):
        self.cont = False
        self.t.join()
        self.ct.join()
        self.remote.install_handler('rx', self.old_handler)

    def rx_handler(self, data):
        """Receive handler receives xbee RF packets and sends their
        contents to the port."""

        self.port.write(data['rf_data'])

    def listen_thread(self):
        "Waits for data from port and sends it to xbee."
        
        while(self.cont):
            # if we have chars in the buffer, we want to read all of
            # them, but if not we want to block until at least one is
            # available.
            n = self.port.inWaiting()
            r = self.port.read(n if n>0 else 1)
            if len(r)>0:
                try:
                    self.remote.write(r)
                except ReceiveError as e:
                    print "listen_thread got ReceiveError sending to %s"%hex(struct.unpack('>q', self.remote.source_addr)[0])

    def cmd_thread(self):
        """
        Waits for commands on the command port and relays them to the
        Xbee. Commands are formatted as the 2-letter command string,
        plus a space and any parameter to the command, terminated by a
        newline. The parameter should be string escaped and can not
        contain a newline.

        The response is sent back as a string-escaped string or, if
        the command did return anything, the string 'True' or 'False'
        indicating success or failure, followed by a newline."""
        
        while(self.cont):
            r = self.cmd_port.readline()
            if len(r)>=2:
                cmdstring = codecs.decode(r, 'string_escape')
                print "Executing remote command %s with parameter %s"%(cmdstring[:2],`cmdstring[3:-1]`)
                try:
                    if len(cmdstring)>3:
                        response = self.remote.at(cmdstring[:2], parameter=cmdstring[3:-1])
                    else:
                        response = self.remote.at(cmdstring[:2])
                except ReceiveError as e:
                    print "command thread got ReceiveError sending to %s"%hex(struct.unpack('>q', self.remote.source_addr)[0])
                    response=False
                print "Got response",`response`
                if type(response) is type(""):
                    self.cmd_port.write(codecs.encode(response, 'string_escape')+'\n')
                else:
                    self.cmd_port.write(`response`+'\n')
                    
class pipe_manager:
    def __init__(self):

        # temp dir for the private endpoints
        self.tmpdir = tempfile.mkdtemp(prefix="xbee")
        self.socats={}
        self.pipeobjs={}
        self.cont = True
        self.need_discover = False
        self.manager = None
        
    def __del__(self):
        self.close()
        
    def start(self, manager):
        self.t =  threading.Thread(target=self.monitor_pipes, args=(manager,))
        self.t.start()

    def close(self):
        self.cont = False;
        try:
            self.t.join()
        except:
            pass
        
        for p in self.pipeobjs.values():
            p.port.close()
            p.cmd_port.close()
            p.remote.close()
            p.close()
        for s in self.socats.values():
            s[0].terminate()
            s[0].wait()
            s[1].terminate()
            s[1].wait()
        os.rmdir(self.tmpdir)

    def node_id_handler(self, data):
        "respond to a node id packet by doing a discover"
        print "pipe manager received node id packet"
        self.manager.id_handler(data)
        self.need_discover = True
        
    def monitor_pipes(self):
        ser=serial.Serial("/dev/ttyS0",57600, timeout=3)
        ser.flushInput()
        self.manager = xbee_interface(ser)

        orig_id_handler = self.manager.install_handler('node_id_indicator', self.node_id_handler)
        
        failures = 0
        pipe_ctr=0
        self.need_discover=True
        try:
            while True:
                if self.need_discover:
                    print "Checking for new Xbees"
                    # need to restore original node id handler here
                    prev = self.manager.install_handler('node_id_indicator', orig_id_handler)
                    self.setup_xbee_pipes(self.manager)
                    self.manager.install_handler('node_id_indicator', prev)
                    
                    self.need_discover=False
                pipe_ctr += 1
                if pipe_ctr == 10:
                    pipe_ctr = 0

                try:
                    # read DB from local Xbee. If that fails consistently,
                    # we have a serial problem
                    db = self.manager.at('db')
                    print "Got local signal strength: %s, connection ok"%db
                    faliures = 0
                except:
                    failures += 1
                    print "Could not get signal strength from local Xbee, %d failures"%failures

                if failures==20:
                    print "Too many failures, reinitializing serial port"
                    self.manager.reinit_serial()
                    failures = 0

                time.sleep(10)
        except KeyboardInterrupt:
            del self.manager
            ser.close()
            
            
                
    def setup_xbee_pipes(self, m):
        "Sets up pipes new remote xbees that are found."
        nodes = m.node_discover()

        for n in nodes:
            addr = n['addr_long']
            hexaddr = hex(struct.unpack('>q', addr)[0])

            if self.socats.has_key(hexaddr):
                # already have a pipe for this one
                continue
            
            # get a remote for the xbee
            r= m.open_device(addr, timeout=30)

            print "Spawning socat for node %s (%s)"%(hexaddr, n['node_identifier'])
            socatdata = ['socat',
                        'PTY,link=%s/%s,echo=0,raw'%(self.tmpdir, hexaddr),
                        'PTY,link=xbee_%s,echo=0,raw'%hexaddr]
            socatcmd = ['socat',
                        'PTY,link=%s/%s_cmd,echo=0,raw'%(self.tmpdir, hexaddr),
                        'PTY,link=xbee_%s_cmd,echo=0,raw'%hexaddr]

            self.socats[hexaddr]=(subprocess.Popen(socatdata),
                                  subprocess.Popen(socatcmd))
            time.sleep(2)
            # open the private end of the pipe and link it to the xbee
            port = serial.Serial('%s/%s'%(self.tmpdir, hexaddr),
                                 timeout=1, write_timeout=1)
            cmdport = serial.Serial('%s/%s_cmd'%(self.tmpdir, hexaddr),
                                    timeout=1, write_timeout=1)
            self.pipeobjs[hexaddr] = xbee_serial_pipe(r, port, cmdport)

        
def parse_sample_packet(data):
    """Parses an AT response packet from an IS command to the same
    format as the \"samples\" of remote IO sample packet."""

    digital_mask = struct.unpack('>H', data[1:3])[0]
    analog_mask = struct.unpack('>B', data[3])[0]

    digital_lines = ['dio-0', 'dio-1', 'dio-2', 'dio-3', 'dio-4',
                     'dio-5', 'dio-6', 'dio-7', '', '',
                     'dio-10', 'dio-11', 'dio-12']
    analog_lines = ['adc-0', 'adc-1', 'adc-2', 'adc-3', '', '', '', '%V'] 

    response = {}

    pos = 4
    if digital_mask:
        # we have digital samples
        digital_sample = struct.unpack('>H', data[pos:pos+2])[0]
        pos = pos + 2
        for i,d in enumerate(digital_lines):
            if digital_mask & (1<<i):
                response[d] = True if digital_sample & (1<<i) else False

    if analog_mask:
        for i,a in enumerate(analog_lines):
            if analog_mask & (1<<i):
                response[a] = struct.unpack('>H', data[pos:pos+2])[0]
                pos = pos + 2

    return response

