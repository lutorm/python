#import pyfits
import  os, numpy, glob, struct, pdb
from pylab import *


def aux_plot(fitsfile, hdu, slice, output_file, imagefunc=lambda x:x, **kwargs):
    clf()
    imshow(imagefunc(fitsfile[hdu].data[slice,:,:]),**kwargs)
    colorbar()
    savefig(output_file)
    
def write_all_aux(file, slice, output_file, overwrite=False,
                  imagefunc=lambda x:x, **kwargs):
    """Writes aux plots for all camera HDUs with the specified suffix
    to the output file, where %d is replaced by the camera number."""
    print "Writing aux plots for file %s"%file
    interactive(False)
    inf=pyfits.open(file)
    ncam=inf['MCRX'].header['N_CAMERA']
    for i in range(ncam):
        hdu='CAMERA%d-AUX'%i
        ofnam=output_file%i
        if not overwrite and os.path.exists(ofnam):
            print "\tSkipping %s -- already present"%ofnam
            continue
        print "\t%s to %s"%(hdu,ofnam)
        aux_plot(inf, hdu, slice, ofnam, imagefunc, **kwargs)
    inf.close()

def read_arepo_projected_image(file):
    """Loads an Arepo gas density projection."""
    buf=open(file,'rb').read() 
    sz=struct.unpack_from('2i',buf)
    print "Loading %s: %dx%d"%(file,sz[0],sz[1])
    vectorlen=(len(buf)-8)/(4*product(sz))
    if vectorlen>1:
        print "Image is a vector image with %d components"%vectorlen
        sz=(sz[0],sz[1],vectorlen)
    
    gd=array(struct.unpack_from('%df'%sz[0]*sz[1]*vectorlen,buf,offset=8)).reshape(sz)[:,::-1]
    return gd

def read_arepo_grid_image(file):
    """Loads an Arepo gas density projection."""
    buf=open(file,'rb').read() 
    sz=struct.unpack_from('3i',buf)
    print "Loading %s: %dx%dx%d"%(file,sz[0],sz[1],sz[2])
    vectorlen=(len(buf)-12)/(4*product(sz))
    if vectorlen>1:
        print "Grid is a vector image with %d components"%vectorlen
        sz=(sz[0],sz[1],sz[2],vectorlen)
    
    gd=array(struct.unpack_from('%df'%sz[0]*sz[1]*sz[2]*vectorlen,buf,offset=12)).reshape(sz)[:,:,::-1]
    return gd

def read_makedisk_density_field(file):
    """Loads a gas density dump from MakeNewDisk."""
    buf=open(file,'rb').read() 
    sz=struct.unpack_from('2i',buf)
    print "Loading %s: %dx%d"%(file,sz[0],sz[1])
    R=struct.unpack_from('%dd'%sz[0],buf,offset=8)
    z=struct.unpack_from('%dd'%sz[1],buf,offset=8+8*sz[0])    
    gd=array(struct.unpack_from('%dd'%sz[0]*sz[1],buf,offset=8+8*(sz[0]+sz[1]))).reshape(sz)
    return R,z,gd


def make_arepo_projected_images(files, draw_colorbar=True, draw_title=True, **kwargs):
    """Makes a color image of the projected density outputs from
    Arepo."""

    for i in sorted(files):
        clf()
        gd=read_arepo_projected_image(i)
        imshow(log10(gd),interpolation='nearest',cmap='Paired',**kwargs)
        draw_colorbar and colorbar()
        draw_title and title(i)
        savefig(i+".pdf")

def make_all_images(dirs, filepat, **kwargs):
    for d in dirs:
        files=glob.glob(os.path.join(d,filepat))
        #print os.path.join(d,filepat), files
        make_arepo_projected_images(files, **kwargs)
        
def make_arepo_density_profile(data,nbins=100):

    if type(data) == type("aoeu"):
        gd=read_arepo_projected_image(data)
    else:
        gd=data
        
    xind=arange(-0.5*gd.shape[0], gd.shape[0]-0.5*gd.shape[0])
    yind=arange(-0.5*gd.shape[1], gd.shape[1]-0.5*gd.shape[1])
    
    r=sqrt((tile(xind,(gd.shape[0],1))**2)+(tile(transpose(atleast_2d(yind)),(1,gd.shape[1]))**2))

    # bin it and calculate the average and spread in each bin
    rbin=linspace(0,r.max(),nbins)
    rm=[]
    m=[]
    s=[]
    for i in range(len(rbin)-1):
        mask=(r>rbin[i]) & (r<rbin[i+1])
        m.append(mean(gd[mask]))
        s.append(std(gd[mask]))
        rm.append(0.5*(rbin[i]+rbin[i+1]))
        
    errorbar(rm,m,yerr=s)
    return (array(rm),array(m),array(s))
    
def gd_convergence(files,legendtxt=None):

    rp=[make_arepo_density_profile(f) for f in files]
    # rp.append(make_arepo_density_profile('Sbc_met/proj_density_field_000'))
    # rp.append(make_arepo_density_profile('Sbc_qrng/proj_density_field_000'))
    # rp.append(make_arepo_density_profile('Sbc_90k/proj_density_field_000'))
    # rp.append(make_arepo_density_profile('Sbc_90kqrng/proj_density_field_000'))
    # rp.append(make_arepo_density_profile('Sbc_300k/proj_density_field_000'))
    # rp.append(make_arepo_density_profile('Sbc_300kqrng/proj_density_field_000'))
    # rp.append(make_arepo_density_profile('Sbc_1e6/proj_density_field_000'))
    # rp.append(make_arepo_density_profile('Sbc_1e6qrng/proj_density_field_000'))

    #ind=arange(-450,450)
    #r=sqrt((tile(ind,(900,1))**2)+(tile(transpose(atleast_2d(ind)),(1,900))**2))
    #rp.append(make_arepo_density_profile(exp(-r/450)))

    clf()
    for i in rp:
        plot(i[0],i[2]/i[1])
    if legendtxt is not None:
        legend(legendtxt, loc='upper right')
    else:
        legend(files, loc='upper right')
    ylabel('stdev/average density')
    xlabel('radius')
