import pyfits,pdb
from numpy import *
from pylab import *
from scipy import optimize

def velocities(file_lines, file_nolines, cam, wavelengths):
    """Plot images and velocity diagrams of the selected camera. The
    radial velocity is determined as the minimum of the sed in the
    specified wavelength interval."""

    lf=pyfits.open(file_lines)
    f=pyfits.open(file_nolines)
    l=f['lambda'].data.field('lambda')
    ll=lf['lambda'].data.field('lambda')


    velscale=(l[1]/l[0]-1)*3e5

    lint=where((l>wavelengths[0])&(l<wavelengths[1]))[0]
    llint=where((ll>wavelengths[0])&(ll<wavelengths[1]))[0]
    lcent=where(l>wavelengths[2])[0][0]
    llcent=where(ll>wavelengths[2])[0][0]

    li=lf['CAMERA%d'%cam].data[llint,:,:]*ll[llint][:,newaxis,newaxis]
    i=f['CAMERA%d'%cam].data[lint,:,:]*l[lint][:,newaxis,newaxis]
    lins=lf['CAMERA%d-NONSCATTER'%cam].data[llint,:,:]*ll[llint][:,newaxis,newaxis]
    ins=f['CAMERA%d-NONSCATTER'%cam].data[lint,:,:]*l[lint][:,newaxis,newaxis]

    rv=(calculate_rv_abslines(i)-(lcent-lint[0]))*velscale
    lrv,lsig=(calculate_rv_emlines_fit(li-i))
    lrv=(lrv-(lcent-lint[0]))*velscale
    lsig*=velscale
    rvns=(calculate_rv_abslines(ins)-(lcent-lint[0]))*velscale
    lrvns,lsigns=calculate_rv_emlines_fit(lins-ins)
    lrvns=(lrvns-(lcent-lint[0]))*velscale
    lsigns*=velscale
    minrv=min([rv.min(),rvns.min(), lrv.min(), lrvns.min()])
    maxrv=max([rv.max(),rvns.max(), lrv.max(), lrvns.max()])
    maxsig=max(lsig.max(),lsigns.max())
    #pdb.set_trace()
    clf()
    title('%s camera %d'%(file,cam))
    subplot(421)
    imshow(log(mean(i,axis=0)),interpolation='nearest',cmap=cm.gray)
    title('Intensity with dust')
    subplot(422)
    imshow(log(mean(ins,axis=0)),interpolation='nearest',cmap=cm.gray)
    title('Intensity w/o dust')
    subplot(423)
    imshow(rv, cmap=cm.RdBu,interpolation='nearest',vmin=minrv,vmax=maxrv)
    title('Absorption RV with dust')
    colorbar()
    subplot(424)
    imshow(rvns, cmap=cm.RdBu,interpolation='nearest',vmin=minrv,vmax=maxrv)
    title('Absorption RV w/o dust')
    colorbar()
    subplot(425)
    imshow(lrv-rv, cmap=cm.RdBu,interpolation='nearest',vmin=minrv,vmax=maxrv)
    title('Emission RV with dust')
    colorbar()
    subplot(426)
    imshow(lrvns, cmap=cm.RdBu,interpolation='nearest',vmin=minrv,vmax=maxrv)
    title('Emission RV w/o dust')
    colorbar()
    subplot(427)
    imshow(lsig, cmap=cm.gist_heat,interpolation='nearest',vmin=0,vmax=maxsig)
    title('Emission sigma with dust')
    colorbar()
    subplot(428)
    imshow(lsigns, cmap=cm.gist_heat,interpolation='nearest',vmin=0,vmax=maxsig)
    title('Emission sigma w/o dust')
    colorbar()

def calculate_rv_abslines(image):
    """The rv of the spectrum without emission lines is the minimum of
    the sed for each pixel. Return that as a 2d array."""

    rv=zeros(image.shape[1:])

    for x in range(rv.shape[0]):
        for y in range(rv.shape[1]):
            rv[x,y]=argmin(image[:,x,y]) if image[:,x,y].max()>0 else image.shape[0]/2

    return rv

def calculate_rv_emlines(image):
    """The rv of the continuum-subtracted line emission is just the
    intensity-weighted mean bin."""
    x=arange(image.shape[0])
    # if there are negative elements add a constant offset so they go away
    minval=image.min(axis=0)
    maxval=image.max(axis=0)
    # if maxval<0, then the pixel is hopeless
    # to avoid getting spurious effects in low snr pixels, set all
    # data to zero if it's less than 1e-3 of the max in that pixel

    pdb.set_trace()
    bad=(maxval<0) | ((minval/maxval)<1e-3)
    d=where(bad[newaxis,:,:],0,image)
    minval[(minval>0)|bad]=0
    d-=minval[newaxis,:,:]
    rv= sum(x[:,newaxis,newaxis]*d,axis=0)/sum(d, axis=0)
    sigma=sqrt(
        sum(x[:,newaxis,newaxis]**2*d,axis=0)/sum(d, axis=0) -rv**2)
    rv[rv!=rv]=image.shape[0]/2
    sigma[sigma!=sigma]=0
    assert rv.min()>=0
    assert rv.max()<=x[-1]
    assert sigma.min()>=0
    return rv,sigma

def calculate_rv_emlines_fit(image, pause=False):
    l=arange(image.shape[0])
    fitfunc=lambda p,x: p[0]*exp(-(x-p[1])**2/(2*p[2]**2))+p[3]
    errfunc = lambda p, x, y: fitfunc(p, x) - y
    rv=zeros(image.shape[1:])
    sigma=zeros(image.shape[1:])
    for y in range(image.shape[1]):
        for x in range(image.shape[2]):
            p0=[1.,image.shape[0]/2.,image.shape[0]/2.,0.]
            p1, success = optimize.leastsq(errfunc, p0[:], 
                                           args=(l, image[:,y,x]))
            if (p1[0]>1e-8) and (p1[0]+p1[3]>0) and (p1[1]>0) and (p1[1]<l[-1]) and (p1[2]<l[-1]/3):
                print "Fit (%d,%d): mean=%f, sigma=%f, amplitude=%e"%(x,y,p1[1],p1[2],p1[0])
                rv[y,x]=p1[1] 
                sigma[y,x]=p1[2]
            else:
                print "Fit (%d,%d): Bad fit or insufficient data"%(x,y),p1
                rv[y,x]=NaN 
                sigma[y,x]=0
                
            if pause:
                clf()
                plot(l,image[:,y,x])
                plot([fitfunc(p1,ll) for ll in l])
                draw()
                raw_input()
    return rv,sigma
