littleEndian = True
bigEndian = False
dionysus=True
upsand=False
euterpe=False

# for 32 bit machines
Character = 'c'
Float = 'f'
Float32 = 'f'
Float64 = 'd'
Int8 = 'b'
Int16 = 'h'
Int32 = 'i'
Int = 'i'
Boolean = 'bool'
