#!/usr/bin/python

import serial,time,commands,re,datetime,sys

titletext="Governator"

smarthddtemp="/root/smarthddtemp"
clr="\xfe\x58"

drives=["hda","sda","sdb","sdc","sdd","sde","sdf","sdg","sdh"]
drives_per_line=2
page_time=10

# temp probes and titles
probes={"Case":[16,49,45,141,0,8,0,7],
        "Drive bay":[16, 87, 149, 141, 0, 8, 0, 7]}
#probe={45:"Case", 149:"Drive bay"}
probe_wait=0.2

# filesystems to display used space for
filesystems=["/","/home","/data","/tmp","/var"]

# fan pwm values
pwm=[255,255,255]

def pr(a):
    print a
    
# decode a 1-wire response packet
def decode_1w_response(s):
    # map into numbers
    l=map(ord,s)
    #print "mapped ",l
    start = 0
    response=[]
    more = True
    while(more):
        assert l[start]==0x23
        assert l[start+1]==0x2a
        length=l[start+2]&0x7F
        assert l[start+3]==0x31
        
        reply= l[start+4:start+4+length]
        #print "   reply: ",reply
        if length==1: # simple return byte
            response.append(reply[0])
        elif reply[0] == 0 and reply[-1] == 0:
            # success, append response
            response.append(reply[1:-1])
        else:
            print "error"
            
        # check for more
        if l[start+2] & 0x80:
            # more packets
            start+=length+4
        else:
            more=False
    #print "returning ",response
    return response
        
# send a 1-wire search command to find devices
def enum_1w():
    #print "sending search command"
    cmd="\xfe\xc8\x02"
    f.write(cmd)
    r=f.read(200)
    #print "raw response",r
    r=decode_1w_response(r)
    #print "decoded response",r
    #print "found 1-wire devices:"
    #map(pr,r)
    return r

# ask 1-wire temp probes to measure temperature
def conv_temp():
    #print "sending convert temperature command"
    cmd="\xfe\xc8\x01\x01\x10\x00\xcc\x44"
    f.write(cmd)
    r=f.read(200)
    r=decode_1w_response(r)
    #print "\tresponse ",r

# read temp value from a 1-wire temp probe
def read_temp(addr):
    #print "sending read temp command to device ",addr
    addrstr=""
    for i in addr:
        addrstr=addrstr+chr(i)
    cmd="\xfe\xc8\x01\x03\x50\x48\x55"+addrstr+"\xbe"

    f.write(cmd)
    r=f.read(200)
    r=decode_1w_response(r)
    #print "temperature ",r[0]
    return r[0]

# convert ds18s20 data to temperature
def ds18s20_to_temp(t):
    temp=t[0]/2.*(127.-t[1])/abs(127.-t[1])
    th=t[2]
    tl=t[3]
    count_remain=t[6]
    count_per_c=t[7]

    temperature=temp-0.25+(count_per_c-count_remain)/count_per_c
    #print "temperature: ",temperature,"C"
    return temperature

# read fan speeds
def read_fan_speed(fan):
    f.write("\xfe\xc1"+chr(fan))
    time.sleep(probe_wait)
    r=f.read(200)
    assert(r[0:5]=="\x23\x2a\x03\x52"+chr(fan))
    period=ord(r[5])*256+ord(r[6])
    n=1
    rpm=18750000/(period*n)
    return rpm

# get hard drive temperature using SMART
def hdtemp(dev):
    t=commands.getoutput(smarthddtemp+" /dev/"+dev)
    #print t
    return int(t)

def hdhealth(dev):
    t=commands.getoutput("smartctl -H -d ata /dev/"+dev)
    health=re.findall("test result: (.*)$",t)[0]
    return health

# get cpu and mb temp from sensors
def cputemp():
    s=commands.getoutput("/usr/bin/sensors").splitlines()
    kuk=open("/var/log/mtx.log","w")
    kuk.write(`s`)
    cpu=0
    mb=0
    for i in s:
        try:
            if i[0:3]=="CPU":
                ss=i.split()
                #cpu=int(re.findall("^(..).*C",ss[2])[0])
                cpu=int(ss[2][0:3])
            if i[0:3]=="M/B":
                ss=i.split()
                #mb=int(re.findall("^(..).*C",ss[2])[0])
                mb=int(ss[2][0:3])
        except:
            print "Error reading temp: ",i
    return (cpu,mb)
            
# return status of md device
def mdstat(dev):
    s=commands.getoutput("mdadm --detail /dev/"+dev+" | grep 'State :'")
    state=re.findall("State : (.*)$",s)
    return state[0]

# Return system load average
def loadavg():
    file=open("/proc/loadavg")
    a=file.read().split()
    #a=commands.getoutput("uptime").split()
    return a

# Return system uptime
def uptime():
    s=commands.getoutput("uptime").split()
    return s[2][:-1]
    
# return memory usage
def memusage():
    s=commands.getoutput("free -m").splitlines()
    mem=s[1].split()
    swap=s[3].split()
    total_mem=int(mem[1])
    used_mem=int(mem[2])
    total_swap=int(swap[1])
    used_swap=int(swap[2])
    return (total_mem,used_mem,total_swap,used_swap)

# file system free space
def diskusage(disk):
    s=commands.getoutput("df "+disk).split()[-2]
    return s
    
# display a page (should be less than 40 chars)
def mtx_display(text):
    print "displaying:\n",text
    f.write(clr+text)

def disp_frontpage():
    str=titletext.center(20)+"\n"
    t=time.asctime()[4:]
    str+=t.center(20)
    mtx_display(str)
    time.sleep(page_time)
    
def disp_drive_temps():
    str="Drive temperature:\n"
    line=1
    for i in range(len(drives)):
        str+=drives[i]+": "+`hdtemp(drives[i])`+"C  "
        if i%drives_per_line == drives_per_line-1:
            if line==0:
                str+="\n"
                line+=1
            else:
                mtx_display(str)
                str=""
                time.sleep(page_time)
                line=0
    if str!="":
        mtx_display(str)
        time.sleep(page_time)

def disp_drive_health():
    str="Drive health:\n"
    line=1
    for i in range(len(drives)):
        str+=drives[i]+": "
        h=hdhealth(drives[i])
        if h=="PASSED":
            str+="OK   "
        else:
            str+=h[0:4]+" "
        if i%drives_per_line == drives_per_line-1:
            if line==0:
                str+="\n"
                line+=1
            else:
                mtx_display(str)
                str=""
                time.sleep(page_time)
                line=0
    if str!="":
        mtx_display(str)
        time.sleep(page_time)

def disp_probe_temps():
    #a=enum_1w() # we now explicitly address the probes
    f.read(200)
    conv_temp()
    str=""
    line=0
    for i in probes: #range(len(a)):
        t=ds18s20_to_temp(read_temp(probes[i]))
        
        str+=i+" temp: "+`int(t)`+"C\n"
        #str+=probes[a[i][2]]+" temp: "+`int(t)`+"C\n"
        #str+="Probe "+`a[i][2]`+": "+`t`+"C\n"
        if line==0:
            line+=1
        else:
            mtx_display(str)
            str=""
            time.sleep(page_time)
            line=0
    if str!="":
        mtx_display(str)
        time.sleep(page_time)

def disp_cpu_temp():
    temp=cputemp()
    str="CPU temp: "+`temp[0]`+"C\nM/B temp: "+`temp[1]`+"C"
    mtx_display(str)
    time.sleep(page_time)
    
def disp_fan_rpm():
    str=""
    line=0
    for i in range(3):
        rpm=read_fan_speed(i)
        str+="Fan "+`i`+": "+`rpm`+" rpm\n"
        if line==0:
            line+=1
        else:
            mtx_display(str)
            str=""
            time.sleep(page_time)
            line=0
    if str!="":
        mtx_display(str)
        time.sleep(page_time)

# displays state of raid arrays
def disp_mdstate():
    str="RAID array status:\n"
    line=1
    mds=commands.getoutput("cat /proc/mdstat|grep md|awk '{print $1}'|sort")
    for m in mds.splitlines():
        s=mdstat(m)
        str+=m+": "+s+"\n"
        if line==0:
            line+=1
        else:
            mtx_display(str)
            str=""
            time.sleep(page_time)
            line=0
    if str!="":
        mtx_display(str)
        time.sleep(page_time)

# displays memory and swap usage
def disp_mem_usage():
    usage=memusage()
    str=" RAM: "+`usage[0]`.rjust(4)+"MB total\nUsed: "+`usage[1]`.rjust(4)+"MB ("+\
         `int(1.0*usage[1]/usage[0]*100)`+"%)"
    mtx_display(str)
    time.sleep(page_time)
    str="Swap: "+`usage[2]`.rjust(4)+"MB total\nUsed: "+`usage[3]`.rjust(4)+"MB ("+\
         `int(1.0*usage[3]/usage[2]*100)`+"%)"
    mtx_display(str)
    time.sleep(page_time)

# displays disk usage
def disp_disk_usage():
    str="Disk usage\n"
    line=1
    for i in range(len(filesystems)):
        str+=(filesystems[i]+": "+diskusage(filesystems[i]))
        if line==0:
            str+="\n"
            line+=1
        else:
            mtx_display(str)
            str=""
            time.sleep(page_time)
            line=0
    if str!="":
        mtx_display(str)
        time.sleep(page_time)

def disp_loadavg():
    l=loadavg()
    upt=uptime()
    s="Uptime: "+upt+"\nLoad: "+l[0]+" "+l[1]+" "+l[2]
    mtx_display(s)
    time.sleep(page_time)

    
pages=[disp_frontpage,disp_cpu_temp,disp_probe_temps,\
#       disp_drive_temps,disp_drive_health,\
       disp_mdstate,disp_mem_usage,disp_disk_usage,\
       disp_loadavg]

try:
    f=serial.Serial('/dev/ttyUSB0',19200,timeout=probe_wait)
except:
    print "Could not open connection to LCD"
    sys.exit(1)
    
f.write("\xfe\x50\x35\xfe\x44")
f.read(200)

while(True):
    for p in pages:
        p()

