from numpy import sqrt
# we use the numpy sqrt which will work on just numbers too
import pdb

def sigma(data):
    """Calculates mean and standard deviation of the sequence in data
    using the Knuth method. Returns mean, sigma, n."""
    S=None
    mean=None
    n=0

    for d in data:
        #pdb.set_trace()
        if n==0:
            # initialize
            n+=1
            delta = d
            mean = delta/n
            S = delta*(d-mean)
        else:
            n+=1
            delta = d - mean
            mean = mean + delta/n
            S += delta*( d - mean )


    if n>1:
        sigma=sqrt(S/(n-1))
    else:
        sigma=0.*mean # to yield array results if mean is

    return mean,sigma,n

