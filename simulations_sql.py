# sql alchemy bindings for the simulations sql data

from sqlalchemy import *
from sqlalchemy.orm import mapper, relation, sessionmaker

# at some point all the numeric columns started returning "Decimal"
# types instead of floats, which wreaked havoc. Fix this
def make_asfloat(c):
    if isinstance(c.type, Numeric):
        c.type.asdecimal=False


class Gadget_simulation (object):    
    def parent_attr(self):
        return None
class Gadget_object (object):
    def parent_attr(self):
        return "simulation"
class Sunrise_simulation (object):
    def parent_attr(self):
        return "gadget_simulation"
class Sunrise_object (object):
    def parent_attr(self):
        return "simulation"
class Snapshot (object):
    def __repr__(self):
        return "Snapshot(ID: %d Snapshot file: %s)" % \
               (self.id, self.snapshot_file)
    def parent_attr(self):
        return "simulation"
class Snapshot_object (object):
    def parent_attr(self):
        return "snapshot"
    def center(self):
        return '(%1.3g, %1.3g, %1.3g)' % \
               (self.galcenter_x, self.galcenter_y, self.galcenter_z)
class Camera (object):
    def __repr__(self):
        return "Camera(ID: %d Snapshot_ID: %d Camera# %d Snapshot file: %s)" % \
               (self.id, self.snapshot_id, self.number, \
                self.snapshot.snapshot_file)
    def parent_attr(self):
        return "snapshot"
class Image_object (object):
    def __repr__(self):
        return "Image_object(ID: %d Camera_ID: %d Object# %s Has dust? %s Camera# %d)" % \
               (self.id, self.camera_id, self.number, self.has_dust, self.camera.number)
    def parent_attr(self):
        return "camera"
    def name(self):
        if self.number!=None:
            return "Object #%d%s" % (self.number, self.has_dust and
                                     " with dust" or
                                     " without dust")
        else:
            return "Full-camera object %s" % ( self.has_dust and
                                     " with dust" or
                                     " without dust")
    def gas_metallicity(self):
        return self.m_metals/self.m_g
    def stellar_metallicity(self):
        return self.m_metals_stars/self.m_stars
    def magnitude(self, filter):
        """Returns the magnitude object for a given filter name."""
        return maq.filter_by(object=self).join('filter').filter_by(name=filter).one()
    def defined_magnitudes(self):
        return [x.filter.name for x in self.magnitudes]
    
    def line_strength(self, line):
        """Returns the line strength object for a given line name."""
        return lsq.filter_by(object=self).join('line').filter_by(name=line).one()
    def defined_lines(self):
        return [x.line.name for x in self.line_strengths]
        
class Line_strength (object):
    def __repr__(self):
        return "Line_strength(ID: %d Line_ID: %d Strength: %e)" % \
               (self.id, self.line_id, self.luminosity)
class Magnitude (object):
    def __repr__(self):
        return "Magnitude(ID: %d Filter_ID: %d Magnitude: %f)" % \
               (self.id, self.filter_id, self.ab_mag)
class Line (object):
    pass
class Filter (object):
    pass

def setup(metadata, transactional):
    global m
    global gadget_simulations_table, gadget_objects_table, sunrise_simulations_table, sunrise_objects_table, snapshots_table, snapshot_objects_table, cameras_table, image_objects_table, line_strengths_table, magnitudes_table, line_names_table, filters_table, selection_table, selectionitems_table
    global session, gsq, goq, ssq, soq, snq, caq, ioq, maq, lsq, liq, fiq

    try:
        m
        return
    except:
        pass

    m=metadata
    m.bind.echo=False

    gadget_simulations_table = Table ('gadgetsims', m, autoload = True)
    gadget_objects_table = Table ('gadgetobjects', m, autoload =True)
    sunrise_simulations_table = Table ('sunrisesims',m,autoload=True)
    sunrise_objects_table = Table ('sunriseobjects',m,autoload=True)
    snapshots_table=Table('snapshots',m,autoload=True)
    snapshot_objects_table = Table('snapshotobjects', m, autoload = True)
    cameras_table = Table ('cameras',m,autoload=True)
    image_objects_table = Table ('imageobjects',m,autoload=True)
    line_strengths_table = Table ('linestrengths',m,autoload=True)
    magnitudes_table = Table ('magnitudes',m,autoload=True)
    line_names_table = Table ('linenames',m,autoload=True)
    filters_table = Table ('filters',m,autoload=True)
    selection_table = Table ('selection',m,autoload=True)
    selectionitems_table = Table ('selectionitems', m, autoload=True)
    
    map( lambda t: map(make_asfloat, t[1].columns), m.tables.items())

    mapper (Gadget_simulation, gadget_simulations_table, \
            properties = {'sunrise_simulations':
                          relation (Sunrise_simulation,
                                    backref = 'gadget_simulation',
                                    cascade = "all, delete"),
                          'objects':
                          relation (Gadget_object,
                                    backref = 'simulation',
                                    cascade = "all, delete") })
    mapper (Gadget_object, gadget_objects_table)
    mapper (Sunrise_simulation, sunrise_simulations_table, \
            properties = {'snapshots': relation (Snapshot,
                                                 backref = 'simulation',
                                                 cascade = "all, delete"),
                          'objects': relation (Sunrise_object,
                                               backref = 'simulation',
                                               cascade = "all, delete") })
    mapper (Sunrise_object, sunrise_objects_table)
    mapper (Snapshot, snapshots_table, \
            properties = {'cameras':
                          relation (Camera,
                                    backref = 'snapshot',
                                    cascade = "all, delete"),
                          'objects': relation (Snapshot_object,
                                               backref = 'snapshot',
                                               cascade = "all, delete") })
    mapper (Snapshot_object, snapshot_objects_table)
    mapper (Camera, cameras_table, \
            properties = {'objects':
                          relation (Image_object ,
                                    backref = 'camera',
                                    cascade = "all, delete")})
    mapper (Image_object, image_objects_table, \
            properties = {'magnitudes':
                          relation (Magnitude,
                                    backref = 'object',
                                    cascade = "all, delete"),
                          'line_strengths':
                          relation (Line_strength,
                                    backref = 'object',
                                    cascade = "all, delete") })
    mapper (Line_strength, line_strengths_table)
    mapper (Magnitude, magnitudes_table)
    mapper (Line, line_names_table, \
            properties = {'line_strengths':
                          relation (Line_strength,
                                    backref = 'line',
                                    cascade = "all, delete")})
    mapper (Filter, filters_table, \
            properties = {'magnitudes':
                          relation (Magnitude,
                                    backref = 'filter',
                                    cascade = "all, delete")})

    Session = sessionmaker(autoflush=True, transactional=transactional)
    #session = create_session()#Session ()
    session = Session()

    gsq = session.query(Gadget_simulation)
    goq = session.query (Gadget_object)
    ssq = session.query (Sunrise_simulation)
    soq = session.query (Sunrise_object)
    snq = session.query (Snapshot)
    caq = session.query (Camera)
    ioq = session.query (Image_object)
    maq = session.query (Magnitude)
    lsq = session.query (Line_strength) 
    liq = session.query (Line) 
    fiq = session.query (Filter)


