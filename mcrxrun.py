import os, pickle, glob, re, commands , sys, math, pdb, getopt
from local_mcrxrun import *

rundir = ".mcrxrun"
runfile = "runinfo"
simdir = "" # set from run

sfrhist_job_skeleton="""

sfrhist <configfile>
if [ $? -eq 0 ]
then
    touch <completefile>
    submit_and_print_job <nextjob> > <nextjobidfile>
else
    echo FAILED!
fi
"""

makegrid_skeleton ="""

makegrid <configfile>
if [ $? -eq 0 ]
then
    touch <completefile>
    submit_and_print_job <nextjob> > <nextjobidfile>
else
    echo FAILED!
fi

"""

try:
    mcrx_job_skeleton=batch_system.mcrx_job_skeleton
except:
    mcrx_job_skeleton="""
RESUBMIT_COMMAND=\"submit_and_print_job <jobfile> > <jobidfile>\"; export RESUBMIT_COMMAND
#MCRX_PP_CMD=\"submit_stack_postprocess <nextjob>\"; export MCRX_PP_CMD
# prevent mcrx by submitting or doing pp by this:
MCRX_PP_CMD=""; export MCRX_PP_CMD
WALL_CLOCK_LIMIT=`getwallclocklimit`; export WALL_CLOCK_LIMIT

cat <outfile> >> <outfile>-total
rm <outfile>

echo RESTART

mcrx <configfile>
MCRXSTATUS=$?
#cp /tmp/mcrx/mcrx_<snapshotno>.fits* $PBS_O_WORKDIR;
#CPSTATUS=$?
if [ $MCRXSTATUS -eq 42 ]
then
    echo COMPLETE

    #if [ $CPSTATUS -eq 0 ]
    #then
        touch <completefile>
        submit_stack_postprocess # must be here to include this job
        #rm -rf /tmp/mcrx
    #else    
    #    echo Failed copying files to $PBC_O_WORKDIR -- retaining output
    #fi
else
    echo FAILED!
fi
"""

pp_job_skeleton ="""
RESUBMIT_COMMAND=\"submit_and_print_job <jobfile> > <jobidfile>\"; export RESUBMIT_COMMAND
unset MCRX_PP_CMD;export MCRX_PP_CMD
WALL_CLOCK_LIMIT=`getwallclocklimit`; export WALL_CLOCK_LIMIT

mcrx <mcrxconfigfile>
if [ $? -eq 0 ]
then
    broadband <configfile>
    if [ $? -eq 0 ]
    then
        echo COMPLETE
        touch <completefile>
    else
        echo FAILED!
    fi
else
    echo FAILED!
fi

"""

class mcrxrun:
    "This class contains the snapshots list and a PID."
    def __init__(self, file):
        if os.path.exists (file):
            # load run data
            f = open (file)
            u = pickle.Unpickler (f)
            self.pid = u.load ()
            self.simdir = u.load ()
            self.snapshots = u.load ()
        else:
            raise RuntimeError,"No mcrxrun session found"
                
def commandline():
    """The function called from the executable. Parses the
    command-line options and runs the requested function."""

    # parse options
    (opts,args)=getopt.getopt(sys.argv[1:],'',
                              ['sfrhist_limit=','makegrid_limit=', 'mcrx_limit=',
                               'pp_limit=','makegrid_ncpus=','mcrx_ncpus=',
                               'queue='])
    #evaluate options by setting variables
    for o in opts:
        var=o[0].replace("--","")
        print "Setting ",var,"=",o[1]
        exec(var+"=\""+o[1]+"\"")

    print pp_limit
    if len(args)<1:
        print "mcrxrun commands:"
        print "  create\tCreates job files"
        print "  start <n>\tStarts jobs (or job # n)"
        print "  status\tReports job status"
        print "  restart\tRestarts failed or held jobs"
        print "  hold\tHolds jobs in queue"
        print "  kill\tKills jobs in queue"
        print "  cleanup\tRemoves all mcrxrun files"
        sys.exit(1)

    file = os.path.join ( rundir,runfile)

    # first try to create mcrxrun object
    try:
        run = mcrxrun(file)
    except:
        # the exception means no session was defined, so they only valid
        # command is create
        if (len (args) ==1) and (args [0] == "create"):
            run = create_run(file)        
            create_config_files (run)
            create_jobs (run)
            sys.exit(0)
        else:
            print "No mcrxrun session found"
            sys.exit(1)

    # we have an existing run
    # first argument is command
    cmd=args[0]
    # remaining are job numbers
    if len(args)>1:
        job_numbers=map(int,args[1:])
    else:
        job_numbers=None

    snapshots=run.snapshots
    print "Working on jobs ",job_numbers
    if cmd == "restart" :
        restart_jobs (snapshots, job_numbers)
        report_status (snapshots)
    elif cmd == "start" :
        start_jobs (snapshots, job_numbers)
        report_status (snapshots)
    elif cmd == "cleanup" :
        cleanup (snapshots)
    elif cmd == "hold" :
        hold_jobs (snapshots, job_numbers)
    elif cmd == "kill" :
        if raw_input ("Kill will kill all queued, held and running jobs.\nProceed? (y/N)") \
           != "y" :
            print "Not done"
            sys.exit(1)
        kill_jobs (snapshots, job_numbers)
    elif cmd == "status" :
        report_status (snapshots)
    elif cmd == "create" :
        print "A session already exists, do \'cleanup\' first"
    else:
        print "Unknown command: "+cmd
        sys.exit(1)


def create_run(file):
    global simdir
    "Creates the mcrx run file and returns a mcrxrun object."
    rundir = os.path.dirname (file)
    if not os.path.exists(rundir) or \
       not os.path.isdir (rundir) :
        print "Creating mcrxrun directory"
        os.mkdir (rundir)
        
    pid = os.getpid()
    simdir = os.getcwd()
    snapshots = make_file_list (pid)

    # save run data
    f = open (file, 'w')
    u = pickle.Pickler (f)
    u.dump (pid)
    u.dump (simdir)    
    u.dump (snapshots)
    f.close()

    # return snapshot object
    return mcrxrun(file)

def make_file_list (pid):
    global snapshot_file_base
    snapshot_regexp=re.compile('((%s)_(\d+))(?:\.hdf5)?$'%snapshot_file_base)
    f=os.listdir('.')
    files = [snapshot_regexp.match(i) for i in f if snapshot_regexp.match(i) != None]
    #pdb.set_trace()
    files.sort (cmp = lambda x,y: cmp(x.group(0), y.group(0)))
    snapshots = [(f.group(), "sfrhist-%s-%d"%(f.group(1), pid),
                  "makegrid-%s-%d"%(f.group(1), pid),
                  "mcrx-%s-%d"%(f.group(1), pid),
                  "postprocess-%s-%d"%(f.group(1), pid),
                  f.group(2), f.group(3), pid) for f in files]
#     snapshots = [(f.group(), "sfrhist-" + f.group(1) + "-" + `pid`,
#                   "makegrid-" + f.group(1) + "-" + `pid`,
#                   "mcrx-" + f.group(1) + "-" + `pid`,
#                   "postprocess-" + f.group(1) + "-" + `pid`) for f in files]
    print len (snapshots), "snapshots found"
    return snapshots

def create_sfrhist_config (snapshots):
    for s in snapshots:
        grid_name = os.path.splitext(s [0])[0].replace ("snapshot", "grid") 
        c = """# sfrhist configuration file automatically generated by mcrxrun
include_file\tsfrhist.stub
include_file\tmakegrid.stub
snapshot_file\t%s
output_file\t%s.fits
use_counters\tfalse
"""%(s[0],grid_name)
        g = open (s[1]+ ".config", 'w')
        g.write (c)

# def create_makegrid_config (snapshots):
#     for s in snapshots:
#         grid_name = s [0].replace ("snapshot", "grid") 
#         c = """# makegrid configuration file automatically generated by mcrxrun
# include_file\tmakegrid.stub
# input_file\t""" + s [0] + """.fits
# output_file\t""" + grid_name + """.fits
# use_counters\tfalse
# """
#         g = open (s[2] + ".config", 'w')
#         g.write (c)

def create_mcrx_config (snapshots):
    for s in snapshots:
        grid_name = os.path.splitext(s [0])[0].replace ("snapshot", "grid") 
        mcrx_name = os.path.splitext(s [0])[0].replace ("snapshot", "mcrx") 
        c = """# mcrx configuration file automatically generated by mcrxrun
include_file\tmcrx.stub
input_file\t%s.fits
output_file\t%s.fits
use_counters\tfalse
postprocess_command_variable\tMCRX_PP_CMD
#skip_postprocessing\ttrue
"""%(grid_name, mcrx_name)
        g = open (s[3] + ".config", 'w')
        g.write (c)

def create_postprocess_config (snapshots):
    for s in snapshots:
        mcrx_name = os.path.splitext(s [0])[0].replace ("snapshot", "mcrx") 
        bb_name = os.path.splitext(s [0])[0].replace ("snapshot", "broadband") 
        c = """# broadband configuration file automatically generated by mcrxrun
include_file\tbroadband.stub
input_file\t%s.fits
output_file\t%s.fits
use_counters\tfalse
"""%(mcrx_name, bb_name)
        g = open (s[4] + ".config", 'w')
        g.write (c)

def create_config_files (run):
    global simdir
    simdir=run.simdir
    snapshots=run.snapshots
    
    create_sfrhist_config (snapshots)
    #create_makegrid_config (snapshots)
    create_mcrx_config (snapshots)
    create_postprocess_config (snapshots)

def sub_skeleton (skeleton, base_name, snapshot, substitutions=[]) :
    global rundir, simdir, queue_class, queue
    substitutions += [ \
        # load leveler stuff
        ("<jobname>", "%s%s_%d"%(base_name[:2],snapshot[6],snapshot[7])),
        ("<outfile>", os.path.join(simdir,base_name + ".out")),
        ("<errfile>", os.path.join(simdir,base_name + ".err")),
        ("<class>", queue_class ),
        ("<queue>", queue ),
        # job stuff
        ("<configfile>", os.path.join(simdir,base_name + ".config")),
        ("<completefile>",
         os.path.join (simdir,rundir, base_name + "-complete")),
        ("<jobfile>", os.path.join(simdir,base_name + ".job")),
        ("<simdir>", simdir),
        ("<ncpu_keyword>", ncpu_keyword)
        ]

    #if job_type != serial_job_type:
    #    skeleton = skeleton.replace ("<node>", "#@ node         = 1" ) 
    #else:
    #    skeleton = skeleton.replace ("<node>", "" ) 

    # apply subs
    for i in substitutions:
        skeleton = skeleton.replace(i[0],i[1])
        
    return skeleton

def create_sfrhist_job (skeleton , snapshot):
    global queue_class , sfrhist_limit, rundir, sfrhist_job_skeleton
    global queue_class , makegrid_limit, rundir, makegrid_skeleton
    ll = skeleton + sfrhist_job_skeleton
    substitutions=[
        ("<nextjob>", os.path.join(simdir,snapshot[2] + ".job")),
        ("<ncpus>", sfrhist_ncpus),
        ("<nextjobidfile>",
         os.path.join (simdir, rundir, snapshot [2] + "-jobid") ),
        ("<jobtype>", serial_job_type ),
        ("<limit_hours>", format_time(sfrhist_limit) )
        ]
    # use makegrid parameters for sfrhist job too
    substitutions=[
        ("<nextjob>", os.path.join(simdir,snapshot[3] + ".job")),
        ("<ncpus>", makegrid_ncpus),
        ("<nextjobidfile>",
         os.path.join (simdir, rundir, snapshot [3] + "-jobid") ),
        ("<jobtype>", job_type ),
        ("<limit_hours>", format_time(makegrid_limit) )
        ]
    ll=sub_skeleton (ll, snapshot[1], snapshot, substitutions)

    f = open (#os.path.join (rundir,
        snapshot[1] + ".job", 'w')
    f.write (ll)

# def create_makegrid_job (skeleton , snapshot):
#     ll = skeleton + makegrid_skeleton
#     substitutions=[
#         ("<nextjob>", os.path.join(simdir,snapshot[3] + ".job")),
#         ("<ncpus>", makegrid_ncpus),
#         ("<nextjobidfile>",
#          os.path.join (simdir, rundir, snapshot [3] + "-jobid") ),
#         ("<jobtype>", job_type ),
#         ("<limit_hours>", format_time(makegrid_limit) )
#         ]
#     ll=sub_skeleton (ll, snapshot[2], substitutions)

#     f = open (#os.path.join (rundir,
#         snapshot [2] + ".job", 'w')
#     f.write (ll)

def create_mcrx_job (skeleton , snapshot):
    global queue_class , mcrx_limit, rundir, mcrx_job_skeleton
    ll = skeleton + mcrx_job_skeleton
    substitutions=[
        ("<ncpus>", mcrx_ncpus),
        ("<jobidfile>",
         os.path.join (simdir, rundir, snapshot [3] + "-jobid")),
        ("<nextjob>", os.path.join(simdir,snapshot[4] + ".job")),
        ("<nextjobidfile>",
         os.path.join (simdir, rundir, snapshot [4] + "-jobid") ),
        ("<jobtype>", serial_job_type ),
        ("<limit_hours>", format_time(mcrx_limit) ),
        ("<snapshotno>",snapshot[0].replace("snapshot_",""))
        ]
    ll=sub_skeleton (ll, snapshot[3], snapshot, substitutions)

    f = open (#os.path.join (rundir,
        snapshot [3] + ".job", 'w')
    f.write (ll)

def create_pp_job (skeleton , snapshot):
    global queue_class , pp_limit, rundir, pp_job_skeleton
    ll = skeleton + pp_job_skeleton
    #print "pp limit is ",pp_limit
    substitutions=[
        ("<ncpus>", pp_ncpus),
        ("<jobidfile>",
         os.path.join (simdir, rundir, snapshot [4] + "-jobid") ),
        ("<mcrxconfigfile>",
         os.path.join(simdir,snapshot[3] + ".config")),        
        ("<jobtype>", serial_job_type ),
        ("<limit_hours>", format_time(pp_limit) ),
        ("<snapshotno>",snapshot[0].replace("snapshot_",""))
        ]
    ll=sub_skeleton (ll, snapshot[4], snapshot, substitutions)

    f = open (#os.path.join (rundir,
        snapshot [4] + ".job", 'w')
    f.write (ll)

def create_jobs (run):
    global rundir, queue_class , sfrhist_limit
    global simdir
    simdir=run.simdir
    snapshots=run.snapshots

    print "Creating batch files"
    skeleton = batch_system.job_skeleton
    for s in snapshots:
        create_sfrhist_job (skeleton, s)
        #create_makegrid_job (skeleton, s)
        create_mcrx_job (skeleton, s)
        create_pp_job (skeleton, s)

def start_jobs (snapshots,job_numbers=None):
    "Starts jobs. If job_numbers is empty, all jobs are started."
    if job_numbers==None:
        job_numbers=range (len(snapshots))
        print "Submitting sfrhist jobs"
    else:
        print "Submitting sfrhist jobs: ", job_numbers
        
    for i in job_numbers:
        # submit sfrhist jobs
        print  "\t" +  snapshots [i]  [0]
        if job_status (snapshots [i] [1]) == "- ":
            submit_job ( snapshots [i] [1])
        else:
            print "\t\tJob already started"

def submit_job (job):
    jobid = batch_system.submit_job(job+".job")

    f = open (os.path.join (rundir, job + "-jobid"), 'w')
    f.write (jobid)

def release_job (job):
    jobidfile=os.path.join (rundir, job + "-jobid")
    if os.path.exists (jobidfile):
        # the job has been queued try to release it
        try:
            f = open (jobidfile, 'r')
            jobid = f.read ().splitlines () [0]
            status = commands.getoutput (batch_system.release_command + jobid).splitlines () [2]
        except :
            pass

def hold_job (job):
    jobidfile=os.path.join (rundir, job + "-jobid")
    if os.path.exists (jobidfile):
        # the job has been queued try to hold it
        try:
            f = open (jobidfile, 'r')
            jobid = f.read ().splitlines () [0]
            status = commands.getoutput (batch_system.hold_command + jobid).splitlines () [2]
        except :
            pass

def kill_job (job):
    jobidfile=os.path.join (rundir, job + "-jobid")
    if os.path.exists (jobidfile):
        # the job has been queued try to kill it
        try:
            f = open (jobidfile, 'r')
            jobid = f.read ().splitlines () [0]
            status = commands.getoutput (batch_system.kill_command + jobid).splitlines () [2]
        except :
            pass

# extract a job status letter from the queue status message
def job_status (job):
    if os.path.exists (os.path.join (rundir, job + "-complete")):
        # complete
        return "C "
    jobidfile=os.path.join (rundir, job + "-jobid")
    if os.path.exists (jobidfile):
        # the job has been queued , query status
        try:
            status = batch_system.read_job_status(jobidfile)
        except :
            # no status, but job ID and not complete : means failed 
            return "F!"
        return status
    # not complete, and no job ID -- has not been submitted yet
    return "- " 

# now unneeded?
#def report_sfrhist_status (snapshots):
#   print "sfrhist jobs status :"
#   for i in range (0, len (snapshots), sfrhist_n):
#        print snapshots [i] [0] + ": " + job_status (snapshots [i] [1])

# goes through and reports the status of the jobs         
def report_status (snapshots):
    print "jobs status :"
    for i in range(len(snapshots)):
        s = snapshots[i]
        msg = "%d: %s:\tsfrhist: %s\tmcrx: %s\tpostprocess: %s"%(i,os.path.splitext(s [0])[0],job_status(s[1]),job_status(s[3]), job_status(s[4]))
        
#         msg = `i`+": "+s [0] + ": sfrhist: " + job_status (s [1]) + \
#               ",  makegrid: " + job_status (s [2]) + \
#               ",  mcrx: " + job_status (s [3]) + ", postprocess: " + \
#               job_status(s[4])
        print msg

def check_job_and_restart_release (job):
    "Restarts/releases job if its status is F or H. Returns true if action was takes."
    status = job_status (job) 
    if status == "F!"  :
        # failed, restart it
        print "restarting " + job
        submit_job (job)
        return True
    elif status == batch_system.status_held :
        # on hold, release
        print "releasing from hold " + job
        release_job (job)
        return True
    else:
        return False

def restart_jobs (snapshots, job_numbers=None):
    "Restarts jobs. If job_numbers is empty, all jobs are restarted."
    if job_numbers==None:
        job_numbers=range (len(snapshots))
        print "Submitting sfrhist jobs"
    else:
        print "Submitting sfrhist jobs: ", job_numbers
        
    for i in job_numbers:
        s =snapshots[i]
        if(not check_job_and_restart_release(s[1])):
            if(not check_job_and_restart_release(s[2])):
                if(not check_job_and_restart_release(s[3])):
                    check_job_and_restart_release(s[4])

def check_job_and_kill (job):
    status = job_status (job) 
    if (status == batch_system.status_queued) or \
       (status == batch_system.status_running) or \
       (status == batch_system.status_held) or \
       (status == batch_system.status_notqueued):
        print "killing " + job
        kill_job (job)

def kill_jobs (snapshots, job_numbers=None):
    if job_numbers==None:
        job_numbers=range (len(snapshots))
        print "Killing jobs"
    else:
        print "Killing jobs: ", job_numbers
    for i in job_numbers:
        s=snapshots[i]
        check_job_and_kill(s[1])
        check_job_and_kill(s[2])
        check_job_and_kill(s[3])
        check_job_and_kill(s[4])

def check_job_and_hold (job):
    status = job_status (job) 
    if (status == batch_system.status_queued) or \
       (status == batch_system.status_notqueued) :
        # in queue, hold it
        print "holding " + job
        hold_job (job)

def hold_jobs (snapshots, job_numbers=None):
    if job_numbers==None:
        job_numbers=range (len(snapshots))
        print "Holding jobs"
    else:
        print "Holding jobs: ", job_numbers
        
    for i in job_numbers:
        s=snapshots[i]
        check_job_and_hold(s[1])
        check_job_and_hold(s[2])
        check_job_and_hold(s[3])
        check_job_and_hold(s[4])

def cleanup (snapshots):
    if raw_input ("Cleanup will delete all generated files and kill dangling jobs.\nProceed? (y/N)") \
       != "y" :
        print "Not done"
        return

    # kill jobs
    kill_jobs(snapshots)

    for s in snapshots:
        rm (s [1] + ".config")
        rm (s [1] + ".job")
        rm (s [2] + ".config")
        rm (s [2] + ".job")
        rm (s [3] + ".config")
        rm (s [3] + ".job")
        rm (s [4] + ".config")
        rm (s [4] + ".job")

    for f in os.listdir ( rundir):
        rm (os.path.join ( rundir, f))

def format_time(hours):
    "Formats a decimal hours value to a <hh>:<mm>:<ss> string."
    # integer hours
    h=int(math.floor(hours))
    # decimal minutes
    m=(hours-h)*60
    # seconds
    s=int((m-math.floor(m))*60)
    # we frequently have to run this on machines with old-ass pythons,
    # so we can't use the nice ternary operator syntax
    if batch_system.use_seconds:
        return "%d:%02d:%02d"%(h,int(m),s)
    else:
        return "%d:%02d"%(h,int(m))

def rm (file):
    try:
        os.remove (file)
    except:
        pass


