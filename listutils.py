import types

# functions for optionally picking out elements of tuple lists
optional_ith = lambda element, i: element if not type(element)==types.TupleType else element[i]
optional_1st = lambda l: optional_ith (l, 0)
optional_2nd = lambda l: optional_ith (l, 1)

optional_iths = lambda l, i: [ optional_ith(element,i) for element in l]
optional_1sts = lambda l: optional_iths(l,0)
optional_2nds = lambda l: optional_iths(l,1)

