from math import *
from numpy import *

def pos_append_to_config(fnam,i, pos_fun, dir_fun, up_fun, fov_fun):
    fov=fov_fun(i)
    pos=pos_fun(i)
    dir=dir_fun(i)
    up=up_fun(i)
    print pos,dir,up,fov
    f=open(fnam, 'a')
    
    f.write("camera_position %e %e %e\n" % (pos[0], pos[1], pos[2]))
    f.write("camera_direction %e %e %e\n" % (dir[0], dir[1], dir[2]))
    f.write("camera_up %e %e %e\n" % (up[0], up[1], up[2]))
    f.write("camerafov %e\n" % fov)
    f.close()

    
def append_to_config(fnam,i, phi_fun, theta_fun, dist_fun, fov_fun):
    phi=phi_fun(i)
    theta=theta_fun(i)
    dist=dist_fun(i)
    fov=fov_fun(i)
    pos=(sin(phi)*sin(theta)*dist, cos(phi)*sin(theta)*dist, cos(theta)*dist)
    dir=(-sin(phi)*sin(theta),-cos(phi)*sin(theta),-cos(theta))
    up=(0.,0.,1.)
    print phi,pos,dir,up
    f=open(fnam, 'a')
    
    f.write("camera_position %e %e %e\n" % (pos[0], pos[1], pos[2]))
    f.write("camera_direction %e %e %e\n" % (dir[0], dir[1], dir[2]))
    f.write("camera_up %e %e %e\n" % (up[0], up[1], up[2]))
    f.write("camerafov %e\n" % fov)
    f.close()

def simple_rotation(fnam, i, rate):
    append_to_config(fnam, i,
                     lambda x: x*2.*pi/rate,
                     lambda x: pi/2-.1,
                     lambda x: 10000.,
                     lambda x: 0.02)

    
def link_config(config_file, pos_file):
    """adds "include_file pos_file" to teh config file."""
    f=open(config_file,'a')
    f.write("include_file %s" % pos_file)
    f.close()

def link_campos(config_file, pos_file):
    """adds "camera_positions pos_file" to teh config file."""
    f=open(config_file,'a')
    f.write("camera_positions %s" % pos_file)
    f.close()

def pos_append_to_campos(fnam,i, pos_fun, dir_fun, up_fun, fov_fun):
    fov=fov_fun(i)
    pos=pos_fun(i)
    dir=dir_fun(i)
    up=up_fun(i)
    print pos,dir,up,fov
    f=open(fnam, 'a')
    
    f.write("%e %e %e\t%e %e %e\t%e %e %e\t%e\n" % (pos[0], pos[1], pos[2],dir[0], dir[1], dir[2],up[0], up[1], up[2], fov))
    f.close()

def rotationmatrix(axis, angle):
    """Returns the rotation matrix for the specified axis and angle."""
    axis=axis/sqrt(dot(axis,axis))
    x=axis[0]
    y=axis[1]
    z=axis[2]
    c=cos(angle)
    s=sin(angle)
    C=(1-c)
    xs = x*s
    ys = y*s
    zs = z*s
    xC = x*C
    yC = y*C
    zC = z*C
    xyC = x*yC
    yzC = y*zC
    zxC = z*xC
    rot = matrix( [ [x*xC+c, xyC-zs, zxC+ys],
                    [xyC+zs, y*yC+c, yzC-xs],
                    [zxC-ys, yzC+xs, z*zC+c] ] )
    return rot
    
def rotate_around(current_pos, current_dir, up, fov, center, axis, rate):
    """Generates functors that rotate around the axis centered at the
    specified position. Positions should be numpy arrays."""

    delta = current_pos-center
    theta = lambda t: rate*t
    
    M=lambda t: rotationmatrix(axis, theta(t))

    return (lambda t: (center+delta*M(t)).A[0],
            lambda t: (current_dir*M(t)).A[0],
            lambda t: (up*M(t)).A[0],
            lambda t: fov)
            
    
    

                   
