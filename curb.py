# Read power data from CURB api and log to RRD

import requests, os, time
import heating
import pdb

# These are from
# https://github.com/Curb-v2/third-party-app-integration/blob/master/docs/api.md
#client_id = 'iKAoRkr3qyFSnJSr3bodZRZZ6Hm3GqC3'
#client_secret = 'dSoqbfwujF72a1DhwmjnqP4VAiBTqFt3WLLUtnpGDmCf6_CMQms3WEy8DxCQR3KY'

#From Curb support
client_id = '8GqFQL3YmY9UA3za6BclIDkkVK5oNEsJ'
client_secret = 'Yb7DNYThf3FY3YhaWUT1rtIUGFq8qd6eNAEcEW9avyeu0XXpuxmireGm3BasRBQa'

access_token = None
location_id = None
verbose = True

def get_access_token():
    if verbose:
        print("Getting access token")
        
    r = requests.post('https://energycurb.auth0.com/oauth/token',
                      data = {"grant_type": "password",
                              "audience": "app.energycurb.com/api",
                              "username": "patrik-web@familjenjonsson.org",
                              "password": os.environ['CURBPASS'],
                              "client_id": client_id,
                              "client_secret": client_secret
                              }
                      )
    global access_token
    try:
        access_token = r.json()['access_token']
        if verbose:
            print("Successfully got access token %s"%access_token)
    except:
        if verbose:
            print("No access_token found in response")
            print(`r`)
    
def get_locations():
    if verbose:
        print("Getting locations")
    r = requests.get('https://app.energycurb.com/api/v3/locations',
                      headers = {"authorization": "Bearer %s"%access_token}
                     )
    global location_id
    location_id = "65e2d362-bfdf-4625-a26e-5419e98aa631"

    # check that this location is present
    loc = [l for l in r.json() if l['id'] == location_id]

    assert len(loc)==1
    return loc



def get_installations():
    if verbose:
        print("Getting installations")

    r = requests.get('https://app.energycurb.com/api/locations/%s/installations'%location_id,
                      headers = {"authorization": "Bearer %s"%access_token}
                     )

    return r.json()

def get_latest():
    if verbose:
        print("Getting latest data")
        
    r = requests.get('https://app.energycurb.com/api/latest/%s'%location_id,
                      headers = {"authorization": "Bearer %s"%access_token}
                     )

    return r.json()



def curb_log():
    os.chdir('/home/patrik/heating')

    get_access_token()
    get_locations()
    
    datafuncs = [
        # Watts drawn from the grid (negative if net production)
        ('Grid_supply', lambda val: val['net']),
        ('House_consumption', lambda val: val['consumption']),
        ('House_production', lambda val: val['production']),
     ]

    failures=0
    while True:
        #if failures>10:
        #    print "too many failures"
        #    break

        try:
            latest = get_latest()
        except:
            # no response, wait and retry
            print ("Exception getting latest data")
            failures += 1
            time.sleep(15)
            continue
        
        if 'status' in latest:
            print("Got status in response: %s"%`latest`)
            failures += 1
            
            # this means access token expired, get new
            get_access_token()
            continue

        #print latest
        #pdb.set_trace()
        data = {}
        for d in datafuncs:
            try:
                data[d[0]] = d[1](latest)
            except:
                pass

        print data
        heating.update_database(data)
        failures=0
        time.sleep(10)

    
