from numpy import *
from scipy.optimize import newton
import scipy
scipy.pkgload()
from scipy.integrate import quad
import pdb
import pylab

def interpolate_point(x0, y0,
                      x1, y1,
                      x,
                      logarithmic):
    assert x>=x0
    assert x<=x1

    if logarithmic and ((y0==0) or (y1==0)):
        # in this case a power-law segment is undefined and we revert
        # to linear, it's important enough to be able to have zeros in
        # the segment that we don't want to generate an error
        logarithmic=False
        
    if logarithmic:
        assert y0>0
        assert y1>0
            
        alpha = log(y1/y0)/log(x1/x0); 
        return y0*pow(x/x0, alpha);
    else:
        return (y1-y0)/(x1-x0)*(x-x0)+y0;

def interpolate_point_in_array(ax,ay,x,logarithmic):
    """Interpolates the point x in the function defined by ax and ay."""
    assert x>=ax[0]
    assert x<=ax[-1]
    if x==ax[-1]:
        return ay[-1]
    sel=argwhere(x<ax)[:,0]
    y = interpolate_point(ax[sel[0]-1], ay[sel[0]-1],
                          ax[sel[0]],   ay[sel[0]],
                          x,
                          logarithmic=logarithmic)
    return y

def slope(x0,y0,x1,y1,logarithmic):
    if logarithmic:
        return log(y1/y0)/log(x1/x0);
    else:
        return (y1-y0)/(x1-x0)
        
def interpolate_slope_in_array(ax,ay,x,logarithmic):
    """Returns the slope at point x in the function defined by ax and ay. If logarithmic, returns alpha, the power-law slope."""
    assert x>=ax[0]
    assert x<=ax[-1]
    sel=argwhere(x<ax)[:,0]
    if sel.shape[0]==0:
        # first point
        i=0
    else:
        i=sel[0]
    alpha = log(ay[i+1]/ay[i])/log(ax[i+1]/ax[i]); 
    return alpha
    
def array_interpolator(ax,ay,logarithmic):
    """Returns an ufunc that interpolates points onto the function defined by arrays ax and ay."""
    f = lambda x: interpolate_point_in_array(ax,ay,x,logarithmic)
    af = frompyfunc(f,1,1)
    return af

def interpolate_onto(x,y,newx,logarithmic):
    """Interpolates the function values defined by arrays x and y onto array newx."""
    return array_interpolator(x,y,logarithmic)(newx).astype('float64')

def integrate_segment(x0, y0,
                      x1, y1,
                      logarithmic):

    assert x1>=x0
    if x0==x1:
        return 0
    if logarithmic and ((y0==0) or (y1==0)):
        # in this case a power-law segment is undefined and we revert
        # to linear, it's important enough to be able to have zeros in
        # the segment that we don't want to generate an error
        logarithmic=False

    if logarithmic:
        assert y0>0
        assert y1>0

        xr= x1/x0
        logxr = log(xr)
        alpha = log(y1/y0)/logxr

        if abs(alpha+1)>1e-4:
            # use analytic integral formula
            return y0*x0*( xr**(alpha+1) -1 )/(alpha+1)
        else:
            # use expansion when alpha is close to -1
            return y0*x0*( logxr + 0.5*logxr*logxr*(alpha+1) )
    else:
        return 0.5*(y1+y0)*(x1-x0)

def powerlaw_solve_func(alpha,x0,y0,xr, integral):
    if abs(alpha+1)<1e-4:
        return integral-y0*x0*( log(xr) + 0.5*log(xr)**2*(alpha+1) )
    else:
        return integral-y0*x0*( xr**(alpha+1) -1 )/(alpha+1)

def powerlaw_solve_fprime(alpha,x0,y0,xr, integral):
    if abs(alpha+1)<1e-4:
        return -0.5*y0*x0*log(xr)**2
    else:
        return y0*x0*( (xr**(alpha+1) -1 ) -
                       xr**(alpha+1)*log(xr)*(alpha+1) )/(alpha+1)**2

def solve_for_segment(x0,y0,x1,integral,logarithmic,guess=None):
    """Solves for the right point of a segment that has the specified integral. Guess contains the initial guess for alpha for power laws."""
    assert x1>x0
    if logarithmic and (y0==0):
        # in this case a power-law segment is undefined and we revert
        # to linear, it's important enough to be able to have zeros in
        # the segment that we don't want to generate an error
        logarithmic=False
        
    if logarithmic:
        assert y0>0
        assert integral>0

        xr= x1/x0
        logxr = log(xr)

        # power-law alpha can only be found numerically...
        f=lambda alpha: powerlaw_solve_func(alpha,x0,y0,xr,integral)
        df=lambda alpha: powerlaw_solve_fprime(alpha,x0,y0,xr,integral)
        try:
            # need a good initial guess for alpha -- get from original func?
            alpha = newton(f,guess if guess!=None else 1,fprime=df)
            #print "alpha is ",alpha
        except RuntimeError,e:
            # if we failed to converge, start plowing through guesses
            print "failed to converge at x=",x1,"trying brute force"
            guess = 5
            alpha=None
            while alpha==None:
                try:
                    #print "trying ",guess
                    alpha = newton(f,guess,fprime=df)
                except RuntimeError,e:
                    try:
                        #print "trying ",-guess
                        alpha = newton(f,-guess,fprime=df)
                    except RuntimeError,e:
                        guess *= 2

            print "success, alpha=",alpha
            #pdb.set_trace()
        y1= y0*pow(xr, alpha);
    else:
        y1= 2*integral/(x1-x0)-y0
        
    #print integrate_segment(x0,y0,x1,y1,logarithmic)-integral
    return y1


    
# ufuncs used by integrate_array and integrate_between
aintegrate_segment_lF = frompyfunc(lambda x0,y0,x1,y1:
                                   integrate_segment(x0,y0,x1,y1,logarithmic=False),4,1)
aintegrate_segment_lT = frompyfunc(lambda x0,y0,x1,y1:
                                   integrate_segment(x0,y0,x1,y1,logarithmic=True),4,1)

def integrate_array(x,y,logarithmic):
    """Integrates an array by using the above functions."""
    assert x.shape[0]>1
    if logarithmic:
        return sum(aintegrate_segment_lT(x[:-1],y[:-1],x[1:],y[1:]))
    else:
        return sum(aintegrate_segment_lF(x[:-1],y[:-1],x[1:],y[1:]))

def integrate_between(x,y,xmin,xmax,logarithmic):
    """Integrates a function defined by the x, y arrays between xmin
    and xmax."""
    assert xmin>=x[0]
    assert xmax<=x[-1]

    # we just have to handle the partial segments on the edges
    lsel=argwhere(x<=xmax)[:,0]
    rsel=argwhere(x>=xmin)[:,0]
    sel = intersect1d(lsel,rsel)
    s=0
    #pdb.set_trace()
    # if sel is empty then the interval is within one segment
    if sel.shape[0]==0:
        s=integrate_segment(xmin,
                            interpolate_point_in_array(x,y,xmin,logarithmic),
                            xmax,
                            interpolate_point_in_array(x,y,xmax,logarithmic),
                            logarithmic=logarithmic)
        return s

    # so interval is not confined within one segment
    # first deal with partial end segments
    if xmin < x[sel[0]]:
        s+=integrate_segment(xmin,
                             interpolate_point(x[sel[0]-1], y[sel[0]-1],
                                               x[sel[0]],   y[sel[0]],
                                               xmin,
                                               logarithmic=logarithmic),
                             x[sel[0]], y[sel[0]],
                             logarithmic=logarithmic)
    if xmax > x[sel[-1]]:
        s+=integrate_segment(x[sel[-1]], y[sel[-1]],
                             xmax,
                             interpolate_point(x[sel[-1]],   y[sel[-1]],
                                               x[sel[-1]+1], y[sel[-1]+1],
                                               xmax,
                                               logarithmic=logarithmic),
                             logarithmic=logarithmic)

    # and then the interior segments, if any
    if sel.shape[0]>1:
        s+= integrate_array(x[sel],y[sel],logarithmic)
    return s

def mean(a,b,logarithmic,wa=1,wb=1):
    if logarithmic:
        return exp( (wa*log(a)+wb*log(b))/(wa+wb) )
    else:
        return (wa*a+wb*b)/(wa+wb)
    
def resample_starting_with(x,y,newx,ystart,logarithmic):
    """Resamples the function defined by x and y onto newx, preserving the integral. The first sample is ystart, the rest are constrained."""
    assert newx[0]>=x[0]
    assert newx[-1]<=x[-1]

    newy=empty(shape=newx.shape)
    # we have to loop to do this because each segment depends on the previous...
    
    #newy[0]=interpolate_point_in_array(x,y,newx[0],logarithmic)
    newy[0]=ystart
    for xi in range(newx.shape[0]-1):
        integral = integrate_between(x,y,newx[xi],newx[xi+1],logarithmic)
        guess= slope(newx[xi],newy[xi],newx[xi+1], interpolate_point_in_array(x,y,newx[xi+1],logarithmic),logarithmic) if logarithmic else None
        newy_i = interpolate_point_in_array(x,y,newx[xi+1],logarithmic)
        try:
            newy_s = solve_for_segment(newx[xi],newy[xi],newx[xi+1],
                                       integral, logarithmic, guess=guess)
            newy[xi+1] = mean(newy_s, newy_i, logarithmic,wa=1)
        except:
            # we failed to converge despite trying hard. probably this means the function shrunk too fast the slope underflowed.
            newy[xi+1] = newy_i

            #raise
#print "resampled", \
            #interpolate_point_in_array(x,y,newx[xi+1],logarithmic), \
             # "to",newy[xi+1]
    return newy

def rmsdiff(ax0,ay0,ax1,ay1,xmin,xmax,logarithmic):
    """Calculates the rms difference between the functions defined by
    (x,y) and (xp,yp), respectively, integrated between xmin and xmax."""
    if logarithmic:
        intf=lambda x: (log(interpolate_point_in_array(ax0,ay0,x,logarithmic))-
                        log(interpolate_point_in_array(ax1,ay1,x,logarithmic)))**2
    else:
        intf=lambda x: (interpolate_point_in_array(ax0,ay0,x,logarithmic) -
                        interpolate_point_in_array(ax1,ay1,x,logarithmic))**2
    return sqrt(quad(intf,xmin,xmax)[0])

def resample_minimize_func(x,y,newx,ystart,logarithmic):
    print "using starting guess",ystart,y[0]
    newy=resample_starting_with(x,y,newx,ystart,logarithmic)
    if logarithmic:
        rms=sqrt(sum((log(newy)-log(interpolate_onto(x,y,newx,logarithmic)))**2))
    else:
        rms=sqrt(sum((newy-interpolate_onto(x,y,newx,logarithmic))**2))
    print "rms difference is",rms
    return rms

def test():
    """Tests that the integrate_between function works. Points should be on curve'"""
    
    x=logspace(0,3,10)
    y=2./x**2
    xx=logspace(0,1,100)
    yy=[integrate_between(x,y,xxi,10,True) for xxi in xx]
    ayy=2*(1./xx-1./10)

    maxerr=max(abs(yy/ayy-1))
    print "Max fractional difference ", maxerr
    assert maxerr<1e-13
    print "test passed"
    

def bestfit_minfunc(x,y,x0,x1,y0,integral):
    """Returns the rms for segment for the current assumed y0 and
    integral."""
    alphaguess= slope(x0, interpolate_point_in_array(x,y, x0, True),
                      x1, interpolate_point_in_array(x,y, x1, True),
                      True)
    y1=solve_for_segment(x0,y0,x1,integral,True,guess=alphaguess)
    tempx=array([x0,x1])
    tempy=array([y0,y1])
    return rmsdiff(x,y,tempx,tempy,x0,x1,True)
    
def solve_for_bestfit_segment(x,y,x0,x1,integral):
    """Solves for the segment that preserves the integral between x0
    and x1 and minimizes the log rms difference between the functions."""
    #y0guess=integrate_between(x,y,x0,x1,True)/(x1-x0)
    y0guess=interpolate_point_in_array(x,y,x0,True)
    
    minfunc=lambda y0: bestfit_minfunc(x,y,x0,x1,exp(y0),integral)
    #pdb.set_trace()
    y0 = exp(scipy.optimize.fmin_bfgs(minfunc,log(y0guess),epsilon=0.01*y0guess))
    alphaguess= slope(x0, interpolate_point_in_array(x,y, x0, True),
                      x1, interpolate_point_in_array(x,y, x1, True),
                      True)
    y1=solve_for_segment(x0,y0,x1,integral,True,guess=alphaguess)
    return (y0,y1)

def bestfit_array(x,y,newx):
    newy=empty(shape=newx.shape)
    nexty=None
    for xi in range(newx.shape[0]-1):
        integral=integrate_between(x,y,newx[xi],newx[xi+1],True)
        yseg=solve_for_bestfit_segment(x,y,newx[xi],newx[xi+1],integral)

        if xi==0:
            newy[0]=yseg[0]
        else:
            newy[xi]=exp(0.5*(log(yseg[0])+log(nexty)))
            
        nexty=yseg[1]

        pylab.loglog([newx[xi],newx[xi+1]],yseg,'r-')

    newy[-1]=nexty
    return newy

def make_loop(v1,v2):
    """Takes two arrays and makes a loop of them by flipping the
    second before adding it."""
    length=v1.shape[0]
    assert length==v2.shape[0]
    loop=empty(shape=(2*length), dtype=float64)
    loop[:length]=v1
    loop[length:]=v2[::-1]
    return loop


def rebin_factor( a, newshape ):
    '''Rebin an array to a new shape.  newshape must be a factor of
    a.shape.
    '''
    assert len(a.shape) == len(newshape)
    assert not sometrue(mod( a.shape, newshape ))

    slices = [ slice(None,None, old/new) for old,new in zip(a.shape,newshape) ]
    return a[slices]

def delta_quantity(a):
    """Calculates the bin width values of an array."""
    n=a.shape[0]
    da=empty(n)
    if n>1:
        da[1:-1] = 0.5*(a[2:]-a[0:-2])
        da[0] = a[1]-a[0]
        da[-1] = a[-1]-a[-2]
    else:
        da=1.0
    return da
