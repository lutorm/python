from pylab import *
import pyfits, numpy, vecops, stats, pylab,pdb,pyx,pyxutil

def sed_generator(files, cam, logarithmic):
    """Returns a generator that spits out the SED of the specified
    column in the specified files. If logarithmic is true, then it
    returns the log of the SED."""

    for f in files:
        ff=pyfits.open(f)
        iq=ff['INTEGRATED_QUANTITIES'].data
        sed=iq.field("L_lambda%d"%cam)+iq.field("L_lambda_ir%d"%cam)
        if logarithmic:
            sed=log10(sed)
        yield sed

def p04comparison(files,cams,p04file, relative=False, legendtxt=None):

    clf()
    
    p04data=numpy.loadtxt(p04file)
    pl=p04data[:,0]*1e-6

    if not iterable(cams):
        cams=[cams]
    if not iterable(files):
        files=[files]
    l=pyfits.open(files[0])['INTEGRATED_QUANTITIES'].data.field("lambda")
    if relative:
        pylab.semilogx(l,l*0)

    lines=[]
    for c in cams:
        
        p1=p04data[:,c+1]*1e-7

        mean,sigma,n = stats.sigma(sed_generator(files, c, not relative))
        if relative:
            lines.append(pylab.semilogx(l,l*mean/p1-1))
            fill(vecops.make_loop(l,l),
                 vecops.make_loop(l*(mean+sigma)/p1-1, \
                                  l*(mean-sigma)/p1-1), \
                 alpha=0.3,facecolor=lines[-1][0].get_color())
        else:
            lines.append(loglog(pl,p1))
            lines.append(loglog(l,l*10**mean))
            fill(vecops.make_loop(l,l), \
                 vecops.make_loop(l*10**(mean+sigma), l*10**(mean-sigma)), \
                 alpha=0.3,facecolor=lines[-1][0].get_color())

    if legendtxt!=None:
        figlegend(lines,legendtxt,'upper right')
    ylabel('Sunrise/RADICAL-1')
    
    grid(True)


def pyxp04comparison(files,cams,p04file, relative=False, legendtxt=None):

    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                        x=pyx.graph.axis.log(min=1e-7,max=1e-3,title="$\lambda$/m"),
                        y= (pyx.graph.axis.lin(title="Sunrise/RADICAL-1") if relative else pyx.graph.axis.log(title="SED")))

    p04data=load(p04file)
    pl=p04data[:,0]*1e-6

    if not iterable(cams):
        cams=[cams]
    if not iterable(files):
        files=[files]
    l=pyfits.open(files[0])['INTEGRATED_QUANTITIES'].data.field("lambda")
    if relative:
        g.plot(pyx.graph.data.array([l,l*0.0],x=1,y=2,title=None), \
        styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,pyx.color.grey.black])])

    fills=[]
    palette=pyx.color.linearpalette.Rainbow
    n=len(cams)
    ncol=n
    i=0
    for c,lt in zip(cams,legendtxt):
        
        p1=p04data[:,c+1]*1e-7

        mean,sigma,n = stats.sigma(sed_generator(files, c, not relative))
        if relative:
            fills.append((g.plot(pyx.graph.data.array([l,l*(mean+sigma)/p1-1],x=1,y=2,title=None),
                                styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.THIN,pyx.style.linestyle.solid,palette.select(i,ncol),pyx.color.transparency(1.0)])]),
                         g.plot(pyx.graph.data.array([l,l*(mean-sigma)/p1-1],x=1,y=2,title=None),
                                styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.THIN,pyx.style.linestyle.solid,palette.select(i,ncol),pyx.color.transparency(1.0)])])))
            g.plot(pyx.graph.data.array([l,l*mean/p1-1],x=1,y=2,title=lt), \
                   styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.normal,pyx.style.linestyle.solid,palette.select(i,ncol)])])
        else:
            fills.append((g.plot(pyx.graph.data.array([l,l*10**(mean+sigma)],
                                                      x=1,y=2,title=None),
                                 styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.THIN,pyx.style.linestyle.solid,palette.select(i,ncol),pyx.color.transparency(1.0)])]),
                          g.plot(pyx.graph.data.array([l,l*10**(mean-sigma)],x=1,y=2,title=None),
                                styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.THIN,pyx.style.linestyle.solid,palette.select(i,ncol),pyx.color.transparency(1.0)])])))
            g.plot(pyx.graph.data.array([l,l*10**mean],x=1,y=2,title=lt+' Sunrise'), \
                   styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.normal,pyx.style.linestyle.solid,palette.select(i,ncol)])])
            g.plot(pyx.graph.data.array([pl,p1],x=1,y=2,title=lt+' RADICAL'), \
                   styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.normal,pyx.style.linestyle.dashed,palette.select(i,ncol)])])

        i+=1
    g.finish()
    # now we can fill the paths
    i=0
    for f in fills:
        p=f[0].path.joined(f[1].path.reversed())
        g.fill(p,[palette.select(i,ncol),
                  pyx.color.transparency(0.65), \
                  ])
        i+=1

    return g

def makepath(x,y):
    """Returns a pyx path joining the points in data."""
    p=pyx.path.path(pyx.path.moveto(x[0],y[0]))
    for xx,yy in zip(x,y):
        p.append(pyx.path.lineto(xx,yy))
    return p
        
def parse_temp(file):
    f=open(file)
    temp=[float(x.split()[3]) for x in f.xreadlines() if "Temp cell" in x]
    temp.reverse()
    temp=numpy.array(temp)
    return temp

def parse_gridstruct(file):
    grid=load(file)
    r=numpy.sqrt(grid[:,0]*grid[:,0]+grid[:,1]*grid[:,1])
    theta=numpy.arctan(grid[:,2]/numpy.sqrt(grid[:,0]*grid[:,0]+grid[:,1]*grid[:,1]))
    theta=theta*180/pi
    return r,theta

#r,theta=parse_gridstruct('gridstruct.txt')
#temp=parse_temp("tau100_newint_temps.txt")
#t2=parse_temp("tau100/tau100_ml13_temps.txt")

#sel=abs(r-2.0)<0.05
#sel=abs(theta-2.5)<0.5
#plot(theta[sel],temp[sel],'.')

def parse_radical_temp(file):
    f=open(file)
    f.readline()
    d=[x.split() for x in f.readlines()]
    r=numpy.array([float(x[0])
       for x in d])
    theta=numpy.array([float(x[1])
       for x in d])
    temp=numpy.array([float(x[2])
       for x in d])
    r=r*6.6846e-14
    theta=theta*180/pi-90
    return r,theta,temp
                         
