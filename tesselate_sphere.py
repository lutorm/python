from numpy import *
import random,pyfits

def draw_point():
    """Draws a random point on the unit sphere."""
    phi=random.uniform(0,2*pi)
    costheta=random.uniform(-1,1)
    sintheta=sqrt(1-costheta*costheta)
    return (cos(phi)*sintheta,sin(phi)*sintheta,costheta)

def mc(vc):
    """Calculates the solid angle of each camera by monte carlo
    putting down points on the sphere and assigning it to the closest
    camera. vc is an array of camera position vectors on the unit sphere."""
    

    hist=zeros(vc.shape[0])

    n=100000
    for i in range(n):
        p=draw_point()
        m=argmax(dot(vc,p))
        #print p,dot(vc,p),m
        hist[m]+=1

    print hist
    print hist/n*4*pi
    
def load_cameras(file):
    f=pyfits.open(file)

    vc=[]
    c=0
    try:
        while(True):
            h=f['CAMERA%d-PARAMETERS'%c].header
            pos=array((h['CAMPOSX'],h['CAMPOSY'],h['CAMPOSZ']))
            pos/=sqrt(dot(pos,pos))
            vc.append(pos)
            c+=1
    except KeyError:
        pass

    return array(vc)

def read_campos():
    f=open('campos')
    cams=[(float(l.split()[0])*pi/180,float(l.split()[1])*pi/180) for l in f.readlines()]
    vc=[(cos(phi)*sin(theta),sin(phi)*sin(theta),cos(theta)) for theta,phi in cams]
    return array(vc)
