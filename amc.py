import pyx, pyxutil, os, pyfits, pdb
from numpy import *
from pylab import *

def determine_reflambda(I,a,k,l,imax,reemission,constrain_reemission,plotname):
    if reemission:
        # with reemission, we use albedo bias factors instead of value
        aki=a*k*I
        if constrain_reemission:
            lref = where((a*k>max(aki)/imax)&((1-a)*k>max((1-a)*k*I)/imax))[0][-1]
        else:
            lref = where((a*k>max(aki)/imax))[0][-1]
            
        print "aki and (1-a)ki sets lambda = ",l[lref]

        g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                            x=pyx.graph.axis.log(min=1e-8,max=1e-3,title="$\lambda$/m"),
                            y=pyx.graph.axis.log(min=1e2,max=3e5,title="g/cm$^2$"))

        data=[ \
            (pyx.graph.data.array([l,aki],x=1,y=2,title='$a \kappa I$'), pyx.color.grey.black),
            (pyx.graph.data.array([l,(1-a)*k*I],x=1,y=2,title='$(1-a)\kappa$'), pyx.color.rgb.blue),
            (pyx.graph.data.array([l,I/I*max(aki)/imax],x=1,y=2,title=r'$max(a\kappa I)/I_{max}$'), pyx.color.rgb.red),
            (pyx.graph.data.array([l,I/I*max((1-a)*k*I)/imax],x=1,y=2,title=r'$max((1-a)\kappa I)/I_{max}$'), pyx.color.rgb.green)
            ]

        for d,c in data:
            g.plot(d,styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,c])])

        g.plot(pyx.graph.data.array([array((l[lref],l[lref])),array((1e-100,1e100))],x=1,y=2,title=r'$\lambda=%3.1f\mu m$'%(l[lref]*1e6)),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.dashed,pyx.color.rgb.blue])])
        g.writePDFfile(plotname)

    else:
        # akI (that is, assuming tau_exit=0)
        aki=a*k*I
        lref = where(k<max(aki)/imax)[0][0]-1
        print "aki sets lambda = ",l[lref]

        g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                            x=pyx.graph.axis.log(min=1e-8,max=1e-3,title="$\lambda$/m"),
                            y=pyx.graph.axis.log(min=1e2,max=3e5,title="g/cm$^2$"))

        data=[ \
            (pyx.graph.data.array([l,k],x=1,y=2,title='$\kappa$'), pyx.color.rgb.blue),
            (pyx.graph.data.array([l,aki],x=1,y=2,title='$a \kappa I$'), pyx.color.grey.black),
            (pyx.graph.data.array([l,I/I*max(aki)/imax],x=1,y=2,title=r'$\kappa_{ref}=max(a\kappa I)/I_{max}$'), pyx.color.rgb.red)
            ]

        for d,c in data:
            g.plot(d,styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,c])])

        g.plot(pyx.graph.data.array([array((l[lref],l[lref])),array((1e-100,1e100))],x=1,y=2,title=r'$\lambda=%3.1f\mu m$'%(l[lref]*1e6)),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.dashed,pyx.color.rgb.blue])])
        g.writePDFfile(plotname)

    return lref

def determine_taue(I,a,k,l,lref,imax,reemission,constrain_reemission,plotname):
    if reemission:
        # tau_e
        valid=where((k<k[lref])&(a>0))[0]
        # first approx
        tauev1=(log(imax/(a/a[lref]*k/k[lref]*I)))/(1-k/k[lref])
        tauev2=(log(imax/((1-a)/(1-a[lref])*k/k[lref]*I)))/(1-k/k[lref])
        #pdb.set_trace()
        if constrain_reemission:
            taue=min(min(tauev1[valid]),min(tauev2[valid]))
        else:
            taue=min(tauev1[valid])
        print "Min tau_e=",taue
        g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                            x=pyx.graph.axis.log(min=1e-8,max=1e-3,title="$\lambda$/m"),
                            y=pyx.graph.axis.log(min=1,max=50,title=""))

        g.plot(pyx.graph.data.array([l[valid],tauev1[valid]],x=1,y=2,title=r'$\tau_e$ due to scattering'),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,pyx.color.grey.black])])
        g.plot(pyx.graph.data.array([l[valid],tauev2[valid]],x=1,y=2,title=r'$\tau_e$ due to reemission'),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,pyx.color.rgb.blue])])

        g.plot(pyx.graph.data.array([l,taue*l/l],x=1,y=2,title=r'min $\tau_e$=%3.1f'%taue),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.dashed,pyx.color.grey.black])])

        g.writePDFfile(plotname)
    else:
        # tau_e
        valid=where(k>k[lref])[0]
        # first approx
        tauev=(log(imax/(a*k/k[lref]*I)))/(1-k/k[lref])
        # corrective step
        #tauev=log(imax*tauev/(a*I*(1-k/k[lref]+k/k[lref]*tauev)))/(1-k/k[lref])

        tauev=where((k>=k[lref])|(a==0),1e100,tauev)
        taue=min(tauev)
        print "Min tau_e=",taue
        g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                            x=pyx.graph.axis.log(min=1e-8,max=1e-3,title="$\lambda$/m"),
                            y=pyx.graph.axis.log(min=1,max=1e2,title=""))

        g.plot(pyx.graph.data.array([l,tauev],x=1,y=2,title=r'$\tau_e$'),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,pyx.color.grey.black])])
        g.plot(pyx.graph.data.array([l,taue*l/l],x=1,y=2,title=r'min $\tau_e$=%3.1f'%taue),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.dashed,pyx.color.grey.black])])

        g.writePDFfile(plotname)


    return taue

def determine_intensities(I,a,k,l,lref,taue,imax,reemission,plotname):
    # scattering intensity
    if reemission:
        Iscat=lambda taus: I*a/a[lref]*exp(taus*(1-k/k[lref]))*k/k[lref]*(1-exp(-taue))
        Ireem=lambda taus: I*(1-a)/(1-a[lref])*exp(taus*(1-k/k[lref]))*k/k[lref]*(1-exp(-taue))
        I2=I*exp(-k/k[lref]*taue)
        print "Max scattered intensity for tau_s=0: ",max(Iscat(0))
        print "Max scattered intensity for tau_s=tau_e: ",max(Iscat(taue))
        print "Max reemitted intensity for tau_s=0: ",max(Ireem(0))
        print "Max reemitted intensity for tau_s=tau_e: ",max(Ireem(taue))

        g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                            x=pyx.graph.axis.log(min=1e-8,max=1e-3,title="$\lambda$/m"),
                            y=pyx.graph.axis.log(min=1e-2,max=15,title=""))

        data=[(pyx.graph.data.array([l,Iscat(0.0)],x=1,y=2,title=r'$I_s(\tau_s=0.0)$'), pyx.color.rgb.red,pyx.style.linestyle.solid),
              (pyx.graph.data.array([l,Iscat(1.0)],x=1,y=2,title=r'$I_s(\tau_s=1.0)$'), pyx.color.rgb.green,pyx.style.linestyle.solid),
              (pyx.graph.data.array([l,Iscat(taue)],x=1,y=2,title=r'$I_s(\tau_s=%3.1f)$'%taue), pyx.color.rgb.blue,pyx.style.linestyle.solid),
              (pyx.graph.data.array([l,Ireem(0.0)],x=1,y=2,title=r'$I_r(\tau_s=0.0)$'), pyx.color.rgb.red,pyx.style.linestyle.dashed),
              (pyx.graph.data.array([l,Ireem(1.0)],x=1,y=2,title=r'$I_r(\tau_s=1.0)$'), pyx.color.rgb.green,pyx.style.linestyle.dashed),
              (pyx.graph.data.array([l,Ireem(taue)],x=1,y=2,title=r'$I_r(\tau_s=%3.1f)$'%taue), pyx.color.rgb.blue,pyx.style.linestyle.dashed),
              (pyx.graph.data.array([l,I],x=1,y=2,title='$I_0$'), pyx.color.grey.black,pyx.style.linestyle.solid)]


        for d,c,ls in data:
              g.plot(d,styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,ls,c])])

        g.plot(pyx.graph.data.array([l,I2],x=1,y=2,title=r'$I_1$'),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.dashed, pyx.color.rgb.black])])
        g.plot(pyx.graph.data.array([l,l/l*imax],x=1,y=2,title=r'$I_{max}$'),styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.dotted, pyx.color.rgb.black])])

        g.writePDFfile('I_scatter')
    else:
        # forgot to copy...
        pass

    return I2

def pyx_opacities():
    o=loadtxt(os.path.join(os.getenv('HOME'),'dust_data/kext_albedo_WD_MW_3.1_60'),skiprows=50)
    l=o[:,0]*1e-6
    a=o[:,1]
    # "fix" the albedo by extrapolating as lambda^-2
    ll=where(l>19.7e-6)[0][0]
    a[ll+1:]=a[ll]*(l[ll+1:]/l[ll])**-2
    k=o[:,4]/(1-a)
    I=l/l
    imax=10

    reemission=True
    constrain_reemission=False

    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                        x=pyx.graph.axis.log(min=1e-8,max=1e-3,title="$\lambda$/m"),
                        y=pyx.graph.axis.log(min=1e-1,max=1e6,title="$\kappa$/(g/cm$^2$)"))

    data=[ (pyx.graph.data.array([l,k],x=1,y=2,title='MW R=3.1'), pyx.color.grey.black) ]

    for d,c in data:
        g.plot(d,styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,c])])
    g.writePDFfile('opacity')


    g=pyx.graph.graphxy(width=12,key=pyx.graph.key.key(),
                        x=pyx.graph.axis.log(min=1e-8,max=1e-3,title="$\lambda$/m"),
                        y=pyx.graph.axis.log(min=1e-4,max=1,title="$albedo$"))

    data=[ \
        (pyx.graph.data.array([l,a],x=1,y=2,title='MW R=3.1'), pyx.color.grey.black)
        ]

    for d,c in data:
        g.plot(d,styles=[pyx.graph.style.line(lineattrs=[pyx.style.linewidth.Thin,pyx.style.linestyle.solid,c])])
    g.writePDFfile('albedo')

    lref = determine_reflambda(I,a,k,l,imax,reemission,constrain_reemission,
                               'aki')

    taue = determine_taue(I,a,k,l,lref,imax,reemission,constrain_reemission                  ,'taue')

    I2 = determine_intensities(I,a,k,l,lref,taue,imax,reemission,
                               'I_scatter')

    # and now for stratum 2

    lref = determine_reflambda(I2,a,k,l,imax,reemission,constrain_reemission,'aki2')

    taue = determine_taue(I2,a,k,l,lref,imax,reemission,constrain_reemission,'taue2')

def compare(file1, file2, l, suffix="-STAR"):
    """Plots images of two outputs from ir_thick_test."""

    f1=pyfits.open(file1)
    f2=pyfits.open(file2)

    i1=f1['CAMERA0%s'%suffix].data[l,:,:]
    i2=f2['CAMERA0%s'%suffix].data[l,:,:]

    # get max values excluding the brightest pixel which could be the
    # point source at long wavelengths
    imax=max(sort(i1.flatten())[-2], sort(i2.flatten())[-2])
    imin=min(i1[i1>0].min(), i2[i2>0].min())
    if imin/imax<1e-5:
        imin=imax*1e-5

    clf()
    subplot(131)
    imshow(log10(i1), interpolation='nearest', vmin=log10(imin),
    vmax=log10(imax))
    title(file1)

    subplot(132)
    imshow(log10(i2), interpolation='nearest', vmin=log10(imin), vmax=log10(imax))
    title(file2)

    subplot(133)
    imshow(log10(i1/i2),interpolation='nearest', vmin=-1, vmax=1)
    colorbar()
    title('ratio')
    
    
